﻿<?php 
/*
------------------
Language: English
------------------
*/

// Main
define('LANG_PAGE_TITLE', 'The Wide Condo', true);
define('LANG_HEADER_TITLE', 'The Wide Condo Phuket, Thailand', true);
define('LANG_SITE_NAME', 'The Wide Condo', true);
define('LANG_SLOGAN', 'WIDER your day with THE BEST CONDOTEL in Phuket Town', true);

define("WEBSITE_URL", "http://www.thewidecondo.com/", true);
define("WEBSITE_TITLE", "The Wide Condo : WIDER your day with THE BEST CONDOTEL in Phuket Town", true);
define("WEBSITE_KEYWORDS", "The Wide Condo, Phuket Condo", true);
define("WEBSITE_DESCRIPTION", "The Wide Condo based in Phuket, Thailand", true);
	
define("POWER_BY_URL", "http://www.phuketwebstudio.com", true);
define("POWER_BY_TITLE", "Phuket Web Studio", true);
define("COPYRIGHT_YEAR", date("Y"), true);

// TOP Menu
define('NAV_Home', 'หน้าแรก', true);
define('NAV_Accommodation', 'ห้องพัก', true);
define('NAV_Promotions', 'โปรโมชั่น', true);
define('NAV_Facilities', 'สิ่งอำนวยความสะดวก', true);
define('NAV_Gallery', 'แกลลอรี่', true);
define('NAV_Location', 'แผนที่', true);
define('NAV_Contact', 'ติดต่อเรา', true);

// CONTENT Text
define('TXT_Availability', 'Check Availability<br>AND book now', true);

// Footer Menu
define('FOOT_TXT_CALLUS', 'Call us', true);
define('FOOT_TXT_COTACT_BY_SOCIAL', 'OR Contact us thru other channels :', true);
define('FOOT_TXT_SLOGAN', '<span class="txt-purple-color">WIDER</span> your day with <span class="txt-purple-color">THE BEST<br>CONDOTEL</span> in Phuket Town', true);

?>