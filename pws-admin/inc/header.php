<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="author" content="<?php echo POWER_BY_TITLE; ?>">

<link rel="shortcut icon" href="<?php echo PATH_ASSETS; ?>images/favicon_1.ico">

<title><?php echo $oConfig->name; ?> - <?php echo $oConfig->meta_title; ?></title>
<meta name="description" content="<?php echo $oConfig->meta_description; ?>">
<meta name="keywords" content="<?php echo $oConfig->meta_keywords; ?>">

<!--Morris Chart CSS -->
<link rel="stylesheet" href="<?php echo PATH_ASSETS; ?>plugins/morris/morris.css">

<link href="<?php echo PATH_ASSETS; ?>css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo PATH_ASSETS; ?>css/core.css" rel="stylesheet" type="text/css" />
<link href="<?php echo PATH_ASSETS; ?>css/components.css" rel="stylesheet" type="text/css" />
<link href="<?php echo PATH_ASSETS; ?>css/icons.css" rel="stylesheet" type="text/css" />
<link href="<?php echo PATH_ASSETS; ?>css/pages.css" rel="stylesheet" type="text/css" />
<link href="<?php echo PATH_ASSETS; ?>css/responsive.css" rel="stylesheet" type="text/css" />

<link href="<?php echo PATH_ASSETS; ?>plugins/flag-icon-css/css/flag-icon.css" rel="stylesheet">

<!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

<script src="<?php echo PATH_ASSETS; ?>js/modernizr.min.js"></script>

