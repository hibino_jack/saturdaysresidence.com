<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-sm-4">
                        <img src="<?php echo $logo; ?>" class="img-responsive" />
                    </div>
                    <div class="col-sm-4">
                        <ul>
                            <li><a href="http://www.fotohotelphuket.com/" target="_blank">Foto Hotel</a></li>
                            <li><a href="http://www.blumonkeyhotels.com/" target="_blank">Blue Monkey</a></li>
                            <li><a href="http://www.twovillasholiday.com/" target="_blank">Two Villa Holidays</a></li>
                            <li><a href="http://www.theattitudeclub.com/" target="_blank">The Attitude Club</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-4">
                        <ul>
                            <li><a href="<?php echo BASE_URL.$lang_id; ?>/field-guide/">Field Guide</a></li>
                            <li><a href="<?php echo BASE_URL.$lang_id; ?>/our-story/">Our Story</a></li>
                            <li><a href="<?php echo BASE_URL.$lang_id; ?>/press/">Press</a></li>
                            <li><a href="<?php echo BASE_URL.$lang_id; ?>/careers/">Careers</a></li>
                            <li><a href="<?php echo BASE_URL.$lang_id; ?>/reservation-policy/">Reservation Policy</a></li>
                            <li><a href="<?php echo BASE_URL.$lang_id; ?>/features/">Features</a></li>
                            <!--<li><a href="<?php //echo BASE_URL.$lang_id; ?>/terms-conditions/">Terms &amp; Conditions</a></li>
                            <li><a href="<?php //echo BASE_URL.$lang_id; ?>/privacy/">Privacy Policy</a></li>-->
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="row subscription">
                <form name="subscribe" class="footer-subscribe" action="post">
                    <p>Subscribe to get the best deal, promotions, news from Saturdays Residence</p>
                    <div class="input-group">
                        <!--<input type="text" placeholder="Search for..." id="email" required>-->
                        <input type="email" name="email" placeholder="your email" required />
                        <span class="input-group-btn">
                            <!--<button class="btn btn-lg btn-primary" type="button">Subscribe</button>-->
                            <input type="hidden" name="module" value="footer-subscribe" />
                            <input type="submit" value="Subscribe" class="btn btn-lg btn-primary"  name="submit" />
                          </span>
                    </div><!-- /input-group -->
                </form>
                </div>

                <div class="row social-media">
                    <a href="<?php echo $oConfig->link_instagram; ?>" target="_blank"><span
                                class="fa fa-instagram"></span></a>
                    <a href="<?php echo $oConfig->link_facebook; ?>" target="_blank"><span
                                class="fa fa-facebook"></span></a>
                </div>

            </div>

        </div>
    </div>

</footer>
<div class="footer-bottom">
    <div class="container">
        <div class="row">
            <div class="col-md-6 copyright">
                &copy; 2017 Saturdays Residence, All rights reserved.
            </div>
            <div class="col-md-6 contact-link">
                <a href="<?php echo BASE_URL.$lang_id; ?>/contact/">contact us</a>
            </div>
        </div>
    </div>
</div>
     
     <!-- Modal -->
    <div class="modal fade" id="booking-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content modal-booking">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i></button>
          </div>
          <div class="modal-body ">
           <div class="row">
               <div class="col-lg-7">
               	 <h4><span class="selectdate-title">Select Your Dates</span></h4>
                 <div class="datepicker"></div>
               </div>
               
               <div class="col-lg-5 ">
                    <div class="row">
                    	<div class="col-xs-6">
                        	<label>Check-in</label>
                			<input type="text" id="input1" size="10">
                        </div>
                        <div class="col-xs-6">
                        	<label>Check-out</label>
                			<input type="text" id="input2" size="10">
                        </div>
                    </div>
                     <div class="row">
                         <div class="col-sm-4 numbers-row">
                         	
        					<label for="rooms"><span class="label-number">Room(s)</span>
        					<input type="text" name="rooms" id="rooms" value="1">
        					</label>

                         </div>
                         
                          <div class="col-sm-4 numbers-row">
                         	
        					<label for="adults"><span class="label-number">adult(s)</span>
        					<input type="text" name="adults" id="adults" value="1">
        					</label>

                         </div>
                         
                         
                         <div class="col-sm-4 numbers-row">
                         	
        					<label for="children"><span class="label-number">Child(ren)</span>
        					<input type="text" name="children" id="children" value="0">
        					</label>

                         </div>
                     </div>
                     
                      <div class="row">
                      	<div class="col-md-12">
                        	<a href="javascript:void(0);" id="promo-show">Apply Special Code ></a>
                			<div class="row promo-box" style="display:none;">
                            	<div class="col-xs-6">
                            		<input type="text" name="promo-code" id="promo-code">
                                </div>
                                <div class="col-xs-6">
                                	<button class="btn btn-primary">Apply</button>
                                </div>
                            </div>
                        </div>
                      </div>
                      
                      <div class="row">
                      	<div class="col-md-12">
                            <button class="btn btn-primary btn-lg">Book Now</button>
                        </div>
                      </div>
                      
               
               </div>
            </div>
           	
          </div>
          
        </div>
      </div>
    </div>