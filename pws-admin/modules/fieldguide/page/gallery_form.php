<?php
if($action == "gallery" && $task == "update")    {
    if($$module_class->itemid) {
        $data = $$module_class->Media->select($$module_class->itemid);
        $itemid = $$module_class->itemid;
        if($data){
            extract($data[0]);
            //PRE($data);
        }
    }
}
?>

<div class="row">
    <div class="col-md-12">
        <?php echo $languages->dropdown_languages($module, $action, $path, $itemid, $task); ?>
        <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"><?php echo $module_class." ".$action." ".$task;?></h3>
        </div>
        <div class="panel-body">
            <form name="update-form" id="update-form" class="form-horizontal" role="form" data-parsley-validate novalidate enctype="multipart/form-data">
                <div class="alert alert-danger alert-dismissable hidden">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <h4>Oh snap!</h4>
                    <p>This form seems to be invalid :(</p>
                </div>
                <div class="alert alert-success alert-dismissable hidden">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <h4>Yay!</h4>
                    <p>Everything seems to be ok., Re-submit your form then. :)</p>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="col-md-2 control-label">Photo </label>
                            <div class="col-md-10">
                                <?php
                                $pic = BASE_UPLOAD.$module."/".$fieldguide_id."/".$filename;
                                $pic_realpath = PATH_UPLOAD_ROOT.$module."/".$fieldguide_id."/".$filename;
                                if(is_file($pic_realpath)) {
                                    ?>
                                    <a class="image-popup" href="<?php echo $pic; ?>" title="<?php echo $title;?>">
                                        <img src="<?php echo PATH_ROOT; ?>timthumb.php?src=<?php echo $pic_realpath; ?>&w=500&h=400" width="500" class="img-responsive img-thumbnail">
                                    </a>
                                <?php }else{ ?>
                                    <img src="<?php echo BASE_IMAGES; ?>nophoto.jpg" width="250" height="200" class="img-responsive img-thumbnail"/>
                                <?php } ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label">Field Guide Page </label>
                            <div class="col-md-10">
                                <?php
                                $tbFieldguide = $$module_class->tbFieldguide;
                                $field = "title";
                                $where = "`$tbFieldguide`.`lang_id`='$lang_id' AND `$tbFieldguide`.`is_active`='Y'";
                                ECHO DROPDOWNITEM("fieldguide_id", $fieldguide_id, $$module_class->$tbFieldguide, "id", $field, $where);
                                ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label">Title</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" name="title" parsley-trigger="change" required placeholder="Enter Title" value="<?php echo $title;?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label">Set to default</label>
                            <div class="col-md-10">
                                <?php
                                $is_default_data = array("Y" => "Enable", "N" => "Disabled");
                                echo RADIOITEMMANUAL("is_default", $is_default, $is_default_data);
                                ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label">Active</label>
                            <div class="col-md-10">
                                <?php
                                $is_active_data = array("Y" => "Enable", "N" => "Disabled");
                                echo RADIOITEMMANUAL("is_active", $is_active, $is_active_data);
                                ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group pull-right">
                            <input type="hidden" id="module" name="module" value="<?php echo $module; ?>">
                            <input type="hidden" id="action" name="action" value="gallery">
                            <input type="hidden" id="task" name="task" value="update_gallery_item">
                            <input type="hidden" id="lang_id" name="lang_id" value="<?php echo $lang_id; ?>">
                            <input type="hidden" id="itemid" name="itemid" value="<?php echo $itemid; ?>">
                            <input type="hidden" id="path" name="path" value="cp">

                            <input type="hidden" id="categories_id" name="categories_id" value="<?php echo $categories_id; ?>">
                            <input type="hidden" id="filename" name="filename" value="<?php echo $filename; ?>">

                            <button type="submit" class="btn btn-default waves-effect waves-light btn-lg">Submit</button>
                            <button type="reset" class="btn btn-white waves-effect waves-light btn-lg">Cancel</button>
                        </div>
                    </div>

                </div>

            </form>

        </div>
    </div>

    </div>
</div>