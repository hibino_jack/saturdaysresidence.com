<?php
$data = $$module_class->select($lang_id, NULL, 'parent');
$datatotal = count($data);
//PRE($data);
?>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default panel-border">
            <div class="panel-heading">
                <?php echo $languages->dropdown_languages($module, $action, $path, $itemid, $task); ?>
                <h4 class="header-title"><b><?php echo $module_class." ".$action;?></b></h4>
                <p class="text-muted font-13 m-b-10">Total <?php echo count($$module_class->select($lang_id, NULL, 'all')); ?> record(s) on the list.</p>
            </div>
            <div class="panel-body">
                <table id="datatable-buttons" class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Title</th>
                        <th>Filename</th>
                        <th>Last Update</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if($data) {
                        $row_no = 1;
                        for($i=0; $i < $datatotal; $i++) { ?>
                            <tr>
                                <td><?php echo $row_no; ?></td>
                                <td><strong><?php echo $data[$i]['title']; ?></strong></td>
                                <td><a href="<?php echo PATH_ROOT.$lang_id."/contents/".$data[$i]['filename'].".html"; ?>" target="_blank"><?php echo $data[$i]['filename']; ?>.html</a></td>
                                <td><?php echo date("d F Y",strtotime($data[$i]['updated_date'])); ?></td>
                                <td><span class="label label-table label-<?php echo ($data[$i]['is_active'] == "Y") ? "success" : "inverse"; ?>"><?php echo ($data[$i]['is_active'] == "Y") ? "Active" : "Disabled"; ?></span></td>
                                <td class="actions">
                                    <button class="btn btn-default btn-xs btn-icon fa fa-pencil btn-attr-item" data-lang_id="<?php echo $lang_id;?>" data-module="<?php echo $module;?>" data-action="update" data-id="<?php echo $data[$i]['id']; ?>" data-task="<?php echo $task;?>" data-path="<?php echo $path;?>"></button>
                                    <button class="btn btn-danger btn-xs btn-icon fa fa-trash-o delete-item" data-lang_id="<?php echo $lang_id;?>" data-module="<?php echo $module;?>" data-action="delete" data-id="<?php echo $data[$i]['id']; ?>" data-task="<?php echo $task;?>" data-path="<?php echo $path;?>"></button>
                                     <?php if($i == 0 || $i == 5){ ?>                                    
                                    <button class="btn btn-success btn-xs btn-icon fa fa-picture-o btn-attr-item" data-lang_id="<?php echo $lang_id;?>" data-module="<?php echo $module;?>" data-action="gallery" data-id="<?php echo $data[$i]['id']; ?>" data-task="list" data-path="<?php echo $path;?>"></button>
                                    <?php } ?>
                                </td>
                            </tr>

                            <?php
                            //get Sub menus
                            $data_submenu = $$module_class->select($lang_id, NULL, $data[$i]['id'], "Y");
                            //PRE($data_submenu);
                            if($data_submenu) {
                                $row_no_sub = $row_no + 1;
                                for($t=0; $t < count($data_submenu); $t++) { ?>
                                    <tr>
                                        <td><?php echo $row_no_sub; ?></td>
                                        <td><strong class="m-l-15"> - <?php echo $data_submenu[$t]['title']; ?></strong></td>
                                        <td><a href="<?php echo PATH_ROOT.$lang_id."/contents/".$data_submenu[$t]['filename'].".html"; ?>" target="_blank"><?php echo $data_submenu[$t]['filename']; ?>.html</a></td>
                                        <td><?php echo date("d F Y",strtotime($data_submenu[$t]['updated_date'])); ?></td>
                                        <td><span class="label label-table label-<?php echo ($data_submenu[$t]['is_active'] == "Y") ? "success" : "inverse"; ?>"><?php echo ($data_submenu[$t]['is_active'] == "Y") ? "Active" : "Disabled"; ?></span></td>
                                        <td class="actions">
                                            <button class="btn btn-default btn-xs btn-icon fa fa-pencil btn-attr-item" data-lang_id="<?php echo $lang_id;?>" data-module="<?php echo $module;?>" data-action="update" data-id="<?php echo $data_submenu[$t]['id']; ?>" data-task="<?php echo $task;?>" data-path="<?php echo $path;?>"></button>
                                            <button class="btn btn-danger btn-xs btn-icon fa fa-trash-o delete-item" data-lang_id="<?php echo $lang_id;?>" data-module="<?php echo $module;?>" data-action="delete" data-id="<?php echo $data_submenu[$t]['id']; ?>" data-task="<?php echo $task;?>" data-path="<?php echo $path;?>"></button>
                                            <button class="btn btn-success btn-xs btn-icon fa fa-picture-o btn-attr-item" data-lang_id="<?php echo $lang_id;?>" data-module="<?php echo $module;?>" data-action="gallery" data-id="<?php echo $data_submenu[$t]['id']; ?>" data-task="<?php echo $task;?>" data-path="<?php echo $path;?>"></button>
                                        </td>
                                    </tr>
                                    <?php
                                    $row_no_sub++;
                                } //for
                            }
                            ?>


                            <?php
                            $row_no = $row_no + count($data_submenu);
                            $row_no++;
                        } //for
                    }else{ ?>
                        <tr>
                            <td colspan="8">Sorry, Record not found</td>
                        </tr>
                    <?php }?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>