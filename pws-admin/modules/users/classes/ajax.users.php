<?php
    if($module == 'users') {
        $module_class = ucfirst($module);
        require_once('class.'.$module.'.php');
        $$module_class = new $module_class;
        switch ( $action ) {
            case 'login':
                //$page_content = $$module_class->login($post_vars['username'], $post_vars['passwd'], $post_vars['remember'], "fetchObj");
                $page_content = $$module_class->login($post_vars['username'], $post_vars['passwd'], $post_vars['remember']);
                break;
            case 'token':
                $page_content = $$module_class->generate_api_token_key($post_vars);
                break;
            case 'check_username':
                $page_content = $$module_class->check_duplicate_username($post_vars['username']);
                break;
            case 'check_email':
                $page_content = $$module_class->check_duplicate_email($post_vars['email']);
                break;
            case 'reset_password':
                $page_content = $$module_class->reset_password($post_vars['email']);
                break;
            case 'new':
                $page_content = $$module_class->insert($post_vars);
                break;
            case 'update':
                $page_content = $$module_class->update($post_vars);
                break;
            case 'delete':
                $page_content = $$module_class->delete($post_vars);
                break;
        }
	echo $page_content;
	exit();
	}
?>