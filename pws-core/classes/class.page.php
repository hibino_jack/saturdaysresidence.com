<?php
/*
 * http://www.tutdepot.com/simple-template-system/
 */

class Page extends DB {
    //private $globals = array();
    private $vars = array();
/*
    public function __construct(){
    }

    public function global_vars($key, $value = null) {
        if(is_array($key)) 	{
            foreach($key as $k => $v) {
                $this->global_vars($k, $v);
            }
        } else {
            $this->globals[$key] = $value;
        }
    }

    public function load($tpl, $arr = array(), $return = false) {
        if(file_exists($tpl)) {
            foreach($arr as $key => $value) {
                $$key = $value;
            }
            unset($arr);

            foreach($this->globals as $key => $value) {
                $$key = $value;
            }

            ob_start();
            require_once($tpl);
            $template = ob_get_contents();
            ob_end_clean();

            if($return == false) {
                echo $template;
            } else {
                return $template;
            }
        } else {
            return false;
        }
    }
*/
    /*
     * http://chadminick.com/articles/simple-php-template-engine.html
     */
    public function __get($name) {
        return $this->vars[$name];
    }

    public function __set($name, $value) {
        if($name == 'view_template_file') {
            throw new Exception("Cannot bind variable named 'view_template_file'");
        }
        $this->vars[$name] = $value;
    }
/*
    public function render($view_template_file) {
        if(array_key_exists('view_template_file', $this->vars)) {
            throw new Exception("Cannot bind variable called 'view_template_file'");
        }

        extract($this->vars);
        ob_start();
        include($view_template_file);
        return ob_get_clean();
    }
*/

    public function render($view_template_file, $type = "html") {
        global $oConfig;
        if(array_key_exists('view_template_file', $this->vars)) {
            throw new Exception("Cannot bind variable called 'view_template_file'");
        }
/*
 *      //set path to Admin panel and website
        if ($this->module_name) {
            $path_to_tpl = PATH_ADMIN_MODULES_ROOT.strtolower($this->module_name)."/page/";
            $view_tpl_locate = file_get_contents($path_to_tpl.$view_template_file);
        } else {
            $path_to_tpl = PATH_MODULES_ROOT.strtolower($this->module_name)."/page/";
            $view_tpl_locate = file_get_contents($path_to_tpl.$view_template_file);
        }
*/
        if(file_exists($view_template_file))
            $view_tpl_locate = file_get_contents($view_template_file);

        if($type == "html") {
            $view_tpl_locate = str_replace(array_keys($this->vars), array_values($this->vars), $view_tpl_locate);
            return $view_tpl_locate;
        } else {
            extract($this->vars);
            ob_start();
            require_once($view_template_file);
            return ob_get_clean();
        }
    }



//=============== SETLIMIT :: Page navigation Control ====================== ~ J :: Test Drive
    public function set_pageno($pageno)
    {
        $this->pageno = $pageno;
    }

    public function set_perpage($perpage)
    {
        $this->perpage = $perpage;
    }

    public function set_sortby($sortby)
    {
        $this->sortby = $sortby;
    }

    public function set_orderby($orderby)
    {
        $this->orderby = $orderby;
    }

    public function set_data_total($data_total)
    {
        $this->data_total = $data_total;
    }

    public function order_page()
    {
        if ($this->sortby)
            $data = " ORDER BY $this->sortby " . $this->orderby;
        return $data;
    }

    public function limit_page()
    {
        if ($this->perpage && $this->pageno) {
            $record_start = $this->page_record_start($this->perpage, $this->pageno);
            return " LIMIT $record_start, $this->perpage ";
        }
    }

    public function page_record_start($perpage, $pageno)
    {
        if ($pageno == "" || $pageno == 0)
            $record_no = 0;
        else
            $record_no = $perpage * ($pageno - 1);
        return $record_no;
    }

    public function total_page($data_total, $perpage)
    {
        return ceil($data_total / $perpage);
    }

    /*
        public function page_navigation($data_total, $link_string = NULL) {
            $page_link = "";
            $page_number = $this->pageno;
            $perpage = $this->perpage;
            $total_page = $this->total_page($data_total, $perpage);
            if($link_string)
                $link_string = $link_string."&";

            if($total_page > 1)
            {
                $page_link = "Page : ";
                for($pageno=1; $pageno <= $total_page; $pageno++)
                {
                    //$q = $querystring."-".$page;
                    $q = $link_string."&amp;-".$pageno;
                    if($this->pageno == $pageno)
                        $page_link.= "<b>$pageno</b>&nbsp;&nbsp;";
                    else
                        $page_link.= "<a href='".$q."'>$pageno</a>&nbsp;&nbsp;";
                }
            }
            return $page_link;
        }
    */
    public function page_navigation($data_total, $link_string = NULL)
    {
        $page_nav = "";
        $perpage = $this->perpage;
        $pageno = $this->pageno;
        $total_page = $this->total_page($data_total, $perpage);

        if ($total_page > 1) {
            $next_page = $pageno + 1;
            $prev_page = $pageno - 1;

            $page_nav = '<ul class="pager">';

            if ($prev_page > 0) {
                $q = $link_string . "/" . $prev_page . "/";
                $page_nav .= '<li class="previous"><a href="' . $q . '">< next article</a></li>';
            }

            if ($total_page > $pageno) {
                $q = $link_string . "/" . $next_page . "/";
                $page_nav .= '<li class="next"><a href="' . $q . '">prev article ></a></li>';
            }

            $page_nav .= '</ul>';
        }
        return $page_nav;
    }

}

//$Page = new Page();

?>
