<footer class="footer text-right">
    <?php echo SYSTEM_TITLE." ".SYSTEM_VERSION; ?> <?php echo "by <a href='".POWER_BY_URL."' target='_blank'>".POWER_BY_TITLE."</a>"; ?> © <?php echo COPYRIGHT_YEAR; ?>. All rights reserved.
</footer>