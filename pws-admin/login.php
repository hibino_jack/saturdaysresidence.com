<?php
require_once("../pws-core/site.config.php");
$users_class_path = PATH_ADMIN_MODULES_ROOT.'users/classes/class.users.php';
if(file_exists($users_class_path))
    require_once($users_class_path);

//INSTANCE
$users = new Users();

//Set Language
if(empty($lang_id))
    $lang_id = "en";

if(isset($_SESSION[$users->get_session_name()]))  {
    redirect(PATH_ADMIN.'index.php');
    exit();
}
//echo "<br>SESS: ";
//echo $_SESSION[$users->get_session_name()];
//echo "<br>";
//session_destroy();
//PRE($_SESSION);
?>
<!DOCTYPE html>
<html lang='<?php echo $lang_id; ?>'>
    <head>
        <?php require_once(PATH_ADMIN_INC_ROOT."header.php"); ?>
    </head>
    <body>

        <div class="account-pages"></div>
        <div class="clearfix"></div>
        <div class="wrapper-page">
        	<div id="login-box" class="card-box">
            <div class="panel-heading"> 
                <h3 class="text-center"> Sign In to <strong class="text-custom">Saturdays Residence</strong> </h3>
            </div>
            <div class="panel-body">
            <form class="form-horizontal m-t-20" name="login-check" id="login-check" action="../rest/api.php/">
                <div class="alert alert-danger alert-dismissable hidden">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <h4>Oh snap!</h4>
                    <p>This form seems to be invalid :(</p>
                </div>
                <div class="alert alert-success alert-dismissable hidden">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <h4>Yay!</h4>
                    <p>Everything seems to be ok :)</p>
                </div>

                <div class="form-group ">
                    <div class="col-xs-12">
                        <input class="form-control" type="text" required="" placeholder="Username or Email" name="username" id="username" value="">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-xs-12">
                        <input class="form-control" type="password" required="" placeholder="Password" name="passwd" id="passwd" value="">
                    </div>
                </div>

                <div class="form-group ">
                    <div class="col-xs-12">
                        <div class="checkbox checkbox-primary">
                            <input id="checkbox-remeber" name="remeber" type="checkbox" value="Yes">
                            <label for="checkbox-remeber">Remember me</label>
                        </div>
                    </div>
                </div>
                
                <div class="form-group text-center m-t-40">
                    <div class="col-xs-12">
                        <input type="hidden" name="module" value="users">
                        <input type="hidden" name="action" value="login">
                        <input type="hidden" name="path" value="cp">
                        <button class="btn btn-pink btn-block text-uppercase waves-effect waves-light" type="submit">Log In</button>
                    </div>
                </div>

                <div class="form-group m-t-30 m-b-0">
                    <div class="col-sm-12">
                        <a href="javascript:void(0);" class="text-dark" id="click-forget-password"><i class="fa fa-lock m-r-5"></i> Forgot your password?</a>
                    </div>
                </div>
            </form> 
            
            </div>   
            </div>
            <div id="reset-password-box">
            <div class="card-box">
                <div class="panel-heading">
                    <h3 class="text-center"> Reset Password </h3>
                </div>

                <div class="panel-body">
                    <form method="post" action="#" role="form" class="text-center" id="reset-password-form">
                        <div class="alert alert-info alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            Enter your <b>Email</b> and instructions will be sent to you!
                        </div>
                        <div class="alert alert-danger alert-dismissable hidden">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            <h4>Oh snap!</h4>
                            <p>This form seems to be invalid :(</p>
                        </div>
                        <div class="alert alert-success alert-dismissable hidden">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            <h4>Yay!</h4>
                            <p>Everything seems to be ok :)</p>
                        </div>

                        <div class="form-group m-b-0">
                            <div class="input-group">
                                <input type="email" class="form-control" placeholder="Enter Email" required  name="email" value="">
                                <span class="input-group-btn">
                                    <input type="hidden" name="module" value="users">
                                    <input type="hidden" name="action" value="reset_password">
                                    <input type="hidden" name="path" value="cp">
									<button type="submit" class="btn btn-pink w-sm waves-effect waves-light">Reset</button>
								</span>
                            </div>
                        </div>

                    </form>
                </div>
            </div>

            <div class="row">
            	<div class="col-sm-12 text-center">
            		<p>Do you have an account? <!--<a href="page-register.html" class="text-primary m-l-5"><b>Sign Up</b></a>--> <a href="javascript:void(0);" class="text-primary m-l-5" id="click-sign-in"><b>Sign In</b></a></p>
                </div>
            </div>
            </div>
        </div>


        <?php require_once(PATH_ADMIN_INC_ROOT."footer-script.php"); ?>

        <script src='https://unpkg.com/axios/dist/axios.min.js'></script>
        <script src="<?php echo BASE_ADMIN_MODULES; ?>users/classes/users.js"></script>
        <script type="text/javascript">
            Users.initLogin();

            //Hide Reset form
            $("#reset-password-box").hide();
            $( "#click-sign-in" ).click(function() {
                $("#reset-password-box").toggle("slow");
                $("#login-box").toggle("slow");
            });

            $( "#click-forget-password" ).click(function() {
                $("#reset-password-box").toggle("slow");
                $("#login-box").hide("slow");
            });

        </script>

	</body>
</html>