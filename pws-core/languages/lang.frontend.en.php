﻿<?php 
/*
------------------
Language: English
------------------
*/

// Main
define('PAGE_TITLE', 'The Wide Condo', true);
define('HEADER_TITLE', 'The Wide Condo Phuket, Thailand', true);
define('SITE_NAME', 'The Wide Condo', true);
define('SLOGAN', 'WIDER your day with THE BEST CONDOTEL in Phuket Town', true);

define("WEBSITE_URL", "http://www.thewidecondo.com/", true);
define("WEBSITE_TITLE", "The Wide Condo : WIDER your day with THE BEST CONDOTEL in Phuket Town", true);
define("WEBSITE_KEYWORDS", "The Wide Condo, Phuket Condo", true);
define("WEBSITE_DESCRIPTION", "The Wide Condo based in Phuket, Thailand", true);
	
define("POWER_BY_URL", "http://www.phuketwebstudio.com", true);
define("POWER_BY_TITLE", "Phuket Web Studio", true);
define("COPYRIGHT_YEAR", date("Y"), true);

// TOP Menu
define('NAV_Home', 'Home', true);
define('NAV_Accommodation', 'Accommodation', true);
define('NAV_Promotions', 'Promotions', true);
define('NAV_Facilities', 'Facilities', true);
define('NAV_Gallery', 'Gallery', true);
define('NAV_Location', 'Location', true);
define('NAV_Contact', 'Contact', true);

// CONTENT Text
define('TXT_Availability', 'Check Availability<br>AND book now', true);

// Footer Menu
define('FOOT_TXT_CALLUS', 'Call us', true);
define('FOOT_TXT_COTACT_BY_SOCIAL', 'OR Contact us thru other channels :', true);
define('FOOT_TXT_SLOGAN', '<span class="txt-purple-color">WIDER</span> your day with <span class="txt-purple-color">THE BEST<br>CONDOTEL</span> in Phuket Town', true);

?>