<?php
$module = "Fieldguide";
$module_class = strtolower($module);
$lang_id = $_GET["lang_id"];
$filename = $_GET["title"];
if(file_exists(PATH_ADMIN_MODULES_ROOT.$module_class.'/classes/class.'.$module_class.'.php')) {    
	require_once(PATH_ADMIN_MODULES_ROOT.$module_class . '/classes/class.'.$module_class.'.php');    
	$$module_class = new $module();
	
	$data = $$module_class->get_by_filename($lang_id,$filename);
	extract($data[0]);
	//PRE($data);
}

//$image_header = "http://placehold.it/800x550&text=".$title;	
if($header_photo != "")
{
	$pic = BASE_UPLOAD.$module_class."/".$id."/".$header_photo;
	$pic_realpath = PATH_UPLOAD_ROOT.$module_class."/".$id."/".$header_photo;
	if(is_file($pic_realpath)) {
		$image_header = PATH_ROOT."timthumb.php?src=".$pic."&w=800&h=550";
	}
}

?>
	<section class="content-area">

    	<div class="field-guide">
            <div class="container">
            	<div class="row bg-white">
                	<div class="col-md-12">

							<!-- leftside-->
                            <div class="field-guide-leftside col-md-8">
                            
                            	<!--page-heading-->
                                 <div class="row page-heading small">
                                           	<div class="col-sm-8">
                                                    <h1><span>the</span> Field Guide</h1>
                                                   
                                             </div>
                                             <div class="col-sm-4">
                                             	<a href="<?php echo BASE_URL.$lang_id; ?>/field-guide/" class="back-link">< BACK TO MAIN</a>
                                             </div>
                                 </div>
                                 
                            	
                                 <!--article-detail-->
                                 <div class="row article-detail">
                                 	<div class="col-md-12">
                                    	<div class="row text-center">
                                        	<h2><?php echo $title; ?></h2>
                                        </div>
                                     </div>
                                 
                                 	<div class="col-md-12">
                                    	<div class="row">
                                        <?php if($image_header){ ?>
                                        <p><img src="<?php echo $image_header; ?>" class="img-responsive"></p>
                                        <?php } ?>
                                    	<?php echo $description; ?>
                                        
                                        </div>
                                    </div>
                                 </div>
                                 
                                 
                                 <!--pagination
                               <nav class="row  article-item"  aria-label="...">
                                  <ul class="pager">
                                    <li class="previous"><a href="#">< prev article</a></li>
                                    <li class="next"><a href="#">next article > </a></li>
                                  </ul>
                                </nav>
                                 -->
                                 
                            </div>
                            
                            <!-- rightside-->
                            <?php include(PATH_THEMES_ROOT."inc/nav-right.php"); ?>

                    </div>
                </div>
                
            </div>
        
        </div>
    
    
    
    </section>