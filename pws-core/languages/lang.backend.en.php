<?php 
/*
------------------
Language: English
------------------
*/

// Main
define('LANG_PAGE_TITLE', 'Backend Management System', true);
define('LANG_HEADER_TITLE', 'ADMINISTRATOR LOGIN', true);
define('LANG_SITE_NAME', 'Phuket Web Studio CMS', true);
define('LANG_SLOGAN', 'Phuket Web Studio CMS', true);

define("WEBSITE_URL", "http://www.thewidecondo.com/", true);
define("WEBSITE_TITLE", "Phuket Web Studio CMS", true);
define("WEBSITE_KEYWORDS", "Phuket Web Studio CMS", true);
define("WEBSITE_DESCRIPTION", "Phuket Web Studio CMS", true);
	
define("POWER_BY_URL", "http://www.phuketwebstudio.com", true);
define("POWER_BY_TITLE", "", true);
define("COPYRIGHT_YEAR", date("Y"), true);

// Menu
define('LANG_MENU_HOME', 'Home', true);


// Button
define('BTN_SHORTCUT', 'Shortcut', true);
define('BTN_BACK', 'Back', true);
define('BTN_LOGOUT', 'Logout', true);
define('BTN_LOGIN', 'Login', true);
define('BTN_SEND', 'Send', true);
define('BTN_SUBMIT', 'Submit', true);
define('BTN_CLEAR', 'Clear', true);
define('BTN_UPDATE', 'Update', true);
define('BTN_ADD', 'Add', true);
define('BTN_EDITR', 'Edit', true);
define('BTN_DELETE', 'Delete', true);
define('BTN_MANAGE', 'Manage', true);


?>