<?php
/*
 * PDO Database class ~ J
 */
class DB {
    //database configuration
	protected static $connection = false;
	protected static $setlimit = false;
	var $pageno;
	var $perpage;
	var $data_total;
	var $sortby;
	var $orderby = "ASC";

    function __construct() {
    }

    public static function open(){
		if (!self::$connection){
			self::$connection = new PDO('mysql:host='.DATABASE_HOST.';port='.DATABASE_PORT.';dbname='.DATABASE_NAME, DATABASE_USER, DATABASE_PASS);
			//self::$connection = new PDO('mysql:host='.$this->db_host.';port='.$this->db_port.';dbname='.$this->db_name, $this->db_user, $this->db_passwd);
			self::$connection->exec("set names utf8");
			self::$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		} else {
		    echo "Connecting Error"; die();
        }
		return self::$connection;
	}

	public static function close(){
		$db = self::$connection;
		if($db)
			return $db = null;
	}

	/* Hidden by ~ J :: Please use below
	public static function prepare($sql){
		$db = self::$connection;
		if(!$db)
			$db = self::open();
		return $db->prepare($sql);
	}*/

	public static function prepare($sql){
		global $Page;
		if($Page->perpage) {
			if(empty($Page->pageno))
				$Page->pageno = 1;

			$set_limit_page = limit_page();
			$sql = $sql.$set_limit_page;
		}

		$db = self::$connection;
		if(!$db)
			$db = self::open();

		return $db->prepare($sql);
	}

	public static function lastQuery($stmt){
		return $stmt->queryString;
	}

	public static function execute($stmt, $params=false){
		//$db = self::$connection;
		self::$connection->exec("set names utf8");
		return (!$params) ? $stmt->execute() : $stmt->execute($params);
	}

	//additionally query ~JaCk
	public static function query($sql, $parameter = NULL, $table = NULL, $field = NULL, $params = false){
		//$db = self::$connection;
		try {
			$stmt = self::prepare($sql);
			//print_r($sql);
            //echo "<br />";
			//print_r($stmt);
            //echo "<br />";
			//exit;
			switch($parameter):
				case 'fetchAssoc':
					self::execute($stmt, $params);
					$result = self::fetchAssoc($stmt);
					break;
				case 'fetchAllObj':
					self::execute($stmt, $params);
					$result = self::fetchAllObj($stmt);
					break;
				case 'fetchObj':
					self::execute($stmt, $params);
					$result = self::fetchObj($stmt);
					break;
				case 'fetchAllArray':
					self::execute($stmt, $params);
					$result = self::fetchAllArray($stmt);
					break;
				case 'fetchColumn':
					self::execute($stmt, $params);
					$result = self::fetchColumn($stmt);
					break;
				case 'count':
                    self::execute($stmt, $params);
                    $result = self::rowCount($stmt);
                    //return $count = $stmt->rowCount();
					break;
				case 'lastInsertId':
                    self::execute($stmt, $params);
					$result = self::lastInsertId($stmt);
					break;
				case 'InsertedReturnId':
                    self::execute($stmt, $params);
					$result = self::InsertedReturnId($stmt, $table, $field = NULL);
					break;
                case '':
				default:
                    $result = DB::execute($stmt, $params);
                    break;
				endswitch;
			} catch(Exception $e) {
				$result = "DB Error: ". $e->getMessage();
			}
			return $result;
		}

		public static function escape($data){
			// return mysql_real_escape_string($data);
			// return PDO::quote($data);

			// $db = self::$connection;
			// return mysqli_real_escape_string($db,$data);
			if($data){
				$data = trim($data);
				//$message = filter_var($data,FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH);
				$message = filter_var($data,FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW || FILTER_FLAG_STRIP_HIGH);
				return $message;
				//} else {
				//    return '';
			}
		}

		public static function fetchAllObj($stmt){
			return $stmt->fetchAll(PDO::FETCH_OBJ);
		}

		public static function fetchObj($stmt){
			return $stmt->fetch(PDO::FETCH_OBJ);
		}

		public static function fetchAllArray($stmt){
			return $stmt->fetchAll();
		}

		public static function fetchArray($stmt){
			return $stmt->fetch();
		}

		public static function fetchAssoc($stmt){
			return $stmt->fetchAll(PDO::FETCH_ASSOC);
		}

        public static function fetchColumn($stmt){
            return $stmt->fetchColumn();
        }

        public static function rowCount($stmt){
            return $stmt->rowCount();
        }

		public static function lastInsertId($stmt){
            $lastId = self::$connection->lastInsertId();
            return $lastId;
		}
/*
		public static function InsertedReturnId($stmt, $table, $field = NULL){
            if($field == "")
                $field = "id";

            if($table == "")
                $table = "_info";

			$get_last_id_temp = $stmt->fetch(PDO::FETCH_ASSOC);
			if($get_last_id_temp){
				$lastId = $get_last_id_temp[0][$field];
				unset($get_last_id_temp);
			}

			if($field == "")
				$field = "id";
			
			$sql = "SELECT MAX($field) FROM $table";
			$stmt = self::prepare($sql);
			self::execute($stmt);
			$id = self::fetchArray($stmt);
			return $id['MAX('.$field.')'];
		}
*/

		public static function getFieldTypes($stmt){
			foreach(range(0, $stmt->columnCount() - 1) as $column_index):
				$columnMeta = $stmt->getColumnMeta($column_index);
				$field_types[] = $columnMeta['native_type'];
			endforeach;
			return $field_types;
		}

		public static function getFieldNames($stmt){
			foreach(range(0, $stmt->columnCount() - 1) as $column_index):
				$columnMeta = $stmt->getColumnMeta($column_index);
				$field_names[] = $columnMeta['name'];
			endforeach;
			return $field_names;
		}


	}
?>
