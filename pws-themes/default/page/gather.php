<?php
$module = "Gather";
$module_class = strtolower($module);

if(file_exists(PATH_ADMIN_MODULES_ROOT.$module_class.'/classes/class.'.$module_class.'.php')) {    
	require_once(PATH_ADMIN_MODULES_ROOT.$module_class . '/classes/class.'.$module_class.'.php');    
	$$module_class = new $module();
	
	$data = $$module_class->select();
}

$data_content[0]['description'] = str_ireplace(array("\r","\n",'\r','\n'),'', $data_content[0]['description']);

//$image_header = "http://placehold.it/1450x500&text=".$data_content[0]['title'];
if($data_content[0]['header_photo'] != "")
{
	$pic = BASE_UPLOAD."contents/".$data_content[0]['id']."/".$data_content[0]['header_photo'];
	$pic_realpath = PATH_UPLOAD_ROOT."contents/".$data_content[0]['id']."/".$data_content[0]['header_photo'];
	if(is_file($pic_realpath)) {
		$image_header = PATH_ROOT."timthumb.php?src=".$pic."&w=1450&h=500";
	}
}

//Set CSS for each module
if(file_exists(PATH_THEMES_ROOT."page/".$module_class."/".$module_class.".css")) {
    $page_css.= "<link href='".PATH_ASSETS."plugins/".$module_class."/".$module_class.".css' rel='stylesheet' type='text/css' />\n";
}

//Set JS for each module
if(file_exists(PATH_THEMES_ROOT."page/".$module_class."/".$module_class.".js")) {
    $page_js.= "<script type='text/javascript' src='".PATH_THEMES."page/".$module_class."/".$module_class.".js'></script>\n";
    $doc_ready_js.= $module.".initFrontend();\n";
}
?>
<!--header-->
<header> 
    <div class="page-heading">
        <div class="container">
            <div class="row">
            	<div class="col-md-offset-3 col-md-6 text-center">
            	<h1><?php echo $data_content[0]['title']; ?></h1>
                <?php if(!empty($data_content[0]['subtitle'])){ ?>           
                    <h3 style="font-style:italic"><?php echo $data_content[0]['subtitle']; ?></h3>
				<?php }
				if(!empty($data_content[0]['short_description'])){ ?>                                
                    <p><?php echo $data_content[0]['short_description']; ?></p>
				<?php } ?>       
				</div>
            </div>
		</div>
    </div>
    <?php if($image_header){ ?>
    <!-- room header Section -->
    <div class="inner-header">
        <div class="container">
                <div class="row">
                    <div class="inner-header-fill" style="background-image:url('<?php echo $image_header; ?>');"></div>
                </div>
         </div>
    </div>
    <?php } ?>
</header>

<section class="content-area page-gather">

    <?php if(!empty($data_content[0]['description'])){ ?>
    <div id="innerpage_intro">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="row innerpage_intro <?php echo ($image_header) ? "":"no-margin"; ?>">
				<?php echo $data_content[0]['description']; ?>
				<a href="" class="btn btn-default" data-toggle="modal" data-target="#rep-modal"><i class="fa fa-file-text-o"></i> submit RFP</a>
				</div><!--//saturdays_intro-->
			</div><!--//col-md-6-->
		</div>
	 </div>
  </div>
    <?php } ?>

    <?php
if(is_array($data) && (count($data) > 0))
{
	foreach($data as $item){
		
	$image_header = "http://placehold.it/800x550&text=".$item["name"];
	if($item["filename"] != "")
	{
		$pic = BASE_UPLOAD.$module_class."/".$item["id"]."/".$item["filename"];
		$pic_realpath = PATH_UPLOAD_ROOT.$module_class."/".$item["id"]."/".$item["filename"];
		if(is_file($pic_realpath)) {
			$image_header = PATH_ROOT."timthumb.php?src=".$pic."&w=800&h=550";
		}
	}
		
?>
    
       <!-- paragraphs-box Section -->
    <div class="paragraphs-box">
        <div class="container">
            <div class="row paragraphs-items">
            	<div class="col-md-6">
                	<div class="row paragraphs-image">
                		<img src="<?php echo $image_header; ?>" class="img-responsive" />
                    </div>
                </div>
               	<div class="col-md-6">
                	<div class="row paragraphs-content">
                    	
                            <h3><?php echo $item["name"] ?></h3>
                            <?php echo $item["description"] ?>
                           
                    </div>
                </div>
            </div>
        </div>
    </div>
 <?php 
	} //foreach
} //if
?>    
    </section>   
    


    
    
    <!-- Modal -->
    <div class="modal fade" id="rep-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content modal-booking">
          <div class="modal-header">
          		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i></button>
          		<h3>Submit an RFP</h3>
          </div>
          <div class="modal-body ">
              <form name="gather-form" id="gather-form" class="gather-form" action="post" style="overflow:hidden">

                  <div class="row">
                      <div class="col-md-12">
                          <h4>My Details</h4>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-xs-6">
                          <label for="fname">First Name *</label>
                          <input type="text" class="" id="fname" size="10" name="fname" placeholder="First Name" required>
                      </div>
                      <div class="col-xs-6">
                            <label for="lname">Last Name *</label>
                            <input type="text" class="" id="lname" size="10" name="lname" placeholder="Last Name" required>
                        </div>
                    </div>
                    <div class="row">
                    	<div class="col-xs-6">
                            <label for="email">Email *</label>
                            <input type="email" class="" id="email" size="10" name="email" placeholder="Email" required>
                        </div>
                        <div class="col-xs-6">
                            <label for="phone">Phone</label>
                            <input type="text" class="" id="phone" size="10" name="phone" placeholder="Phone">
                        </div>
                    </div>
                     <div class="row">
                    	<div class="col-xs-6">
                            <label for="city">City / Province</label>
                            <input type="text" class="" id="city" size="10" name="city" placeholder="City">
                        </div>
                        <div class="col-xs-6">
                            <label for="country">Country</label>
                            <input type="text" class="" id="country" size="10" name="country" placeholder="Country">
                        </div>
                    </div>
               
                	<div class="row">
                		<div class="col-md-12">
                        	<h4>Event Details</h4>
                        </div>     
                     </div>
                     
                     <div class="row">
                    	<div class="col-xs-12">
                            <label for="type_event">Type of event</label>
                            <select id="type_event" name="type_event" class="">
                            	<option value="" selected="selected">- None -</option>
                                <option value="social_event">Social Event</option>
                                <option value="meetings">Meetings</option>
                                <option value="weddings">Weddings</option>
                                <option value="spaces">Spaces</option>
                                <option value="catering">Catering</option>
                                <option value="other">Other</option>
                            </select>
                        </div>
                    </div>
                    
                    <div class="row">
                    	<div class="col-xs-6">
                            <label>Date of event *</label>
                            <input class="datepicker2" type="text" name="date_event" placeholder="Date of event" required>
                        </div>
                        <div class="col-xs-6 numbers-row">
                            <label for="attendees"><span class="label-number">No. of attendees *</span>
        					<input type="text" class="" name="attendees" id="attendees" value="30" placeholder="No. of attendees" required>
        					</label>
                        </div>
                    	
                    </div>
                    <div class="row">
                    	<div class="col-md-12">
                            <div class="checkbox checkbox-inline">
                                <input type="checkbox" id="is_food" value="Yes">
                                <label for="is_food"> I will need food & beverage </label>
                            </div>
                            <br clear="all" />
                            <div class="checkbox checkbox-inline">
                                <input type="checkbox" id="is_beak_room" value="Yes">
                                <label for="is_beak_room"> I will need break out rooms </label>
                            </div>
                        </div>
                    </div>
                	<div class="row">
                		<div class="col-md-12">
                        	<h4>Addtional Message</h4>
                        </div>     
                     </div>
                     <div class="row">
                    	<div class="col-md-12">
                            <textarea id="add_msg" class="" rows="7" name="add_msg"></textarea>
                        </div>
                    </div>
                           
               		<div class="row">
                		<div class="col-md-12">
                            <input type="hidden" name="module" value="gather-form" />
                        	<button type="submit" class="btn btn-primary">submit</button>
                        </div>
                   </div>

              </form>
           </div>
          </div>
      </div>
    </div>