<?php
/*
 * Module controller
 * Include and Declare Class for Module
 */
if(!isset($module))
    $module = "media"; //necessary value

if(!$action)
    $action = "list";

if(!$path)
    $path = "cp";

$module_class = ucfirst($module);
$module_file_class = PATH_ADMIN_MODULES_ROOT.$module."/classes/class.".$module.".php";

// Declare Main Class
if(file_exists($module_file_class)) {
    require_once($module_file_class);
    $$module_class = new $module_class;
}

//set global js
$doc_ready_js.= $oController->doc_ready_js($module);

    $$module_class->module_name = $module;
    $$module_class->action = $action;
    $$module_class->task = $task;
    $$module_class->lang_id = $lang_id;
    $$module_class->itemid = $itemid;
    $$module_class->path = $path;

    $$module_class->module_action_page = PATH_ADMIN_MODULES_ROOT . $module . "/page/" . $$module_class->action . ".php";
    if(!file_exists($$module_class->module_action_page)) {
        $$module_class->module_action_page = PATH_ADMIN_ROOT."error404.php";
    }

//Select an action
switch ($$module_class->action) {
    case "new":
        require_once(PATH_ADMIN_MODULES_ROOT . strtolower($$module_class->module_name) . "/page/form.php");
        $doc_ready_js.= "Global.initForm();\n";
        break;
    case "update":
        require_once(PATH_ADMIN_MODULES_ROOT . strtolower($$module_class->module_name) . "/page/form.php");
        $doc_ready_js.= "Global.initForm();\n";
        break;
    default:
    case "list":
        require_once($$module_class->module_action_page);
        $page_js.= "
            <link href='".PATH_ASSETS."plugins/dropzone/dropzone.css' rel='stylesheet' type='text/css' />
            <script src='".PATH_ASSETS."plugins/dropzone/dropzone.js'></script>
        ";
        $page_css.= "
            <link rel='stylesheet' href='".PATH_ADMIN_MODULES.strtolower($$module_class->module_name)."/page/style.css'>
        ";
        $doc_ready_js.= "Global.initList();\n";
    break;
}
?>