<?php
class Languages extends DB {

    var $tbLanguages = "pws_languages";

    function __construct()
    {
        $this->Page = new Page();
        $this->Page->module_name = "Languages"; // ucfirst
        $this->module_name = strtolower($this->Page->module_name); // strtolower
    }

    function duplicate($id = NULL, $name = NULL)
    {
        if ($id)
            $ct[] = "$this->tbLanguages.id = '$id' ";
        if ($name)
            $ct[] = "$this->tbLanguages.name = '$name' ";

        if (is_array($ct)) {
            $condition = implode("AND ", $ct);
            $condition = "WHERE " . $condition;
        }

        $field = "*";
        $sql = "SELECT $field FROM $this->tbLanguages ";
        $sql .= $condition;
        return $data = DB::query($sql, "count");
    }

    function check_duplicate_lang_id($id)
    {
        $sql = "SELECT * FROM $this->tbLanguages WHERE id = '$id'";
        $data = DB::query($sql, "count");
        if ($data > 0) {
            $json_data = array('message' => 'Duplicated', 'result' => $data);
        } else {
            $json_data = array('message' => 'OK', 'result' => $data);
        }
        echo json_encode($json_data);
    }

    function select($id = NULL, $default = NULL, $is_active = NULL, $query_type = NULL)
    {
        if (!isset($query_type))
            $query_type = "fetchAssoc";

        $sql = "SELECT * FROM `$this->tbLanguages`";

        if ($id)
            $ct[] = "$this->tbLanguages.`id` = '$id' ";
        if ($default)
            $ct[] = "`$this->tbLanguages`.`default` = '$default' ";
        if ($is_active)
            $ct[] = "`$this->tbLanguages`.`is_active` = '$is_active' ";
        if (is_array($ct))
            $sql = $sql . " WHERE " . implode(" AND ", $ct);

        $data = DB::query($sql, $query_type);
        return $data;
    }

    function insert($post_vars)
    {
        if ($post_vars)
            extract($post_vars);

        $name = DB::escape($name);
        $icon = DB::escape($icon);

        $field = "`id`, `name`, `icon`, `default`, `is_active`";
        $value = "'$id', '$name', '$icon', '$default', '$is_active'";
        $sql = "INSERT INTO $this->tbLanguages ($field) VALUES ($value)";
        $id = DB::query($sql, "lastInsertId");
        if ($id) {
            return $id;
        } else {
            return false;
        }
    }

    function update($post_vars)
    {
        if ($post_vars)
            extract($post_vars);

        $name = DB::escape($name);
        $icon = DB::escape($icon);

        $f = " `$this->tbLanguages`.`name` = '$name' ";
        if ($icon)
            $f .= ", `$this->tbLanguages`.`icon` = '$icon' ";
        if ($default)
            $f .= ", `$this->tbLanguages`.`default` = '$default' ";
        if ($is_active)
            $f .= ", `$this->tbLanguages`.`is_active` = '$is_active' ";

        $sql = "UPDATE $this->tbLanguages SET $f WHERE `$this->tbLanguages`.`id` = '$itemid'";
        return $data = DB::query($sql);
    }

    function update_status($id, $is_active)
    {
        $field = "is_active = '$is_active' ";
        $sql = "UPDATE $this->tbLanguages SET $field WHERE id = '$id'";
        return $data = DB::query($sql);
    }

    function delete($id)
    {
        if ($id != "en") {
            $sql = "DELETE FROM $this->tbLanguages WHERE id = '$id' ";
            $data = DB::query($sql);
            if ($data) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    ///Additional Functions
    function dropdown_languages($module, $action, $path, $itemid, $task = NULL) {
        $dd_data = '';
        $url_path = "&path=cp";
        if($module)
            $url_path.= "&module=".$module;
        if($action)
            $url_path.= "&action=".$action;
        if($task)
            $url_path.= "&task=".$task;
        if($itemid)
            $url_path.= "&itemid=".$itemid;

        if ($module && $action) {
            $data = $this->select(NULL, NULL, "Y");
            if ($data) {
                $dd_data = '
                <div class="btn-group pull-right m-t-5 m-r-5">
                <button type="button" class="btn btn-default dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Languages <span class="m-l-5"><i class="fa fa-language"></i></span></button>
                <ul class="dropdown-menu drop-menu-right" role="menu">';
                foreach ($data as $lang_item) {
                    $dd_data .= '
                            <li>
                                <a href="index.php?lang_id=' . $lang_item["id"] .$url_path. '">
                                    <span class="flag-icon flag-icon-' . $lang_item["icon"] . '"></span> Switch to ' . $lang_item["name"] . '
                                </a>
                            </li>';
                    unset($li_active);
                }
                unset($data_lang);
                $dd_data .= '</ul>
                            </div>';

            }
        }
        return $dd_data;
    }

}
?>