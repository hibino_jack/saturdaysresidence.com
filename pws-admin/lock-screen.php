<?php
require_once("../pws-core/site.config.php");

$users_class_path = PATH_ADMIN_MODULES_ROOT.'users/classes/class.users.php';
if(file_exists($users_class_path))
    require_once($users_class_path);

//INSTANCE
$users = new Users();

//Set Language
if(empty($lang_id))
    $lang_id = "en";
/*
if(isset($_SESSION[$users->get_session_name()]))  {
    redirect(PATH_ADMIN.'index.php');
    exit();
} else {
    redirect(PATH_ADMIN.'login.php');
    exit();
}
*/
//echo "<br>SESS: ";
//echo $_SESSION[$users->get_session_name()];
//echo "<br>";
if(!isset($oUser)) {
    redirect(PATH_ADMIN.'login.php');
    exit();
}
?>
<!DOCTYPE html>
<html>
	<head>
        <?php require_once(PATH_ADMIN_INC_ROOT."header.php"); ?>
	</head>
	<body>

		<div class="account-pages"></div>
		<div class="clearfix"></div>
		<div class="wrapper-page">
			<div class=" card-box">
				
				<div class="panel-body">
                    <form class="form-horizontal text-center" name="login-check" id="login-check">
						<div class="user-thumb">
                            <?php
                            //PRE($oUser);
                            $pic = BASE_UPLOAD."users/".$oUser->id."/".$oUser->profile_photo;
                            $pic_realpath = PATH_UPLOAD_ROOT."users/".$oUser->id."/".$oUser->profile_photo;
                            if(is_file($pic_realpath)) {
                                ?>
                                <img src="<?php echo $pic; ?>" alt="<?php echo $oUser->first_name." ".$oUser->last_name; ?>" class="img-responsive img-circle img-thumbnail">
                            <?php }else{ ?>
                                <img src="<?php echo BASE_IMAGES; ?>nophoto.jpg" alt="<?php echo $oUser->first_name." ".$oUser->last_name; ?>" class="img-responsive img-circle img-thumbnail">
                            <?php } ?>
                        </div>
                        <div class="form-group">
							<h3><?php echo $oUser->first_name." ".$oUser->last_name; ?></h3>
							<p class="text-muted">
								Enter your password to access the admin.
							</p>
                            <br />
                            <div class="alert alert-danger alert-dismissable hidden">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                <h4>Oh snap!</h4>
                                <p>This form seems to be invalid :(</p>
                            </div>
                            <div class="alert alert-success alert-dismissable hidden">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                <h4>Yay!</h4>
                                <p>Everything seems to be ok :)</p>
                            </div>

							<div class="input-group m-t-30">
                                <input type="hidden" name="module" value="users">
                                <input type="hidden" name="action" value="login">
                                <input type="hidden" name="path" value="cp">
                                <input type="hidden" name="username" value="<?php echo $oUser->username; ?>">
                                <input class="form-control" type="password" required="" placeholder="Password" name="passwd" value="">
								<span class="input-group-btn">
									<button type="submit" class="btn btn-pink w-sm waves-effect waves-light">
										Log In
									</button> 
								</span>
							</div>
						</div>
					</form>
       

				</div>
			</div>
			
			<div class="row">
				<div class="col-sm-12 text-center">
					<p>
						Not <?php echo $oUser->first_name." ".$oUser->last_name; ?>?<a href="login.php" class="text-primary m-l-5"><b>Sign In</b></a>
					</p>
				</div>
			</div>

		</div>


        <?php require_once(PATH_ADMIN_INC_ROOT."footer-script.php"); ?>

		<script src='https://unpkg.com/axios/dist/axios.min.js'></script>
        <script src="<?php echo BASE_ADMIN_MODULES; ?>users/classes/users.js"></script>
        <script type="text/javascript">
            Users.initLockScreen();
        </script>
        <?php $users->logout(); ?>
    </body>
</html>