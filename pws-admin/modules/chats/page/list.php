<?php
//$data = $$module_class->select();
//$datatotal = count($data);
//PRE($data);

//GET $data from API
//$conditions = "filter=sender_id,eq,".$oUser->id;
//$data = readAPIFile($$module_class->tbChats, 1, '', '');
//if($data) {
//    $datatotal = count($data);
//}
//PRE($data);
?>

<div class="row">
    <div class="col-md-4 col-lg-3">
        <div class="profile-detail card-box">
            <div>
                <?php
                $pic = BASE_UPLOAD."users/".$oUser->id."/".$oUser->profile_photo;
                $pic_realpath = PATH_UPLOAD_ROOT."users/".$oUser->id."/".$oUser->profile_photo;
                if(is_file($pic_realpath)) {
                    ?>
                    <img src="<?php echo $pic; ?>" class="img-circle" alt="<?php echo $oUser->first_name." ".$oUser->last_name; ?>">
                <?php }else{ ?>
                    <img src="<?php echo BASE_IMAGES; ?>nophoto.jpg" class="img-circle" alt="<?php echo $oUser->first_name." ".$oUser->last_name; ?>">
                <?php } ?>

                <hr>

                <div class="text-left">
                    <p class=""><strong class="m-l-0"><?php echo $oUser->first_name." ".$oUser->last_name; ?></strong></p>

                    <p class="text-muted font-13"><strong>Mobile :</strong> <br><span class="m-l-0">-</span></p>

                    <p class="text-muted font-13"><strong>Email :</strong> <br><span class="m-l-0"><a href="mailto:<?php echo $oUser->email; ?>"><?php echo $oUser->email; ?></a></span></p>

                    <p class="text-muted font-13"><strong>Last Login :</strong> <br><span class="m-l-0"><?php echo $oUser->last_login; ?></span></p>

                </div>


                <div class="button-list m-t-20">
                    <button type="button" class="btn btn-facebook waves-effect waves-light">
                        <i class="fa fa-facebook"></i>
                    </button>

                    <button type="button" class="btn btn-twitter waves-effect waves-light">
                        <i class="fa fa-twitter"></i>
                    </button>

                    <button type="button" class="btn btn-linkedin waves-effect waves-light">
                        <i class="fa fa-linkedin"></i>
                    </button>

                    <button type="button" class="btn btn-dribbble waves-effect waves-light">
                        <i class="fa fa-dribbble"></i>
                    </button>

                </div>
            </div>

        </div>

    </div>

    <div class="col-lg-9">
        <div class="portlet">
            <div class="portlet-heading bg-info">
                <h3 class="portlet-title">You want to chat with ?</h3>
                <div class="portlet-widgets">
                    <a href="javascript:;" data-toggle="reload"><i class="ion-refresh"></i></a>
                    <span class="divider"></span>
                    <a data-toggle="collapse" data-parent="#accordion1" href="#bg-info"><i class="ion-minus-round"></i></a>
                    <span class="divider"></span>
                    <a href="#" data-toggle="remove"><i class="ion-close-round"></i></a>
                </div>
                <div class="clearfix"></div>
            </div>
            <?php
            //$data_users = $users->select(NULL, NULL, "Y");
            $conditions = "filter[]=id,neq,".$oUser->id."&filter[]=is_active,eq,Y";
            $data_users = readAPIFile("pws_users", "1", "", $conditions);
            //PRE($data_users);
            ?>

            <div id="bg-info" class="panel-collapse collapse in">
                <div class="portlet-body">
                        <h4 class="m-t-0 m-b-20 header-title"><b>Users <span class="text-muted">(<?php echo count($data_users); ?>)</span></b></h4>
                        <div class="friend-list">
                            <?php
                            if($data_users) {
                                for($i=0; $i < count($data_users); $i++) {
                                    echo '<a href="javascript:void(0);" class="btn-chat-with" data-sender_id="'.$oUser->id.'" data-receiver_id="'.$data_users[$i]['id'].'">';

                                    $pic = BASE_UPLOAD."users/".$data_users[$i]['id']."/".$data_users[$i]['profile_photo'];
                                    $pic_realpath = PATH_UPLOAD_ROOT."users/".$data_users[$i]['id']."/".$data_users[$i]['profile_photo'];
                                    if(is_file($pic_realpath)) {
                                        echo '<img src="'.$pic.'" class="img-circle thumb-md" alt="'.$data_users[$i]['first_name'].' '.$data_users[$i]['last_name'].'">';
                                    }else{
                                        echo '<img src="'.BASE_IMAGES.'nophoto.jpg" class="img-circle thumb-md" alt="'.$data_users[$i]['first_name'].' '.$data_users[$i]['last_name'].'">';
                                    }
                                    echo '</a>';

                                }
                            }
                            ?>
                            <a href="javascript:void(0);" class="text-center">
                                <span class="extra-number">+</span>
                            </a>
                        </div>

                </div>
            </div>
        </div>
    </div>

    <!-- CHAT -->
    <div class="col-lg-9">

        <div class="card-box">
            <h4 class="m-t-0 m-b-20 header-title"><b>Chat Box</b></h4>

            <div class="chat-conversation" id="chat-conversation">
                <div id='notice_msg'></div>
                <ul class="conversation-list nicescroll" id="conversation-list">
                </ul>

                <div class="row">
                    <form id="dashboard-chat" name="dashboard-chat" action="">
                        <div class="col-sm-8 chat-inputbar">
                            <input type="hidden" id="module" name="module" value="<?php echo $module; ?>">
                            <input type="hidden" id="action" name="action" value="new">
                            <input type="hidden" id="task" name="task" value="list">
                            <input type="hidden" id="lang_id" name="lang_id" value="<?php echo $lang_id; ?>">
                            <input type="hidden" id="path" name="path" value="cp">

                            <input type="hidden" id="room_id" name="room_id" value="<?php echo (empty($room_id) || $room_id == "0") ? 1 : $room_id; ?>">
                            <input type="hidden" id="sender_id" name="sender_id" value="<?php echo (empty($sender_id) || $sender_id == "0") ? $oUser->id : $sender_id; ?>">
                            <input type="hidden" id="receiver_id" name="receiver_id" value="<?php echo (empty($receiver_id) || $receiver_id == "0") ? '0' : $receiver_id; ?>">
                            <div id="ReactJS-seen_date"></div>


                            <input type="hidden" id="chat-name" name="chat-name" value="<?php echo $oUser->first_name." ".$oUser->last_name; ?>">
                            <input type="text" id="message" name="message" class="form-control chat-input" placeholder="Enter your text">
                        </div>
                        <div class="col-sm-4 chat-send">
                            <button type="submit" class="btn btn-md btn-info btn-block waves-effect waves-light">Send</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
    <!-- end col-->

</div>

<style type="text/css">
    .conversation-text i {
        font-size: 0.7em;
        font-style: normal;
        color: #c6c6c6;
    }
</style>
