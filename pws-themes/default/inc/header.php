<?php /*
<!-- Header -->
<header>
    <div class="page-heading">
        <div class="container">
            <div class="row">
                <div class="col-md-offset-3 col-md-6 text-center">
                    <h1><?php echo $data_content[0]['title']; ?></h1>
                    <p><?php echo $data_content[0]['short_description']; ?></p>
                </div>
            </div>
        </div>
    </div>

    <!-- header photo -->
    <div class="inner-header">
        <div class="container">
            <div class="row">
                <?php if(empty($header_photo)) { ?>
                    <div class="inner-header-fill" style="background-image:url('http://placehold.it/1450x500&text=<?php echo $data_content[0]['title']; ?>');"></div>
                <?php } else {
                    $pic = BASE_UPLOAD."contents/".$data_content[0]['id']."/".$data_content[0]['header_photo'];
                    $pic_realpath = PATH_UPLOAD_ROOT."contents/".$data_content[0]['id']."/".$data_content[0]['header_photo'];
                    if(is_file($pic_realpath)) { ?>
                    <div class="inner-header-fill" style="background-image:url('<?php echo $pic; ?>');"></div>
                    <?php } ?>
                <?php } ?>
            </div>
        </div>
    </div>
</header>
<!-- //Header -->
<?php */ ?>