<?php
if(!isset($_SESSION) && !headers_sent()) session_start(); //Start Session
setlocale(LC_ALL, 'th_TH');
$timezone = "Asia/Bangkok";
if(function_exists('date_default_timezone_set')) date_default_timezone_set($timezone);

/*
 * Core Configuration of Cupid CMS
 */
$site['SYSTEM_VERSION'] = '1.0';
$site['SYSTEM_BUILD'] = '1 beta';
$site['SYSTEM_AUTH'] = "Phuket Web Studio";
$site['SYSTEM_SITE_URL'] = "http://www.phuketwebstudio.com";
$site['SYSTEM_SITE_NAME'] = "Cupid CMS";
$site['SYSTEM_MAIL'] = "jack@phuketwebstudio.com";

//$site['root'] = "/saturdaysresidence.com/"; // source file folder /PWS /localhost
$site['root'] = "/"; // source file folder - live site of saturdaysresidence.com

define("POWER_BY_URL", $site['SYSTEM_SITE_URL'], true);
define("POWER_BY_TITLE", $site['SYSTEM_AUTH'], true);
define("COPYRIGHT_YEAR", date("Y"), true);
define('SYSTEM_TITLE', $site['SYSTEM_SITE_NAME']);
define('SYSTEM_VERSION', $site['SYSTEM_VERSION'].".".$site['SYSTEM_BUILD']);

/*
 * Database Setup
 */
$db['host'] = 'localhost';
$db['sock'] = '';
$db['port'] = '';
//$db['user'] = 'root';
//$db['passwd'] = '1234567890';
//$db['db'] = 'pws_satdayresidence';

$db['user'] = 'saturday_pd2017';
$db['passwd'] = 'v1VLW2CB';
$db['db'] = 'saturday_pd2017';

$db['charset'] = 'utf8';

// define for data layer
define('DATABASE_HOST', $db['host']);
define('DATABASE_SOCK', $db['sock']);
define('DATABASE_PORT', $db['port']);
define('DATABASE_USER', $db['user']);
define('DATABASE_PASS', $db['passwd']);
define('DATABASE_NAME', $db['db']);
define('DATABASE_CHARSET', $db['charset']);

/*
 *
 * Set up database connection and global function
 * -------- DO NOT EDIT ANYTHING BELOW ----------
 *
 */

require_once("classes/class.db.php");
require_once("classes/class.sitefunctions.php");
require_once("classes/class.page.php"); //Declare $Page for render() html

if(!isset($base_url)) $base_url = base_url(); //Set WWW Base URL
if(!isset($base_path)) $base_path = base_path(); //Set root path URL
if(!isset($home_url)) $home_url = home_url(); //home www url path
if(!isset($curPageURL)) $curPageURL = curPageURL(); //current path

// Set Paths and URL
$site['host'] = $home_url; // = "http://localhost";
$site['url'] = $base_url;  // = dynamic url
$site['base'] = $base_path; // = dynamic physical path

$site['dir_root'] = "{$_SERVER['DOCUMENT_ROOT']}"; // = absolute path
//$site['path_root'] = "{$_SERVER['DOCUMENT_ROOT']}{$site['root']}";

$site['cache'] = "{$site['root']}cache/";
$site['api'] = "{$site['root']}rest/";

$site['core'] = "{$site['root']}pws-core/";
$site['assets'] = "{$site['core']}assets/";
$site['classes'] = "{$site['core']}classes/";
$site['languages'] = "{$site['core']}languages/";

$site['admin'] = "{$site['root']}pws-admin/";
$site['admin_inc'] = "{$site['admin']}inc/";
$site['admin_modules'] = "{$site['admin']}modules/";

$site['media'] = "{$site['root']}media/";
$site['mediaImages'] = "{$site['media']}images/";
$site['mediaUpload'] = "{$site['media']}uploads/";

/**
 * define root path, absolute path, physical path
 */
define('SITE_ROOT', $site['host']);
define('BASE_URL', $site['url']);
define('PATH_URL', $site['base']);
define('PATH_ROOT', $site['root']);
define('DOCUMENT_ROOT', $site['dir_root']);

define('PATH_CACHE', $site['cache']);
define('PATH_API', $site['api']);

define('PATH_CORE', $site['core']);
define('PATH_CLASSES', $site['classes']);
define('PATH_ASSETS', $site['assets']);
define('PATH_LANGUAGES', $site['languages']);

define('PATH_CACHE_ROOT', DOCUMENT_ROOT.$site['cache']);
define('PATH_API_ROOT', DOCUMENT_ROOT.$site['api']);

define('PATH_CORE_ROOT', DOCUMENT_ROOT.$site['core']);
define('PATH_CLASSES_ROOT', DOCUMENT_ROOT.$site['classes']);
define('PATH_ASSETS_ROOT', DOCUMENT_ROOT.$site['assets']);
define('PATH_LANGUAGES_ROOT', DOCUMENT_ROOT.$site['languages']);

define('PATH_ADMIN', $site['admin']);
define('PATH_ADMIN_INC', $site['admin_inc']);
define('PATH_ADMIN_MODULES', $site['admin_modules']);

define('PATH_ADMIN_ROOT', DOCUMENT_ROOT.$site['admin']);
define('PATH_ADMIN_INC_ROOT', DOCUMENT_ROOT.$site['admin_inc']);
define('PATH_ADMIN_MODULES_ROOT', DOCUMENT_ROOT.$site['admin_modules']);

define('PATH_MEDIA', $site['media']);
define('PATH_IMAGES', $site['mediaImages']);
define('PATH_UPLOAD', $site['mediaUpload']);

define('PATH_MEDIA_ROOT', DOCUMENT_ROOT.$site['media']);
define('PATH_IMAGES_ROOT', DOCUMENT_ROOT.$site['mediaImages']);
define('PATH_UPLOAD_ROOT', DOCUMENT_ROOT.$site['mediaUpload']);

/**
 * define full URL path
 */
define('BASE_API', SITE_ROOT."{$site['api']}");
define('BASE_CACHE', SITE_ROOT."{$site['cache']}");

define('BASE_CORE', SITE_ROOT."{$site['core']}");
define('BASE_ASSETS', SITE_ROOT."{$site['assets']}");
define('BASE_CLASSES', SITE_ROOT."{$site['classes']}");
define('BASE_LANGUAGES', SITE_ROOT."{$site['languages']}");

define('BASE_ADMIN', SITE_ROOT."{$site['admin']}");
define('BASE_ADMIN_INC', SITE_ROOT."{$site['admin_inc']}");
define('BASE_ADMIN_MODULES', SITE_ROOT."{$site['admin_modules']}");

define('BASE_MEDIA', SITE_ROOT."{$site['media']}");
define('BASE_IMAGES', SITE_ROOT."{$site['mediaImages']}");
define('BASE_UPLOAD', SITE_ROOT."{$site['mediaUpload']}");

//Set Language
//require_once("classes/class.language.php");
require_once(PATH_ADMIN_MODULES_ROOT."languages/classes/class.languages.php");
if($_GET['lang_id'])
    $lang_id = $_GET['lang_id'];

if(empty($lang_id))
    $lang_id = "en";

$languages = new Languages();
$oLanguages = $languages->select($lang_id, NULL, NULL, "fetchObj");

//Set MVC Controller :: $oController
require_once("classes/class.controller.php");
//$oController = new Controller();

//Site Config
//require_once("classes/class.config.php");
require_once(PATH_ADMIN_MODULES_ROOT."config/classes/class.config.php");
$config = new Config();
$oConfig = $config->select($lang_id, NULL, NULL, "fetchObj");

$site['themes'] = "{$site['root']}pws-themes/";

if($oConfig->theme_name) {
    echo $site['themes'] = "{$site['themes']}{$oConfig->theme_name}";
    $site['theme_style'] = $oConfig->theme_style;
} else {
//    $site['themes'] = "/saturdaysresidence.com/pws-themes/default/";
    $site['themes'] = "/pws-themes/default/";
    $site['theme_style'] = "basic";
}

$site['base_css'] = "{$site['themes']}css/";
$site['base_js'] = "{$site['themes']}js/";
$site['base_img'] = "{$site['themes']}img/";
$site['base_modules'] = "{$site['themes']}modules/";

define('BASE_THEMES', $site['themes']);
define('BASE_CSS', $site['base_css']);
define('BASE_JS', $site['base_js']);
define('BASE_IMG', $site['base_img']);

define('PATH_THEMES', SITE_ROOT.$site['themes']);
define('PATH_CSS', SITE_ROOT.$site['base_css']);
define('PATH_JS', SITE_ROOT.$site['base_js']);
define('PATH_IMG', SITE_ROOT.$site['base_img']);

define('PATH_THEMES_ROOT', DOCUMENT_ROOT.$site['themes']);
define('PATH_CSS_ROOT', DOCUMENT_ROOT.$site['base_css']);
define('PATH_JS_ROOT', DOCUMENT_ROOT.$site['base_js']);
define('PATH_IMG_ROOT', DOCUMENT_ROOT.$site['base_img']);

define('PATH_MODULES', SITE_ROOT.$site['base_modules']);
define('PATH_MODULES_ROOT', DOCUMENT_ROOT.$site['base_modules']);

define('MAXSIZE_UPLOAD', 5000000);
define('MAX_FILES_UPLOAD', 5);

$module = (isset($_REQUEST['module']) ? $_REQUEST['module'] : (isset($_POST['module']) ? $_POST['module'] : (isset($_GET['module']) ? $_GET['module'] : "")));
$action = (isset($_REQUEST['action']) ? $_REQUEST['action'] : (isset($_POST['action']) ? $_POST['action'] : (isset($_GET['action']) ? $_GET['action'] : "")));
$task = (isset($_REQUEST['task']) ? $_REQUEST['task'] : (isset($_POST['task']) ? $_POST['task'] : (isset($_GET['task']) ? $_GET['task'] : "")));
$path = (isset($_REQUEST['path']) ? $_REQUEST['path'] : (isset($_POST['path']) ? $_POST['path'] : (isset($_GET['path']) ? $_GET['path'] : "")));
$itemid = (isset($_REQUEST['itemid']) ? $_REQUEST['itemid'] : (isset($_POST['itemid']) ? $_POST['itemid'] : (isset($_GET['itemid']) ? $_GET['itemid'] : "")));

if($_SESSION['oUser'])
    $oUser = (object)$_SESSION['oUser'];

//Clear old temp
delete_old_tmp(PATH_CACHE_ROOT);
?>