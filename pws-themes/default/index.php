<?php
//PRE($_GET);
//PRE($data_content);
//$description = str_ireplace(array("\r","\n",'\r','\n'),'', $description);
//exit;
?>
<!DOCTYPE html>
<html lang="<?php echo $oConfig->lang_id; ?>">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo $meta_title; ?></title>
    <meta name="description" content="<?php echo $meta_title; ?>">
    <meta name="keywords" content="<?php echo $meta_keyword; ?>">

    <meta name="author" content="<?php echo POWER_BY_TITLE; ?>">

    <?php include(PATH_THEMES_ROOT."inc/header.script.php"); ?>

</head>

<body id="<?php echo ($filename == "contact") ? 'page-contact':'page-top';?>" class="index <?php echo ($filename == "index") ? 'homepage':'';?> ">
    <?php include(PATH_THEMES_ROOT."inc/nav.php"); ?>

    <?php
    if($filename == "" || $filename == "index") {
        include(PATH_THEMES_ROOT."inc/header.carousel.php");
    } else {
        include(PATH_THEMES_ROOT."inc/header.php");
    }
    ?>


    
        <?php
		if($filename == "" || $filename == "index") {
            include(PATH_THEMES_ROOT."page/contents.php");
        } else {
			if($_GET["title"] != "") //detail page
			{
				include(PATH_THEMES_ROOT."page/".$filename."-detail.php");
			}else{
				$template = $filename;
                $default_arr = array("terms", "features", "ownership", "privacy");
				if(in_array($filename,$default_arr))
					$template = "default";
				
				include(PATH_THEMES_ROOT."page/".$template.".php");
			}
        }
        ?>
    
    <?php include(PATH_THEMES_ROOT."inc/footer.php"); ?>
    <?php include(PATH_THEMES_ROOT."inc/footer.script.php"); ?>
</body>
</html>
