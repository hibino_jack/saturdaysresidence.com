<?php
if($action == "update")    {
    if($$module_class->itemid) {
        $data = $$module_class->select($lang_id, $$module_class->itemid);
        $itemid = $$module_class->itemid;
        if($data){
            extract($data[0]);
            //PRE($data);
        }
    }
}
?>

<div class="row">
    <div class="col-md-12">
        <?php echo $languages->dropdown_languages($module, $action, $path, $itemid, $task); ?>
        <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"><?php echo $module_class." ".$action;?></h3>
        </div>
        <div class="panel-body">
            <form name="create-form" id="create-form" class="form-horizontal" role="form" data-parsley-validate novalidate enctype="multipart/form-data">
                <div class="alert alert-danger alert-dismissable hidden">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <h4>Oh snap!</h4>
                    <p>This form seems to be invalid :(</p>
                </div>
                <div class="alert alert-success alert-dismissable hidden">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <h4>Yay!</h4>
                    <p>Everything seems to be ok., Re-submit your form then. :)</p>
                </div>

                <div class="row">
                    <div class="col-md-12">

                        <div class="form-group">
                            <label class="col-md-2 control-label">Parent Menu</label>
                            <div class="col-md-10">
                                <?php
                                $tbMenus = $$module_class->tbMenus;
                                $field = "name";
                                $where = "`$tbMenus`.`lang_id`='$lang_id' AND `$tbMenus`.`parent_id`='0' AND `$tbMenus`.`is_active`='Y'";
                                //$parent_data = SELECT($$module_class->tbMenus, $field, $where, NULL, NULL, NULL, NULL, 'debug');
                                //print_r($parent_data);
                                echo DROPDOWNITEM("parent_id", $parent_id, $$module_class->tbMenus, "id", $field, $where);

                                ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label">Name</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" name="name" parsley-trigger="change" required placeholder="Enter Name" value="<?php echo $name;?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label">Link</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" name="link" parsley-trigger="change" required placeholder="Enter Link URL" value="<?php echo $link;?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Enable to Top Navigation</label>
                            <div class="col-sm-10">
                                <?php
                                $top_nav_data = array("Y" => "Enable", "N" => "Disabled");
                                echo RADIOITEMMANUAL("top_nav", $top_nav, $top_nav_data);
                                ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Enable to Footer Navigation</label>
                            <div class="col-sm-10">
                                <?php
                                $foot_nav_data = array("Y" => "Enable", "N" => "Disabled");
                                echo RADIOITEMMANUAL("foot_nav", $foot_nav, $foot_nav_data);
                                ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label">Icon</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" name="icon" parsley-trigger="change" required placeholder="Enter Icon Code" value="<?php echo $icon;?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label">Icon file</label>
                            <div class="col-md-10">
                                <input type="file" name="file_icon" id="file_icon" class="filestyle" data-buttonbefore="true"><br />
                                <?php
                                $pic = BASE_UPLOAD.$module."/".$id."/".$file_icon;
                                $pic_realpath = PATH_UPLOAD_ROOT.$module."/".$id."/".$file_icon;
                                if(is_file($pic_realpath)) {
                                    ?>
                                    <a class="image-popup" href="<?php echo $pic; ?>" title="Logo">
                                        <img src="<?php echo PATH_ROOT; ?>timthumb.php?src=<?php echo $pic_realpath; ?>&w=250&h=200" width="250" height="200">
                                    </a>
                                <?php }else{ ?>
                                        <img src="<?php echo BASE_IMAGES; ?>nophoto.jpg" width="250" height="200"/>
                                <?php } ?>

                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Active</label>
                            <div class="col-sm-10">
                                <?php
                                $is_active_data = array("Y" => "Enable", "N" => "Disabled");
                                echo RADIOITEMMANUAL("is_active", $is_active, $is_active_data);
                                ?>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="row">
                <div class="col-md-12">
                    <div class="form-group pull-right">
                        <input type="hidden" id="module" name="module" value="<?php echo $module; ?>">
                        <input type="hidden" id="action" name="action" value="<?php echo $action; ?>">
                        <input type="hidden" id="lang_id" name="lang_id" value="<?php echo $lang_id; ?>">
                        <input type="hidden" id="itemid" name="itemid" value="<?php echo $itemid; ?>">
                        <input type="hidden" id="path" name="path" value="cp">

                        <button type="submit" class="btn btn-default waves-effect waves-light btn-lg">Submit</button>
                        <button type="reset" class="btn btn-white waves-effect waves-light btn-lg">Cancel</button>
                    </div>
                </div>
                </div>
            </form>

        </div>
    </div>

    </div>
</div>