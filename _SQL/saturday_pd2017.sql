-- phpMyAdmin SQL Dump
-- version 4.4.15
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 24, 2017 at 11:19 AM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.3.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `saturday_pd2017`
--

-- --------------------------------------------------------

--
-- Table structure for table `pws_careers`
--

DROP TABLE IF EXISTS `pws_careers`;
CREATE TABLE IF NOT EXISTS `pws_careers` (
  `id` int(11) NOT NULL,
  `lang_id` varchar(3) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(100) NOT NULL,
  `no_position` varchar(100) NOT NULL,
  `job_type` varchar(100) NOT NULL,
  `exp` varchar(100) NOT NULL,
  `age` varchar(100) NOT NULL,
  `description` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `sort` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_date` datetime NOT NULL,
  `is_active` enum('Y','N') NOT NULL DEFAULT 'Y'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pws_chats`
--

DROP TABLE IF EXISTS `pws_chats`;
CREATE TABLE IF NOT EXISTS `pws_chats` (
  `id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL DEFAULT '1',
  `sender_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL DEFAULT '0',
  `message` text NOT NULL,
  `sent_date` datetime DEFAULT NULL,
  `seen_date` datetime DEFAULT NULL,
  `filename` varchar(200) NOT NULL,
  `is_active` enum('Y','N') DEFAULT 'Y'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pws_chats_rooms`
--

DROP TABLE IF EXISTS `pws_chats_rooms`;
CREATE TABLE IF NOT EXISTS `pws_chats_rooms` (
  `id` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `joined_members` varchar(255) NOT NULL,
  `is_active` enum('Y','N') DEFAULT 'Y'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pws_chats_rooms`
--

INSERT INTO `pws_chats_rooms` (`id`, `created_by`, `created_date`, `joined_members`, `is_active`) VALUES
(1, 1, '2016-11-16 11:44:55', 'all', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `pws_config`
--

DROP TABLE IF EXISTS `pws_config`;
CREATE TABLE IF NOT EXISTS `pws_config` (
  `id` int(3) NOT NULL,
  `lang_id` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'en',
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telephone` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `contact_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `contact_email` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_keywords` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `meta_description` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `link_facebook` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `link_youtube` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `link_skype` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `link_twitter` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `link_instagram` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `link_linkedin` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `link_pinterest` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `link_tripadvisor` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `link_googleplus` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `link_line` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `facebook_embed` text COLLATE utf8_unicode_ci NOT NULL,
  `googlemap_embed` text COLLATE utf8_unicode_ci NOT NULL,
  `file_map` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file_logo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `theme_id` int(3) NOT NULL DEFAULT '1',
  `is_active` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pws_config`
--

INSERT INTO `pws_config` (`id`, `lang_id`, `name`, `email`, `address`, `telephone`, `contact_name`, `contact_email`, `meta_title`, `meta_keywords`, `meta_description`, `link_facebook`, `link_youtube`, `link_skype`, `link_twitter`, `link_instagram`, `link_linkedin`, `link_pinterest`, `link_tripadvisor`, `link_googleplus`, `link_line`, `facebook_embed`, `googlemap_embed`, `file_map`, `file_logo`, `theme_id`, `is_active`) VALUES
(1, 'en', 'Saturdays Residence', 'info@saturdaysresidence.com', 'The Saturdays Residence 136/278 Moo 4 Rawai, Phuket 83130 Thailand', 'Sale +66(0) 99 149 5900 &nbsp;&nbsp;/&nbsp;&nbsp; Residence +66 (0) 86- 4768563', 'Reservation Team', 'rsvn@saturdaysresidence.com', 'Saturdays Residence is a member of The Attitude Club, Phuket.', 'Saturdays Residence,condo', 'Saturdays Residence near Nai Harn Beach in southern Phuket is much more than a pleasant place to live. Residents enjoy an entirely new kind of lifestyle in a community of large, villa-style living spaces with authentic tropical appeal.', 'http://www.facebook.com/saturdaysresidence', 'SaturdaysCondo', 'SaturdaysCondo', 'SaturdaysCondo', 'http://www.instagram.com/saturdayscondo', 'SaturdaysCondo', 'SaturdaysCondo', 'SaturdaysCondo', 'SaturdaysCondo', 'SaturdaysCondo', 'https://www.facebook.com/SaturdaysCondo', '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15811.471665753732!2d98.33138299999997!3d7.8038049999999926!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x4c2b3ec35034c86f!2sSaturdays+condominium!5e0!3m2!1sth!2sth!4v1485011036097" frameborder="0" style="border:0" allowfullscreen></iframe>', 'location_map_photo.jpg', 'logo.png', 1, 'Y'),
(1, 'th', 'ภูเก็ตเว็บสตูดิโอ ดอท คอม', 'sale@phuketwebstudio.com', '95 Moo 1, Soi Choeng Thale 14, Si Sunthon Rd., Choeng Thale District., Thalang, Phuket 83110', '+660802003255', 'contact', 'contact@phuketwebstudio.com', 'ภูเก็ตเว็บสตูดิโอ', 'Cupid-CMS,Cupid,CMS,PWS,phuketwebstudio,Phuket Web Studio,ภูเก็ตเว็บสตูดิโอ', 'ภูเก็ตเว็บสตูดิโอ Cupid-CMS via Phuket Web Studio', 'PhuketWebStudio', 'PhuketWebStudio', 'PhuketWebStudio', 'PhuketWebStudio', 'PhuketWebStudio', 'PhuketWebStudio', 'PhuketWebStudio', 'PhuketWebStudio', 'PhuketWebStudio', 'PhuketWebStudio', 'https://www.facebook.com/phuketwebstudio', 'https://www.google.co.th/maps/place/Phuket+Web+Studio/@7.990852,98.301412,15z/data=!4m5!3m4!1s0x0:0x96493a64f4f41058!8m2!3d7.990852!4d98.301412', 'location_map_photo.jpg', 'logo.png', 1, 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `pws_config_paypal`
--

DROP TABLE IF EXISTS `pws_config_paypal`;
CREATE TABLE IF NOT EXISTS `pws_config_paypal` (
  `id` int(11) NOT NULL,
  `paypal_account` varchar(250) NOT NULL,
  `paypal_user` varchar(100) NOT NULL,
  `paypal_pwd` varchar(100) NOT NULL,
  `paypal_method` varchar(200) NOT NULL,
  `paypal_returnurl` varchar(255) NOT NULL,
  `paypal_cancelurl` varchar(255) NOT NULL,
  `paypal_paymentaction` varchar(100) NOT NULL,
  `paypal_amt` varchar(100) NOT NULL,
  `paypal_currentcycode` varchar(50) NOT NULL,
  `paypal_desc` text NOT NULL,
  `is_active` enum('Y','N') DEFAULT 'Y'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pws_contents`
--

DROP TABLE IF EXISTS `pws_contents`;
CREATE TABLE IF NOT EXISTS `pws_contents` (
  `id` int(11) NOT NULL,
  `lang_id` varchar(2) NOT NULL DEFAULT 'en',
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `filename` varchar(100) NOT NULL,
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `subtitle` varchar(100) NOT NULL,
  `short_description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description2` longtext NOT NULL,
  `header_photo` varchar(255) NOT NULL,
  `meta_title` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `meta_keyword` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `meta_description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_date` datetime NOT NULL,
  `is_active` enum('Y','N') NOT NULL DEFAULT 'Y'
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pws_contents`
--

INSERT INTO `pws_contents` (`id`, `lang_id`, `parent_id`, `filename`, `title`, `subtitle`, `short_description`, `description`, `description2`, `header_photo`, `meta_title`, `meta_keyword`, `meta_description`, `created_by`, `updated_by`, `updated_date`, `is_active`) VALUES
(1, 'en', 0, 'index', 'Home', 'Sleep Less Dream More', '', '<p>Welcome to your tropical island retreat with views of the Andaman Sea from classic New York Bay apartment-style residences in Saiyuan on the south coast of Phuket in sunny southern Thailand.</p>\r\n\r\n<p>Stay at Saturdays and ease into a lifestyle of contemporary comfort and convenience designed to inspire you.\r\n</p>\r\n                  \r\n					\r\n                     <div class="row global-icon-wrapper">\r\n                        <span class="col-sm-3 col-xs-6"><i class="global-icons icon-pool"></i>Pool</span>\r\n                        <span class="col-sm-3 col-xs-6"><i class="global-icons icon-restaurant"></i>Cafe</span>\r\n                        <span class="col-sm-3 col-xs-6"><i class="global-icons icon-gym"></i>Gym Fitness</span>\r\n                        <span class="col-sm-3 col-xs-6"><i class="global-icons icon-shuttle"></i>Shuttle Service</span>\r\n                    </div>\r\n', '', '', 'Saturday Residence', 'saturdays apartment phuket,saturdays residences phuket,apartment saiyuan.residence', 'Welcome to your tropical island retreat with views of the Andaman Sea from classic New York Bay apartment-style residences in Saiyuan.', 2, 2, '2017-02-23 09:28:42', 'Y'),
(2, 'en', 0, 'sleep', 'Sleep', '', 'The longer you stay the more you save!', '     <!-- hotel-policy Section -->\r\n    <div class="hotel-policy">\r\n        <div class="container">\r\n            <div class="row hotel-policy-box">\r\n            	<h3><span>Residence Policy</span></h3>\r\n            	<div class="col-md-4">\r\n                     <!-- Nav tabs -->\r\n                      <ul class="nav nav-tabs" role="tablist">\r\n                        <li role="presentation" class="active"><a href="#cancellation" aria-controls="cancellation" role="tab" data-toggle="tab">Cancellation Policy</a></li>\r\n                        <li role="presentation"><a href="#general" aria-controls="general" role="tab" data-toggle="tab">At Our Reception</a></li>\r\n                        <li role="presentation"><a href="#checkin-checkout" aria-controls="checkin-checkout" role="tab" data-toggle="tab">Check in / Check Out</a></li>\r\n                      </ul>\r\n                 </div>\r\n                 <div class="col-md-8">\r\n                 	<!-- Tab panes -->\r\n                      <div class="tab-content">\r\n                        <div role="tabpanel" class="tab-pane active" id="cancellation">\r\n                        	<p>We require 30 days for amendment or cancellation notice prior to your scheduled arrival, otherwise you will be charged a cancellation fee of full amount deposit. </p>\r\n                        </div>\r\n                        <div role="tabpanel" class="tab-pane" id="general">\r\n                        	\r\n<p>a. To take good care of and preserve the premise as an ordinary prudent person would do his/her own expense, unless such damage is caused by normal wear and tear, fire or other casualty ( cause other than by the negligence or misconduct of the Tenant, his/her family members or visitors ).</p>\r\n<p>b. Not to do or permit anyone to do anything,which may cause nuisance, annoyance or interference to the peacefulness and comfort of other Tenants in the premises, the building or its compound.</p><p>\r\n</p><p>c. To be responsible for the safety and appropriate conduct of all members of his/her family, visitor, personal servants and subordinates and to ensure they observe and comply with the rules and regulations.</p>\r\n<p>d. Not to assign any right hereunder, sub-lease, part of all the premises or allow occupancy to any third persons, whether in whole or in part without prior written consent from the Landlord.</p>\r\n<p>e. Not to make any alternations, additions, decoration or removal to or from the premises without the prior written consent from the Landlord. All authorization alternations, additions and decorations made to the premises, which are not permanently made or are removable without causing serious damage to the premise or any part thereof shall not become the property of the Landlord, and upon the expiration of termination of the agreement, or decoration out of the premise. In such cases, the Tenant shall be responsible for the cost of restoration of the premises to the original condition at the time of lease period</p>\r\n<p>f. Not to bring, keep or permit to be kept or brought on the premises, the building or any part thereof any materials of a dangerous, explosive, inflammable or obnoxious nature or the keeping of which may contravene any statutes, regulations or by-laws or in respect of which an increased rate of insurance may be required or the keeping of which cause any insurance policy in respect of the premises to become null and void</p>\r\n<p>g. Not to throw or permit to be throw dirt, rubbish, rages, boxes or other refuse out of the premises and in common parts of the building, except into proper bins or other container provided for such purposes as provided by the Landlord.</p>\r\n<p>h. Durian fruit is not allowed</p>\r\n<p>i. Pet is not allowed </p>\r\n\r\n                        </div>\r\n                        <div role="tabpanel" class="tab-pane" id="checkin-checkout">\r\n                        	<p>Your independence is your prerogative at Saturdays. When you arrive at the Lobby Cafe, relax with a snack before making your way to your residence. Our staff are on hand to help you settle in and we are happy to help with anything you might need during normal working hours.   </p>\r\n                        </div>\r\n                      </div>\r\n                      \r\n                 </div>\r\n     		</div>\r\n        </div>\r\n    </div>\r\n', '<p>Relaxing and letting go of life’s stresses and distractions is essential to our physical and spiritual well being, but how often do we make the effort to detach from the material world and stop to appreciate the simple pleasures in life?</p>\r\n\r\n<p>That’s why at Saturdays you won’t find a television in the bedroom. With views of the pool, the surrounding jungle-clad hills and the Andaman sea, there’s plenty to see that will inspire you and lull you into a peaceful trouble-free deep sleep in the softest beds.</p> \r\n\r\n<p>Each residence has en-suite bathrooms, an outdoor terrace and a fully functional kitchen that is reminiscent of Boston’s Modernism era with a breakfast bar, electric hob, vintage-style fridge and built-in cupboards for independent self catering with style and convenience.\r\n</p>\r\n\r\n<p>“The longer you stay the more you save!”<br>\r\nStay for a week, a month or a whole year at a time with special discount deals for long-term stays at Saturdays. </p>\r\n\r\n<p><a href="http://www.saturdaysresidence.com/en/offers/">Click here</a> to see the latest Saturdays Stay deals. </p>\r\n', '', 'Sleep', 'Sleep', 'Sleep', 2, 2, '2017-02-23 12:44:46', 'Y'),
(3, 'en', 0, 'taste', 'Taste', '', 'Ease into your stay when you check in at Saturdays at the spacious, naturally lit and airy atrium lounge. Meet with friends and acquaintances for a chat on comfy couches with a fabulously fresh coffee or smoothie designed to your taste.', '              <h2>Origami</h2>\r\n					<p>Go Zen for hassle-free breakfast lunch or dinner with easy wholesome choices at the Origami deli overlooking the pool, where simplicity and style are combined in the design of the restaurant as well as your meals. </p>\r\n					\r\n                    \r\n', '', '3_170223102113_taste_header.jpg', 'taste', 'saturdays cafe,cafe,restaurant', 'Ease into your stay when you check in at Saturdays at the spacious and naturally', 2, 2, '2017-02-23 10:21:13', 'Y'),
(4, 'en', 0, 'do', 'Do', '', '', '', '', '', 'Do', 'Do', 'Staying at Saturdays means becoming reacquainted with yourself.', 2, 2, '2017-02-22 14:55:48', 'Y'),
(5, 'en', 0, 'gather', 'Gather', '', 'We are happy to help with any other arrangements you may need to make meetings go smoothly.', '<p>For executives staying at Saturdays we have facilities to help you conduct business with comfort and convenience, including the Saturdays Meeting Room, which can hold up to 20 people and features audio-video conference technology. We are happy to help with any other arrangements you may need to make meetings go smoothly.</p>', '', '5_170223103541_gather_header.jpg', 'gather', 'gather', 'gather', 2, 2, '2017-02-23 10:35:41', 'Y'),
(6, 'en', 0, 'gallery', 'Gallery', '#saturdaysresidence', 'Share your Saturdays experiences on social media with the tag #saturdaysresidence', '', '', '', 'gallery', 'gallery', 'gallery', 2, 2, '2017-02-08 22:09:49', 'Y'),
(7, 'en', 0, 'offers', 'Offers', '', 'Find your perfect retreat with seasonal experiences, unique packages and special rates at Saturdays Residence.', '<h3 style="padding-bottom:25px;">For more information</h3>\r\n<p> please contact +66 (0) 86- 4768563 / +66 (0) 92-3846532 </p>\r\n<p>or E-mail <a href="mailto:rsvn@saturdaysresidence.com">rsvn@saturdaysresidence.com</a></p>\r\n<p>address : 136/275-276 Saiyuan-Kata Rd., Rawai Sub-district, Muang District, Phuket 83130 </p>', '', '7_170223110240_offer_header.jpg', 'offers', 'Privacy Policy', 'Find your perfect retreat with seasonal experiences, unique packages and special rates at Saturdays Residence.', 2, 2, '2017-02-23 11:09:12', 'Y'),
(8, 'en', 0, 'fieldguide', 'Field Guide', '', '', '', '', '', 'Field Guide', 'Field Guide', 'Field Guide', 1, 1, '2017-01-28 00:02:09', 'Y'),
(9, 'en', 0, 'ourstory', 'Our Story', '', 'Saturdays is a time for recharging.', '<!-- paragraphs-box Section -->\r\n    <div class="paragraphs-box" style="margin-top:30px;">\r\n        <div class="container">\r\n            <div class="row paragraphs-items">\r\n            	<div class="col-md-6">\r\n                	<div class="row paragraphs-image">\r\n                		<img src="http://www.saturdaysresidence.com/media/uploads/contents/9/smn_contents_20170209231403.jpg" class="img-responsive">\r\n                    </div>\r\n                </div>\r\n               	<div class="col-md-6">\r\n                	<div class="row paragraphs-content">\r\n                    	<h3>How Saturdays Began  </h3>\r\n                            <h4>Saturdays is a time for recharging. </h4>\r\n                           \r\n                            <p>For most of us, Saturday is our favourite day of the week. It''s the first day off after a hard week''s work, when you can forget about the stress of the office and there''s still Sunday to come, making Saturday all the more relaxing and unrushed. </p>\r\n                           \r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    \r\n     \r\n    \r\n    \r\n    <!-- room header Section -->\r\n    <div class="inner-header">\r\n        <div class="container">\r\n                <div class="row">\r\n                    <div class="inner-header-fill" style="background-image:url(''http://www.saturdaysresidence.com/media/uploads/contents/9/smn_contents_20170209232517.jpg'');">\r\n                    	<div class="caption">\r\n                        	\r\n                           	<small>Saturdays is a time for recharging. A time to gather thoughts and start turning dreams into reality.\r\nWe built Saturdays so others can have their own special place to be inspired. A place where every day feels like Saturday, inside and out – inspiring living spaces with special places for daydreaming, relaxing and just doing the things we love. \r\n</small>\r\n                        	<p>Saturdays is a place where dreams come true! </p>\r\n                            <h4>Wirachai Pranwerapaiboon</h4>\r\n                            <h5>CEO, Saturdays Residence</h5>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n         </div>\r\n    </div>\r\n  \r\n  	<div></div>\r\n    \r\n      \r\n      \r\n      \r\n    <!-- paragraphs-box Section -->\r\n    <div class="paragraphs-box" style="margin-top:50px;">\r\n        <div class="container">\r\n            <div class="row paragraphs-items">\r\n            	<div class="col-md-6">\r\n                	<div class="row paragraphs-image">\r\n                		<img src="http://www.saturdaysresidence.com/media/uploads/contents/9/smn_contents_20170209232445.jpg" class="img-responsive">\r\n                    </div>\r\n                </div>\r\n               	<div class="col-md-6">\r\n                	<div class="row paragraphs-content">\r\n                    	\r\n                            <h3>Architecture &amp; Design </h3>\r\n                            <h4>Five bespoke holiday-home</h4>\r\n                            <p>Five bespoke holiday-home concepts inspired by Bostonesque art deco, each designed to cater to a specific living style for couples and families, where every day feels like Saturday.</p>\r\n                           \r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    \r\n     <!-- room header Section -->\r\n    <div class="inner-header">\r\n        <div class="container">\r\n                <div class="row">\r\n                    <div class="inner-header-fill" style="background-image:url(''http://www.saturdaysresidence.com/media/uploads/contents/9/smn_contents_20170209232501.jpg'');">\r\n                    	<div class="caption">\r\n                        	<p>The five lifestyle concepts are Love, Let, Lux, Live and Learn.</p>\r\n                        	<small>Common interior features of the five concepts are high vaulted living space with full-length glazing framed with authentic tones of ancient teak. Extra high ceilings give owners premium space with innovative indoor and outdoor cozy corners and dens (dream spaces) dedicated to personal pursuits.</small>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n         </div>\r\n    </div>\r\n  \r\n  	<div></div>', '', '', 'Our Story', 'Our Story', 'Our Story', 2, 2, '2017-02-20 22:41:20', 'Y'),
(10, 'en', 0, 'press', 'Press', '', '', '', '', '', 'Press', 'Press', 'Press', 2, 2, '2017-02-23 11:12:39', 'Y'),
(11, 'en', 0, 'careers', 'Careers', '', 'Are you looking for a career in one of Thailand’s most reputable and forward thinking developers of contemporary lifestyle concepts?', '<h2>HOW TO APPLY</h2>\r\n<p>If you are passionate about delivering a personalized customer experience, have an eye for detail and creative flare, you could be the perfect fit for our growing team of professionals based in Phuket, Thailand.</p>\r\n<p>For more information about career opportunities with the Attitude Club email</p>\r\n<h3><i class="fa fa-file-text-o"></i> SEND US YOUR C.V To</h3>\r\n<a href="mailto:careers@theattitudeclub.com" class="email-link">careers@theattitudeclub.com</a>\r\n                    ', '', '', 'Careers', 'Careers,Job oppoptunity', 'Are you looking for a career in one of Thailand’s most reputable and forward thinking developers of contemporary lifestyle concepts?', 2, 2, '2017-02-07 07:51:49', 'Y'),
(12, 'en', 0, 'reservationpolicy', 'Residence Policy', '', '', ' <h4>Cancellation Policy</h4>\r\n<p>We require 30 days for amendment or cancellation notice prior to your scheduled arrival, otherwise you will be charged a cancellation fee of full amount deposit.</p>\r\n<h4>General Saturdays Residence Policy</h4>\r\n<ul style="list-style-type:lower-alpha;">\r\n<li>To take good care of and preserve the premise as an ordinary prudent person would do his/her own expense, unless such damage is caused by normal wear and tear, fire or other casualty ( cause other than by the negligence or misconduct of the Tenant, his/her family members or visitors ).</li>\r\n<li>Not to do or permit anyone to do anything,which may cause nuisance, annoyance or interference to the peacefulness and comfort of other Tenants in the premises, the building or its compound.</li>\r\n<li>To be responsible for the safety and appropriate conduct of all members of his/her family, visitor, personal servants and subordinates and to ensure they observe and comply with the rules and regulations.</li>\r\n<li>Not to assign any right hereunder, sub-lease, part of all the premises or allow occupancy to any third persons, whether in whole or in part without prior written consent from the Landlord.</li>\r\n<li>Not to make any alternations, additions, decoration or removal to or from the premises without the prior written consent from the Landlord. All authorization alternations, additions and decorations made to the premises, which are not permanently made or are removable without causing serious damage to the premise or any part thereof shall not become the property of the Landlord, and upon the expiration of termination of the agreement, or decoration out of the premise. In such cases, the Tenant shall be responsible for the cost of restoration of the premises to the original condition at the time of lease period</li>\r\n<li>Not to bring, keep or permit to be kept or brought on the premises, the building or any part thereof any materials of a dangerous, explosive, inflammable or obnoxious nature or the keeping of which may contravene any statutes, regulations or by-laws or in respect of which an increased rate of insurance may be required or the keeping of which cause any insurance policy in respect of the premises to become null and void</li>\r\n<li>Not to throw or permit to be throw dirt, rubbish, rages, boxes or other refuse out of the premises and in common parts of the building, except into proper bins or other container provided for such purposes as provided by the Landlord.</li>\r\n<li>Durian fruit is not allowed</li>\r\n<li>Pet is not allowed</li>\r\n</ul>\r\n                    \r\n<h4>Check in / Check Out</h4>\r\n<p>Your independence is your prerogative at Saturdays. When you arrive at the Lobby Cafe, relax with a snack before making your way to your residence. Our staff are on hand to help you settle in and we are happy to help with anything you might need during normal working hours.   </p>\r\n					\r\n                    ', '', '', 'Reservation Policy', 'Reservation Policy,cancellation policy,residence policy', 'Reservation Policy', 2, 2, '2017-02-07 08:09:48', 'Y'),
(13, 'en', 0, 'features', 'Features', '', '', '<h4>Digital Security</h4>\r\n<p>Your Saturdays keycard and digital lock system for your residence gives you peace of mind with high-grade personal security. Keep valuables and belongings safe in your residence in the complimentary safety deposit box. </p>\r\n\r\n<h4>Household Amenities</h4>\r\n<p>Bed linen, kitchen utensils, washing machine, hair dryer, and in-room ironing facilities are provided for your convenience as well as high-speed Internet access. </p>\r\n', '', '13_170223125412_features.jpg', 'Features', 'Features,apartment,kitchen,single bedroom apartment,studio apartment,internet,swimming pool', 'Your Saturdays keycard and digital lock system for your residence gives you peace of mind with high-grade personal security.', 2, 2, '2017-02-23 12:54:12', 'Y'),
(14, 'en', 0, 'terms', 'Terms & Conditions', '', '', '<h4>SH GROUP WEBSITE TERMS & CONDITIONS</h4>\r\n\r\n<p>These terms and conditions ("Terms & Conditions") apply to all websites (each a "Site") of the hotel brands owned and licensed by SH Group Operations, L.L.C. and its affiliates ("SH Group," "us," "we," "our"), including 1 Hotels and Baccarat Hotels & Resorts.</p>\r\n\r\n<p>Your use of this Site is subject to these Terms & Conditions along with any specific terms and conditions set forth on the individual pages within this Site (collectively, the "Terms & Conditions") and all applicable regulations and laws, including, but not limited to, those relating to trademark, copyright, and other intellectual property rights. Your submission of information on this Site is subject to SH Group''s<a href="http://prod.1hotels.com/storage/globalpage/privacy-policy" target="_blank">Privacy Statement</a>, which is hereby incorporated into these Terms & Conditions.</p>\r\n\r\n<p>PLEASE READ THE TERMS & CONDITIONS CAREFULLY BEFORE OBTAINING OR USING ANY MATERIALS, PRODUCTS, SERVICES OR INFORMATION THROUGH THE SITE. BY ACCESSING THE SITE, YOU AGREE TO ACCEPT, WITHOUT QUALIFICATION OR LIMITATION, ALL OF THE TERMS & CONDITIONS. We reserve the right to amend any section of the Terms & Conditions without notice by posting an amended statement on the Site. Your continued access or use of the Site means that you accept and agree to the revised Terms & Conditions. Please exit the Site immediately if you do not accept these Terms & Conditions (as amended from time to time).</p>\r\n\r\n<h4>USE OF THE SITE</h4>\r\n<h4>General</h4>\r\n<p>The services of the Site are not intended for and should not be used by minors. You may only use the services of this Site if you are at least 18 years of age and can form legally binding contracts under applicable law. Additional terms and conditions may apply to reservations, transactions, purchases, activities, or uses that may occur on or through this Site. You agree to comply with these Terms & Conditions and any applicable additional terms and conditions. Accounts / Passwords We may enable you to establish an account with a username and password to access and use the certain areas of the Site, and other services. You are responsible for preserving the confidentiality of your passwords, log-in, and account information. You will be financially accountable for all uses of the Site by you and/or by any third party using your account information. You agree to (a) immediately notify us of any unauthorized use of your password or account or any other breach of security, and (b) ensure that you exit from your account at the end of each session. It is your sole responsibility to control the dissemination and use of your password, control access to and use of your account, and notify us when you desire to cancel your account. We will not be responsible or liable for any loss or damage arising from your failure to comply with this provision.</p>\r\n\r\n<h4>Credit Card Information</h4>\r\n<p>If available, the storage of payment card information on the site is optional unless a guest opts for a pre-check in, in which case the storage of such information is mandatory. It is not required to use the Site. Saving payment card information may expedite your future reservation process. Payment card information may be maintained until (and after) the expiration date of the payment card and you will be responsible for updating any payment card information that you provide. You acknowledge that SH Group may communicate this information to facilitate reservations or as requested by you, including through applications on any device that you use to interact with SH Group.</p>\r\n\r\n<h4>Reservations</h4>\r\n<p>You may only use the Site to make reservations or purchases for yourself and your invited guests or an individual for whom you have been authorized in advance to act. Unless you have obtained prior written permission from SH Group, you may not use the Site to make reservations or purchases for other purposes, including, but not limited to, commercial objectives of reselling rooms or reservations, advertising, marketing, posting, or otherwise distributing rooms, reservations, or availability (including without limitation on third-party web sites), making speculative, false, or fraudulent reservations, or reserving rooms in anticipation of demand. Reservations made in violation of these Terms & Conditions may be cancelled without notice at SH Group''s sole discretion. SH Group reserves the right to cancel without notice reservations that transpired in violation of these Terms & Conditions. Additional terms and conditions regarding your reservation or purchases may apply.</p><h4>Promotional Information<p></p><p>\r\nThe Site may provide or display information relating to specific programs, offers, or promotions ("Promotional Offers"). Any such Promotional Offer is subject to its specific terms, conditions and restrictions. Please read carefully and refer to the terms, conditions and restrictions of each Promotional Offer. SH Group reserves the right to alter or withdraw any such Promotional Offer at any time without notice. Each Promotional Offer is void where prohibited by law.</p>\r\n\r\n</h4><h4>Links to Third Party Websites</h4>\r\n<p>The Site may contain links to third party websites. SH Group provides these links solely for your convenience. The inclusion of these links in no way indicates SH Group''s endorsement, approval, or support of such site''s content, products, services, or operators. We undertake no obligation to monitor or review any sites linked to or from the Site. Your use of any such site is subject to the terms and conditions of such sites and at your own risk and. We are not responsible for the practices, services, products, or content of third party sites. Additionally, you agree not to link your site or any other third party site to the Sites without express prior written consent of SH Group. In addition, certain actions on the Site may result in specific advertising from third parties, including information that may be displayed in other windows or in third party sites.</p>\r\n\r\n<h4>SH Group''s Right to Cancel / Errors / Mistakes</h4>\r\n<p>The Site may contain typographical, technical inaccuracies, or other errors in connection with information displayed on the Site, including, but not limited to, fees, rates, or availability applicable to your transaction. SH Group assumes no responsibility or liability for such inaccuracies, errors, or omissions. SH Group reserves the right not to honor reservations or information affected by such inaccuracies, errors, or omissions. SH Group reserves the right to make corrections, changes, cancellations, and/or improvements to such information or reservations based on such information, at any time, including after a reservation is confirmed.</p>\r\n\r\n<h4>Miscommunications / Lost Transactions</h4>\r\n<p>SH Group assumes no responsibility or liability for communication errors, difficulties, failures, or other malfunctions or lost, stolen, or misdirected transmissions, transactions, entries, or messages on or in connection with the Site. SH Group is not responsible for any incorrect information associated with any transmission or transaction to or on the Site regardless of whether such incident is the result of system error, user error, or human error.</p>\r\n\r\n<h4>Availability</h4>\r\n<p>The Site is available to anyone with Internet access. The Site may become unavailable due to maintenance or repairs or due to computer malfunctions, crashes, disruption in Internet service or other unforeseen circumstances. A reference to a product or service on the Site does not imply that such product or service is or will be available in your location. The content on the Site, including advertising content, is intended for use and display only where permissible in accordance with applicable laws and regulations. Each product or service is void where prohibited by law.</p><h4>Prohibited Uses<p></p><p>\r\nAny commercial use of this Site is strictly prohibited unless you have obtained express prior written consent from SH Group. You may not use this Site to post or transmit any unlawful, threatening, libelous, infringing, defamatory, indecent, inflammatory, obscene, pornographic or profane material or any material that could constitute or encourage conduct that would be considered a criminal offense, give rise to civil liability, or otherwise violate any law, or for any other purpose that is unlawful or prohibited by these Terms & Conditions. You agree that you will not (a) interfere or disrupt the Site, (b) use or attempt to use any robot, spider, automatic device, or automatic or manual process to monitor or copy the Site without SH Group''s prior express written consent, (c) use any device, software, or other instrument to monitor, copy, interfere with, or attempt to interfere with this Site, its content, or its operation without SH Group''s prior express written consent, or (d) use, place, or distribute any viruses, worms, time bombs, and/or other computer programming routines that damage, disrupt, intercept, or harm (or intend to do any of the preceding) the Site, including the underlying hardware, software, systems, and any users of the Site or their devices.</p></h4><h4>SH Group Disclaimer</h4><p>\r\nCertain pages on this Site and certain independent sites affiliated with SH Group or an SH Group affiliate, may contain information about specific projects associated with SH Group or its affiliates. WITH RESPECT TO SUCH PAGES AND SITES, MATERIALS ON SUCH PAGES AND SITES DO NOT CONSTITUTE AN OFFER OR SOLICITATION OF ANY KIND, NOR ARE THEY INTENDED TO BE DISTRIBUTED IN OR CONSTITUTE AN OFFER IN ANY STATE OR COUNTRY IN WHICH SH GROUP OR THE APPROPRIATE DEVELOPER OF A PROJECT, HAS NOT REGISTERED TO SELL PROPERTY, IF REQUIRED.</p>\r\n\r\n<h4>User Generated Content Terms of Service</h4>\r\n<p>By responding with #sharemy1pic, you agree to the following terms:</p>\r\n\r\n<p>You hereby grant to SH Group, L.L.C., its affiliates and related entities (collectively, "1 Hotels") a perpetual, irrevocable, royalty-free, worldwide, nonexclusive license to publish, reproduce, display, perform, distribute, adapt, edit, modify, translate, create derivative works based upon, and otherwise use and sublicense your Facebook, Instagram or Twitter photograph(s) that have been tagged with #sharemy1pic, #explore1hotels, #1hotels, #1hotel or that have otherwise been mutually agreed upon between you and 1 Hotels ("Photos"), or any portion of your Photos, in any manner in its sole discretion, including but not limited to on its webpages or in its marketing and advertising materials. You hereby represent and warrant that: (a) you own all rights in and to your Photos; (b) you have permission from any person(s) appearing in your Photos to grant the rights herein; (c) you are not a minor; and (d) 1 Hotels'' use of your Facebook, Instagram or Twitter handle or Photos will not violate the rights of any third party or any law. You hereby release, discharge and agree to hold harmless 1 Hotels'' and any person acting on 1 Hotels'' behalf from any liability related in any way to 1 Hotels'' use of your Facebook, Instagram or Twitter handle or your Photos.</p>\r\n\r\n<h4>INTELLECTUAL PROPERTY</h4>\r\n<h4>Copyright</h4>\r\n<p>Unless otherwise noted, all content on this Site is the copyrighted property of SH Group, its affiliates, or licensors. All content of this Site is protected by United States copyright laws as well as the applicable laws of other jurisdictions.</p>\r\n\r\n<h4>General / Ownership</h4>\r\n<p>Unless expressly stated otherwise, the content included on the Site, including, but not limited to text, software, photographs, graphics, images, artwork, illustrations, video, sound, music, names, logos, trademarks and service marks, are the property of SH Group or its licensors or suppliers. This content is protected by copyright, trademark and other laws. No right, title, or interest to the content is granted by your use of the Site, other than a right to review the content using a standard Internet browser to conduct customary web browsing. All other uses, including making copies of any content on the Site, are strictly prohibited. Except for non-commercial individual private use, the downloading, retransmission, or reproduction of the Site (or any part of its content) is strictly prohibited. You may not modify, reproduce, distribute, retransmit, disseminate, sell, publish, circulate or broadcast any material for any purpose other than personal, non-commercial use (or legitimate activities of a travel agent or travel professional) without SH Group''s express written consent.</p>\r\n\r\n<h4>SH Group Trademarks</h4><p>\r\nSH Group and other related marks used on the Site including, without limitation, "Baccarat" and "1 Hotels" and all derivatives thereof, are trademarks of SH Group and its affiliates. The use of the ® symbol (if used) designates marks that are registered with the U.S. Patent and Trademark Office, and such marks may also be registered with the trademark offices of certain other countries. Those marks and related names, trademarks, designs, logos, and trade dress shown on the Site are owned by SH Group (or its affiliates) and protected by the trademark laws of the United States and other jurisdictions whether or not the ® symbol is, or is not, used.</p>\r\n\r\n<h4>Materials Submitted by You</h4>\r\n<p>Unless specifically requested, we do not solicit nor do we wish to receive any confidential, secret or proprietary information or other material from you through the Site, any of its services, by email, or in any other way. Any information or material submitted by you, and which has not been specifically requested by us, such as questions, comments, searches, and suggestions, will be deemed not to be confidential, secret or proprietary. You agree that any information or materials submitted by you to the Site, whether ideas, creative concepts or other materials, in any format, including, but not limited to, writings, images, illustrations, audio recordings, and video recordings, may be used, reproduced and disclosed by us without restriction for whatever purpose we deem fit and without payment of any sum or acknowledgement of you as their source. You also warrant that any "moral rights" in posted materials have been irrevocably waived by the appropriate authors. WE SHALL HAVE NO LIABILITY FOR ANY LOSS OR DAMAGE SUFFERED BY YOU AS A RESULT OF USE OR DISCLOSURE OF SUCH MATERIALS BY US TO THE EXTENT PERMITTED BY LAW. This paragraph does not affect any rights you may have under data privacy laws that protect your personal information or similar privacy laws, to the extent that such rights cannot be excluded.</p>\r\n\r\n<h4>Notice and Procedures for Making Claims of Copyright Infringement</h4>\r\n<p>SH Group respects the intellectual property rights of third parties. SH Group responds to notices of alleged copyright infringement according to the Digital Millennium Copyright Act ("DMCA") at 17 U.S.C. § 512 et seq. Whether or not SH Group believes it is liable for any copyright infringement for which it is provided notice, SH Group''s acknowledgment may include removing or disabling access to material claimed to be the subject of infringing activity and/or terminating an individual''s access to the Site, at SH Group''s discretion and operating within the guidelines of the DMCA.</p>\r\n\r\n<p>If you believe that your work has been copied in a manner that constitutes copyright infringement, please contact: SH Group Finance Department 1140 Avenue of the Americas 5th Floor New York, NY 10036 646-595-1400 <a href="mailto:shgroupnotice@shgroup.com">shgroupnotice@shgroup.com</a></p>\r\n\r\n<p>With a copy to <a href="mailto:legalnotice@starwood.com">legalnotice@starwood.com</a>.</p>\r\n\r\n<h4>Include the following information:</h4>\r\n<p>• Your name, address, telephone number and email address;</p>\r\n<p>• A description of the allegedly infringing material and where it is located on the Site;</p>\r\n<p>• A description of the copyrighted work that you claim has been infringed;</p>\r\n<p>• A statement by you under penalty of perjury that your notice is accurate, that you are the copyright owner or that the copyright holder has authorized you to act on its behalf; and</p>\r\n<p>• A statement by you that you have a good faith belief that the disputed use is not authorized by the copyright owner, its agents, or the law;</p>\r\n<p>• Your written or electronic signature attesting to the above.</p><h4>GENERAL LEGAL TERMS</h4><h4>Right to Restrict Access</h4><p>\r\nIn addition to any other rights or remedies available to SH Group, SH Group may, without any liability whatsoever, at its sole discretion restrict or terminate your access or use of this Site at any time and without notice.\r\n</p>\r\n\r\n<h4>Limitation of Liability</h4>\r\n<p>TO THE FULLEST EXTENT PERMITTED BY LAW, SH GROUP, ITS OFFICERS, DIRECTORS, EMPLOYEES, AGENTS AND ASSIGNS, HEREBY DISCLAIM ALL LIABILITY FOR ANY LOSS, COST OR DAMAGE (DIRECT, INDIRECT, CONSEQUENTIAL, OR OTHERWISE) SUFFERED BY YOU AS A RESULT OF YOUR USE OF THE SITE OR FROM ANY COMPUTER VIRUS TRANSMITTED THROUGH THE SITE, OR OTHER SITES ACCESSED FROM THE SITE, WHETHER SUCH LOSS, COST OR DAMAGE ARISES FROM OUR NEGLIGENCE OR OTHERWISE AND EVEN IF WE ARE EXPRESSLY INFORMED OF THE POSSIBILITY OF SUCH LOSS OR DAMAGE. IN NO EVENT SHALL OUR TOTAL LIABILITY TO YOU FOR ALL DAMAGES, COSTS, LOSSES AND CAUSES OF ACTION IN THE AGGREGATE (WHETHER IN CONTRACT, TORT, INCLUDING, BUT NOT LIMITED TO, NEGLIGENCE, STRICT LIABILITY OR PRODUCT LIABILITY) AND WHETHER OR NOT WE WERE OR SHOULD HAVE BEEN AWARE OR ADVISED OR THE POSSIBILITY OF SUCH DAMAGE ARISING FROM THESE TERMS AND CONDITIONS, INCLUDING OUR PRIVACY STATEMENT, OR USE OF THE SITE EXCEED, IN THE AGGREGATE, $100.00 (US).</p>\r\n\r\n<h4>Warranty Disclaimer</h4>\r\n<p>THE SITE IS PROVIDED "AS IS," AND YOUR USE THEREOF IS AT YOUR OWN RISK. SH GROUP, ITS OFFICERS, DIRECTORS, EMPLOYEES, AGENTS AND ASSIGNS, DISCLAIM, TO THE FULLEST EXTENT PERMITTED BY LAW, ALL EXPRESS AND IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, PERFORMANCE, TITLE AND NON-INFRINGEMENT. SH GROUP DOES NOT WARRANT THAT THE SITE WILL BE FREE FROM VIRUSES, AVAILABLE, ACCESSIBLE, ERROR-FREE, UNINTERRUPTED OR THAT THE CONTENTS WILL BE ACCURATE. ALTHOUGH SH GROUP TAKES REASONABLE STEPS TO SECURE THE SITE, YOU ACKNOWLEDGE THAT THE INTERNET IS NOT A COMPLETELY SECURE MEDIUM AND SH GROUP MAKES NO WARRANTIES, EXPRESS OR IMPLIED, THAT ANY INFORMATION OR MATERIALS YOU POST ON OR TRANSMIT THROUGH THE SITE WILL BE SAFE FROM UNAUTHORIZED ACCESS OR USE. IF YOU ARE DISSATISFIED WITH THE SITE, YOUR SOLE REMEDY IS TO DISCONTINUE USING THE SITE.</p>\r\n\r\n<h4>Indemnity</h4>\r\n<p>You will indemnify SH Group and its officers, directors, agents, employees and representatives, against any loss, damage or cost incurred by us arising out of your use of the Site, any of its services or any information accessible over or through the Site, your submission or transmission of information or material on or through the Site or your violation of these Terms & Conditions or any other laws, regulations and rules. You will also indemnify against any claims that information or material that you have submitted to us is in violation of any law or in breach of any third party rights (including, but not limited to, claims in respect of defamation, invasion of privacy, breach of confidence, infringement of copyright or infringement of any other intellectual property right). We reserve the right to exclusively defend and control any claims arising from the above and any such indemnification matters and that you will fully cooperate with us in any such defenses.</p>\r\n\r\n<h4>Jurisdiction / Governing Law / Choice of Forum</h4>\r\n<p>This Site is operated and controlled by SH Group from its offices within the State of New York, United States of America. These Terms & Conditions and any dispute arising out of or related to the Terms & Conditions or use of Site shall be governed in all respects by and construed and enforced in accordance with the laws of the State of New York, U.S.A., without regard to its conflicts of law principles. Exclusive jurisdiction over any cause of action arising out of these Terms & Conditions or your use of the Site shall be in state or federal courts located in or near New York, New York, U.S.A. You further agree to submit to the exercise of personal jurisdiction of such courts for the purpose of litigating any such claim or action.</p>\r\n\r\n<h4>No Waiver</h4>\r\n<p>SH Group''s failure to enforce or insist upon strict performance of any provision of these Terms & Conditions shall not constitute a waiver of the provision. Neither a course of dealing or conduct between you and SH Group nor any trade practices shall be considered to modify these Terms & Conditions.</p><h4>Severability</h4><p>\r\nIf any part of these Terms & Conditions is unenforceable, the unenforceable part shall be construed to reflect, as nearly as possible, the original intentions of the parties. All other provisions of these Terms & Conditions shall remain in full force and effect.</p>\r\n\r\n<h4>Forward Looking Statements</h4>\r\n<p>Certain statements and assumptions in this Site contain or are based upon "forward-looking" information and are being made pursuant to the safe harbor provisions of the Private Securities Litigation Reform Act of 1995. When we use the words "will," may," "estimate," "anticipate," "should," "believe," "expect," "intend," or similar expressions, we intend to identify forward-looking statements. These statements are subject to numerous uncertainties and assumptions as described in our Annual Report on Form 10-K, Quarterly Reports on Form 10-Q and other SEC filings which could cause our actual results to differ materially from those expressed in or implied by the content of this Site. Forward looking statements made in this web site are made only as of the date of their initial publication and we undertake no obligation to publicly update any of these forward looking statements as actual events unfold.</p>\r\n\r\n<h4>Question and Comments</h4>\r\n<p>If you have any comments or questions about our privacy practices or your experience with the Site, please contact us at <a href="mailto:shgroupnotice@shgroup.com">shgroupnotice@shgroup.com</a>.</p>\r\n\r\n<p>Last Updated: August 18, 2014</p>\r\n\r\n<p>©2014 SH Group Operations, L.L.C. All Rights Reserved.</p>\r\n', '', '', 'Terms Conditions', 'Terms Conditions', 'Terms Conditions', 2, 2, '2017-02-07 08:15:56', 'Y');
INSERT INTO `pws_contents` (`id`, `lang_id`, `parent_id`, `filename`, `title`, `subtitle`, `short_description`, `description`, `description2`, `header_photo`, `meta_title`, `meta_keyword`, `meta_description`, `created_by`, `updated_by`, `updated_date`, `is_active`) VALUES
(15, 'en', 0, 'privacy', 'Privacy POLICY', '', '', '                    <h4>SH Group Privacy Statement </h4>\r\n                    <p>This Privacy Statement applies to all websites (each a "Site") of the hotel brands owned and licensed by SH Group Operations, L.L.C. and its affiliates ("SH Group," "us," "we," "our"), including 1 Hotels and Baccarat Hotels & Resorts. </p>\r\n                    <p>SH Group is committed to respecting the privacy of all users ("users," "you") of the Site. We have established and implemented this Privacy Statement to inform you of the information we collect from you on the Site, how we use your information, with whom and for what purposes we may share your information, and the choices you can make about our use of your information. </p>\r\n					<h4>Types of Personal Information We Collect </h4>\r\n                    <p>SH Group collects personal information that users choose to submit through the Site. This may include, but is not limited to: </p>\r\n\r\n<p>• Your name, gender, home and work contact details, date and place of birth, business title, nationality, passport and visa information; </p>\r\n<p>• Your payment card details, online user accounts details, profile or password details and frequent flyer or travel partner program affiliations and member numbers; </p>\r\n<p>• Information you provide in connection with our membership and loyalty programs; </p>\r\n<p>• Information you provide regarding your marketing and consumption preferences or in the course of participating in surveys, promotional offers or contests; </p>\r\n<p>• Any information necessary to fulfill special requests (e.g. preferences requiring specific accommodations, purchase of any goods and services); </p>\r\n<p>• Contact and other relevant details concerning employees of corporate accounts and vendors and other individuals with whom we do business (e.g., meeting and event planners or travel agents); and </p>\r\n<p>• Information you provide in connection with an employment application submitted online. </p><p>\r\n\r\n</p><p>From time to time, you may provide us with personal information about others (for example, individuals for whom you are making a reservation, joint travelers, or in connection with a forward-to-friend feature). In such instances, we rely on you to obtain such other persons'' consent for disclosing their information to us, which we will use in accordance with the terms of this Privacy Statement. </p>\r\n<p>You may always choose what personal information (if any) you wish to provide to us. If you choose not to provide certain details, however, some of your transactions with us may be impacted. </p>\r\n\r\n<h4>Personal Information From Children </h4>\r\n<p>SH Group does not knowingly collect personal information from individuals under 18 years of age. As a parent or legal guardian, please do not to allow your children to submit personal information without your permission. </p>\r\n\r\n<h4>How We Use Personal Information </h4>\r\n<p>SH Group may use personal information in a variety of ways, including through personal contact, through emails and correspondence, via our Sites, and through third parties, such as travel agents and other business partners. We use personal information for the purposes disclosed in this Privacy Statement, or as permitted by law. In general we use personal information in order to: </p>\r\n<p>• Provide the services you request, such as to facilitate reservations, send a confirmation, send you a pre-arrival message and provide information about the area and the hotel; </p>\r\n<p>• Provide you with information about meeting, event or celebration planning and access to specific account information for administrative purposes; </p>\r\n<p>• Assist with the planning of meetings and events; </p>\r\n<p>• Service your account and services preferences; </p>\r\n<p>• Administer our membership and loyalty programs </p>\r\n<p>• Send you periodic customer satisfaction, market research or quality assurance surveys; </p>\r\n<p>• Comply with legal and regulatory obligations; </p>\r\n<p>• Send you promotions and offers from SH Group which may be of interest to you, including: </p>\r\n<p>o Promotions and news such as your account information, exclusive offers, hotel openings; </p>\r\n<p>o Hotel specials such as last minute rates and personalized destinations deals and packages; </p>\r\n<p>o SH Group membership and loyalty programs and promotions; and </p>\r\n<p>o Partner offers, such as financial products, car hire, and club memberships </p>\r\n<p>• Protect our rights and property; </p>\r\n<p>• Learn about our markets and improve our products and services; and </p>\r\n<p> • Ship products, information or provide services to you. </p>\r\n\r\n<p>To better serve you, we may combine personal information we receive online with information you directly provide to us and information we obtain from third parties such as airline, credit card, and other partners, travel agents, event planners, information from social media sites consistent with your settings on such sites, such as social media sign-on programs, and other third-party sources. Such information may include: </p><p>\r\n</p>\r\n<p>• Guest stay information, including the hotels where you have stayed, date of arrival and departure, goods and services purchased, observations regarding your service and consumption preferences (including room, holiday preferences, amenities requested, facilities or other services used), and special requests; </p>\r\n<p>• Telephone numbers dialed, faxes sent/received or receipt of telephone messages when communicated through the telephone services we may provide guests during their stay; </p>\r\n<p>• Information collected through the use of closed circuit television systems, card key and other security systems; and </p>\r\n<p>• Additional contact information about you that we may obtain through third parties with whom we do business (such as travel agents or similar providers). For example, if we know your name and postal address, we may request your email address from your travel agent. </p>\r\n\r\n<h4>With Whom We Share Personal Information </h4>\r\n<p>We may share your personal information with affiliates and trusted third parties who assist us by providing products and services to you including, but not limited to: </p>\r\n<p>• On-property service providers, such as concierge services, spa treatments, golf or dining; </p>\r\n<p>• Co-sponsors of promotions; </p>\r\n<p>• Meeting service providers if part of a group event or meeting; </p>\r\n<p>• Business partners, such as rental car companies and airlines; and </p>\r\n<p>• Service providers that provide administration functions, reservation services, program management, communications and marketing services, advertising services, information technology services, website management, data hygiene and data analytics services billing services, and accounting and legal advice. </p>\r\n<p>We may also share your personal information with our affiliates or third parties that we feel may offer products or services that may be of interest to you. </p>\r\n<p>As we develop our business, we might sell, buy, restructure or reorganize businesses or assets. In the event of a merger, consolidation, sale, liquidation or transfer of assets, SH Group may, in its sole and absolute discretion, transfer, sell or assign information collected to one or more affiliated or unaffiliated third parties. </p>\r\nFinally, we may disclose your personally identifiable information to third parties in order to comply with law, law enforcement agencies, respond to emergencies, to protect our rights in or the safety of our Sites, or to protect the safety or rights of other users of the Sites and our customers. <p></p><p>\r\n</p>\r\n<h4>Other Information </h4>\r\n<p>When you visit and interact with the Sites, SH Group and third parties with whom SH Group has contracted to provide services to SH Group, may collect certain information (for example, the Site pages you visit, and the number of visits to our sites). We use cookies to collect this information (please see our Statement on the Use of Cookies). </p>\r\n<p>\r\nWe may also aggregate or anonymize general statistics that we gather about users, sales, traffic patterns, and services and provide these statistics to third parties; however, when we do, these statistics will not include any personal information that identifies individuals. </p>\r\n<p>\r\nBecause this information does not personally identify you, such information may be disclosed for any purpose. In some instances, we may combine this information with personal information. If we do combine this information with personal information, the combined information will be treated by us as personal information in accordance with this Privacy Statement. \r\n</p>\r\n\r\n<h4>Updating Your Information and Marketing Choices </h4>\r\n<p>\r\nIf you prefer not to receive email marketing materials from us, you may opt-out at any time by using the unsubscribe function in the email you receive from us. Opt-out requests can take up to ten business days to be effective. </p>\r\n<p>\r\nPlease note, however, that if you change the communications you receive from us as described above, we will not be able to remove your personal information from the databases of affiliates or business partners with whom we have already shared your personal information as of the date of your opt-out request. </p>\r\n<p>\r\nCalifornia Residents: Residents of California that have provided their personal information to the SH Group may request information about our disclosures of certain categories of personal information to third parties for their direct marketing purposes. Such requests must be submitted to us at one of the following addresses: <a href="mailto:shgroupnotice@shgroup.com">shgroupnotice@shgroup.com</a>. Within thirty days of receiving such a request, we will provide a list of the categories of personal information disclosed to third parties for third-party direct marketing purposes during the immediately preceding calendar year, along with the names and addresses of these third parties. This request may be made no more than once per calendar year. We reserve our right not to respond to requests submitted to addresses other than the addresses specified in this paragraph. </p>\r\n<h4>How We Respond to Do Not Track Signals </h4>\r\n<p>SH Group does not track its users over time and across third party websites to provide targeted advertising and therefore does not respond to Do Not Track (DNT) signals. Third parties that have content embedded on the Sites, such as a social feature may set cookies on a user''s browser and/or obtain information about the fact that a web browser visited a specific Site from a certain IP address. Third parties cannot collect any other personally identifiable information from the Sites unless you provide it to them directly. </p><p>\r\n</p>\r\n<h4>Links to Third Party Websites </h4>\r\n<p>The Site may contain links to third parties'' websites. Please note that we are not responsible for the collection, use, maintenance, sharing, or disclosure of data and information by such third parties. </p>\r\n<p>\r\nOther third-party websites include the landing page of the high-speed Internet providers at our hotels, as well as social media sites (such as Facebook and Twitter) on which SH Group or our properties may have accounts or fan pages where you may be able to post information and materials. </p>\r\n<p>\r\nIf you provide information on third party sites, the privacy policy and terms of service on those sites are applicable. We encourage you to read the privacy policies of websites that you visit before submitting personal information. </p>\r\n<h4>Protecting Personal Information</h4>\r\n<p>\r\nSH Group will take reasonable measures to: (i) protect personal information from unauthorized access and disclosure, and (ii) keep personal information accurate and up-to-date as appropriate. We also seek to require our affiliates and service providers with whom we share personal information to exercise reasonable efforts to maintain the confidentiality of personal information about you. For online transactions, we use reasonable technology to protect the personal information that you transmit to us via our site. Unfortunately, however, no security system or system of transmitting data over the Internet can be guaranteed to be entirely secure and we do not guarantee the security of any information you provide to SH Group through the Sites. \r\n</p>\r\n<p>\r\nFor your own privacy protection, we encourage you not to include sensitive personal information in any emails you send to us. Please do not send credit card numbers or any sensitive personal information to us via email. We will not contact you by mobile/text messaging or email to ask for your confidential personal information or credit card details. We will only ask for your confidential personal information or credit card details by telephone when you are booking a reservation by telephone. We will not contact you to ask for your account log-in information. If you receive this type of request, you should not respond to it. We also ask that you please notify us at <a href="mailto:shgroupnotice@shgroup.com">shgroupnotice@shgroup.com</a>. </p>\r\n\r\n<h4>Policy Changes </h4>\r\n<p>\r\nBy visiting this Site, you consent to the collection and use of information by us as set forth in this Privacy Statement. If we decide to make a change to this Privacy Statement, such change will be reflected herein and the last date on which the Privacy Statement was updated will be listed below. \r\n</p>\r\n<h4>Question and Comments </h4>\r\n<p>\r\nIf you have any comments or questions about our privacy practices or your experience with the Site, please contact us at <a href="mailto:shgroupnotice@shgroup.com">shgroupnotice@shgroup.com</a>. </p>\r\n<p>\r\nLast Updated: August 18, 2014 </p>\r\n<p>\r\n©2014 SH Group Operations, L.L.C. All Rights Reserved.</p>\r\n					\r\n                    ', '', '', 'Privacy', 'Privacy,privacy policy', 'Privacy', 2, 2, '2017-02-07 08:15:02', 'Y'),
(16, 'en', 0, 'contact', 'Contact', '', '', '<div style="text-align: center;">\r\n<h5>Saturdays Residence Reservation</h5>\r\n<p>136/275-276 Saiyuan-Kata Rd., Rawai Sub-district, Muang District, Phuket 83130 <br>\r\nPhone: +66 (0) 86- 4768563 / +66 (0) 92-3846532</p>\r\n\r\n<h5>Saturdays Property Sale</h5>\r\n<p>136/278 Moo 4 Rawai, Phuket 83130 Thailand<br>Phone: +66 (0) 99 149 5900</p>\r\n</div>', '', '', 'contact', 'contact', 'contact', 2, 2, '2017-02-23 20:08:41', 'Y'),
(17, 'en', 0, 'home-block-17', 'SLEEP & STAY', '', '', '<p>Relaxing and letting go of life’s stresses and distractions is essential to our physical and spiritual well being, but how often do we make the effort to detach from the material world and stop to appreciate the simple pleasures in life?</p>', '', '17_170205120559_homepage1.jpg', 'VIEW ROOMS', '', '/en/sleep/', 2, 2, '2017-02-23 09:34:00', 'Y'),
(18, 'en', 0, 'home-block-18', 'Taste', '', '', '<p>Ease into your stay when you check in at Saturdays at the spacious, naturally lit and airy atrium lounge. Meet with friends and acquaintances for a chat on comfy couches with a fabulously fresh coffee or smoothie designed to your taste.</p>', '', '18_170205120634_homepage2.jpg', 'More details', '', '/en/taste/', 2, 2, '2017-02-23 10:12:28', 'Y'),
(19, 'en', 0, 'home-block-19', 'Discover', '', '', '<p>Staying at Saturdays gets you close to nature in the secluded Saiyuan area at the south end of the island, where the pace of life is detached and relaxed with distinct rustic appeal. The surrounding hills and nearby beaches are still very naturally attractive and dotted with hidden gems. </p>', '', '19_170205120652_homepage3.jpg', '', '', '', 2, 2, '2017-02-09 11:20:59', 'Y'),
(20, 'en', 0, 'home-block-20', 'gather', '', '', '<p>For executives staying at Saturdays we have facilities to help you conduct business with comfort and convenience, including the Saturdays Meeting Room.</p>', '', '20_170223094603_gather_homepage.jpg', 'More details', '', '/en/gather/', 2, 2, '2017-02-23 09:46:20', 'Y'),
(21, 'en', 0, 'home-block-21', 'The Rooftop', '', '', '<p>Open seasonally, The Rooftop at Saturdays Residence is the perfect vantage point to take in Nai Harn skyline view. Relax in our plunge pool with a handcrafted cocktail, or warm up next to the fire pits with a glass of wine.</p>', '', '21_170223094321_rooftop.jpg', '', '', '', 2, 2, '2017-02-23 09:43:21', 'Y'),
(22, 'en', 0, 'home-block-22', 'Offers', '', '', '<p>Find your perfect retreat with seasonal experiences, unique packages and special rates at Saturdays Residence.</p>', '', '22_170223094920_offer_homepage.jpg', 'More details', '', '/en/offers/', 2, 2, '2017-02-23 09:49:20', 'Y'),
(23, 'en', 0, 'home-block-23', 'Do', '', '', '<p>Staying at Saturdays means becoming reacquainted with yourself – doing things by yourself and enjoying a healthy dose of independence with the convenience of everything you need on-hand at Saturdays or just outside your door.</p>', '', '23_170205120740_homepage7.jpg', 'More details', '', '/en/do/', 2, 2, '2017-02-23 09:51:16', 'Y'),
(24, 'en', 0, 'home-block-24', 'naiharn beach', '', '', '<p>Take time out at Nai Harn Beach in the placid, protected cove at the southern tip of the island sporting one of the best stretches of white sand in Phuket.</p>', '', '24_170223095252_naiharn.jpg', 'read more', '', '/en/field-guide/nai-harn-beach.html', 2, 2, '2017-02-23 09:53:08', 'Y'),
(25, 'en', 0, 'ownership', 'Ownership', '', 'Investing in Saturdays Residence is a great way to own and earn from your property with the backing of The Attitude Club, which as a 20-years track record in trendy property with real earning potential.', '<p>Get on board with Saturdays Residence and you get professional management team from the same award-winning that runs one of Phuket’s most popular contemporary resorts, Foto Hotel. Our dynamic team won the Basic Design City Hotel in the Thailand Boutique Awards 2014 for the boutique Blu Monkey Bed&amp;Breakfast in Phuket Town.</p>\r\n\r\n<p>The commitment of the professional property management team over the past 20 years, focus on successful investment which generate the most advantage to in the investor. The team consider in branding, as one of service design significant aspect, in order to maximise asset value that reach ti sustainable business. </p>', '', '25_170223154429_ownership_header.jpg', 'Ownership', 'ownership', 'ownership', 2, 2, '2017-02-23 15:44:29', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `pws_contents_media`
--

DROP TABLE IF EXISTS `pws_contents_media`;
CREATE TABLE IF NOT EXISTS `pws_contents_media` (
  `id` int(11) NOT NULL,
  `content_id` int(11) NOT NULL,
  `categories_id` int(11) NOT NULL,
  `title` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `button_label` varchar(50) CHARACTER SET utf8 NOT NULL,
  `button_link` text CHARACTER SET utf8 NOT NULL,
  `filename` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `is_default` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `is_flag` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `is_active` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y'
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pws_contents_media`
--

INSERT INTO `pws_contents_media` (`id`, `content_id`, `categories_id`, `title`, `button_label`, `button_link`, `filename`, `is_default`, `is_flag`, `created_by`, `created_date`, `is_active`) VALUES
(29, 1, 1, 'A Cozy Saturdays Residence, alive with vibe of island living appeal and everything you need to refresh, relax, retreat and recharge.', 'Explore rooms', '/en/sleep/', '1_170210013943_slide1.jpg', 'N', 'N', 2, '2017-02-09 12:39:43', 'Y'),
(32, 6, 1, 'Learn', '', '', '6_170210112522_img_7992-hdr.jpg', 'N', 'N', 2, '2017-02-09 22:25:22', 'Y'),
(33, 6, 1, 'Learn', '', '', '6_170210112525_img_8029-hdr.jpg', 'N', 'N', 2, '2017-02-09 22:25:25', 'Y'),
(34, 6, 1, 'Learn', '', '', '6_170210112527_img_8151-hdr.jpg', 'N', 'N', 2, '2017-02-09 22:25:27', 'Y'),
(40, 6, 1, 'Lux', '', '', '6_170210113440_dsc_0020.jpg', 'N', 'N', 2, '2017-02-09 22:34:40', 'Y'),
(41, 6, 1, 'Lux', '', '', '6_170210113440_img_8444.jpg', 'N', 'N', 2, '2017-02-09 22:34:40', 'Y'),
(42, 6, 1, 'Lux', '', '', '6_170210113441_img_8068-hdr.jpg', 'N', 'N', 2, '2017-02-09 22:34:41', 'Y'),
(44, 6, 1, 'Lux', '', '', '6_170210113441_img_8481.jpg', 'N', 'N', 2, '2017-02-09 22:34:41', 'Y'),
(46, 1, 1, 'A Cozy Saturdays Residence, alive with vibe of island living appeal and everything you need to refresh, relax, retreat and recharge.', 'Explore Rooms', '/en/sleep/', '1_170223092239_first-photo.jpg', 'N', 'N', 2, '2017-02-23 09:22:39', 'Y'),
(47, 6, 1, 'Contents Photo ID#6', '', '', '6_170223193533_4.jpg', 'N', 'N', 2, '2017-02-23 19:35:33', 'Y'),
(48, 6, 1, 'Contents Photo ID#6', '', '', '6_170223193533_3.jpg', 'N', 'N', 2, '2017-02-23 19:35:33', 'Y'),
(49, 6, 1, 'Contents Photo ID#6', '', '', '6_170223193534_2.jpg', 'N', 'N', 2, '2017-02-23 19:35:34', 'Y'),
(50, 6, 1, 'Contents Photo ID#6', '', '', '6_170223193534_9.jpg', 'N', 'N', 2, '2017-02-23 19:35:34', 'Y'),
(51, 6, 1, 'Contents Photo ID#6', '', '', '6_170223193535_10.jpg', 'N', 'N', 2, '2017-02-23 19:35:35', 'Y'),
(52, 6, 1, 'Contents Photo ID#6', '', '', '6_170223193536_5.jpg', 'N', 'N', 2, '2017-02-23 19:35:36', 'Y'),
(53, 6, 1, 'Contents Photo ID#6', '', '', '6_170223193536_6.jpg', 'N', 'N', 2, '2017-02-23 19:35:36', 'Y'),
(54, 6, 1, 'Contents Photo ID#6', '', '', '6_170223193536_8.jpg', 'N', 'N', 2, '2017-02-23 19:35:36', 'Y'),
(55, 6, 1, 'Contents Photo ID#6', '', '', '6_170223193536_13.jpg', 'N', 'N', 2, '2017-02-23 19:35:36', 'Y'),
(56, 6, 1, 'Contents Photo ID#6', '', '', '6_170223193537_11.jpg', 'N', 'N', 2, '2017-02-23 19:35:37', 'Y'),
(57, 6, 1, 'Contents Photo ID#6', '', '', '6_170223193538_14.jpg', 'N', 'N', 2, '2017-02-23 19:35:38', 'Y'),
(58, 6, 1, 'Contents Photo ID#6', '', '', '6_170223193538_12.jpg', 'N', 'N', 2, '2017-02-23 19:35:38', 'Y'),
(59, 6, 1, 'Contents Photo ID#6', '', '', '6_170223193805_15.jpg', 'N', 'N', 2, '2017-02-23 19:38:05', 'Y'),
(60, 1, 1, 'A Cozy Saturdays Residence, alive with vibe of island living appeal and everything you need to refresh, relax, retreat and recharge.', 'Explore rooms', '/en/sleep/', '1_170223202837_pool.jpg', 'N', 'N', 2, '2017-02-23 20:28:37', 'Y'),
(61, 1, 1, 'A Cozy Saturdays Residence, alive with vibe of island living appeal and everything you need to refresh, relax, retreat and recharge.', 'Explore rooms', '/en/sleep/', '1_170223202837_library.jpg', 'N', 'N', 2, '2017-02-23 20:28:37', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `pws_do`
--

DROP TABLE IF EXISTS `pws_do`;
CREATE TABLE IF NOT EXISTS `pws_do` (
  `id` int(11) NOT NULL,
  `lang_id` varchar(3) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(100) NOT NULL,
  `description` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(200) NOT NULL,
  `link_caption` varchar(100) NOT NULL,
  `filename` varchar(200) NOT NULL,
  `meta_title` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `meta_keyword` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `meta_description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `sort` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_date` datetime NOT NULL,
  `flag_homepage` enum('Y','N') NOT NULL DEFAULT 'N',
  `is_active` enum('Y','N') NOT NULL DEFAULT 'Y'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pws_do`
--

INSERT INTO `pws_do` (`id`, `lang_id`, `parent_id`, `name`, `description`, `link`, `link_caption`, `filename`, `meta_title`, `meta_keyword`, `meta_description`, `sort`, `created_by`, `updated_by`, `updated_date`, `flag_homepage`, `is_active`) VALUES
(1, 'en', 0, 'Swim', '<p><span style="font-style:italic">“Swimming is simply moving meditation.”</span><br>\r\n<small>– Cesar Nikko Caharian</small></p>\r\n\r\n<p>Saturdays residences are centered around two beautifully arranged almost full-length Olympic-sized swimming pools (24m and 25m) and saunas so you can make swimming part of your daily exercise regime.  Add steam therapy to your health regime as well with free use of Saturdays authentic Scandinavian-style timber saunas situated in the separate male and female poolside changing rooms.\r\n</p>', 'http://www.google.com', 'see detail', '1_170223102740_swim.jpg', 'The thing have to do No. 1', 'The thing have to do No. 1', 'The thing have to do No. 1', 0, 1, 1, '2017-02-23 10:27:40', 'N', 'Y'),
(2, 'en', 0, 'Read', '<p><span style="font-style:italic">“I took a deep breath and listened to the old bray of my heart. I am, I am, I am.”</span><br>\r\n<small>– Sylvia Plath, The Bell Jar</small></p>\r\n\r\n<p>Inspiration often comes when we least expect it, but we need peace and quiet with time to reflect in a space conducive to creativity. Lose yourself in a book at Saturdays Library, perched in a quiet spot at the end of the swimming pool and stocked with inspiring publications.</p>', 'http://www.google.com', 'see detail', '2_170208220031_read.jpg', 'The thing have to do No. 1', 'The thing have to do No. 1', 'The thing have to do No. 1', 0, 1, 1, '2017-02-08 09:03:07', 'N', 'Y'),
(3, 'en', 0, 'Bike', '<p><span style="font-style:italic">“Nothing compares to the simple pleasure of riding a bike” </span><br>\r\n<small>– John F Kennedy</small></p>\r\n\r\n<p>There are many undiscovered spots off the beaten track to explore in the countryside surrounding Saturdays that can only be reached on two wheels. Borrow a Saturdays mountain bike and take a leisurely ride along the winding lanes and forest trails, or test your steel with laps around Nai Harn Lake.</p>', '', '', '3_170223103146_bike.jpg', 'The thing have to do No. 3', 'The thing have to do No. 3', 'The thing have to do No. 3', 0, 1, 1, '2017-02-23 10:31:46', 'N', 'Y'),
(4, 'en', 0, 'Cook', '<p><span style="font-style:italic">“Cooking is at once child''s play and adult joy. And cooking done with care is an act of love.”</span><br>\r\n<small>– Craig Claiborne, New York Times food critic</small></p>\r\n\r\n<p>Taking the time to prepare our own cuisine with loving care is a therapy that can be enjoyed alone or as a family affair in your spacious, smartly designed Saturdays kitchen – fitted with a halogen hob, a large vintage-style refrigerator and ample storage and prep surfaces. \r\nAnd when you’re done, take your time indulging in your personalized Saturdays dining experience.</p>\r\n', 'http://www.google.com', 'Do 4', '4_170223103022_cook.jpg', 'Do 4', 'Do 4', 'Do 4', 0, 1, 1, '2017-02-23 10:30:22', 'N', 'Y'),
(6, 'en', 0, 'Gym', '<p>Recharge 24/7. The Field House is fully equipped with stationary bicycles, resistance treadmills, kettlebells, and yoga mats. Refresh with filtered water and organic cotton towels.<br></p>', '', '', '6_170223112231_gym.jpg', '', '', '', 0, 2, 2, '2017-02-23 11:22:31', 'N', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `pws_fieldguide`
--

DROP TABLE IF EXISTS `pws_fieldguide`;
CREATE TABLE IF NOT EXISTS `pws_fieldguide` (
  `id` int(11) NOT NULL,
  `lang_id` varchar(2) NOT NULL DEFAULT 'en',
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `filename` varchar(100) NOT NULL,
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `subtitle` varchar(100) NOT NULL,
  `short_description` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `header_photo` varchar(255) NOT NULL,
  `meta_title` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `meta_keyword` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `meta_description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_date` datetime NOT NULL,
  `is_active` enum('Y','N') NOT NULL DEFAULT 'Y'
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pws_fieldguide`
--

INSERT INTO `pws_fieldguide` (`id`, `lang_id`, `parent_id`, `filename`, `title`, `subtitle`, `short_description`, `description`, `header_photo`, `meta_title`, `meta_keyword`, `meta_description`, `created_by`, `updated_by`, `updated_date`, `is_active`) VALUES
(1, 'en', 0, 'nai-harn-beach', 'Nai Harn Beach', 'barboon', 'barboon', '<p>Take time out at Nai Harn Beach in the placid, protected cove at the southern tip of the island sporting one of the best stretches of white sand in Phuket. The beauty spot is just a short bike ride or drive from Saturdays and the shallow, clear and calm water in the bay is a perfect place for the family to splash and lounge about.</p>\r\n<p><img src="/timthumb.php?src=http://www.saturdaysresidence.com/media/uploads/fieldguide/1/1_170210052324_naiharn.jpg&amp;w=800&amp;h=550" class="img-responsive"></p>\r\n\r\n<p>Nai Harn’s Buddhist temple often holds traditional temple fairs, where you can have your fortune told, ride the Ferris wheel and snack at the exotic street food stalls.</p>\r\n\r\n<p>Nai Harn Lake, just off the beach, is a lovely spot for early morning and evening jogs and bike rides. </p>\r\n\r\n<p>Make your way to the top of Promthep Cape before dusk to witness one of the world’s most stunning sunsets.</p>\r\n', '1_170223095644_naiharn.jpg', 'Nai Harn Beach', 'nai harn,beach', 'Take time out at Nai Harn Beach in the placid, protected cove at the southern tip of the island sporting one of the best stretches of white sand in Phuket.', 2, 2, '2017-02-23 09:58:19', 'Y'),
(2, 'en', 0, 'island-hopping', 'Island Hopping', 'g ege eg', 'fbd erger', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi commodo vel diam eu volutpat. Praesent id mi et nisl mollis dictum ac in leo. Integer tempor nulla id consequat ultrices. In sed dignissim augue, eget condimentum sem. Nulla ut orci eget justo tempor egestas. Curabitur a porta elit. </p>', '2_170208225319_island_hopping.jpg', 'Island Hopping', 'Island Hopping, chalong pier', 'Chalong Pier, just a short distance from Saturdays, is the set off point for boat trips to nearby islands', 2, 2, '2017-02-08 09:53:19', 'Y'),
(3, 'en', 0, 'saiyuan', 'Saiyuan', 'Topic 33333', 'Topic 33333', '<p>Staying at Saturdays gets you close to nature in the secluded Saiyuan area at the south end of the island, where the pace of life is detached and relaxed with distinct rustic appeal. The surrounding hills and nearby beaches are still very naturally attractive and dotted with hidden gems such as the Big Buddha, art villages, wellness retreats, local and international restaurants, bars and snack shacks.</p>', '3_170208224859_saiyuan.jpg', 'Saiyuan', 'Saiyuan', 'Staying at Saturdays gets you close to nature in the secluded Saiyuan area at the south end of the island', 2, 2, '2017-02-08 09:49:17', 'Y'),
(4, 'en', 0, 'seafood-market', 'Seafood Market', '', '', '<p>Part of the fun staying at Saturdays is having the independence to cook for yourself and have the space to dine and entertain in your own residence. Find the freshest seafood for your home cooking at the Rawai seafood market next to Chalong Pier, where the catch of the day is as fresh as it gets and very affordable. </p>', '4_170208225844_rawai_seafood_market.jpg', 'Seafood Market', 'Seafood Market', 'Find the freshest seafood for your home cooking at the Rawai seafood market next to Chalong Pier', 2, 2, '2017-02-08 09:58:44', 'Y'),
(5, 'en', 0, 'thai-boxing', 'Thai Boxing', '', '', '<p>Many fitness fanatics and mixed martial arts practitioners from around the world come to Phuket to add Thai Boxing to their repertoire. There are a number of Thai Boxing gyms situated in Rawai and Chalong, near Saturdays, which are run by former international champions and train some of the world’s top athletes in this arena, but beginners are always welcome too. </p>', '5_170208230210_thai_boxing.jpg', 'Thai Boxing', 'thai boxing', 'Many fitness fanatics and mixed martial arts practitioners from around the world come to Phuket to add Thai Boxing to their repertoire.', 2, 2, '2017-02-08 10:02:10', 'Y'),
(6, 'en', 0, 'fitness', 'Fitness & Weight Loss, Well being', '', '', '<p>The tranquil tropical surroundings and ancient health and well-being practices infused in the local culture are in abundance at various spas and retreats near Saturdays, where you can indulge in traditional Thai massage, yoga, and meditation courses to balance the mind, body and spirit. Saturdays staff can help you decide on the best local option for you.</p>\r\n<p><img src="http://www.saturdaysresidence.com/media/uploads/fieldguide/6/6_170208230522_fitness.jpg" class="img-responsive"></p>', '6_170223100208_wellness.jpg', 'Fitness & Weight Loss, Well being', 'fitness,weight loss,well being', 'The tranquil tropical surroundings and ancient health and well-being practices infused in the local culture are in abundance at various spas and retreats near Saturdays', 2, 2, '2017-02-23 10:02:53', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `pws_fieldguide_media`
--

DROP TABLE IF EXISTS `pws_fieldguide_media`;
CREATE TABLE IF NOT EXISTS `pws_fieldguide_media` (
  `id` int(11) NOT NULL,
  `fieldguide_id` int(11) NOT NULL,
  `categories_id` int(11) NOT NULL,
  `title` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `filename` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `is_default` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `is_flag` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `is_active` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pws_fieldguide_media`
--

INSERT INTO `pws_fieldguide_media` (`id`, `fieldguide_id`, `categories_id`, `title`, `filename`, `is_default`, `is_flag`, `created_by`, `created_date`, `is_active`) VALUES
(1, 3, 1, 'Fieldguide Photo ID#3', '3_170121154435_mac_os_x_fluid_colors-wide.jpg', 'N', 'N', 1, '2017-01-21 15:44:35', 'Y'),
(2, 3, 1, 'Fieldguide Photo ID#3', '3_170121154435_wdf_2345936.jpg', 'N', 'N', 1, '2017-01-21 15:44:35', 'Y'),
(3, 3, 1, 'Fieldguide Photo ID#3', '3_170121154435_matrix_binary-wide.jpg', 'N', 'N', 1, '2017-01-21 15:44:35', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `pws_gather`
--

DROP TABLE IF EXISTS `pws_gather`;
CREATE TABLE IF NOT EXISTS `pws_gather` (
  `id` int(11) NOT NULL,
  `lang_id` varchar(3) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(100) NOT NULL,
  `description` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(200) NOT NULL,
  `link_caption` varchar(100) NOT NULL,
  `filename` varchar(200) NOT NULL,
  `meta_title` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `meta_keyword` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `meta_description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `sort` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_date` datetime NOT NULL,
  `flag_homepage` enum('Y','N') NOT NULL DEFAULT 'N',
  `is_active` enum('Y','N') NOT NULL DEFAULT 'Y'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pws_gather`
--

INSERT INTO `pws_gather` (`id`, `lang_id`, `parent_id`, `name`, `description`, `link`, `link_caption`, `filename`, `meta_title`, `meta_keyword`, `meta_description`, `sort`, `created_by`, `updated_by`, `updated_date`, `flag_homepage`, `is_active`) VALUES
(1, 'en', 0, 'Meetings', '<p>Book the right space for your work and wallet. Turnkey event planning makes it easy to add elements like digital guides and theme-based modules for mindful meetings. Play your content in tech-ready meeting rooms. <br></p>', 'www.Gather.com', 'see Gather', '1_170223103652_meeting.jpg', 'The thing have to Gather No. 1', 'The thing have to do No. 1', 'The thing have to Gather No. 1', 0, 1, 1, '2017-02-23 10:36:52', 'N', 'Y'),
(2, 'en', 0, 'catering', '<p>Make your event farm-stand fresh. The culinary team can create food and drink options unique to your event and needs and taste.</p>', 'www.do.com', 'see detail', '2_170208222919_catering.jpg', 'The thing have to do No. 1', 'The thing have to do No. 1', 'The thing have to do No. 1', 0, 1, 1, '2017-02-08 09:29:19', 'N', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `pws_languages`
--

DROP TABLE IF EXISTS `pws_languages`;
CREATE TABLE IF NOT EXISTS `pws_languages` (
  `id` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `icon` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `default` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `is_active` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pws_languages`
--

INSERT INTO `pws_languages` (`id`, `name`, `icon`, `default`, `is_active`) VALUES
('en', 'English', 'gb', 'Y', 'Y'),
('th', 'Thai', 'th', 'N', 'N');

-- --------------------------------------------------------

--
-- Table structure for table `pws_media`
--

DROP TABLE IF EXISTS `pws_media`;
CREATE TABLE IF NOT EXISTS `pws_media` (
  `id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `categories_id` int(11) NOT NULL,
  `title` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `filename` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `is_default` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `is_flag` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `is_active` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pws_menus`
--

DROP TABLE IF EXISTS `pws_menus`;
CREATE TABLE IF NOT EXISTS `pws_menus` (
  `id` int(11) NOT NULL,
  `lang_id` varchar(3) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(100) NOT NULL,
  `link` varchar(200) NOT NULL,
  `top_nav` enum('Y','N') NOT NULL DEFAULT 'Y',
  `foot_nav` enum('Y','N') NOT NULL DEFAULT 'Y',
  `icon` varchar(100) NOT NULL,
  `file_icon` varchar(200) NOT NULL,
  `sort` int(11) NOT NULL,
  `is_active` enum('Y','N') DEFAULT 'Y'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pws_modules`
--

DROP TABLE IF EXISTS `pws_modules`;
CREATE TABLE IF NOT EXISTS `pws_modules` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `title` varchar(100) NOT NULL,
  `icon` varchar(100) NOT NULL,
  `file_icon` varchar(200) NOT NULL,
  `sort` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `is_active` enum('Y','N') DEFAULT 'Y'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pws_offers`
--

DROP TABLE IF EXISTS `pws_offers`;
CREATE TABLE IF NOT EXISTS `pws_offers` (
  `id` int(11) NOT NULL,
  `lang_id` varchar(3) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(100) NOT NULL,
  `description` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(200) NOT NULL,
  `link_caption` varchar(100) NOT NULL,
  `filename` varchar(200) NOT NULL,
  `meta_title` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `meta_keyword` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `meta_description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `sort` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_date` datetime NOT NULL,
  `flag_homepage` enum('Y','N') NOT NULL DEFAULT 'N',
  `is_active` enum('Y','N') NOT NULL DEFAULT 'Y'
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pws_press`
--

DROP TABLE IF EXISTS `pws_press`;
CREATE TABLE IF NOT EXISTS `pws_press` (
  `id` int(11) NOT NULL,
  `lang_id` varchar(3) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(100) NOT NULL,
  `description` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `filename` varchar(200) NOT NULL,
  `photoname` text NOT NULL,
  `meta_title` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `meta_keyword` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `meta_description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `sort` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_date` datetime NOT NULL,
  `is_active` enum('Y','N') NOT NULL DEFAULT 'Y'
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pws_press`
--

INSERT INTO `pws_press` (`id`, `lang_id`, `parent_id`, `name`, `description`, `filename`, `photoname`, `meta_title`, `meta_keyword`, `meta_description`, `sort`, `created_by`, `updated_by`, `updated_date`, `is_active`) VALUES
(5, 'en', 0, 'Saturdays Condo Launches Rider Safety Campaign in Rawai', '<div align="center"><br></div><p>Local developer aims for ''100% awareness'' in road safety campaign targeting young motorists with crash helmet giveaway.</p>\r\n\r\n<p>Directors and staff from Saturdays Condo in Phuket showed their support for new road safety awareness campaign, ''100% Riders Safety'', by giving away 200 crash helmets to secondary school and college students in Rawai district on Sunday (Sept 7). </p>\r\n\r\n<p>The helmet giveaway aims to encourage young riders and passengers to always wear crash helmets when travelling by motorbike or bicycle.</p>\r\n\r\n<p>Ms Kanokpan Pranveerapaiboon, Real Estate Director of The Attitude Club, which operates Saturdays Condo in Saiyuan, said the goal was to promote road safety awareness and reduce the number of fatalities among young riders in the Rawai area. </p>\r\n\r\n<center><iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2FSaturdaysCondo%2Fvideos%2F1554458578127422%2F&amp;width=500&amp;show_text=false&amp;height=280&amp;appId" style="border:none;overflow:hidden" scrolling="no" allowtransparency="true" height="280" frameborder="0" width="500"></iframe></center>\r\n\r\n<p>“Young motorbike riders are most at risk of being in a serious accident. Our goal is to encourage young riders to be safety conscious by always wearing a helmet when riding motorbikes and bicycles, even if just for short distances,” said Ms Kanokpan.</p>\r\n\r\n<p>“A high percentage of young riders in motorbike accidents suffer from serious head injuries, resulting in brain damage, disabilities and sometimes death. As well as preventing uneccesary loss of life, we hope that students weraing the helmets will encourage safety awareness in all riders,” she said.</p>\r\n', 'saturdays-condo-launches-rider-safety-campaign-in-rawai', '5_170207213207_new1.jpg', 'Saturdays Condo Launches Rider Safety Campaign in Rawai', 'Saturdays, Saturdays Condo', 'Local developer aims for &#39;100% awareness&#39; in road safety campaign targeting young motorists with crash helmet giveaway.', 0, 2, 2, '2017-02-23 15:08:13', 'Y'),
(11, 'en', 0, 'Thailand Property Awards 2016 สุดยอดนักพัฒนาอสังหาฯไทย', '<p>Saturdays Residence Phuket by The Attitude Club คว้ารางวัลโครงการอสังหาริมทรัพย์และผู้พัฒนา ( Highly Commended : Best Condo Development ) สุดยอดนักพัฒนาอสังหาริมทรัพย์ไทย จัดโดย Property Guru ด้วย 3 เหตุผลหลัก ได้แก่ งานดีไซน์การตกแต่ง คุณภาพงาน และความคุ้มค่าด้านการลงทุน งานนี้นำทีมรับรางวัลโดย คุณวีระชัย ปรานวีระไพบูลย์ ประธานกรรมการบริหาร ,คุณกนกพรรณ ปรานวีระไพบูลย์ ,คุณพลภัทร จันทร์วิเมลือง ,คุณปุญญาพร ธนัชชวัล และทีมงาน เมื่อวันที่ 22 กันยายน 2559 ที่ผ่านมา ณ โรงแรมพลาซ่า แอทธินี กรุงเทพฯ อะ รอยัล เมอริเดียน</p><p><img style="width: 800px;" src="http://www.saturdaysresidence.com/media/uploads/press/11/smn_press_20170223144947.jpg"><br></p>\r\n\r\n\r\n<p>ขอบพระคุณลูกค้าทุกท่านที่ให้การสนับสนุน และร่วมสร้างแรงบันดาลใจที่ยิ่งใหญ่ด้วยกันมาตลอด\r\nสนใจสอบถามรายละเอียด โทร 099 1495900 / FB : SATURDAYS PHUKET</p>\r\n\r\n\r\n\r\n', 'thailand-property-awards-2016', '11_170207220443_new2_1.jpg', 'Thailand Property Awards 2016', 'Thailand Property,Saturdays Residence', 'Saturdays Residence Phuket by The Attitude Club คว้ารางวัลโครงการอสังหาริมทรัพย์และผู้พัฒนา ( Highly Commended : Best Condo Development )', 0, 2, 2, '2017-02-23 14:49:57', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `pws_sleep`
--

DROP TABLE IF EXISTS `pws_sleep`;
CREATE TABLE IF NOT EXISTS `pws_sleep` (
  `id` int(11) NOT NULL,
  `lang_id` varchar(3) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(100) NOT NULL,
  `room_type` varchar(150) NOT NULL,
  `ft_roomtype` varchar(50) NOT NULL,
  `ft_view` varchar(50) NOT NULL,
  `ft_bedrooms` varchar(50) NOT NULL,
  `short_description` text NOT NULL,
  `description` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `filename` varchar(200) NOT NULL,
  `photoname` text NOT NULL,
  `meta_title` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `meta_keyword` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `meta_description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `sort` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_date` datetime NOT NULL,
  `flag_homepage` enum('Y','N') NOT NULL DEFAULT 'N',
  `is_active` enum('Y','N') NOT NULL DEFAULT 'Y'
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pws_sleep`
--

INSERT INTO `pws_sleep` (`id`, `lang_id`, `parent_id`, `name`, `room_type`, `ft_roomtype`, `ft_view`, `ft_bedrooms`, `short_description`, `description`, `filename`, `photoname`, `meta_title`, `meta_keyword`, `meta_description`, `sort`, `created_by`, `updated_by`, `updated_date`, `flag_homepage`, `is_active`) VALUES
(6, 'en', 0, 'Live Pool View', '1 Bedroom Suite Pool View', 'live', 'pool_view', '1_bedrooms', '<p>Terrific for two, the single-bedroom studio apartments covering 57sqm and overlooking the Saturdays swimming pools, comprise an expansive living space with a kitchen and breakfast bar, a bedroom with an en-suite bathroom plus a private hobby space and an open-air terrace accessible from the bedroom and living area.</p>\r\n					\r\n<div class="row global-icon-wrapper">\r\n<span class="col-sm-3 col-xs-6"><i class="global-icons icon-pool"></i>Pool</span>\r\n<span class="col-sm-3 col-xs-6"><i class="global-icons icon-wifi"></i>Public Wifi</span>\r\n<span class="col-sm-3 col-xs-6"><i class="global-icons icon-kingbed"></i>King bed</span>\r\n<span class="col-sm-3 col-xs-6"><i class="global-icons icon-living"></i>Living area</span>\r\n</div>', '  \r\n     <!-- hotel-policy Section -->\r\n    <div class="inroom-amenties">\r\n        <div class="container">\r\n            <div class="row inroom-amenties-box">\r\n            	<h3><span>In-Room Amenities</span></h3>\r\n            	<div class="row global-icon-wrapper">\r\n                        <span class="col-md-2 col-sm-3 col-xs-6"><i class="global-icons icon-tv"></i>lCD TV</span>\r\n                        <span class="col-md-2 col-sm-3 col-xs-6"><i class="global-icons icon-hairdryer"></i>HAIR DRYER</span>\r\n                        <span class="col-md-2 col-sm-3 col-xs-6"><i class="global-icons icon-coffee-machine"></i>COFFEE MAKER MACHINE</span>\r\n                        <span class="col-md-2 col-sm-3 col-xs-6"><i class="global-icons icon-kitchenaid"></i>KITCHENAID</span>\r\n                         <span class="col-md-2 col-sm-3 col-xs-6"><i class="global-icons icon-fridge"></i>REFRIDGERATOR</span>\r\n                        <span class="col-md-2 col-sm-3 col-xs-6"><i class="global-icons icon-air-conditioner"></i>AIR CONDITIONER</span>\r\n                        <span class="col-md-2 col-sm-3 col-xs-6"><i class="global-icons icon-bath-tub"></i>bath tub</span>\r\n                        <span class="col-md-2 col-sm-3 col-xs-6"><i class="global-icons icon-bath-towels"></i>bath towels</span>\r\n                        <span class="col-md-2 col-sm-3 col-xs-6"><i class="global-icons icon-gym"></i>Fitness</span>\r\n                        <span class="col-md-2 col-sm-3 col-xs-6"><i class="global-icons icon-concierge"></i>Concierge</span>\r\n\r\n\r\n\r\n                </div>\r\n     		</div>\r\n        </div>\r\n    </div>\r\n\r\n', 'live-pool-view', '6_170208194508_live_pool_view.jpg', 'Live Pool View', 'live,Live Pool View', 'The single-bedroom studio apartments covering 57sqm and overlooking the Saturdays swimming pools.', 0, 2, 2, '2017-02-23 12:02:31', 'N', 'Y'),
(7, 'en', 0, 'Live Pool Access', '1 Bedroom Suite Pool Access', 'live', 'pool_access', '1_bedrooms', '<p>Plunge into the Olympic-size (24m and 25m) swimming pool right outside your ground-floor single bedroom 57sqm apartment, which opens out onto the wooden pool deck. Inside there is an expansive living space with a kitchen and breakfast bar, a bedroom with an en-suite bathroom plus a private hobby space and an open-air terrace accessible from the bedroom and living area.\r\n</p>\r\n					\r\n<div class="row global-icon-wrapper">\r\n                        <span class="col-sm-3 col-xs-6"><i class="global-icons icon-pool"></i>Pool</span>\r\n                        <span class="col-sm-3 col-xs-6"><i class="global-icons icon-wifi"></i>Free Wifi</span>\r\n                        <span class="col-sm-3 col-xs-6"><i class="global-icons icon-kingbed"></i>King bed</span>\r\n                        <span class="col-sm-3 col-xs-6"><i class="global-icons icon-living"></i>Living area</span>\r\n                    </div>', '  \r\n     <!-- hotel-policy Section -->\r\n    <div class="inroom-amenties">\r\n        <div class="container">\r\n            <div class="row inroom-amenties-box">\r\n            	<h3><span>In-Room Amenities</span></h3>\r\n            	<div class="row global-icon-wrapper">\r\n                        <span class="col-md-2 col-sm-3 col-xs-6"><i class="global-icons icon-tv"></i>lCD TV</span>\r\n                        <span class="col-md-2 col-sm-3 col-xs-6"><i class="global-icons icon-hairdryer"></i>HAIR DRYER</span>\r\n                        <span class="col-md-2 col-sm-3 col-xs-6"><i class="global-icons icon-coffee-machine"></i>COFFEE MAKER MACHINE</span>\r\n                        <span class="col-md-2 col-sm-3 col-xs-6"><i class="global-icons icon-kitchenaid"></i>KITCHENAID</span>\r\n                         <span class="col-md-2 col-sm-3 col-xs-6"><i class="global-icons icon-fridge"></i>REFRIDGERATOR</span>\r\n                        <span class="col-md-2 col-sm-3 col-xs-6"><i class="global-icons icon-air-conditioner"></i>AIR CONDITIONER</span>\r\n                        <span class="col-md-2 col-sm-3 col-xs-6"><i class="global-icons icon-bath-tub"></i>bath tub</span>\r\n                        <span class="col-md-2 col-sm-3 col-xs-6"><i class="global-icons icon-bath-towels"></i>bath towels</span>\r\n                        <span class="col-md-2 col-sm-3 col-xs-6"><i class="global-icons icon-gym"></i>Fitness</span>\r\n                        <span class="col-md-2 col-sm-3 col-xs-6"><i class="global-icons icon-concierge"></i>Concierge</span>\r\n\r\n\r\n\r\n                </div>\r\n     		</div>\r\n        </div>\r\n    </div>\r\n\r\n', 'live-pool-access', '7_170208195819_live_pool_access.jpg', 'Live Pool Access', 'live,live pool access,pool access, 1 bedroom suite', 'Plunge into the Olympic-size (24m and 25m) swimming pool right outside your ground-floor single bedroom 57sqm apartment', 0, 2, 2, '2017-02-23 12:42:39', 'N', 'Y'),
(8, 'en', 0, 'Learn Pool View', '2 Bedrooms Suite Pool View', 'learn', 'pool_view', '2_bedrooms', '<p>Spend quality time with your family, doing things together in the large living area with an open kitchen, or retreat into the private sunlight-filled ‘learn’ space to study, work or reflect in the two-bedroom 84sqm apartments. You can find peace and quiet in the open air, too, from the the bedroom terraces overlooking the swimming pools.</p>\r\n					\r\n                    \r\n                     <div class="row global-icon-wrapper">\r\n                        <span class="col-sm-3 col-xs-6"><i class="global-icons icon-pool"></i>Pool</span>\r\n                        <span class="col-sm-3 col-xs-6"><i class="global-icons icon-wifi"></i>Public Wifi</span>\r\n                        <span class="col-sm-3 col-xs-6"><i class="global-icons icon-kingbed"></i>King bed</span>\r\n                        <span class="col-sm-3 col-xs-6"><i class="global-icons icon-living"></i>Living area</span>\r\n                    </div>', '  \r\n     <!-- hotel-policy Section -->\r\n    <div class="inroom-amenties">\r\n        <div class="container">\r\n            <div class="row inroom-amenties-box">\r\n            	<h3><span>In-Room Amenities</span></h3>\r\n            	<div class="row global-icon-wrapper">\r\n                        <span class="col-md-2 col-sm-3 col-xs-6"><i class="global-icons icon-tv"></i>lCD TV</span>\r\n                        <span class="col-md-2 col-sm-3 col-xs-6"><i class="global-icons icon-hairdryer"></i>HAIR DRYER</span>\r\n                        <span class="col-md-2 col-sm-3 col-xs-6"><i class="global-icons icon-coffee-machine"></i>COFFEE MAKER MACHINE</span>\r\n                        <span class="col-md-2 col-sm-3 col-xs-6"><i class="global-icons icon-kitchenaid"></i>KITCHENAID</span>\r\n                         <span class="col-md-2 col-sm-3 col-xs-6"><i class="global-icons icon-fridge"></i>REFRIDGERATOR</span>\r\n                        <span class="col-md-2 col-sm-3 col-xs-6"><i class="global-icons icon-air-conditioner"></i>AIR CONDITIONER</span>\r\n                        <span class="col-md-2 col-sm-3 col-xs-6"><i class="global-icons icon-bath-tub"></i>bath tub</span>\r\n                        <span class="col-md-2 col-sm-3 col-xs-6"><i class="global-icons icon-bath-towels"></i>bath towels</span>\r\n                        <span class="col-md-2 col-sm-3 col-xs-6"><i class="global-icons icon-gym"></i>Fitness</span>\r\n                        <span class="col-md-2 col-sm-3 col-xs-6"><i class="global-icons icon-concierge"></i>Concierge</span>\r\n\r\n\r\n\r\n                </div>\r\n     		</div>\r\n        </div>\r\n    </div>\r\n\r\n', 'learn-pool-view', '8_170208203614_learn_pool_view.jpg', 'Learn Pool View', 'learn,pool view,2 bedrooms', 'Spend quality time with your family, doing things together in the large living area with an open kitchen', 0, 2, 2, '2017-02-23 12:13:46', 'N', 'Y'),
(9, 'en', 0, 'Learn Pool Access', '2 Bedrooms Suite Pool Access', 'learn', 'pool_access', '2_bedrooms', '<p>Keep the kids entertained all day with direct access to the swimming pool from these large two-bedroom arrangements (84sqm) with an open kitchen and breakfast bar and two bathrooms on the ground floor plus an extra private study space inside.</p>\r\n					\r\n                    \r\n                     <div class="row global-icon-wrapper">\r\n                        <span class="col-sm-3 col-xs-6"><i class="global-icons icon-pool"></i>Pool</span>\r\n                        <span class="col-sm-3 col-xs-6"><i class="global-icons icon-wifi"></i>Public Wifi</span>\r\n                        <span class="col-sm-3 col-xs-6"><i class="global-icons icon-kingbed"></i>King bed</span>\r\n                        <span class="col-sm-3 col-xs-6"><i class="global-icons icon-living"></i>Living area</span>\r\n                    </div>', '  \r\n     <!-- hotel-policy Section -->\r\n    <div class="inroom-amenties">\r\n        <div class="container">\r\n            <div class="row inroom-amenties-box">\r\n            	<h3><span>In-Room Amenities</span></h3>\r\n            	<div class="row global-icon-wrapper">\r\n                        <span class="col-md-2 col-sm-3 col-xs-6"><i class="global-icons icon-tv"></i>lCD TV</span>\r\n                        <span class="col-md-2 col-sm-3 col-xs-6"><i class="global-icons icon-hairdryer"></i>HAIR DRYER</span>\r\n                        <span class="col-md-2 col-sm-3 col-xs-6"><i class="global-icons icon-coffee-machine"></i>COFFEE MAKER MACHINE</span>\r\n                        <span class="col-md-2 col-sm-3 col-xs-6"><i class="global-icons icon-kitchenaid"></i>KITCHENAID</span>\r\n                         <span class="col-md-2 col-sm-3 col-xs-6"><i class="global-icons icon-fridge"></i>REFRIDGERATOR</span>\r\n                        <span class="col-md-2 col-sm-3 col-xs-6"><i class="global-icons icon-air-conditioner"></i>AIR CONDITIONER</span>\r\n                        <span class="col-md-2 col-sm-3 col-xs-6"><i class="global-icons icon-bath-tub"></i>bath tub</span>\r\n                        <span class="col-md-2 col-sm-3 col-xs-6"><i class="global-icons icon-bath-towels"></i>bath towels</span>\r\n                        <span class="col-md-2 col-sm-3 col-xs-6"><i class="global-icons icon-gym"></i>Fitness</span>\r\n                        <span class="col-md-2 col-sm-3 col-xs-6"><i class="global-icons icon-concierge"></i>Concierge</span>\r\n\r\n\r\n\r\n                </div>\r\n     		</div>\r\n        </div>\r\n    </div>\r\n\r\n', 'learn-pool-access', '9_170208204604_learn_pool_access.jpg', 'Learn Pool Access', 'Learn,Pool access,2 bedrooms', 'Keep the kids entertained all day with direct access to the swimming pool from these large two-bedroom arrangements', 0, 2, 2, '2017-02-23 12:14:25', 'N', 'Y'),
(10, 'en', 0, 'Lux Pool Access', '2 Bedrooms Suite Pool Access', 'lux', 'pool_access', '2_bedrooms', '<p>Wake from your slumber to exotic surroundings right on the poolside in a luxuriously large two-bedroom, three-bathroom corner unit. Covering 95sqm, Lux apartments are the largest of the Saturdays residences, designed to offer prestigious comfort for a family of four to six people. </p>\r\n					\r\n                    \r\n                     <div class="row global-icon-wrapper">\r\n                        <span class="col-sm-3 col-xs-6"><i class="global-icons icon-pool"></i>Pool</span>\r\n                        <span class="col-sm-3 col-xs-6"><i class="global-icons icon-wifi"></i>Public Wifi</span>\r\n                        <span class="col-sm-3 col-xs-6"><i class="global-icons icon-kingbed"></i>King bed</span>\r\n                        <span class="col-sm-3 col-xs-6"><i class="global-icons icon-living"></i>Living area</span>\r\n                    </div>', '  \r\n     <!-- hotel-policy Section -->\r\n    <div class="inroom-amenties">\r\n        <div class="container">\r\n            <div class="row inroom-amenties-box">\r\n            	<h3><span>In-Room Amenities</span></h3>\r\n            	<div class="row global-icon-wrapper">\r\n                        <span class="col-md-2 col-sm-3 col-xs-6"><i class="global-icons icon-tv"></i>lCD TV</span>\r\n                        <span class="col-md-2 col-sm-3 col-xs-6"><i class="global-icons icon-hairdryer"></i>HAIR DRYER</span>\r\n                        <span class="col-md-2 col-sm-3 col-xs-6"><i class="global-icons icon-coffee-machine"></i>COFFEE MAKER MACHINE</span>\r\n                        <span class="col-md-2 col-sm-3 col-xs-6"><i class="global-icons icon-kitchenaid"></i>KITCHENAID</span>\r\n                         <span class="col-md-2 col-sm-3 col-xs-6"><i class="global-icons icon-fridge"></i>REFRIDGERATOR</span>\r\n                        <span class="col-md-2 col-sm-3 col-xs-6"><i class="global-icons icon-air-conditioner"></i>AIR CONDITIONER</span>\r\n                        <span class="col-md-2 col-sm-3 col-xs-6"><i class="global-icons icon-bath-tub"></i>bath tub</span>\r\n                        <span class="col-md-2 col-sm-3 col-xs-6"><i class="global-icons icon-bath-towels"></i>bath towels</span>\r\n                        <span class="col-md-2 col-sm-3 col-xs-6"><i class="global-icons icon-gym"></i>Fitness</span>\r\n                        <span class="col-md-2 col-sm-3 col-xs-6"><i class="global-icons icon-concierge"></i>Concierge</span>\r\n\r\n\r\n\r\n                </div>\r\n     		</div>\r\n        </div>\r\n    </div>', 'lux-pool-access', '10_170208205706_lux_pool_access.jpg', 'Lux Pool Access', 'Lux,Pool Access,2 bedrooms', 'Wake from your slumber to exotic surroundings right on the poolside in a luxuriously large two-bedroom', 0, 2, 2, '2017-02-23 12:40:55', 'N', 'Y'),
(11, 'en', 0, 'Lux Sea View', '2 Bedrooms Suite Sea View', 'lux', 'sea_view', '2_bedrooms', '<p>Elevated luxuriously spacious two-bedroom, three-bathroom corner units covering 95sqm with an encompassing balcony give you a fantastic view of the Andaman sea off the south coast. Inside there’s ample space to engage in family activities in the open-plan living and dining area, or you can retreat to private space open-plan living space or find a corner on the terraces accessible from the living space and bedrooms. </p>\r\n					\r\n                    \r\n                     <div class="row global-icon-wrapper">\r\n                        <span class="col-sm-3 col-xs-6"><i class="global-icons icon-pool"></i>Pool</span>\r\n                        <span class="col-sm-3 col-xs-6"><i class="global-icons icon-wifi"></i>Public Wifi</span>\r\n                        <span class="col-sm-3 col-xs-6"><i class="global-icons icon-kingbed"></i>King bed</span>\r\n                        <span class="col-sm-3 col-xs-6"><i class="global-icons icon-living"></i>Living area</span>\r\n                    </div>', '  \r\n     <!-- hotel-policy Section -->\r\n    <div class="inroom-amenties">\r\n        <div class="container">\r\n            <div class="row inroom-amenties-box">\r\n            	<h3><span>In-Room Amenities</span></h3>\r\n            	<div class="row global-icon-wrapper">\r\n                        <span class="col-md-2 col-sm-3 col-xs-6"><i class="global-icons icon-tv"></i>lCD TV</span>\r\n                        <span class="col-md-2 col-sm-3 col-xs-6"><i class="global-icons icon-hairdryer"></i>HAIR DRYER</span>\r\n                        <span class="col-md-2 col-sm-3 col-xs-6"><i class="global-icons icon-coffee-machine"></i>COFFEE MAKER MACHINE</span>\r\n                        <span class="col-md-2 col-sm-3 col-xs-6"><i class="global-icons icon-kitchenaid"></i>KITCHENAID</span>\r\n                         <span class="col-md-2 col-sm-3 col-xs-6"><i class="global-icons icon-fridge"></i>REFRIDGERATOR</span>\r\n                        <span class="col-md-2 col-sm-3 col-xs-6"><i class="global-icons icon-air-conditioner"></i>AIR CONDITIONER</span>\r\n                        <span class="col-md-2 col-sm-3 col-xs-6"><i class="global-icons icon-bath-tub"></i>bath tub</span>\r\n                        <span class="col-md-2 col-sm-3 col-xs-6"><i class="global-icons icon-bath-towels"></i>bath towels</span>\r\n                        <span class="col-md-2 col-sm-3 col-xs-6"><i class="global-icons icon-gym"></i>Fitness</span>\r\n                        <span class="col-md-2 col-sm-3 col-xs-6"><i class="global-icons icon-concierge"></i>Concierge</span>\r\n\r\n\r\n\r\n                </div>\r\n     		</div>\r\n        </div>\r\n    </div>', 'lux-sea-view', '11_170208210506_lux_sea_view.jpg', 'Lux Sea View', 'lux,sea view,2 bedrooms', 'Elevated luxuriously spacious two-bedroom, three-bathroom corner units covering 95sqm with an encompassing balcony', 0, 2, 2, '2017-02-23 12:41:32', 'N', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `pws_subscription`
--

DROP TABLE IF EXISTS `pws_subscription`;
CREATE TABLE IF NOT EXISTS `pws_subscription` (
  `id` int(11) NOT NULL,
  `name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pws_subscription`
--

INSERT INTO `pws_subscription` (`id`, `name`, `email`, `is_active`) VALUES
(1, 'aaa', 'aaaa@aaa.com', 'Y'),
(2, 'asadad', 'sale@saturdaysresidence.com', 'Y'),
(3, 'fa@gsfsdf.com', 'fa@gsfsdf.com', 'Y'),
(4, 'wad_na@hotmail.com', 'wad_na@hotmail.com', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `pws_taste`
--

DROP TABLE IF EXISTS `pws_taste`;
CREATE TABLE IF NOT EXISTS `pws_taste` (
  `id` int(11) NOT NULL,
  `lang_id` varchar(3) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(100) NOT NULL,
  `description` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `open_hours` varchar(150) NOT NULL,
  `link` varchar(200) NOT NULL,
  `link_caption` varchar(100) NOT NULL,
  `filename` varchar(200) NOT NULL,
  `meta_title` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `meta_keyword` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `meta_description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `sort` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_date` datetime NOT NULL,
  `flag_homepage` enum('Y','N') NOT NULL DEFAULT 'N',
  `is_active` enum('Y','N') NOT NULL DEFAULT 'Y'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pws_taste`
--

INSERT INTO `pws_taste` (`id`, `lang_id`, `parent_id`, `name`, `description`, `open_hours`, `link`, `link_caption`, `filename`, `meta_title`, `meta_keyword`, `meta_description`, `sort`, `created_by`, `updated_by`, `updated_date`, `flag_homepage`, `is_active`) VALUES
(5, 'en', 0, 'Origami', 'Order all-day continental breakfasts, filling sandwiches made to order, and premium pasta dishes to eat in or neatly boxed up for you to take away and picnic at the beach or on your travels.', '8 A.M. - 5 P.M.', 'Taste 1', 'Taste 1', '5_170223102440_origami.jpg', 'Origami', 'Origami', 'Origami', 0, 1, 1, '2017-02-23 10:24:40', 'N', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `pws_themes`
--

DROP TABLE IF EXISTS `pws_themes`;
CREATE TABLE IF NOT EXISTS `pws_themes` (
  `id` int(3) NOT NULL COMMENT 'ID FOCUS',
  `theme_name` varchar(100) DEFAULT NULL,
  `theme_style` varchar(100) DEFAULT NULL,
  `theme_admin_name` varchar(100) DEFAULT NULL,
  `theme_admin_style` varchar(100) DEFAULT NULL COMMENT 'Theme admin style'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pws_themes`
--

INSERT INTO `pws_themes` (`id`, `theme_name`, `theme_style`, `theme_admin_name`, `theme_admin_style`) VALUES
(1, 'default', 'basic', 'default', 'basic');

-- --------------------------------------------------------

--
-- Table structure for table `pws_users`
--

DROP TABLE IF EXISTS `pws_users`;
CREATE TABLE IF NOT EXISTS `pws_users` (
  `id` int(11) NOT NULL,
  `group_id` int(3) DEFAULT '1',
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(150) DEFAULT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `profile_photo` varchar(200) DEFAULT NULL,
  `created_on` datetime NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `ip_address` varchar(80) NOT NULL,
  `status` enum('Active','Inactive','Forget','Pending') DEFAULT 'Pending',
  `is_active` enum('Y','N') DEFAULT 'Y'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pws_users`
--

INSERT INTO `pws_users` (`id`, `group_id`, `username`, `password`, `email`, `salt`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `first_name`, `last_name`, `profile_photo`, `created_on`, `last_login`, `ip_address`, `status`, `is_active`) VALUES
(1, 1, 'JaCk', 'c9ec6d1262b5102a202da594b37fc034', 'jack@phuketwebstudio.com', NULL, NULL, NULL, NULL, NULL, 'Peerawat', 'Promrit', 'profile_photo.png', '2017-01-01 00:00:00', NULL, '8.8.8.8', 'Active', 'Y'),
(2, 1, 'admin', 'cbc0c0b542822d67821e44e1d4b2a1a6', 'admin@example.com', '', '', '', 0, '', 'Administrator', 'Saturday', 'profile_photo.png', '2017-01-30 15:54:24', '0000-00-00 00:00:00', '::1', 'Active', 'Y');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pws_careers`
--
ALTER TABLE `pws_careers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pws_chats`
--
ALTER TABLE `pws_chats`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pws_chats_rooms`
--
ALTER TABLE `pws_chats_rooms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pws_config`
--
ALTER TABLE `pws_config`
  ADD PRIMARY KEY (`id`,`lang_id`);

--
-- Indexes for table `pws_config_paypal`
--
ALTER TABLE `pws_config_paypal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pws_contents`
--
ALTER TABLE `pws_contents`
  ADD PRIMARY KEY (`id`,`lang_id`);

--
-- Indexes for table `pws_contents_media`
--
ALTER TABLE `pws_contents_media`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pws_do`
--
ALTER TABLE `pws_do`
  ADD PRIMARY KEY (`id`,`lang_id`);

--
-- Indexes for table `pws_fieldguide`
--
ALTER TABLE `pws_fieldguide`
  ADD PRIMARY KEY (`id`,`lang_id`);

--
-- Indexes for table `pws_fieldguide_media`
--
ALTER TABLE `pws_fieldguide_media`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pws_gather`
--
ALTER TABLE `pws_gather`
  ADD PRIMARY KEY (`id`,`lang_id`);

--
-- Indexes for table `pws_languages`
--
ALTER TABLE `pws_languages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `lang_id` (`id`);

--
-- Indexes for table `pws_media`
--
ALTER TABLE `pws_media`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pws_menus`
--
ALTER TABLE `pws_menus`
  ADD PRIMARY KEY (`id`,`lang_id`);

--
-- Indexes for table `pws_modules`
--
ALTER TABLE `pws_modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pws_offers`
--
ALTER TABLE `pws_offers`
  ADD PRIMARY KEY (`id`,`lang_id`);

--
-- Indexes for table `pws_press`
--
ALTER TABLE `pws_press`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pws_sleep`
--
ALTER TABLE `pws_sleep`
  ADD PRIMARY KEY (`id`,`lang_id`);

--
-- Indexes for table `pws_subscription`
--
ALTER TABLE `pws_subscription`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pws_taste`
--
ALTER TABLE `pws_taste`
  ADD PRIMARY KEY (`id`,`lang_id`);

--
-- Indexes for table `pws_themes`
--
ALTER TABLE `pws_themes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pws_users`
--
ALTER TABLE `pws_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pws_careers`
--
ALTER TABLE `pws_careers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pws_chats`
--
ALTER TABLE `pws_chats`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pws_chats_rooms`
--
ALTER TABLE `pws_chats_rooms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `pws_config`
--
ALTER TABLE `pws_config`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `pws_config_paypal`
--
ALTER TABLE `pws_config_paypal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pws_contents`
--
ALTER TABLE `pws_contents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `pws_contents_media`
--
ALTER TABLE `pws_contents_media`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=62;
--
-- AUTO_INCREMENT for table `pws_do`
--
ALTER TABLE `pws_do`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `pws_fieldguide`
--
ALTER TABLE `pws_fieldguide`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `pws_fieldguide_media`
--
ALTER TABLE `pws_fieldguide_media`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `pws_gather`
--
ALTER TABLE `pws_gather`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `pws_media`
--
ALTER TABLE `pws_media`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pws_menus`
--
ALTER TABLE `pws_menus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pws_modules`
--
ALTER TABLE `pws_modules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pws_offers`
--
ALTER TABLE `pws_offers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `pws_press`
--
ALTER TABLE `pws_press`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `pws_sleep`
--
ALTER TABLE `pws_sleep`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `pws_subscription`
--
ALTER TABLE `pws_subscription`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `pws_taste`
--
ALTER TABLE `pws_taste`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `pws_themes`
--
ALTER TABLE `pws_themes`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT COMMENT 'ID FOCUS',AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `pws_users`
--
ALTER TABLE `pws_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
