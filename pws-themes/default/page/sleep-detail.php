<?php
$module = "Sleep";
$module_class = strtolower($module);
$lang_id = $_GET["lang_id"];
$filename = $_GET["title"];

if (file_exists(PATH_ADMIN_MODULES_ROOT . $module_class . '/classes/class.' . $module_class . '.php')) {
	require_once(PATH_ADMIN_MODULES_ROOT.$module_class . '/classes/class.'.$module_class.'.php');    
	$$module_class = new $module();

    $data = $$module_class->get_by_filename($lang_id, $filename);
}

extract($data[0]);
//PRE($data[0]);
$short_description = str_ireplace(array("\r","\n",'\r','\n'),'', $short_description);
$description = str_ireplace(array("\r","\n",'\r','\n'),'', $description);

$image_header = "http://placehold.it/1450x500&text=".$name;
if($photoname != "")
{
	$pic = BASE_UPLOAD.$module_class."/".$id."/".$photoname;
	$pic_realpath = PATH_UPLOAD_ROOT.$module_class."/".$id."/".$photoname;
	if(is_file($pic_realpath)) {
		$image_header = PATH_ROOT."timthumb.php?src=".$pic."&w=1450&h=500";
	}
}
?>

<!-- room header Section -->
<header class="inner-header">
    <div class="container">
        <div class="row">
            <div class="inner-header-fill" style="background-image:url('<?php echo $image_header; ?>');"></div>
        </div>
     </div>
</header>
    
<section class="content-area">

	<!-- saturdays_intro Section -->
    <div id="room_intro">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                	<div class="row room_intro">
                    <h2><?php echo $name; ?></h2>
                    
                    <h4><?php echo $room_type; ?></h4>
					<?php echo $short_description; ?>
                    
                    
                    </div><!--//saturdays_intro-->
                </div><!--//col-md-6-->
                
                 <div class="col-md-offset-1 col-md-4">
                 	<div class="row calendar-widget">
                        <div class="row title"><i class="global-icons icon-calendar-l"></i>Please Select Your Dates</div>
                        <div class="row">
                            <div class="check-in form-wrapper" id="edit-checkin" data-toggle="modal"
                                 data-target="#booking-modal">
                                <p class="toplabel">Checkin</p><p class="day">16</p><p class="month">February</p>
                            </div>

                            <div class="check-out form-wrapper" id="edit-checkout" data-toggle="modal"
                                 data-target="#booking-modal">
                                <p class="toplabel">Checkout</p><p class="day">17</p><p class="month">February</p>
                            </div>
                        </div>
                        <div class="row">
                            <center>
                                <button class="btn btn-lg btn-primary" data-toggle="modal" data-target="#booking-modal">
                                    Book Now
                                </button>
                            </center>
                        </div>
                   </div>

                 </div>
                
            </div>
        </div>
    </div>
	<?php echo $description; ?>    
	</section>