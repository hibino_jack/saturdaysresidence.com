<?php
    if($module == 'contents') {
        $module_class = ucfirst($module);
        require_once('class.'.$module.'.php');
        $$module_class = new $module_class;
        switch($action) {
            case 'new':
                if($task == "summernoteUpload"){
                    $page_content = $$module_class->summernoteUpload($post_vars);
                }elseif($task == "summernoteDelete"){
                    $page_content = $$module_class->summernoteDelete($post_vars);
                }else{
                    $page_content = $$module_class->insert($post_vars);
                }
                break;
            case 'update':
                if($task == "summernoteUpload"){
                    $page_content = $$module_class->summernoteUpload($post_vars);
                }elseif($task == "summernoteDelete"){
                    $page_content = $$module_class->summernoteDelete($post_vars);
                }else{
                    $page_content = $$module_class->update($post_vars);
                }
                break;
            case 'delete':
                $page_content = $$module_class->delete($post_vars);
                break;
            case 'header_photo':
				if($task == "delete_header_photo_item"){
					$page_content = $$module_class->delete_photo($post_vars);
				}
                break;
            case 'gallery':
                if($task == "insert_gallery"){
                    //PRE($post_vars);
                    $page_content = $$module_class->Media->insert($post_vars);
                }elseif($task == "update_gallery_item"){
                    $page_content = $$module_class->Media->update($post_vars);
                }elseif($task == "delete_gallery_item"){
                    $page_content = $$module_class->Media->delete($post_vars);
                }elseif($task == "delete_all_gallery_item"){
                    $page_content = $$module_class->Media->delete_all($post_vars);
                } else {
                    $page_content = false;
                }
                break;
        }
         echo $page_content;
        //print_r($page_content);
        exit();
	}
?>