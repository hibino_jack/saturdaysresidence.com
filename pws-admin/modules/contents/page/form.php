<?php
if($action == "update")    {
    if($$module_class->itemid) {
        $data = $$module_class->select($lang_id, $$module_class->itemid);
        $itemid = $$module_class->itemid;

        if($data){
            extract($data[0]);
            //PRE($data);
        }
    }
}
?>

<div class="row">
    <div class="col-md-12">
        <?php echo $languages->dropdown_languages($module, $action, $path, $itemid, $task); ?>
        <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"><?php echo $module_class." ".$action;?></h3>
        </div>
        <div class="panel-body">
            <form name="create-form" id="create-form" class="form-horizontal" role="form" data-parsley-validate novalidate enctype="multipart/form-data">
                <div class="alert alert-danger alert-dismissable hidden">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <h4>Oh snap!</h4>
                    <p>This form seems to be invalid :(</p>
                </div>
                <div class="alert alert-success alert-dismissable hidden">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <h4>Yay!</h4>
                    <p>Everything seems to be ok., Re-submit your form then. :)</p>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <!--
                        <div class="form-group">
                            <label class="col-md-2 control-label">Parent Contents</label>
                            <div class="col-md-10">
                                <?php
//                                $tbContents = $$module_class->tbContents;
//                                $field = "title";
//                                $where = "`$tbContents`.`lang_id`='$lang_id' AND `$tbContents`.`parent_id`='0' AND `$tbContents`.`is_active`='Y'";
//                                ECHO DROPDOWNITEM("parent_id", $parent_id, $$module_class->tbContents, "id", $field, $where);
                                ?>
                            </div>
                        </div>
                        -->
                        <input type="hidden" id="parent_id" name="parent_id" value="<?php echo $parent_id; ?>">


                        <div class="form-group">
                            <label class="col-md-2 control-label">Filename*</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" name="filename" parsley-trigger="change" required placeholder="Enter Filename" value="<?php echo $filename;?>">
                                <span class="font-13 text-muted">e.g. "http://www.domain.com/filename"</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label">Title</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" name="title" parsley-trigger="change" required placeholder="Enter Title" value="<?php echo $title;?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label">Sub Title</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" name="subtitle" parsley-trigger="change" placeholder="Enter Sub Title" value="<?php echo $subtitle;?>">
                            </div>
                        </div>
						<?php if($id != 4) { ?>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Short Description</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" name="short_description" parsley-trigger="change" placeholder="Enter short description" value="<?php echo $short_description;?>">
                            </div>
                        </div>
						<?php } ?>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Content</label>
                            <div class="col-md-10">
                                <textarea class="form-control summernote" rows="5" name="description" id="description" parsley-trigger="change"><?php echo $description;?></textarea>
                            </div>
                        </div>
                        <?php if($id == 2) { ?>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Content 2</label>
                            <div class="col-md-10">
                                <textarea class="form-control summernote" rows="5" name="description2" id="description2" parsley-trigger="change"><?php echo $description2;?></textarea>
                            </div>
                        </div>
                        <?php } ?>
                        <?php if($id != 1) { ?>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Header Photo</label>
                            <div class="col-md-10">
                                <input type="file" name="header_photo" id="header_photo" class="filestyle" data-buttonbefore="true">

                                <?php
                                $pic = BASE_UPLOAD.$module."/".$id."/".$header_photo;
                                $pic_realpath = PATH_UPLOAD_ROOT.$module."/".$id."/".$header_photo;
                                if(is_file($pic_realpath)) {
                                    ?>
                                    <div class="col-sm-4"  style="margin-top:15px; padding-left:0;">
                                    <input type="hidden" id="old_header_photo" name="old_header_photo" value="<?php echo $header_photo; ?>">
                                    <a class="image-popup" href="<?php echo $pic; ?>" title="Map">
                                        <img src="<?php echo PATH_ROOT; ?>timthumb.php?src=<?php echo $pic; ?>&w=250&h=200" width="250" height="200">
                                    </a>
                                    </div>
                                <?php }else{ ?>
                                    <div class="col-sm-4" style=" padding-left:0;">
                                        <img src="<?php echo BASE_IMAGES; ?>nophoto.jpg" width="200" class="gal-detail thumb thumb-img" style="margin-top:15px;"/>
                                    </div>
                                <?php } ?>
                                <?php if($header_photo){ ?>
                            <div class="btn-action" style="margin-top:15px;">
                                <a href="javascript:void(0);" class="btn btn-danger btn-sm delete-item-img"
                                   data-module="<?php echo $module;?>"
                                   data-action="header_photo"
                                   data-task="delete_header_photo_item"
                                   data-lang_id="<?php echo $lang_id;?>"
                                   data-path="cp"
                                   data-filename="<?php echo $header_photo;?>"
                                   data-content_id="<?php echo $itemid;?>"
                                   data-itemid="<?php echo $id; ?>"><i class="md md-close"></i></a>
                            </div>
								<?php } ?>
                            </div>
                        </div>                      
						<?php } ?>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Meta Title</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" name="meta_title" parsley-trigger="change" required placeholder="Enter short description" value="<?php echo $meta_title;?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label">Meta Keyword</label>
                            <div class="col-md-10">
                                <input type="text" value="<?php echo $meta_keyword;?>" name="meta_keyword" parsley-trigger="change" required data-role="tagsinput" placeholder="Add your keywords"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label">Meta Description</label>
                            <div class="col-md-10">
                                <textarea class="form-control" rows="5" name="meta_description" parsley-trigger="change" required><?php echo $meta_description;?></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label">Active</label>
                            <div class="col-md-10">
                                <?php
                                $is_active_data = array("Y" => "Enable", "N" => "Disabled");
                                echo RADIOITEMMANUAL("is_active", $is_active, $is_active_data);
                                ?>
                            </div>
                        </div>


                    </div>

                    <div class="col-md-12">
                        <div class="form-group pull-right">
                            <input type="hidden" id="module" name="module" value="<?php echo $module; ?>">
                            <input type="hidden" id="action" name="action" value="<?php echo $action; ?>">
                            <input type="hidden" id="task" name="task" value="<?php echo $task; ?>">
                            <input type="hidden" id="lang_id" name="lang_id" value="<?php echo $lang_id; ?>">
                            <input type="hidden" id="itemid" name="itemid" value="<?php echo $itemid; ?>">
                            <input type="hidden" id="path" name="path" value="cp">

                            <button type="submit" class="btn btn-default waves-effect waves-light btn-lg">Submit</button>
                            <button type="reset" class="btn btn-white waves-effect waves-light btn-lg">Cancel</button>
                        </div>
                    </div>

                </div>

            </form>
            </div>
        </div>

		<?php 
		$loop = 1;
		if($$module_class->itemid == 1) { 
                for($i = 17; $i < 25 ; $i++){
                    $data_block = $$module_class->select($lang_id, $i);
                    //PRE($data_block);
                    $id = $data_block[0]["id"];
                    $title = $data_block[0]["title"];
                    $description = $data_block[0]["description"];
                    $header_photo = $data_block[0]["header_photo"];
                    $meta_title = $data_block[0]["meta_title"]; //button label
                    $meta_description = $data_block[0]["meta_description"]; //button link
        ?>
        <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Content Block <?php echo $loop;?></h3>
        </div>
        <div class="panel-body">
            <form id="create-multi-form-<?php echo $loop; ?>" name="create-form" class="form-horizontal create-multi-form" role="form" data-parsley-validate novalidate enctype="multipart/form-data">
            
                <div class="row">
                     <div class="col-md-12">
               
                        <div class="form-group">
                            <label class="col-md-2 control-label">Title</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" name="title" parsley-trigger="change" required placeholder="Enter Title" value="<?php echo $title;?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label">Content</label>
                            <div class="col-md-10">
                                <textarea class="form-control summernote" rows="3" name="description" id="description" parsley-trigger="change"><?php echo $description;?></textarea>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-md-2 control-label">Photo</label>
                            <div class="col-md-10">
                                <input type="file" name="header_photo" id="header_photo" class="filestyle" data-buttonbefore="true">

                                <?php
                                $pic = BASE_UPLOAD.$module."/".$id."/".$header_photo;
                                $pic_realpath = PATH_UPLOAD_ROOT.$module."/".$id."/".$header_photo;
                                if(is_file($pic_realpath)) {
                                    ?>
                                    <div class="col-sm-4"  style="margin-top:15px; padding-left:0;">
                                    <input type="hidden" id="old_header_photo" name="old_header_photo" value="<?php echo $header_photo; ?>">
                                    <a class="image-popup" href="<?php echo $pic; ?>" title="Map">
                                        <img src="<?php echo PATH_ROOT; ?>timthumb.php?src=<?php echo $pic; ?>&w=250&h=200" width="250" height="200">
                                    </a>
                                    </div>
                                <?php }else{ ?>
                                    <div class="col-sm-4" style=" padding-left:0;">
                                        <img src="<?php echo BASE_IMAGES; ?>nophoto.jpg" width="200" class="gal-detail thumb thumb-img"  style="margin-top:15px;"/>
                                    </div>
                                <?php } ?>

                                <?php if($header_photo){ ?>
                                    <div class="btn-action" style="margin-top:15px;">
                                        <a href="javascript:void(0);" class="btn btn-danger btn-sm delete-item-img"
                                           data-module="<?php echo $module;?>"
                                           data-action="header_photo"
                                           data-task="delete_header_photo_item"
                                           data-lang_id="<?php echo $lang_id;?>"
                                           data-path="cp"
                                           data-filename="<?php echo $header_photo;?>"
                                           data-content_id="<?php echo $itemid;?>"
                                           data-itemid="<?php echo $id; ?>"><i class="md md-close"></i></a>
                                    </div>
								<?php } ?>

                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-md-2 control-label">Meta Title</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" name="meta_title" parsley-trigger="change"  placeholder="Enter Button Label" value="<?php echo $meta_title;?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label">Button Link</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" name="meta_description" parsley-trigger="change" placeholder="Enter Button Link" value="<?php echo $meta_description;?>" ></textarea>
                            </div>
                        </div>

                	</div>
                    
                    <div class="col-md-12">
                        <div class="form-group pull-right">
                        	<input type="hidden" id="filename" name="filename" value="home-block-<?php echo $i; ?>" />
                            <input type="hidden" id="module" name="module" value="<?php echo $module; ?>">
                            <input type="hidden" id="action" name="action" value="<?php echo $action; ?>">
                            <input type="hidden" id="task" name="task" value="<?php echo $task; ?>">
                            <input type="hidden" id="lang_id" name="lang_id" value="<?php echo $lang_id; ?>">
                            <input type="hidden" id="itemid" name="itemid" value="<?php echo $i; ?>">
                            <input type="hidden" id="path" name="path" value="cp">

                            <button type="submit" class="btn btn-default waves-effect waves-light btn-lg btn-detect-submit">Submit</button>
                            <button type="reset" class="btn btn-white waves-effect waves-light btn-lg">Cancel</button>
                        </div>
                    </div>
                </div>
            </form>
		</div>
        </div>
            <?php 
				$loop++;
					} //for
				}//if home
			?>
    </div>
</div>