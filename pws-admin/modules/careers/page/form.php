<?php
if($action == "update")    {
    if($$module_class->itemid) {
        $data = $$module_class->select($lang_id, $$module_class->itemid);
        $itemid = $$module_class->itemid;
        if($data){
            extract($data[0]);
            //PRE($data);
        }
    }
}
?>

<div class="row">
    <div class="col-md-12">
        <?php echo $languages->dropdown_languages($module, $action, $path, $itemid, $task); ?>
        <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"><?php echo $module_class." ".$action;?></h3>
        </div>
        <div class="panel-body">
            <form name="create-form" id="create-form" class="form-horizontal" role="form" data-parsley-validate novalidate enctype="multipart/form-data">
                <div class="alert alert-danger alert-dismissable hidden">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <h4>Oh snap!</h4>
                    <p>This form seems to be invalid :(</p>
                </div>
                <div class="alert alert-success alert-dismissable hidden">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <h4>Yay!</h4>
                    <p>Everything seems to be ok., Re-submit your form then. :)</p>
                </div>

                <div class="row">
                    <div class="col-md-12">
<!--
                        <div class="form-group">
                            <label class="col-md-2 control-label">Parent Menu</label>
                            <div class="col-md-10">
                                <?php
//                                $tbMenus = $$module_class->tbMenus;
//                                $field = "name";
//                                $where = "`$tbMenus`.`lang_id`='$lang_id' AND `$tbMenus`.`parent_id`='0' AND `$tbMenus`.`is_active`='Y'";
//                                echo DROPDOWNITEM("parent_id", $parent_id, $$module_class->tbMenus, "id", $field, $where);
                                ?>
                            </div>
                        </div>
-->
                        <div class="form-group">
                            <label class="col-md-2 control-label">Name</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" name="name" parsley-trigger="change" required placeholder="Enter Name" value="<?php echo $name;?>">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-md-2 control-label">No Position</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" name="no_position" parsley-trigger="change" required placeholder="Enter No Position" value="<?php echo $no_position;?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label">Job Type</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" name="job_type" parsley-trigger="change" required placeholder="Enter Job Type" value="<?php echo $job_type;?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label">Experience</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" name="exp" parsley-trigger="change" required placeholder="Enter Experience" value="<?php echo $exp;?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label">Age</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" name="age" parsley-trigger="change" required placeholder="Enter Age" value="<?php echo $age;?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label">Description</label>
                            <div class="col-md-10">
                                <textarea class="form-control summernote" rows="5" name="description" id="description" parsley-trigger="change" required><?php echo $description;?></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Active</label>
                            <div class="col-sm-10">
                                <?php
                                $is_active_data = array("Y" => "Enable", "N" => "Disabled");
                                echo RADIOITEMMANUAL("is_active", $is_active, $is_active_data);
                                ?>
                            </div>
                        </div>

                    </div>

                </div>

                <div class="row">
                <div class="col-md-12">
                    <div class="form-group pull-right">
                        <input type="hidden" id="module" name="module" value="<?php echo $module; ?>">
                        <input type="hidden" id="action" name="action" value="<?php echo $action; ?>">
                        <input type="hidden" id="task" name="task" value="<?php echo $task; ?>">
                        <input type="hidden" id="lang_id" name="lang_id" value="<?php echo $lang_id; ?>">
                        <input type="hidden" id="path" name="path" value="cp">

                        <input type="hidden" id="itemid" name="itemid" value="<?php echo $itemid; ?>">

                        <input type="hidden" id="created_by" name="created_by" value="<?php echo (empty($created_by) || $created_by == "0") ? $oUser->id : $created_by; ?>">
                        <input type="hidden" id="updated_by" name="updated_by" value="<?php echo (empty($updated_by) || $updated_by == "0") ? $oUser->id : $updated_by; ?>">

                        <button type="submit" class="btn btn-default waves-effect waves-light btn-lg">Submit</button>
                        <button type="reset" class="btn btn-white waves-effect waves-light btn-lg">Cancel</button>
                    </div>
                </div>
                </div>
            </form>

        </div>
    </div>

    </div>
</div>