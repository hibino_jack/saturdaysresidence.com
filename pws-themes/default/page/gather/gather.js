var Gather = function () {
    var module_is = 'Gather';
    var moduleName = module_is.toLowerCase();

    var handleGatherForm = function () {
        if($("form.gather-form").length){
            $("form.gather-form").validate({
                errorPlacement: function(error,element) {
                    return true;
                },
                submitHandler: function (form) {
                    var formData = new FormData(form);
                    $.ajax({
                        type: "POST",
                        url: base_url + "ajax_operation.php",
                        data:  formData,
                        //mimeType:"multipart/form-data",
                        contentType: false,
                        cache: false,
                        processData:false
                    }).done(function( msg ) {
                        if(msg == "done")
                        {
                            $("form.gather-form")[0].reset();
                            $("form.gather-form").append("<div class='success-message' style='clear:both;text-align:center'><p>Thank You,We already get your message.</p></div>");
                            setTimeout(function() {
                                $("form.footer-subscribe .success-message").hide();
                            }, 5000);
                        }else{
                            $("form.gather-form").append("<div class='success-message' style='clear:both;text-align:center'><p>Something Wrong Please Try Again</p></div>");
                            setTimeout(function() {
                                $("form.gather-form .success-message").hide();
                            }, 5000);
                        }
                    });
                }
            });
        }


    };

    return {
        //main function to initiate the module
        initFrontend: function() {
            handleGatherForm();
        },
        initFrontendForm: function() {
        }
    };

}();