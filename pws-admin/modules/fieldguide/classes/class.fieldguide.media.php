<?php
class FieldguideMedia extends DB {
    var $tbFieldguideMedia = "pws_fieldguide_media";

    function __construct() {
        $this->Page = new Page();
        $this->Page->module_name = "FieldguideMedia"; // ucfirst
        $this->module_name = strtolower($this->Page->module_name); // strtolower
    }

    function select($id = NULL, $fieldguide_id = NULL, $categories_id = NULL, $is_active = NULL, $query_type = NULL) {
        if(!isset($query_type))
            $query_type = "fetchAssoc";

        $sql = "SELECT * FROM `$this->tbFieldguideMedia`";

        if($id)
            $ct[] = "`$this->tbFieldguideMedia`.`id` = '$id' ";
        if($fieldguide_id)
            $ct[] = "$this->tbFieldguideMedia.`fieldguide_id` = '$fieldguide_id' ";
        if($categories_id)
            $ct[] = "$this->tbFieldguideMedia.`categories_id` = '$categories_id' ";
        if($is_active)
            $ct[] = "`$this->tbFieldguideMedia`.`is_active` = '$is_active' ";

        if(is_array($ct))
            $sql = $sql." WHERE ".implode(" AND ", $ct);

        //PRE($sql);
        $data = DB::query($sql, $query_type);
        return $data;
    }

    function insert($post_vars) {
        global $oUser;

        if($post_vars)
            extract($post_vars);

        //Upload Section
        if ($_FILES['file']['tmp_name']) {
            //print_r($_FILES);exit;
            if($_FILES['file']["name"] != "") {
                $updateFile = uploadFile($module, $itemid, NULL, "photo", NULL, NULL);
            }
        }
        //End Upload Section

        //upload file ok insert data to database
        if($updateFile) {
            if(!isset($created_by))
                $created_by = $oUser->id;

            if(!isset($title))
                $title = ucfirst($module)." Photo ID#".$itemid;
            if(!isset($categories_id))
                $categories_id = 1;
            if(!isset($is_default))
                $is_default = "N";
            if(!isset($is_flag))
                $is_flag = "N";
            if(!isset($is_active))
                $is_active = "Y";

            $title = DB::escape($title);

            $field = "`id`, `fieldguide_id`, `categories_id`, `title`, `filename`, `is_default`, `is_flag`, `created_by`, `created_date`, `is_active`";
            $value = "'', '$itemid', '$categories_id', '$title', '$updateFile', '$is_default', '$is_flag', '$created_by', NOW(), '$is_active'";
            $sql = "INSERT INTO $this->tbFieldguideMedia ($field) VALUES ($value)";
            $id = DB::query($sql, "lastInsertId");

            if($id) {
                //return $id;
                $data = array(
                    "id" => $id,
                    "fieldguide_id" => $itemid,
                    "filename" => $updateFile,
                    "status" => "sucess"
                );
                return json_encode($data);
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    //`pws_media` (`id`, `fieldguide_id`, `categories_id`, `title`, `filename`, `is_default`, `is_flag`, `created_by`, `created_date`, `is_active`)
    function update($post_vars) {
        global $oUser;

        if($post_vars)
            extract($post_vars);

        if(!isset($created_by))
            $created_by = $oUser->id;

        $title = DB::escape($title);

        $f = "title = '$title'";

        if($fieldguide_id)
            $f.= ", fieldguide_id = '$fieldguide_id' ";
        if($categories_id)
            $f.= ", categories_id = '$categories_id' ";
        if($filename)
            $f.= ", filename = '$filename' ";
        if($is_default)
            $f.= ", is_default = '$is_default' ";
        if($is_flag)
            $f.= ", is_flag = '$is_flag' ";
        if($created_by)
            $f.= ", created_by = '$created_by' ";
        if($created_date)
            $f.= ", created_date = '$created_date' ";
        if($is_active)
            $f.= ", is_active = '$is_active' ";

        $sql = "UPDATE $this->tbFieldguideMedia SET $f WHERE $this->tbFieldguideMedia.id = '$itemid' ";
        //echo $sql;
        //exit;
        $data = DB::query($sql);
        if($data){
            return $data;
        } else {
            return false;
        }
    }

    function delete($post_vars) {
        if($post_vars)
            extract($post_vars);

        if($itemid) {
            //delete
            $deleteok = removeFile($fieldguide_id, $module, $filename);
            if($deleteok) {
                $sql = "DELETE FROM `$this->tbFieldguideMedia` WHERE `$this->tbFieldguideMedia`.`id` = '$itemid' ";
                $data = DB::query($sql);
                if($data) {
                    return true;
                }
            }
        } else {
            return false;
        }
    }

    function delete_all($post_vars) {
        if($post_vars)
            extract($post_vars);
        //PRE($post_vars);

        if($itemid) {
            $data = $this->select(NULL, $itemid);
            if($data) {
                //PRE($data);
                foreach($data as $item) {
                    //PRE($item);
                    //delete
                    //echo $item['filename']."<br>";
                    $deleteok = removeFile($itemid, $module, $item['filename']);
                    if($deleteok) {
                        $sql = "DELETE FROM `$this->tbFieldguideMedia` WHERE `$this->tbFieldguideMedia`.`id` = '".$item['id']."'";
                        $delete_from_table_ok = DB::query($sql);
                        if($delete_from_table_ok) {
                            unset($deleteok, $delete_from_table_ok, $sql);
                        }
                    }
                }
            }

        } else {
            return false;
        }
    }

}
?>