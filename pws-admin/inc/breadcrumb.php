<!-- Page-Title -->
<div class="row">
    <div class="col-sm-12">
        <div class="page-header-2">
            <?php if(($module != "") && ($module != "config")) { ?>
                <div class="btn-group pull-right m-t-15">
                    <button type="button" class="btn btn-inverse btn-custom btn-rounded waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Shortcut Links <span class="m-l-5"><i class="fa fa-link"></i></span></button>
                    <ul class="dropdown-menu drop-menu-right" role="menu">
                        <li><a href="index.php?lang_id=<?php echo $lang_id; ?>&module=<?php echo $module; ?>&action=list&path=cp">All <?php echo ucfirst($module); ?></a></li>
                        <li><a href="index.php?lang_id=<?php echo $lang_id; ?>&module=<?php echo $module; ?>&action=new&path=cp">Create new <?php echo ucfirst($module); ?></a></li>
                        <?php if($itemid) { ?>
                            <li><a href="index.php?lang_id=<?php echo $lang_id; ?>&module=<?php echo $module; ?>&action=update&path=cp&itemid=<?php echo $itemid; ?>">Update #ITEMID <?php echo $itemid; ?></a></li>
                        <?php } ?>
                    </ul>
                </div>
            <?php } ?>
            <h4 class="page-title"><?php echo $txt_title = (isset($module)) ? ucfirst($module)." Management " : "Dashboard"; ?></h4>
            <ol class="breadcrumb">
                <?php
                if($module) {
                    echo '<li><a href="index.php?lang_id='.$lang_id.'&module='.$module.'">'.ucfirst($module).'</a></li>';
                }
                ?>
                <?php
                if(!isset($action))
                    $action = "list";

                if($module && $action) {
                    if($_REQUEST['itemid']) { $trail_txt = "&itemid=".$_REQUEST['itemid'];}
                    echo '<li><a href="index.php?lang_id='.$lang_id.'&module='.$module.'&action='.$action.$trail_txt.'">'.ucfirst($action).'</a></li>';
                    if($task) {
                        echo '<li><a href="index.php?lang_id='.$lang_id.'&module='.$module.'&action='.$action.'&task='.$task.$trail_txt.'">'.ucfirst($task).'</a></li>';
                    }

                } else {
                    echo '<li class="active">Welcome to '.SYSTEM_TITLE.' admin panel !</li>';
                }
                ?>

                <?php
                if($_REQUEST['itemid']) {
                    echo '<li class="active"> #ITEMID '.$_REQUEST['itemid'].'</li>';
                }
                ?>
            </ol>
        </div>
    </div>
</div>