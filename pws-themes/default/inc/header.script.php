<!-- Bootstrap Core CSS -->
<link href="<?php echo PATH_THEMES; ?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="<?php echo PATH_THEMES; ?>vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Lobster" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Vollkorn:400,400i,700,700i" rel="stylesheet">

<!-- Date picker CSS -->
<link rel="stylesheet" type="text/css" href="<?php echo PATH_THEMES; ?>css/jquery-ui.css">
<!-- Theme CSS -->
<link href="<?php echo PATH_THEMES; ?>css/agency.css" rel="stylesheet">
<link rel="stylesheet" href="<?php echo PATH_THEMES; ?>css/flexslider.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo PATH_THEMES; ?>css/jquery.fancybox.min.css" type="text/css" media="screen" />

<link href="<?php echo PATH_THEMES; ?>css/awesome-bootstrap-checkbox.css" rel="stylesheet">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->


<!-- jQuery -->
<script src="<?php echo PATH_THEMES; ?>vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?php echo PATH_THEMES; ?>vendor/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo PATH_THEMES; ?>js/jquery-ui.js"></script>

<script type="text/javascript" src="<?php echo PATH_THEMES; ?>js/validation/dist/jquery.validate.min.js"></script>


<script src="<?php echo PATH_THEMES; ?>js/jquery.fancybox.min.js"></script>
<script src="<?php echo PATH_THEMES; ?>js/instafeed.min.js"></script>

<?php

// Set CSS and JS
$page_css = $oController->page_css($module);
$page_js = $oController->page_js($module);

?>