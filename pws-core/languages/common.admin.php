<?php
	//header('Cache-control: private'); // IE 6 FIX
	if(isset($_GET['lang_id'])) {
		$lang_id = $_GET['lang_id'];
	} else {
		$lang_id = 'en';
	}

	//$sitelang = 'en';
	$languages_file = '../languages/lang.backend.'.$lang_id.'.php';
	if(is_file($languages_file)) {
        require_once $languages_file;
	} else {
		if (!copy('../languages/lang.backend.en.php', $languages_file)) {
			echo "failed to copy $file...\n";
			exit;
		}
		include_once $languages_file;
	}
?>