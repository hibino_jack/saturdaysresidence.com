var Global = function () {
    var module_is = 'Global';
    var moduleName = module_is.toLowerCase();

    /**
     Portlet Widget
     */
    var Portlet = function() {
        this.$body = $("body"),
            this.$portletIdentifier = ".portlet",
            this.$portletCloser = '.portlet a[data-toggle="remove"]',
            this.$portletRefresher = '.portlet a[data-toggle="reload"]';

        // Panel closest
        var $this = this;
        $(document).on("click",this.$portletCloser, function (ev) {
            ev.preventDefault();
            var $portlet = $(this).closest($this.$portletIdentifier);
            var $portlet_parent = $portlet.parent();
            $portlet.remove();
            if ($portlet_parent.children().length == 0) {
                $portlet_parent.remove();
            }
        });

        // Panel Reload
        $(document).on("click",this.$portletRefresher, function (ev) {
            ev.preventDefault();
            var $portlet = $(this).closest($this.$portletIdentifier);
            // This is just a simulation, nothing is going to be reloaded
            $portlet.append('<div class="panel-disabled"><div class="loader-1"></div></div>');
            var $pd = $portlet.find('.panel-disabled');
            setTimeout(function () {
                $pd.fadeOut('fast', function () {
                    $pd.remove();
                });
            }, 500 + 300 * (Math.random() * 5));
        });
    };

    //initializing nicescroll
    var initNiceScrollPlugin = function() {
        //You can change the color of scroll bar here
        $.fn.niceScroll &&  $(".nicescroll").niceScroll({ cursorcolor: '#98a6ad',cursorwidth:'6px', cursorborderradius: '5px'});
    };

    //initializing Slimscroll
    var initSlimScrollPlugin = function() {
        //You can change the color of scroll bar here
        $.fn.niceScroll &&  $(".slimscroll-noti").slimScroll({ position: 'right',size: "5px", color: '#98a6ad',height: '230px',wheelStep: 10});
    };

    //initializing Icons-Picker
    var initIconPicker = function() {
        $('.icp-auto').iconpicker();
    };

    var initMagnificPopup = function () {
        $(".image-popup").magnificPopup({
            type: "image",
            closeOnContentClick: true,
            image: {
                verticalFit: false
            }
        });
    };

    //initializing Icons-Picker
    var initSelect2 = function() {
        $(".select2").select2();
    };

    var initCreateUpdateForm = function() {
        $('#create-form').parsley().on('field:validated', function () {
            var ok = $('.parsley-error').length === 0;
            $('.alert-success').toggleClass('hidden', !ok);
            $('.alert-danger').toggleClass('hidden', ok);
        }).on('form:submit', function () {
            // if ($('.description').summernote('isEmpty')) {
            //     $('.description').val('');
            // }
            //var formName = $("#create-form");
            var formData = new FormData($("#create-form")[0]);

            /*var module = $("#module").val();
            var action = $("#action").val();
            var task = $("#task").val();
            var lang_id = $("#lang_id").val();
            var path = $("#path").val();
            var itemid = $("#itemid").val();*/

            var module = ($("#module").val()) ? $("#module").val() : '';
            var action = ($("#action").val()) ? $("#action").val() : '';
            var task = ($("#task").val()) ? $("#task").val() : '';
            var lang_id = ($("#lang_id").val()) ? $("#lang_id").val() : '';
            var path = ($("#path").val()) ? $("#path").val() : '';
            var itemid = ($("#itemid").val()) ? $("#itemid").val() : '';

            //alert(" M:" + module + " A:" + action + " T:" + task + " L:" + lang_id + " P:" + path + " I:" + itemid);

            $.ajax({
                type: "POST",
                url: base_url + "oc_ajax.php",
                data: formData,
                //mimeType:"multipart/form-data",
                cache: false,
                contentType: false,
                processData:false
            }).done(function (data) {
                //console.log(data);  // this is currently returning FALSE // Which is totally correct!
				//alert("data = " + data);
                if (data == true || data > 0) {
                    //Loading Sweet Alert
                    swal({
                        title: "Your data was successfully submitted",
                        text: "Please waiting for a moment.",
                        type: "success",
                        timer: 4000,
                        showConfirmButton: false
                    });
                    //window.location = base_url + 'pws-admin/index.php?module=' + module + "&action=list" + "&task=" + task + "&lang_id=" + lang_id;
                    window.location = base_url + 'pws-admin/index.php?module=' + module + "&action=" + action + "&itemid=" + itemid + "&task=" + task + "&lang_id=" + lang_id;
                } else {
                    //Loading Sweet Alert
                    swal({
                        title: "OPP!! Something wrong",
                        text: "Please checking your data again.",
                        type: "warning",
                        timer: 4000,
                        showConfirmButton: false
                    });
                }

            });

            return false; // Don't submit form
        });

        //Set Summernote Upload and Delele function
        initSummernote();
        /*
        $(".summernote").summernote({
            height: 300,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: false                 // set focus to editable area after initializing summernote
        });
        */

    };
	
    var initCreateMultiUpdateForm = function() {
		$(".btn-detect-submit").bind("click",function(){
			//alert("you btn-detect-submit" + $(this).parents("form").attr("id"));
			var currentFormId = "#" + $(this).parents("form").attr("id");
			
			$(currentFormId).parsley().on('field:validated', function () {
				var ok = $('.parsley-error').length === 0;
				$('.alert-success').toggleClass('hidden', !ok);
				$('.alert-danger').toggleClass('hidden', ok);
			}).on('form:submit', function () {
				//alert("form-submit " + $(currentFormId).find("#itemid").val());
				
				//var formName = $("#create-form");
				var formData = new FormData($(currentFormId)[0]);
				/*var module = $("#module").val();
				var action = $("#action").val();
				var task = $("#task").val();
				var lang_id = $("#lang_id").val();
				var path = $("#path").val();
				var itemid = $("#itemid").val();*/

	
				var module = ($(currentFormId).find("#module").val()) ? $(currentFormId).find("#module").val() : '';
				var action = ($(currentFormId).find("#action").val()) ? $(currentFormId).find("#action").val() : '';
				var task = ($(currentFormId).find("#task").val()) ? $(currentFormId).find("#task").val() : '';
				var lang_id = ($(currentFormId).find("#lang_id").val()) ? $(currentFormId).find("#lang_id").val() : '';
				var path = ($(currentFormId).find("#path").val()) ? $(currentFormId).find("#path").val() : '';
				var itemid = ($(currentFormId).find("#itemid").val()) ? $(currentFormId).find("#itemid").val() : '';
	
				//alert(" M:" + module + " A:" + action + " T:" + task + " L:" + lang_id + " P:" + path + " I:" + itemid);
	
				$.ajax({
					type: "POST",
					url: base_url + "oc_ajax.php",
					data: formData,
					mimeType:"multipart/form-data",
					cache: false,
					contentType: false,
					processData:false
				}).done(function (data) {
					//console.log(data);  // this is currently returning FALSE // Which is totally correct!
					//alert("data = " + data);
					if (data == true || data > 0) {
						//Loading Sweet Alert
						swal({
							title: "Your data was successfully submitted",
							text: "Please waiting for a moment.",
							type: "success",
							timer: 4000,
							showConfirmButton: false
						});
						//window.location = base_url + 'pws-admin/index.php?module=' + module + "&action=list" + "&task=" + task + "&lang_id=" + lang_id;
						window.location = base_url + 'pws-admin/index.php?module=' + module + "&action=" + action + "&itemid=1&task=" + task + "&lang_id=" + lang_id;
					} else {
						//Loading Sweet Alert
						swal({
							title: "OPP!! Something wrong",
							text: "Please checking your data again.",
							type: "warning",
							timer: 4000,
							showConfirmButton: false
						});
					}
	
				});
	
				return false; // Don't submit form
			});
	
			//Set Summernote Upload and Delele function
			initSummernote();
			/*
			$(".summernote").summernote({
				height: 300,                 // set editor height
				minHeight: null,             // set minimum height of editor
				maxHeight: null,             // set maximum height of editor
				focus: false                 // set focus to editable area after initializing summernote
			});
			*/
			
		});
		

    };
	

    var initListViewTables = function() {
        "use strict";
        0 !== $("#datatable-buttons").length && $("#datatable-buttons").DataTable({
            dom: "Bfrtip",
            "columnDefs": [{
                "defaultContent": "-",
                "targets": "_all"
            }],
            buttons: [{
                extend: "copy",
                className: "btn-sm"
            }, {
                extend: "csv",
                className: "btn-sm"
            }, {
                extend: "excel",
                className: "btn-sm"
            }, {
                extend: "pdf",
                className: "btn-sm"
            }, {
                extend: "print",
                className: "btn-sm"
            }],
            responsive: !0
        })

        //handleUpdateButton
        $(document).on("click",".btn-attr-item", function(e) {
            e.preventDefault();
            var module = $(this).data('module');
            var action = $(this).data('action');
            var task = $(this).data('task');
            var lang_id = $(this).data('lang_id');
            var path = $(this).data('path');
            var itemid = $(this).data('id');
/*
            var module = (!module || "") ? '' : $(this).data('module');
            var action = (!action || "") ? '' : $(this).data('action');
            var task = (!task || "") ? '' : $(this).data('task');
            var lang_id = (!lang_id || "") ? '' : $(this).data('lang_id');
            var path = (!path || "") ? '' : $(this).data('path');
            var itemid = (!itemid || "") ? '' : $(this).data('itemid');
*/

            window.location = base_url + 'pws-admin/index.php?module=' + module + "&action=" + action + "&task=" + task + "&path=" + path + "&lang_id=" + lang_id + "&itemid=" + itemid;
        });

        //handleDeleteButton
        $(document).on("click",".delete-item", function(e) {
            e.preventDefault();
            var module = $(this).data('module');
            var action = $(this).data('action');
            var task = $(this).data('task');
            var lang_id = $(this).data('lang_id');
            var path = $(this).data('path');
            var itemid = $(this).data('id');

            var whtrow = $(this);
            //var rowindex = $(this).closest('tr').index();
            //rowindex++;
            //row = rowindex;

            if(itemid) {
                swal({
                    title: "Are you sure?",
                    text: "Are you sure that you want to delete this record?",
                    type: "warning",
                    showCancelButton: true,
                    closeOnConfirm: false,
                    confirmButtonText: "Yes, delete it!",
                    confirmButtonColor: "#ec6c62"
                }, function() {
                    $.ajax({
                        type: "POST",
                        url: base_url + "oc_ajax.php",
                        data: "module=" + module + "&action=" + action + "&task=" + task + "&path=" + path + "&lang_id=" + lang_id + "&itemid=" + itemid,
                        success: function(data){
                        }
                    }).done(function(data) {
                        swal("Done!!", "Your data ID #" + itemid + " was successfully deleted!", "success");
                        whtrow.parents('td').closest("tr").remove('tr');
                        //document.getElementById("Table"+table_id).deleteRow(row);
                    }).error(function(data) {
                        swal("Oops", "We couldn't connect to the server!", "error");
                    });
                });
            }
        });
    };

    var initSummernote = function () {
        $('.summernote').summernote({
            height: 350,
            /*cleaner:{
                notTime:2400, // Time to display Notifications.
                action:'both', // both|button|paste 'button' only cleans via toolbar button, 'paste' only clean when pasting content, both does both options.
                newline:'<br>', // Summernote's default is to use '<p><br></p>'
                notStyle:'position:absolute;top:0;left:0;right:0', // Position of Notification
                icon:'<i class="note-icon">[Your Button]</i>',
                keepHtml: false, //Remove all Html formats
                keepClasses: false, //Remove Classes
                badTags: ['style','script','applet','embed','noframes','noscript', 'html'], //Remove full tags with contents
                badAttributes: ['style','start'] //Remove attributes from remaining tags
            },
            */
            callbacks: {
                onImageUpload : function(files, editor, welEditable) {
                    for(var i = files.length - 1; i >= 0; i--) {
                        sendFile(files[i], this);
                    }
                },
                onMediaDelete : function($target, editor, welEditable) {
                    //console.log($target); // img
                    //console.log($target[0].src); // img

                    image_url = $target[0].src;
                    if(image_url){
                        var deleteok = deleteFile(image_url, this);
                    }
                    if(deleteok){
                        //remove element in editor
                        $target.remove();
                    }
                }
            }
        });

        function sendFile(file, el) {
            var form_data = new FormData();

            var module = $("#module").val();
            var action = $("#action").val();
            var task = $("#task").val();
            var lang_id = $("#lang_id").val();
            var path = $("#path").val();
            var itemid = $("#itemid").val();
            if(task == ""){
                task = "summernoteUpload";
            }

            //alert(" M:" + module + " A:" + action + " T:" + task + " L:" + lang_id + " P:" + path + " I:" + itemid);

            form_data.append('file', file);
            $.ajax({
                data: form_data,
                type: "POST",
                url: base_url + 'oc_ajax.php?module=' + module + "&action=" + action + "&task=" + task + "&lang_id=" + lang_id + "&path=" + path + "&itemid=" + itemid,
                cache: false,
                contentType: false,
                processData: false,
                success: function(url) {
                    $(el).summernote('editor.insertImage', url);
                },
                error : function(error) {
                    alert('error');
                },
                complete : function(response) {
                }
            });
        }

        function deleteFile(file, el) {
            var form_data = new FormData();

            var module = $("#module").val();
            var action = $("#action").val();
            var task = $("#task").val();
            var lang_id = $("#lang_id").val();
            var path = $("#path").val();
            var itemid = $("#itemid").val();
            if(task == ""){
                task = "summernoteDelete";
            }

            //alert(" M:" + module + " A:" + action + " T:" + task + " L:" + lang_id + " P:" + path + " I:" + itemid);

            form_data.append('file', file);
            $.ajax({
                data: form_data,
                type: "POST",
                url: base_url + 'oc_ajax.php?module=' + module + "&action=" + action + "&task=" + task + "&lang_id=" + lang_id + "&path=" + path + "&itemid=" + itemid,
                cache: false,
                contentType: false,
                processData: false,
                success: function(url) {
                    return true;
                    //$(el).summernote('editor.insertImage', url);
                },
                error : function(error) {
                    alert('error');
                },
                complete : function(response) {
                }
            });
        }
    };

    var handleDeletePhoto = function () {
		
        $(document).on("click",".delete-item-img", function(e) {
            e.preventDefault();

            var module = $(this).data('module');
            var action = $(this).data('action');
            var lang_id = $(this).data('lang_id');
            var path = $(this).data('path');
            var itemid = $(this).data('itemid');
            var content_id = $(this).data('content_id');
            var filename = $(this).data('filename');
            var task = $(this).data('task');
            if(task == ""){
                task = "delete_item";
            }

            //alert(" M:" + module + " A:" + action + " T:" + task + " L:" + lang_id + " P:" + path + " I:" + itemid + " C:" + content_id + " F:" + filename);

            if(itemid && content_id && filename && task) {
                DeletePhoto(module, action, lang_id, path, itemid, content_id, filename, task);
            }
        });
    }

    function DeletePhoto(module, action, lang_id, path, itemid, content_id, filename, task) {
        if(itemid) {
            swal({
                title: "Are you sure?",
                text: "Are you sure that you want to delete this record?",
                type: "warning",
                showCancelButton: true,
                closeOnConfirm: false,
                confirmButtonText: "Yes, delete it!",
                confirmButtonColor: "#ec6c62"
            }, function() {
                $.ajax({
                    type: "POST",
                    url: base_url + "oc_ajax.php",

                    //data: {"module": module, "action": action, "task": task, "lang_id": lang_id, "path": path, "itemid": itemid, "filename": filename, "content_id": content_id},
                    data: "module=" + module + "&action=" + action + "&task=" + task + "&path=" + path + "&lang_id=" + lang_id + "&itemid=" + itemid + "&content_id=" + content_id + "&filename=" + filename,
                    success: function(data){
                        console.log(data);  // this is currently returning FALSE // Which is totally correct!
                    }
                }).done(function(data) {
					//alert("RETURN " + data);
					if(task == "delete_header_photo_item"){
						swal("Done!!", "Your Photo was successfully deleted!", "success");
						if(module == "contents" && content_id == 1)
						{
						window.location = base_url + 'pws-admin/index.php?module=' + module + "&action=update&itemid=1&task=" + task + "&lang_id=" + lang_id;
						}else{
						window.location = base_url + 'pws-admin/index.php?module=' + module + "&action=update&itemid=" + itemid + "&task=" + task + "&lang_id=" + lang_id;
						}
						
					}else{
						swal("Done!!", "Your data ID #" + itemid + " was successfully deleted!", "success");
	
						$( "div" ).remove(".photo" + itemid);
						$(".portfolioContainer").load();
					}
                    //document.getElementById("Table"+table_id).deleteRow(row);
                }).error(function(data) {
                    swal("Oops", "We couldn't connect to the server!", "error");
                });
            });
        }
    }


    return {
        //main function
        init: function () {
            //initialize here something.
            Portlet();
            initNiceScrollPlugin();
            initSlimScrollPlugin();
        },
        initList: function(){
            initListViewTables();
            initMagnificPopup();
        },
        initForm: function() {
            initSelect2(); // Put Select2 on the top of parley validation
            initIconPicker();
            initMagnificPopup();
            initCreateUpdateForm();
			initCreateMultiUpdateForm();
        },
		initDeletePhoto: function(){
			//alert("initDeletePhoto");
			handleDeletePhoto();
		}		
    };

}();