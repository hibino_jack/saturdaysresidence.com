<?php
require_once("pws-core/site.config.php");
//
//echo "<br>".PATH_THEMES;
//echo "<br>".PATH_THEMES_ROOT;
//
//echo "<br>".SITE_ROOT;
//echo "<br>".PATH_ROOT;
//echo "<br>".BASE_URL;
//echo "<br>".PATH_URL;
//echo "<br>".DOCUMENT_ROOT;
//echo "<br>";
//
//echo "<br>".PATH_CORE;
//echo "<br>".PATH_CLASSES;
//echo "<br>".PATH_ASSETS;
//echo "<br>".PATH_LANGUAGES;
//
//echo "<br>".PATH_ADMIN;
//echo "<br>".PATH_ADMIN_INC;
//echo "<br>".PATH_ADMIN_MODULES;
//
//echo "<br>".PATH_MEDIA;
//echo "<br>".PATH_IMAGES;
//echo "<br>".PATH_UPLOAD;
//
//echo "<br>".BASE_CACHE;
//echo "<br>".BASE_CORE;
//echo "<br>".BASE_ASSETS;
//echo "<br>".BASE_CLASSES;
//
//echo "<br>".BASE_ADMIN;
//echo "<br>".BASE_MEDIA;
//echo "<br><br>";
//PRE($_GET); exit;
//PRE($_POST);
//PRE($oConfig);
//set contents module by default
if(file_exists(PATH_ADMIN_MODULES_ROOT.'contents/classes/class.contents.php')) {
    require_once(PATH_ADMIN_MODULES_ROOT . 'contents/classes/class.contents.php');
    $contents = new Contents();

    //Get Content
    if($_GET) {
        $data_content = $contents->get($_GET['lang_id'], $_GET['filename']);
    } else {
        $data_content = $contents->get($lang_id, 'index');
    }
}

if(empty($data_content)) {
    $error404 = true;
} else {
    extract($data_content[0]);
}

//print_r($data);
if(empty($meta_title)) { $meta_title = $oConfig->meta_description; }
if(empty($meta_keyword)) { $meta_keyword = $oConfig->meta_keywords; }
if(empty($meta_description)) { $meta_description = $oConfig->meta_description; }

$map_image = PATH_THEMES."img/map.png";
if($oConfig->file_map != "")
{
	$pic = BASE_UPLOAD."config/1/".$oConfig->file_map;
	$pic_realpath = PATH_UPLOAD_ROOT."config/1/".$oConfig->file_map;
	if(is_file($pic_realpath)) {
		$map_image = PATH_ROOT."timthumb.php?src=".$pic."&w=1423&h=796";
		//$map_image = $pic;
	}
}

$logo = PATH_THEMES."img/logo.png";
if($oConfig->file_logo != "")
{
	$pic = BASE_UPLOAD."config/1/".$oConfig->file_logo;
	$pic_realpath = PATH_UPLOAD_ROOT."config/1/".$oConfig->file_logo;
	if(is_file($pic_realpath)) {
		$logo = PATH_ROOT."timthumb.php?src=".$pic."&w=110&h=90";
	}
}


//call and render theme
if($error404) {
    include("error404.php");
} else {
    include(PATH_THEMES_ROOT."index.php");
}
exit();
?>
