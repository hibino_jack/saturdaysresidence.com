<?php 
$module2 = "Press";
$module_class2 = strtolower($module2);

if(file_exists(PATH_ADMIN_MODULES_ROOT.$module_class2.'/classes/class.'.$module_class2.'.php')) {    
	require_once(PATH_ADMIN_MODULES_ROOT.$module_class2 . '/classes/class.'.$module_class2.'.php');    
	$$module_class2 = new $module2();
	
	$press_data = $$module_class2->select();
	//PRE($press_data);
}

?>
<div class="field-guide-rightside  col-md-4">
    
    <!-- saturdays-introduction-->
    <div class="row saturdays-introduction">
        
            <div class="col-md-4">
                <img src="<?php echo $logo; ?>" class="img-responsive">
                
            </div>
            <div class="col-md-8">
                <p>Five bespoke holiday-home concepts inspired by Bostonesque art deco, each designed to cater to a specific living style for couples and families, where every day feels like Saturday. </p>
                <p><a href="<?php echo BASE_URL.$lang_id; ?>/our-story/">FIND OUT MORE ></a></p>
            </div>
       
     </div>
     
     <hr>
     
     <?php if(is_array($press_data) && (count($press_data) > 0)){ 
     for($i=0; $i < 2; $i++){
         
        $image_header = "http://placehold.it/800x550&text=".$press_data[$i]['name'];	
        if($press_data[$i]['photoname'] != "")
        {
            $pic = BASE_UPLOAD."press/".$press_data[$i]['id']."/".$press_data[$i]['photoname'];
            $pic_realpath = PATH_UPLOAD_ROOT."press/".$press_data[$i]['id']."/".$press_data[$i]['photoname'];
            if(is_file($pic_realpath)) {
                $image_header = PATH_ROOT."timthumb.php?src=".$pic."&w=800&h=550";
            }
        }
     ?>
     <!--SAturday Latest Offers-->
     <div class="row saturdays-offer">
        <div class="col-md-12">
            <img src="<?php echo $image_header; ?>" class="img-responsive">
        </div>
        <div class="col-md-12 text-center">
            <h3><?php echo $press_data[$i]["name"]; ?></h3>
            <p><?php echo substr(strip_tags($press_data[$i]["description"]),0,60); ?>...</p>
             <p><a href="<?php echo BASE_URL.$lang_id."/press/".$press_data[$i]["filename"]; ?>.html" class="btn btn-default">more details</a></p>
        </div>
     </div>
     <?php }//for
     }//if ?>
                                   
   <hr>
   
   
   <!--Gallery-->
   <div class="row little-gallery">
    <div class="col-md-12">
       <ul id="nav-instafeed" class="row">
       </ul>
       
       <div class="row text-center">
            <h4> #saturdaysresidence </h4>
            <a href="https://www.instagram.com/saturdayscondo/" target="_blank">follow us on instagram > </a>
       </div>
      </div>
      
   </div>
   
     
</div>