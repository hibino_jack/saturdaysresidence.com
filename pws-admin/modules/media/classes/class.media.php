<?php
class Media extends DB {
    var $tbModules = "pws_modules";
    var $tbMedia = "pws_media";

    function __construct() {
        $this->Page = new Page();
        $this->Page->module_name = "Media"; // ucfirst
        $this->module_name = strtolower($this->Page->module_name); // strtolower
    }

    function select($id = NULL, $module_id = NULL, $categories_id = NULL, $is_active = NULL, $query_type = NULL) {
        if(!isset($query_type))
            $query_type = "fetchAssoc";

        $sql = "SELECT * FROM `$this->tbMedia`";

        if($id)
            $ct[] = "`$this->tbMedia`.`id` = '$id' ";
        if($module_id)
            $ct[] = "$this->tbMedia.`module_id` = '$module_id' ";
        if($categories_id)
            $ct[] = "$this->tbMedia.`categories_id` = '$categories_id' ";
        if($is_active)
            $ct[] = "`$this->tbMedia`.`is_active` = '$is_active' ";

        if(is_array($ct))
            $sql = $sql." WHERE ".implode(" AND ", $ct);

        //PRE($sql);
        $data = DB::query($sql, $query_type);
        return $data;
    }

    function insert($post_vars) {
        global $oUser;

        if($post_vars)
            extract($post_vars);

        //Upload Section
        if ($_FILES['file']['tmp_name']) {
            //print_r($_FILES);exit;
            if($_FILES['file']["name"] != "") {
                $updateFile = uploadFile($module, $itemid, NULL, "photo", NULL, NULL);
            }
        }
        //End Upload Section

        //upload file ok insert data to database
        if($updateFile) {
            if(!isset($created_by))
                $created_by = $oUser->id;

            if(!isset($title))
                $title = ucfirst($module)." Photo ID#".$itemid;
            if(!isset($categories_id))
                $categories_id = 1;
            if(!isset($is_default))
                $is_default = "N";
            if(!isset($is_flag))
                $is_flag = "N";
            if(!isset($is_active))
                $is_active = "Y";

            $title = DB::escape($title);

            $field = "`id`, `module_id`, `categories_id`, `title`, `filename`, `is_default`, `is_flag`, `created_by`, `created_date`, `is_active`";
            $value = "'', '$itemid', '$categories_id', '$title', '$updateFile', '$is_default', '$is_flag', '$created_by', NOW(), '$is_active'";
            $sql = "INSERT INTO $this->tbMedia ($field) VALUES ($value)";
            $id = DB::query($sql, "lastInsertId");

            if($id) {
                //return $id;
                $data = array(
                    "id" => $id,
                    "module_id" => $itemid,
                    "filename" => $updateFile,
                    "status" => "sucess"
                );
                return json_encode($data);
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    //`pws_media` (`id`, `module_id`, `categories_id`, `title`, `filename`, `is_default`, `is_flag`, `created_by`, `created_date`, `is_active`)
    function update($post_vars) {
        global $oUser;

        if($post_vars)
            extract($post_vars);

        if(!isset($created_by))
            $created_by = $oUser->id;

        $title = DB::escape($title);

        $f = "title = '$title'";

        if($module_id)
            $f.= ", module_id = '$module_id' ";
        if($categories_id)
            $f.= ", categories_id = '$categories_id' ";
        if($filename)
            $f.= ", filename = '$filename' ";
        if($is_default)
            $f.= ", is_default = '$is_default' ";
        if($is_flag)
            $f.= ", is_flag = '$is_flag' ";
        if($created_by)
            $f.= ", created_by = '$created_by' ";

        //$f.= ", updated_date = NOW() ";
        if($is_active)
            $f.= ", is_active = '$is_active' ";

        $sql = "UPDATE $this->tbMedia SET $f WHERE $this->tbMedia.id = '$itemid' ";
        //echo $sql;
        //exit;
        $data = DB::query($sql);
        if($data){
            return $data;
        } else {
            return false;
        }
    }

    function delete($post_vars) {
        if($post_vars)
            extract($post_vars);

        if($itemid) {
            //delete
            $deleteok = removeFile($module_id, $module, $filename);
            if($deleteok) {
                $sql = "DELETE FROM `$this->tbMedia` WHERE `$this->tbMedia`.`id` = '$itemid' ";
                $data = DB::query($sql);
                if($data) {
                    return true;
                }
            }
        } else {
            return false;
        }
    }

    function delete_all($post_vars) {
        if($post_vars)
            extract($post_vars);
        //PRE($post_vars);

        if($itemid) {
            $data = $this->select(NULL, $itemid);
            if($data) {
                //PRE($data);
                foreach($data as $item) {
                    //PRE($item);
                    //delete
                    //echo $item['filename']."<br>";
                    $deleteok = removeFile($itemid, $module, $item['filename']);
                    if($deleteok) {
                        $sql = "DELETE FROM `$this->tbMedia` WHERE `$this->tbMedia`.`id` = '".$item['id']."'";
                        $delete_from_table_ok = DB::query($sql);
                        if($delete_from_table_ok) {
                            unset($deleteok, $delete_from_table_ok, $sql);
                        }
                    }
                }
            }

        } else {
            return false;
        }
    }

    //Upload dropzone photo
    function dropzoneUpload($post_vars) {
        if($post_vars)
            extract($post_vars);

        //PRE($post_vars);
        if ($_FILES['file']['name']) {
            if (!$_FILES['file']['error']) {
                $name = "smn_".$this->module_name."_".date("YmdHis");
                $ext = explode('.', $_FILES['file']['name']);
                $filename = $name . '.' . $ext[1];

                //$destination = '/saturdaysresidence.com/media/dropzone/' . $filename; //change this directory
                if($itemid) {
                    $target_dir = PATH_UPLOAD_ROOT.$this->module_name."/".$itemid."/";
                    $target_url = BASE_UPLOAD.$this->module_name."/".$itemid."/";
                } else {
                    $target_dir = PATH_UPLOAD_ROOT.$this->module_name."/dropzone/";
                    $target_url = BASE_UPLOAD.$this->module_name."/dropzone/";
                }
                //create folder
                if (!file_exists($target_dir)) {
                    MKDIRS($target_dir);
                }
                $destination =  $target_dir.$filename;
                $location = $_FILES["file"]["tmp_name"];
                $uploadok = move_uploaded_file($location, $destination);
                if($uploadok){
                    //insert in to database
                    //$page_content = $$module_class->dropzoneUpload($module, $action, $task, $itemid, $filename);

                    //$field = "is_active = '$is_active' ";
                    //$sql = "UPDATE $this->tbContents SET $field WHERE id = '$id'";
                    //return $data = DB::query($sql);
                }
                echo $target_url.$filename;//change this URL
            } else {
                echo  $message = 'Ooops! Your upload triggered the following error:  '.$_FILES['file']['error'];
            }
        }
    }

    //Delete dropzone photo
    function dropzoneDelete($post_vars) {
        if($post_vars)
            extract($post_vars);

        //PRE($post_vars);
        $file_name_array = explode('/',$file);
        $file_name = end($file_name_array);
        //echo "<br />file name: ".$file_name;

        $file_path = str_replace(BASE_UPLOAD, PATH_UPLOAD_ROOT, $file);
        //echo "<br />file name: ".$file;
        //echo "<br />file path: ".$file_path;
        $sql = "SELECT id FROM `$this->tbMedia` WHERE `$this->tbMedia`.`filename` = '$file_name'";
        $itemid = DB::query($sql, "fetchColumn");
        unset($sql);

        $delete_result = false;
        if(isset($file_path) && is_file($file_path)) {
            $delete_result = unlink($file_path);
        }

        if($delete_result) {
            //delete data from database
            $sql = "DELETE FROM `$this->tbMedia` WHERE `$this->tbMedia`.`id` = '$itemid' ";
            $data = DB::query($sql);
            if($data) {
                return true;
            }
            return true;
        } else {
            echo 'Uh oh, error :(';
            return false;
        }
    }

}
?>