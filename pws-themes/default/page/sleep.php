<?php
$module = "Sleep";
$module_class = strtolower($module);

if (file_exists(PATH_ADMIN_MODULES_ROOT . $module_class . '/classes/class.' . $module_class . '.php')) {
    require_once(PATH_ADMIN_MODULES_ROOT . $module_class . '/classes/class.' . $module_class . '.php');
    $$module_class = new $module();

    $data = $$module_class->select();
}

//PRE($data);
$data_content[0]['description'] = str_ireplace(array("\r", "\n", '\r', '\n'), '', $data_content[0]['description']);
$data_content[0]['description2'] = str_ireplace(array("\r", "\n", '\r', '\n'), '', $data_content[0]['description2']);
?>
<section class="content-area">
    <div class="page-heading">
        <div class="container">
            <div class="row">
                <div class="col-md-offset-3 col-md-6 text-center">
                    <h1><?php echo $data_content[0]['title']; ?></h1>
                    <?php if (!empty($data_content[0]['subtitle'])) { ?>
                        <h3 style="font-style:italic"><?php echo $data_content[0]['subtitle']; ?></h3>
                    <?php }
                    if (!empty($data_content[0]['short_description'])) { ?>
                        <p><?php echo $data_content[0]['short_description']; ?></p>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <?php if (!empty($data_content[0]['description2'])) { ?>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="row innerpage_intro">
                    <?php echo $data_content[0]['description2']; ?>
                </div><!--//saturdays_intro-->
            </div><!--//col-md-6-->
        </div>
      </div>
			<?php /*
            <div id="innerpage_intro">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-offset-1 col-lg-10">
                        	<div class="row innerpage_intro">
                            <?php //echo $data_content[0]['description2']; ?>
                            </div><!--//saturdays_intro-->
                        </div><!--//col-md-6-->
                 	</div>
                 </div>
              </div> */?>
    <?php } ?>

    <div class="room-filters">
        <div class="container">
            <div class="row">
                <h4 style="margin-top:0;"><span>Filters</span></h4>
                <div class="col-md-offset-2 col-md-8 room-filters-box">
                    <div class="row">
                        <div class="col-sm-4">
                            <select name="no_of_bedrooms" id="no_of_bedrooms" class="form-select">
                                <option value="" selected="selected">No.of bedrooms</option>
                                <option value="1_bedrooms">1 Bedroom</option>
                                <option value="2_bedrooms">2 Bedrooms</option>
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <select name="room_view" id="room_view" class="form-select">
                                <option value="" selected="selected">All view</option>
                                <option value="pool_access">Pool Access</option>
                                <option value="pool_view">Pool View</option>
                                <option value="sea_view">Sea View</option>
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <select name="room_type" id="room_type" class="form-select">
                                <option value="" selected="selected">All Room type</option>
                                <option value="learn">Learn</option>
                                <option value="live">Live</option>
                                <option value="lux">Lux</option>
                            </select>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <!-- paragraphs-3box Section -->
    <?php
    if (is_array($data) && (count($data) > 0)) {
        ?>
        <div class="paragraphs-3box room-type">
            <div class="container">
                <div class="row paragraphs-3box-items">

                    <?php
                    foreach ($data as $item) {
                        //PRE($item);

                        $image_header = "http://placehold.it/800x550&text=" . $item["name"];
                        if ($item["photoname"] != "") {
                            $pic = BASE_UPLOAD . $module_class . "/" . $item["id"] . "/" . $item["photoname"];
                            $pic_realpath = PATH_UPLOAD_ROOT . $module_class . "/" . $item["id"] . "/" . $item["photoname"];
                            if (is_file($pic_realpath)) {
                                $image_header = PATH_ROOT . "timthumb.php?src=" . $pic . "&w=800&h=550";
                            }
                        }

                        ?>
                        <!--BOX-->
                        <div class="col-md-4 filter-div" data-roomtype="<?php echo $item["ft_roomtype"]; ?>"
                             data-bedrooms="<?php echo $item["ft_bedrooms"]; ?>"
                             data-view="<?php echo $item["ft_view"]; ?>">
                            <div class="row room-type-items">
                                <div class="col-sm-12">

                                    <div class="paragraphs-image">
                                        <a href="<?php echo $item["filename"]; ?>.html"><img
                                                    src="<?php echo $image_header; ?>" class="img-responsive"></a>
                                    </div>
                                    <div class="paragraphs-content">
                                        <h2 class="room-type-title"><?php echo $item["name"]; ?></h2>
                                        <p class="room-type-desc"><?php echo $item["room_type"]; ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php

                    }
                    ?>
                </div>
            </div>
        </div>
        <?php
    }
    ?>


    <!-- hotel-policy Section -->
    <?php echo $data_content[0]['description']; ?>

</section>
     