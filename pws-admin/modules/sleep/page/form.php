<?php
if($action == "update")    {
    if($$module_class->itemid) {
        $data = $$module_class->select($lang_id, $$module_class->itemid);
        $itemid = $$module_class->itemid;
        if($data){
            extract($data[0]);
            //PRE($data);
        }
    }
}
?>

<div class="row">
    <div class="col-md-12">
        <?php echo $languages->dropdown_languages($module, $action, $path, $itemid, $task); ?>
        <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"><?php echo $module_class." ".$action;?></h3>
        </div>
        <div class="panel-body">
            <form name="create-form" id="create-form" class="form-horizontal" role="form" data-parsley-validate novalidate enctype="multipart/form-data">
                <div class="alert alert-danger alert-dismissable hidden">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <h4>Oh snap!</h4>
                    <p>This form seems to be invalid :(</p>
                </div>
                <div class="alert alert-success alert-dismissable hidden">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <h4>Yay!</h4>
                    <p>Everything seems to be ok., Re-submit your form then. :)</p>
                </div>

                <div class="row">
                    <div class="col-md-12">
                    
                        <div class="form-group">
                            <label class="col-md-2 control-label">Filename*</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" name="filename" parsley-trigger="change" required placeholder="Enter Filename" value="<?php echo $filename;?>">
                                <span class="font-13 text-muted">e.g. "http://www.domain.com/filename"</span>
                            </div>
                        </div>
                    
                        <div class="form-group">
                            <label class="col-md-2 control-label">Name</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" name="name" parsley-trigger="change" required placeholder="Enter Name" value="<?php echo $name;?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label">Room Type</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" name="room_type" parsley-trigger="change" required placeholder="Enter Room Type" value="<?php echo $room_type;?>">
                            </div>
                        </div>                        

                        <div class="form-group">
                            <label class="col-md-2 control-label">Short Description</label>
                            <div class="col-md-10">
                                <textarea class="form-control summernote" rows="3" name="short_description" id="short_description" parsley-trigger="change" required><?php echo $short_description;?></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label">Description</label>
                            <div class="col-md-10">
                                <textarea class="form-control summernote" rows="8" name="description" id="description" parsley-trigger="change" required><?php echo $description;?></textarea>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-md-2 control-label">Room Type</label>
                            <div class="col-md-4">
                            <?php
							$roomtype_data =  array("learn" => "Learn","live" => "Live","lux" => "Lux");
							echo DROPDOWNMANUAL("ft_roomtype",$ft_roomtype, $roomtype_data,NULL," class='selectpicker' data-style='btn-white'",NULL,"no_default");
							?>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-md-2 control-label">View</label>
                            <div class="col-md-4">
                            <?php
							$view_data =  array("pool_access" => "Pool Access","pool_view" => "Pool view","sea_view" => "Sea view");
							echo DROPDOWNMANUAL("ft_view",$ft_view, $view_data,NULL," class='selectpicker' data-style='btn-white'",NULL,"no_default");
							?>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-md-2 control-label">No. of Bedrooms</label>
                            <div class="col-md-4">
                            <?php
							$bedroom_data =  array("1_bedrooms" => "1 Bedroom","2_bedrooms" => "2 Bedrooms");
							echo DROPDOWNMANUAL("ft_bedrooms",$ft_bedrooms, $bedroom_data,NULL," class='selectpicker' data-style='btn-white'",NULL,"no_default");
							?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label">Photo</label>
                            <div class="col-md-10">
                                <input type="file" name="photoname" id="photoname" class="filestyle" data-buttonbefore="true"><br />
                                <?php
                                $pic = BASE_UPLOAD.$module."/".$id."/".$photoname;
                                $pic_realpath = PATH_UPLOAD_ROOT.$module."/".$id."/".$photoname;
                                if(is_file($pic_realpath)) {
                                    ?>
                                    <div class="col-sm-4"  style="margin-top:15px; padding-left:0;">
                                    <input type="hidden" id="old_photoname" name="old_photoname" value="<?php echo $photoname; ?>">
                                    <a class="image-popup" href="<?php echo $pic; ?>" title="Logo">
                                        <img src="<?php echo PATH_ROOT; ?>timthumb.php?src=<?php echo $pic; ?>&w=250&h=200" width="250" height="200">
                                    </a>
                                    </div>
                                <?php }else{ ?>
                                <div class="col-sm-4" style=" padding-left:0;">
                                        <img src="<?php echo BASE_IMAGES; ?>nophoto.jpg" width="250" height="200" style="margin-top:15px"/>
                                 </div>
                                <?php } ?>

                                <?php if($photoname){ ?>
                            <div class="btn-action" style="margin-top:15px;">
                                <a href="javascript:void(0);" class="btn btn-danger btn-sm delete-item-img"
                                   data-module="<?php echo $module;?>"
                                   data-action="header_photo"
                                   data-task="delete_header_photo_item"
                                   data-lang_id="<?php echo $lang_id;?>"
                                   data-path="cp"
                                   data-filename="<?php echo $photoname;?>"
                                   data-content_id="<?php echo $itemid;?>"
                                   data-itemid="<?php echo $id; ?>"><i class="md md-close"></i></a>
                            </div>
								<?php } ?>

                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label">Meta Title</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" name="meta_title" parsley-trigger="change" required placeholder="Enter short description" value="<?php echo $meta_title;?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label">Meta Keyword</label>
                            <div class="col-md-10">
                                <input type="text" value="<?php echo $meta_keyword;?>" name="meta_keyword" parsley-trigger="change" required data-role="tagsinput" placeholder="Add your keywords"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label">Meta Description</label>
                            <div class="col-md-10">
                                <textarea class="form-control" rows="5" name="meta_description" parsley-trigger="change" required><?php echo $meta_description;?></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Active</label>
                            <div class="col-sm-10">
                                <?php
                                $is_active_data = array("Y" => "Enable", "N" => "Disabled");
                                echo RADIOITEMMANUAL("is_active", $is_active, $is_active_data);
                                ?>
                            </div>
                        </div>

                        <!--<div class="form-group">
                            <label class="col-sm-2 control-label">Show on homepage</label>
                            <div class="col-sm-10">
                                <?php
                                $flag_homepage_data = array("Y" => "Yes");
                                echo CHECKBOXITEMMANUAL("flag_homepage", $flag_homepage, $flag_homepage_data, "data-parsley-errors-container='#span_error_flag_homepage'");
                                ?>
                            </div>
                        </div>-->
                    </div>

                </div>

                <div class="row">
                <div class="col-md-12">
                    <div class="form-group pull-right">
                        <input type="hidden" id="module" name="module" value="<?php echo $module; ?>">
                        <input type="hidden" id="action" name="action" value="<?php echo $action; ?>">
                        <input type="hidden" id="task" name="task" value="<?php echo $task; ?>">
                        <input type="hidden" id="lang_id" name="lang_id" value="<?php echo $lang_id; ?>">
                        <input type="hidden" id="path" name="path" value="cp">

                        <input type="hidden" id="itemid" name="itemid" value="<?php echo $itemid; ?>">

                        <input type="hidden" id="created_by" name="created_by" value="<?php echo (empty($created_by) || $created_by == "0") ? $oUser->id : $created_by; ?>">
                        <input type="hidden" id="updated_by" name="updated_by" value="<?php echo (empty($updated_by) || $updated_by == "0") ? $oUser->id : $updated_by; ?>">

                        <button type="submit" class="btn btn-default waves-effect waves-light btn-lg">Submit</button>
                        <button type="reset" class="btn btn-white waves-effect waves-light btn-lg">Cancel</button>
                    </div>
                </div>
                </div>
            </form>

        </div>
    </div>

    </div>
</div>