<?php
require_once("pws-core/site.config.php");
//print_r($_POST);
//PRE($oConfig);

if (isset($_POST["module"])) {
    if ($_POST["module"] == "footer-subscribe") {
        $module = "Subscription";
        $module_class = strtolower($module);

        if (file_exists(PATH_ADMIN_MODULES_ROOT . $module_class . '/classes/class.' . $module_class . '.php')) {
            require_once(PATH_ADMIN_MODULES_ROOT . $module_class . '/classes/class.' . $module_class . '.php');
            $$module_class = new $module();

            $insertok = $$module_class->subscribe($_POST["email"]);
            if ($insertok)
                echo "done";
            else
                echo "fail";
        }
    }

    if ($_POST["module"] == "gallery") {
        $ret = "";
        if (file_exists(PATH_ADMIN_MODULES_ROOT . 'contents/classes/class.contents.php')) {
            require_once(PATH_ADMIN_MODULES_ROOT . 'contents/classes/class.contents.php');
            $contents = new Contents();
            $data_gallery = $contents->Media->select(NULL, $_POST['content_id']);

            if (isset($data_gallery) && (count($data_gallery) > 11)) {
                for ($i = 12; $i < count($data_gallery); $i++) {
                    if ($data_gallery[$i]["filename"] != "") {
                        $pic = BASE_UPLOAD . "contents/" . $_POST['content_id'] . "/" . $data_gallery[$i]["filename"];
                        $pic_realpath = PATH_UPLOAD_ROOT . "contents/" . $_POST['content_id'] . "/" . $data_gallery[$i]["filename"];
                        if (is_file($pic_realpath)) {
                            $image_header = PATH_ROOT . "timthumb.php?src=" . $pic . "&w=250&h=250";
                        }
                    }

                    $ret .= "<li class='col-md-3 col-sm-4 col-xs-6'>";
                    $ret .= "<a data-fancybox='gallery' href='" . $pic . "' data-caption='Saturdays Residence'>";
                    $ret .= "<img src='" . $image_header . "' alt='Image' style='max-width:100%' />";
                    $ret .= "</a></li>";
                    ?>
                    <?php
                }//for
            }//if
        }//if

        echo $ret;
    }

    if ($_POST["module"] == "contact-form") {
        //recipient
        $to = $oConfig->contact_name . " <" . $oConfig->contact_email . ">";
        //$to = $_POST['fname'] . " " . $_POST['lname'] . " <" . $_POST['email'] . ">";

        //sender
        $from = $oConfig->contact_name . " <" . $oConfig->contact_email . ">";

        //subject and the html message
        $subject = 'Contact Message From ' . $_POST['fname'] . " " . $_POST['lname'];
        $message = '
				<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
				"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
				<html xmlns="http://www.w3.org/1999/xhtml">
				<head></head>
				<body>
				<table>
					<tr><td colspan="2">This is email message from contact form</td></tr>
					<tr><td colspan=2><br>Here is information :<br><br></td></tr>
					<tr><td>Name :</td><td>' . $_POST['fname'] . ' ' . $_POST['lname'] . '</td></tr>
					<tr><td>Email :</td><td>' . $_POST['email'] . '</td></tr>
					<tr><td>Phone :</td><td>' . $_POST['phone'] . '</td></tr>
					<tr><td>Message :</td><td>' . $_POST['msg'] . '</td></tr>
				</table>
				</body>
				</html>';

        //send the mail
        $result = quickmail($from, $to, $subject, $message, $cc = NULL);
        if ($result)
            echo "done";
        else
            echo "fail";
    }

    if ($_POST["module"] == "gather-form") {
        //recipient
        $to = $oConfig->contact_name . " <" . $oConfig->contact_email . ">";
        //$to = $_POST['fname'] . " " . $_POST['lname'] . " <" . $_POST['email'] . ">";

        //sender
        $from = $oConfig->contact_name . " <" . $oConfig->contact_email . ">";

        //subject and the html message
        $subject = 'Gather Reservation From ' . $_POST['fname'] . " " . $_POST['lname'];
        $message = '
				<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
				"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
				<html xmlns="http://www.w3.org/1999/xhtml">
				<head></head>
				<body>
				<table>
					<tr><td colspan="2">This is email message from gather reservation form</td></tr>
					<tr><td colspan=2><br>Here is information :<br><br></td></tr>
					<tr><td>Name :</td><td>' . $_POST['fname'] . ' ' . $_POST['lname'] . '</td></tr>
					<tr><td>Email :</td><td>' . $_POST['email'] . '</td></tr>
					<tr><td>Phone :</td><td>' . $_POST['phone'] . '</td></tr>
					<tr><td>City :</td><td>' . $_POST['city'] . '</td></tr>
					<tr><td>Country :</td><td>' . $_POST['country'] . '</td></tr>
					<tr><td>Type of event :</td><td>' . $_POST['type_event'] . '</td></tr>
					<tr><td>Date of event :</td><td>' . $_POST['date_event'] . '</td></tr>
					<tr><td>No. of attendees :</td><td>' . $_POST['attendees'] . '</td></tr>
					<tr><td>Need food & beverage :</td><td>' . $_POST['is_food'] . '</td></tr>
					<tr><td>Need break out rooms :</td><td>' . $_POST['is_beak_room'] . '</td></tr>
					<tr><td>Message :</td><td>' . $_POST['add_msg'] . '</td></tr>
				</table>
				</body>
				</html>';

        //send the mail
        $result = quickmail($from, $to, $subject, $message, $cc = NULL);
        if ($result)
            echo "done";
        else
            echo "fail";
    }

}

?>