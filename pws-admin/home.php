<?php
$total_users = $users->select(NULL, NULL, "Y", "count");
$total_languages = $languages->select(NULL, NULL, "Y", "count");

if(file_exists(PATH_ADMIN_MODULES_ROOT.'contents/classes/class.contents.php')) {
    require_once(PATH_ADMIN_MODULES_ROOT.'contents/classes/class.contents.php');
    $oContents = new Contents();
    $total_contents = $oContents->select($lang_id, NULL, "all", NULL, "count");
}

if(file_exists(PATH_ADMIN_MODULES_ROOT.'media/classes/class.media.php')) {
    require_once(PATH_ADMIN_MODULES_ROOT.'media/classes/class.media.php');
    $oMedia = new Media();
    $total_media = $oMedia->select(NULL, NULL, NULL, "Y", "count");
}

if(file_exists(PATH_ADMIN_MODULES_ROOT.'chats/classes/class.chats.php')) {
    require_once(PATH_ADMIN_MODULES_ROOT.'chats/classes/class.chats.php');
    $oChats = new Chats();
    //$data_chats = readAPIFile($oChats->tbChats);
}
?>

<div class="row">
    <div class="col-lg-3 col-sm-6">
        <div class="widget-panel widget-style-2 bg-white">
            <i class="md md-filter-9-plus text-primary"></i>
            <h2 class="m-0 text-dark counter font-600"><?php echo $total_contents; ?></h2>
            <div class="text-muted m-t-5">Contents</div>
        </div>
    </div>
    <div class="col-lg-3 col-sm-6">
        <div class="widget-panel widget-style-2 bg-white">
            <i class="md md-collections text-pink"></i>
            <h2 class="m-0 text-dark counter font-600"><?php echo $total_media; ?></h2>
            <div class="text-muted m-t-5">Photos</div>
        </div>
    </div>
    <div class="col-lg-3 col-sm-6">
        <div class="widget-panel widget-style-2 bg-white">
            <i class="md md-translate text-info"></i>
            <h2 class="m-0 text-dark counter font-600"><?php echo $total_languages; ?></h2>
            <div class="text-muted m-t-5">Languages</div>
        </div>
    </div>
    <div class="col-lg-3 col-sm-6">
        <div class="widget-panel widget-style-2 bg-white">
            <i class="md md-account-child text-custom"></i>
            <h2 class="m-0 text-dark counter font-600"><?php echo $total_users; ?></h2>
            <div class="text-muted m-t-5">Users</div>
        </div>
    </div>

</div>

<?php
unset($oContents, $total_contents);
unset($oMedia, $total_media);
unset($total_languages, $total_users);
//PRE($_SESSION);
?>
<div class="row">
    <div class="col-md-4 col-lg-3">
        <div class="profile-detail card-box">
            <div>
                <?php
                $pic = BASE_UPLOAD."users/".$oUser->id."/".$oUser->profile_photo;
                $pic_realpath = PATH_UPLOAD_ROOT."users/".$oUser->id."/".$oUser->profile_photo;
                if(is_file($pic_realpath)) {
                    ?>
                    <img src="<?php echo $pic; ?>" class="img-circle" alt="<?php echo $oUser->first_name." ".$oUser->last_name; ?>">
                <?php }else{ ?>
                    <img src="<?php echo BASE_IMAGES; ?>nophoto.jpg" class="img-circle" alt="<?php echo $oUser->first_name." ".$oUser->last_name; ?>">
                <?php } ?>

                <hr>

                <div class="text-left">
                    <p class=""><strong class="m-l-0"><?php echo $oUser->first_name." ".$oUser->last_name; ?></strong></p>

                    <p class="text-muted font-13"><strong>Mobile :</strong> <br><span class="m-l-0">-</span></p>

                    <p class="text-muted font-13"><strong>Email :</strong> <br><span class="m-l-0"><a href="mailto:<?php echo $oUser->email; ?>"><?php echo $oUser->email; ?></a></span></p>

                    <p class="text-muted font-13"><strong>Last Login :</strong> <br><span class="m-l-0"><?php echo $oUser->last_login; ?></span></p>

                </div>


                <div class="button-list m-t-20">
                    <button type="button" class="btn btn-facebook waves-effect waves-light">
                        <i class="fa fa-facebook"></i>
                    </button>

                    <button type="button" class="btn btn-twitter waves-effect waves-light">
                        <i class="fa fa-twitter"></i>
                    </button>

                    <button type="button" class="btn btn-linkedin waves-effect waves-light">
                        <i class="fa fa-linkedin"></i>
                    </button>

                    <button type="button" class="btn btn-dribbble waves-effect waves-light">
                        <i class="fa fa-dribbble"></i>
                    </button>

                </div>
            </div>

        </div>

    </div>

    <!-- CHAT -->
    <div class="col-lg-9">
        <div class="card-box">
            <h4 class="m-t-0 m-b-20 header-title"><b>Global Administrator Chat</b></h4>

            <div class="chat-conversation" id="chat-conversation">
                <div class="row" id="React-ChatsData"></div>

                <?php
                //$data_chats = $oChats->select(NULL, NULL, "Y", NULL, NULL, "id", "DESC, sent_date ASC LIMIT 5");
                $data_chats = $oChats->select_dashboard();
                if($data_chats) {
                    echo '<ul class="conversation-list nicescroll" id="conversation-list">';
                    for($i=0; $i < count($data_chats); $i++) {
                        //PRE($data_chats[$i]);
                        $pic = BASE_UPLOAD."users/".$data_chats[$i]['sender_id']."/".$data_chats[$i]['profile_photo'];
                        $pic_realpath = PATH_UPLOAD_ROOT."users/".$data_chats[$i]['sender_id']."/".$data_chats[$i]['profile_photo'];
                        if(is_file($pic_realpath)) {
                            $img_src = '<img src="'.$pic.'" alt="'.$data_chats[$i]['first_name'].' '.$data_chats[$i]['last_name'].'">';
                        }else{
                            $img_src = '<img src="'.BASE_IMAGES.'nophoto.jpg" alt="'.$data_chats[$i]['first_name'].' '.$data_chats[$i]['last_name'].'">';
                        }
                        if($oUser->id == $data_chats[$i]['sender_id']) { $odd = ""; } else { $odd = "odd"; }
                        echo '
                        <li class="clearfix '.$odd.'">
                            <div class="chat-avatar">
                                '.$img_src.'
                            </div>
                            <div class="conversation-text">
                                <div class="ctext-wrap">
                                    <i>'.$data_chats[$i]['first_name'].' '.$data_chats[$i]['last_name'].'</i>
                                    <p>'.$data_chats[$i]['message'].'</p>
                                </div>
                                <br /><i>'.date("jS F, Y H:i:s", strtotime($data_chats[$i]['sent_date'])).'</i>
                            </div>
                        </li>
                        ';
                    }
                    echo '</ul>';
                }
                ?>

                <div class="row">
                    <form id="dashboard-chat" name="dashboard-chat" action="">
                    <div class="col-sm-8 chat-inputbar">
                        <input type="hidden" id="module" name="module" value="chats">
                        <input type="hidden" id="action" name="action" value="new">
                        <input type="hidden" id="task" name="task" value="home">
                        <input type="hidden" id="lang_id" name="lang_id" value="<?php echo $lang_id; ?>">
                        <input type="hidden" id="path" name="path" value="cp">

                        <input type="hidden" id="room_id" name="room_id" value="<?php echo (empty($room_id) || $room_id == "0") ? 1 : $room_id; ?>">
                        <input type="hidden" id="sender_id" name="sender_id" value="<?php echo (empty($sender_id) || $sender_id == "0") ? $oUser->id : $sender_id; ?>">
                        <input type="hidden" id="receiver_id" name="receiver_id" value="<?php echo (empty($receiver_id) || $receiver_id == "0") ? '0' : $receiver_id; ?>">
                        <div id="ReactJS-seen_date"></div>


                        <input type="hidden" id="chat-name" name="chat-name" value="<?php echo $oUser->first_name." ".$oUser->last_name; ?>">
                        <input type="text" id="message" name="message" class="form-control chat-input" placeholder="Enter your text">
                    </div>
                    <div class="col-sm-4 chat-send">
                        <button type="submit" class="btn btn-md btn-info btn-block waves-effect waves-light">Send</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
    <!-- end col-->

</div>

<div class="row">
    <div class="col-md-12 col-lg-12">
        <?php
        $data_users = $users->select(NULL, NULL, "Y");
        ?>
        <div class="card-box">
            <h4 class="m-t-0 m-b-20 header-title"><b>Users <span class="text-muted">(<?php echo count($data_users); ?>)</span></b></h4>
            <div class="friend-list">
                <?php
                if($data_users) {
                    for($i=0; $i < count($data_users); $i++) {
                        $pic = BASE_UPLOAD."users/".$data_users[$i]['id']."/".$data_users[$i]['profile_photo'];
                        $pic_realpath = PATH_UPLOAD_ROOT."users/".$data_users[$i]['id']."/".$data_users[$i]['profile_photo'];
                        if(is_file($pic_realpath)) {
                            echo '
                    <a href="javascript:void(0);">
                    <img src="'.$pic.'" class="img-circle thumb-md" alt="'.$data_users[$i]['first_name'].' '.$data_users[$i]['last_name'].'">
                    </a>
                    ';
                        }else{
                            echo '<img src="'.BASE_IMAGES.'nophoto.jpg" class="img-circle thumb-md" alt="'.$data_users[$i]['first_name'].' '.$data_users[$i]['last_name'].'">';
                        }
                    }
                }
                ?>
                <a href="javascript:void(0);" class="text-center">
                    <span class="extra-number">+</span>
                </a>
            </div>
        </div>

    </div>
</div>





<?php
/*
        $page_css.= "
        ";
*/
        $page_js.= "
        <!-- Todojs -->
        <script src='".PATH_ASSETS."/pages/jquery.todo.js' type='text/javascript'></script>

        <!-- chatjs
        <script src='https://unpkg.com/babel-standalone@6.15.0/babel.min.js'></script>
        <script src='".PATH_ASSETS."/pages/jquery.chat.js' type='text/javascript'></script> -->
        <script src='".PATH_ADMIN_MODULES."chats/classes/chats.js' type='text/javascript'></script>
        <script src='".PATH_ASSETS."plugins/moment/moment.js' type='text/javascript'></script>

        <script src='https://unpkg.com/react@15/dist/react.min.js'></script>
        <script src='https://unpkg.com/react-dom@15/dist/react-dom.min.js'></script>
        <script src='https://unpkg.com/react@15/dist/react-with-addons.js'></script>
        <script src='https://cdnjs.cloudflare.com/ajax/libs/react-bootstrap/0.30.6/react-bootstrap.js'></script>
        <script src='https://cdnjs.cloudflare.com/ajax/libs/babel-core/5.8.23/browser.min.js'></script>
        <script src='https://cdnjs.cloudflare.com/ajax/libs/zepto/1.1.6/zepto.min.js'></script>
        <script src='https://unpkg.com/axios/dist/axios.min.js'></script>

        <script src='".PATH_ROOT."rest/lib/php_crud_api_transform.js'></script>
        ";

        $doc_ready_js.= "Chats.init();\n";
?>
<script type="text/babel" src="<?php echo PATH_ADMIN_MODULES; ?>chats/classes/chats.times.js"></script>

<!--
<script type="text/babel" src="<?php //echo PATH_ADMIN_MODULES; ?>chats/classes/AppChats.js"></script>
<style type="text/css">
   .conversation-list {
       height: 400px !important;
   }
</style>
-->

<style type="text/css">
.conversation-text i {
    font-size: 0.7em;
    font-style: normal;
    color: #c6c6c6;
}
</style>