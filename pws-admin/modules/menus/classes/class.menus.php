<?php
class Menus extends DB {

    var $tbMenus = "pws_menus";

    function __construct() {
        $this->Page = new Page();
        $this->Page->module_name = "Menus"; // ucfirst
        $this->module_name = strtolower($this->Page->module_name); // strtolower
    }

    ///Additional Functions
    function duplicate($lang_id = NULL, $id = NULL) {
        if(empty($lang_id))
            $lang_id = "en";

        if($lang_id)
            $ct[] = "$this->tbMenus.lang_id = '$lang_id' ";
        if($id)
            $ct[] = "$this->tbMenus.id = '$id' ";

        if(is_array($ct))
        {
            $condition = implode("AND ", $ct);
            $condition = "WHERE ".$condition;
        }

        $field = "*";
        $sql = "SELECT $field FROM $this->tbMenus ";
        $sql.= $condition;
        return $data = DB::query($sql, "count");
    }

    function select($lang_id = NULL, $id = NULL, $parent_id = NULL, $is_active = NULL, $query_type = NULL) {
        global $oLanguages;

        if(!isset($query_type))
            $query_type = "fetchAssoc";

        if(!isset($lang_id))
            $lang_id = $oLanguages->id;

        $sql = "SELECT * FROM `$this->tbMenus`";

        if($lang_id)
            $ct[] = "`$this->tbMenus`.`lang_id` = '$lang_id' ";

        //if($parent_id)
        //    $ct[] = "$this->tbMenus.`parent_id` = '$parent_id' ";

        if(empty($id)) {
            if($parent_id != "parent") {
                $ct[] = "$this->tbMenus.`parent_id` = '$parent_id' ";
            } else {
                $ct[] = "$this->tbMenus.`parent_id` = '0' ";
            }
        }

        if($id)
            $ct[] = "$this->tbMenus.`id` = '$id' ";
        if($is_active)
            $ct[] = "`$this->tbMenus`.`is_active` = '$is_active' ";

        if(is_array($ct))
            $sql = $sql." WHERE ".implode(" AND ", $ct);

        //PRE($sql);
        $data = DB::query($sql, $query_type);
        return $data;
    }

    function insert($post_vars) {
        global $oLanguages;

        if($post_vars)
            extract($post_vars);

        if(!isset($lang_id))
            $lang_id = $oLanguages->id;

        $name = DB::escape($name);
        $link = DB::escape($link);

        if(!isset($top_nav))
            $top_nav = "Y";
        if(!isset($foot_nav))
            $foot_nav = "N";
        if(!isset($is_active))
            $is_active = "Y";

        $field = "id, lang_id, parent_id, name, link, top_nav, foot_nav, icon, file_icon, sort, is_active";
        $value = "'$itemid', '$lang_id', '$parent_id', '$name', '$link', '$top_nav', '$foot_nav', '$icon', '$file_icon', '$sort', '$is_active'";
        $sql = "INSERT INTO $this->tbMenus ($field) VALUES ($value)";
        $id = DB::query($sql, "lastInsertId");

        if($id) {
            //Loop for insert all languages
            global $languages;
            $data_languages = $languages->select(NULL, NULL, "Y");
            //PRE($data_languages);
            if($data_languages) {
                foreach($data_languages as $lang_item):
                    $duplicate_check = $this->duplicate($lang_item['id'], $id);
                    //PRE($duplicate_check);
                    if($duplicate_check){
                        $post_vars = array(
                            "id" => $id,
                            "lang_id" => $lang_item['id'],
                            "parent_id" => $parent_id,
                            "name" => $name,
                            "link" => $link,
                            "top_nav" => $top_nav,
                            "foot_nav" => $foot_nav,
                            "icon" => $icon,
                            "file_icon" => $file_icon,
                            "sort" => $sort,
                            "is_active" => $is_active
                        );
                        $this->update($post_vars);
                    }else{
                        $value = "'$id', '$lang_item[id]', '$parent_id', '$name', '$link', '$top_nav', '$foot_nav', '$icon', '$file_icon', '$sort', '$is_active'";
                        $sql = "INSERT INTO $this->tbMenus ($field) VALUES ($value)";
                        $processok = DB::query($sql, "lastInsertId");
                    }
                endforeach;
                unset($data_languages);
            }

            //Icon Upload
            if($_FILES['file_icon']['name']){
                // images upload
                $file_icon = $_FILES['file_icon'];
                //print_r($file_icon);
                if(count($file_icon['name']) > 0) {
                    $file_icon_name = uploadFile($this->module_name, $id, $file_icon, "photo", $name.".png");
                    if($file_icon_name)
                        $this->update_file_icon($id, $file_icon_name);
                }
                unset($profile_photo, $file_count);
            }
            return $id;
        } else {
            return false;
        }
    }

    function update($post_vars) {
        global $oLanguages;
        if(!isset($lang_id))
            $lang_id = $oLanguages->id;

        if($post_vars)
            extract($post_vars);

        $name = DB::escape($name);
        $link = DB::escape($link);

        $f = " name = '$name' ";

        if($parent_id)
            $f.= ", parent_id = '$parent_id' ";
        if($link)
            $f.= ", link = '$link' ";
        if($top_nav)
            $f.= ", top_nav = '$top_nav' ";
        if($foot_nav)
            $f.= ", foot_nav = '$foot_nav' ";
        if($icon)
            $f.= ", icon = '$icon' ";
        if($file_icon)
            $f.= ", file_icon = '$file_icon' ";
        if($sort)
            $f.= ", sort = '$sort' ";
        if($is_active)
            $f.= ", is_active = '$is_active' ";

        if($update_all_lang != "Y")
            $cnt =  "AND `$this->tbMenus`.`lang_id` = '$lang_id'";

        $sql = "UPDATE $this->tbMenus SET $f WHERE $this->tbMenus.id = '$itemid' ".$cnt;
        $data = DB::query($sql);
        if($data){
            //Loop for insert all languages
            global $languages;
            $data_languages = $languages->select(NULL, NULL, "Y");
            //PRE($data_languages);
            if($data_languages) {
                foreach($data_languages as $lang_item):
                    $duplicate_check = $this->duplicate($lang_item['id'], $itemid);
                    //PRE($duplicate_check);
                    if($duplicate_check){
                        //do nothing
                    }else{
                        $value = "'$itemid', '$lang_item[id]', '$parent_id', '$name', '$link', '$top_nav', '$foot_nav', '$icon', '$file_icon', '$sort', '$is_active'";
                        $sql = "INSERT INTO $this->tbMenus ($field) VALUES ($value)";
                        $processok = DB::query($sql, "lastInsertId");
                    }
                endforeach;
                unset($data_languages);
            }

            //Icon Upload
            if($_FILES['file_icon']['name']){
                // images upload
                $file_icon = $_FILES['file_icon'];
                //print_r($file_icon);
                if(count($file_icon['name']) > 0) {
                    $file_icon_name = uploadFile($this->module_name, $itemid, $file_icon, "photo", $name.".png");
                    if($file_icon_name)
                        $this->update_file_icon($itemid, $file_icon_name);
                }
                unset($profile_photo, $file_count);
            }

            return $data;
        } else {
            return false;
        }
    }

    function update_file_icon($id, $file_icon) {
        $field = "file_icon = '$file_icon' ";
        $sql = "UPDATE $this->tbMenus SET $field WHERE id = '$id'";
        return $data = DB::query($sql);
    }

    function update_status($id, $is_active) {
        $field = "is_active = '$is_active' ";
        $sql = "UPDATE $this->tbMenus SET $field WHERE id = '$id'";
        return $data = DB::query($sql);
    }

    function delete($post_vars) {
        if($post_vars)
            extract($post_vars);

        //Delete all lang
        //if($lang_id)
        //    $cnt =  "AND `$this->tbMenus`.`lang_id` = '$lang_id'";
        if($itemid) {
            $sql = "DELETE FROM `$this->tbMenus` WHERE `$this->tbMenus`.`id` = '$itemid' " . $cnt;
            $data = DB::query($sql);
            if($data) {
                //delete
                //removeFile($itemid, $this->module_name, "profile_photo.png");
                //delete all files and folders belong to this ID
                return delete_directory(PATH_UPLOAD_ROOT.$this->module_name.'/'.$itemid.'/');
            }
        } else {
            return false;
        }
    }

}
?>