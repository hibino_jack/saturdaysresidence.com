<?php
$data = $$module_class->select($lang_id, NULL, 'parent');
$datatotal = count($data);
//PRE($data);
?>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default panel-border">
            <div class="panel-heading">
                <?php echo $languages->dropdown_languages($module, $action, $path, $itemid, $task); ?>
                <h4 class="header-title"><b><?php echo $module_class." ".$action;?></b></h4>
                <p class="text-muted font-13 m-b-10">Total <?php echo $datatotal; ?> record(s) on the list.</p>
            </div>
            <div class="panel-body">
                <table id="datatable-buttons" class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <!--<th>Link</th>-->
                        <th>Photo</th>
                        <th>Sort</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if($data) {
                        $row_no = 1;
                        for($i=0; $i < $datatotal; $i++) { ?>
                            <tr>
                                <td><?php echo $row_no; ?></td>
                                <td><strong><?php echo $data[$i]['name']; ?></strong></td>
                                <!--<td><?php echo $data[$i]['link']; ?></td>-->
                                <td>
                                    <?php
                                    $pic = BASE_UPLOAD.$module."/".$data[$i]['id']."/".$data[$i]['filename'];
                                    $pic_realpath = PATH_UPLOAD_ROOT.$module."/".$data[$i]['id']."/".$data[$i]['filename'];
                                    if(is_file($pic_realpath)) {
                                        ?>
                                        <a class="image-popup" href="<?php echo $pic; ?>" title="<?php echo $data[$i]['first_name']." ".$data[$i]['last_name']; ?>">
                                            <img src="<?php echo PATH_ROOT; ?>timthumb.php?src=<?php echo $pic; ?>&w=80&h=80" class="thumb-md">
                                        </a>
                                    <?php }else{ ?>
                                        <img src="<?php echo BASE_IMAGES; ?>nophoto.jpg" alt="<?php echo $data[$i]['first_name']." ".$data[$i]['last_name']; ?>" class="thumb-md">
                                    <?php } ?>

                                </td>
                                <td><?php echo $data[$i]['sort']; ?></td>
                                <td><span class="label label-table label-<?php echo ($data[$i]['is_active'] == "Y") ? "success" : "inverse"; ?>"><?php echo ($data[$i]['is_active'] == "Y") ? "Active" : "Disabled"; ?></span></td>
                                <td class="actions">
                                    <button class="btn btn-default btn-xs btn-icon fa fa-pencil btn-attr-item" data-lang_id="<?php echo $lang_id;?>" data-module="<?php echo $module;?>" data-action="update" data-id="<?php echo $data[$i]['id']; ?>" data-task="<?php echo $task;?>" data-path="<?php echo $path;?>"></button>
                                    <button class="btn btn-danger btn-xs btn-icon fa fa-trash-o delete-item" data-lang_id="<?php echo $lang_id;?>" data-module="<?php echo $module;?>" data-action="delete" data-id="<?php echo $data[$i]['id']; ?>" data-task="<?php echo $task;?>" data-path="<?php echo $path;?>"></button>
                                </td>
                            </tr>






                            <?php
                            //get Sub menus
                            $data_submenu = $$module_class->select($lang_id, NULL, $data[$i]['id'], "Y");
                            //PRE($data_submenu);
                            if($data_submenu) {
                                $row_no_sub = $row_no + 1;
                                for($t=0; $t < count($data_submenu); $t++) { ?>
                                    <tr>
                                        <td><?php echo $row_no_sub; ?></td>
                                        <td><strong class="m-l-10"> - <?php echo $data_submenu[$t]['name']; ?></strong></td>
                                        <!--<td><?php echo $data_submenu[$t]['link']; ?></td>-->
                                        <td>
                                            <?php
                                            $pic = BASE_UPLOAD.$module."/".$data_submenu[$t]['id']."/".$data_submenu[$t]['filename'];
                                            $pic_realpath = PATH_UPLOAD_ROOT.$module."/".$data_submenu[$t]['id']."/".$data_submenu[$t]['filename'];
                                            if(is_file($pic_realpath)) {
                                                ?>
                                                <a class="image-popup" href="<?php echo $pic; ?>" title="<?php echo $data_submenu[$t]['name']; ?>">
                                                    <img src="<?php echo PATH_ROOT; ?>timthumb.php?src=<?php echo $pic_realpath; ?>&w=80&h=80" class="thumb-md">
                                                </a>
                                            <?php }else{ ?>
                                                <img src="<?php echo BASE_IMAGES; ?>nophoto.jpg" alt="<?php echo $data_submenu[$t]['name']; ?>" class="thumb-md">
                                            <?php } ?>

                                        </td>
                                        <td><?php echo $data_submenu[$t]['sort']; ?></td>
                                        <td><span class="label label-table label-<?php echo ($data[$t]['is_active'] == "Y") ? "success" : "inverse"; ?>"><?php echo ($data_submenu[$t]['is_active'] == "Y") ? "Active" : "Disabled"; ?></span></td>
                                        <td class="actions">
                                            <button class="btn btn-default btn-xs btn-icon fa fa-pencil btn-attr-item" data-lang_id="<?php echo $lang_id;?>" data-module="<?php echo $module;?>" data-action="update" data-id="<?php echo $data_submenu[$t]['id']; ?>" data-task="<?php echo $task;?>" data-path="<?php echo $path;?>"></button>
                                            <button class="btn btn-danger btn-xs btn-icon fa fa-trash-o delete-item" data-lang_id="<?php echo $lang_id;?>" data-module="<?php echo $module;?>" data-action="delete" data-id="<?php echo $data_submenu[$t]['id']; ?>" data-task="<?php echo $task;?>" data-path="<?php echo $path;?>"></button>
                                        </td>
                                    </tr>
                                    <?php
                                    $row_no_sub++;
                                } //for
                            }
                            ?>


                            <?php
                            $row_no = $row_no + count($data_submenu);
                            $row_no++;
                        } //for
                    }else{ ?>
                        <tr>
                            <td colspan="8">Sorry, Record not found</td>
                        </tr>
                    <?php }?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>