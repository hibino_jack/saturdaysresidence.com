<?php
class Controller extends DB {
	var $module;
	var $action;
	var $task;
	var $page_css;
	var $page_js;
	var $page_content;

    function __construct() {
        //echo "<br><br>oConfig:";
        //print_r($oConfig);
    }

    function __destruct() {
    }

    /*
     * Main CONTROLER Class
     */
    function controller($module, $action, $task = NULL) {
        $this->module = $module;
        $this->action = $action;
        $this->task = $task;
        return $this->mvc($this->module, $this->action, $this->task);
    }

    /*
     * MVC Control class
     */
    function mvc($module, $action, $task = NULL) {
        if($module) {
            $module_page = PATH_ADMIN_MODULES_ROOT.$module."/".$module.".php";
            if(!is_file($module_page))
                $module_page = PATH_ADMIN_ROOT."error404.php";
        }
        else
            $module_page = "home.php";
        return $module_page;
    }

    /*
     * CSS control class
     */
    function page_css($module, $path = "cp") {
        // Declare Main CSS
        if(file_exists(PATH_ADMIN_MODULES_ROOT.$module."/classes/".$module.".css")) {
            $page_css.= '<link href="'.BASE_ADMIN_MODULES.$module.'/classes/'.$module.'.css" rel="stylesheet" type="text/css">
            ';
        }
        return $page_css;
    }

    /*
     * JS control class
     */
    function page_js($module, $path = "cp") {
        // Declare Main JS
        if (file_exists(PATH_ADMIN_MODULES_ROOT.$module."/classes/".$module.'.js')) {
            $page_js.= '<script type="text/javascript" src="'.BASE_ADMIN_MODULES.$module."/classes/".$module.'.js"></script>
            ';
        }
        return $page_js;
    }

    /*
     * JS control class
     */
    function doc_ready_js($module) {
        if($module) {
            $doc_ready_js.= ucfirst($module).'.init();
            ';
        }
        return $doc_ready_js;
    }

}

$oController = new Controller();
?>