<?php
$module = "Careers";
$module_class = strtolower($module);

if(file_exists(PATH_ADMIN_MODULES_ROOT.$module_class.'/classes/class.'.$module_class.'.php')) {    
	require_once(PATH_ADMIN_MODULES_ROOT.$module_class . '/classes/class.'.$module_class.'.php');    
	$$module_class = new $module();
	
	$data = $$module_class->select();
	//PRE($data);
}

//$image_header = "http://placehold.it/1450x500&text=".$data_content[0]['title'];
if($data_content[0]['header_photo'] != "")
{
	$pic = BASE_UPLOAD."contents/".$data_content[0]['id']."/".$data_content[0]['header_photo'];
	$pic_realpath = PATH_UPLOAD_ROOT."contents/".$data_content[0]['id']."/".$data_content[0]['header_photo'];
	if(is_file($pic_realpath)) {
		$image_header = PATH_ROOT."timthumb.php?src=".$pic."&w=1450&h=500";
	}
}
?>
<!--header-->
<header> 
    <div class="page-heading">
        <div class="container">
            <div class="row">
            	<div class="col-md-offset-3 col-md-6 text-center">
            	<h1><?php echo $data_content[0]['title']; ?></h1>
                <?php if(!empty($data_content[0]['subtitle'])){ ?>           
                    <h3 style="font-style:italic"><?php echo $data_content[0]['subtitle']; ?></h3>
				<?php }
				if(!empty($data_content[0]['short_description'])){ ?>                                
                    <p><?php echo $data_content[0]['short_description']; ?></p>
				<?php } ?>       
				</div>
            </div>
		</div>
    </div>
    
    <?php if($image_header){ ?>
    <!-- room header Section -->
    <div class="inner-header">
        <div class="container">
                <div class="row">
                    <div class="inner-header-fill" style="background-image:url('<?php echo $image_header; ?>');"></div>
                </div>
         </div>
    </div>
    <?php } ?>
</header>

<section class="content-area">
    <?php if(!empty($data_content[0]['description'])){ ?>
    <div id="innerpage_intro">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                	<div class="row innerpage_intro <?php echo ($image_header) ? "":"no-margin"; ?>">
                    <?php echo $data_content[0]['description']; ?>
                    </div>
                </div>
         	</div>
         </div>
      </div>
    <?php } ?>
<?php
if(is_array($data) && (count($data) > 0))
{
?>
  <div class="careers">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php foreach($data as $item){ ?>
                    <div class="row careers-box">
                    <h3><?php echo $item["name"]; ?></h3>
                    <h5><?php echo $item["no_position"]; ?> Position(s) / <?php echo $item["job_type"]; ?> / <?php echo $item["exp"]; ?> experienced / AGE <?php echo $item["age"]; ?> years</h5>
                    <?php echo $item["description"]; ?>
                    </div>
                <?php } ?>
            </div>
        </div>
     </div>
  </div>
<?php } ?>      
      
    
    </section>