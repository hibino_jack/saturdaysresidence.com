<?php
class Sleep extends DB {

    var $tbSleep = "pws_sleep";

    function __construct() {
        $this->Page = new Page();
        $this->Page->module_name = "Sleep"; // ucfirst
        $this->module_name = strtolower($this->Page->module_name); // strtolower
    }

    ///Additional Functions
    function duplicate($lang_id = NULL, $id = NULL) {
        if(empty($lang_id))
            $lang_id = "en";

        if($lang_id)
            $ct[] = "$this->tbSleep.lang_id = '$lang_id' ";
        if($id)
            $ct[] = "$this->tbSleep.id = '$id' ";

        if(is_array($ct))
        {
            $condition = implode("AND ", $ct);
            $condition = "WHERE ".$condition;
        }

        $field = "*";
        $sql = "SELECT $field FROM $this->tbSleep ";
        $sql.= $condition;
		$sql .= " ORDER BY created_date DESC";
        return $data = DB::query($sql, "count");
    }

    function select($lang_id = NULL, $id = NULL, $parent_id = NULL, $is_active = NULL, $query_type = NULL) {
        global $oLanguages;

        if(!isset($query_type))
            $query_type = "fetchAssoc";

        if(!isset($lang_id))
            $lang_id = $oLanguages->id;

        $sql = "SELECT * FROM `$this->tbSleep`";

        if($lang_id)
            $ct[] = "`$this->tbSleep`.`lang_id` = '$lang_id' ";

        if(empty($id)) {
            if($parent_id != "parent") {
                $ct[] = "$this->tbSleep.`parent_id` = '$parent_id' ";
            } else {
                $ct[] = "$this->tbSleep.`parent_id` = '0' ";
            }
        }

        if($id)
            $ct[] = "$this->tbSleep.`id` = '$id' ";
        if($is_active)
            $ct[] = "`$this->tbSleep`.`is_active` = '$is_active' ";

        if(is_array($ct))
            $sql = $sql." WHERE ".implode(" AND ", $ct);

        //PRE($sql);
        $data = DB::query($sql, $query_type);
        return $data;
    }

    function insert($post_vars) {
        global $oUser;
        global $oLanguages;

        if($post_vars)
            extract($post_vars);

        if(!isset($lang_id))
            $lang_id = $oLanguages->id;

        if(!isset($created_by))
            $created_by = $oUser->id;

        if(!isset($updated_by))
            $updated_by = $oUser->id;

        if(!isset($is_active))
            $is_active = "Y";
//INSERT INTO `pws_sleep` (`id`, `lang_id`, `parent_id`, `name`, `room_type`, `short_description`, `description`, `photoname`, `meta_title`, `meta_keyword`, `meta_description`, `sort`, `created_by`, `updated_by`, `updated_date`, `flag_homepage`, `is_active`) VALUES


        $name = DB::escape($name);
        $room_type = DB::escape($room_type);
        $short_description = addslashes($short_description);
        $description = addslashes($description);
        $meta_title = DB::escape($meta_title);
        $meta_keyword = DB::escape($meta_keyword);
        $meta_description = DB::escape($meta_description);

        if(!isset($flag_homepage))
            $flag_homepage = 'N';
        if(!isset($is_active))
            $is_active = "Y";

        $field = "`id`, `lang_id`, `parent_id`, `name`, `room_type`,`ft_roomtype` , `ft_view` , `ft_bedrooms`, `short_description`, `description`, `filename`, `photoname`, `meta_title`, `meta_keyword`, `meta_description`, `sort`, `created_by`, `updated_by`, `updated_date`, `flag_homepage`, `is_active`";
        $value = "'$itemid', '$lang_id', '$parent_id', '$name', '$room_type', '$ft_roomtype' , '$ft_view' , '$ft_bedrooms', '$short_description', '$description', '$filename', '$photoname', '$meta_title', '$meta_keyword', '$meta_description', '$sort', '$created_by', '$updated_by', NOW(), '$flag_homepage', '$is_active'";
        $sql = "INSERT INTO $this->tbSleep ($field) VALUES ($value)";
        $id = DB::query($sql, "lastInsertId");

        if($id) {
            //Loop for insert all languages
            global $languages;
            $data_languages = $languages->select(NULL, NULL, "Y");
            //PRE($data_languages);
            if($data_languages) {
                foreach($data_languages as $lang_item):
                    $duplicate_check = $this->duplicate($lang_item['id'], $id);
                    if($duplicate_check){
                        //do nothing
                        $post_vars = array(
                            "id" => $id,
                            "lang_id" => $lang_item['id'],
                            "parent_id" => $parent_id,
                            "name" => $name,
                            "room_type" => $room_type,
                            "short_description" => $short_description,
                            "description" => $description,
                            "filename" => $filename,
                            "open_hours" => $open_hours,
                            "link" => $link,
                            "link_caption" => $link_caption,
                            "photoname" => $photoname,
                            "meta_title" => $meta_title,
                            "meta_keyword" => $meta_keyword,
                            "meta_description" => $meta_description,
                            "sort" => $sort,
                            "created_by" => $created_by,
                            "updated_by" => $updated_by,
                            "updated_date" => $updated_date,
                            "flag_homepage" => $flag_homepage,
                            "is_active" => $is_active
                        );
                        //$this->update($post_vars);
                    }else{
                        $value = "'$id', '$lang_item[id]', '$parent_id', '$name', '$room_type', '$short_description', '$description', '$filename', '$photoname', '$meta_title', '$meta_keyword', '$meta_description', '$sort', '$created_by', '$updated_by', NOW(), '$flag_homepage', '$is_active'";
                        $sql = "INSERT INTO $this->tbSleep ($field) VALUES ($value)";
                        $processok = DB::query($sql, "lastInsertId");
                    }
                endforeach;
                unset($data_languages);
            }

            //Photo Upload
            if($_FILES['photoname']['name']){
                // images upload
                $photoname = $_FILES['photoname'];
                //print_r($photoname);
                if(count($photoname['name']) > 0) {
                    $photoname_name = uploadFile($this->module_name, $id, $photoname, "photo");
                    if($photoname_name)
                        $this->update_photoname($id, $photoname_name);
                }
                unset($photoname_name, $photoname);
            }
            return $id;
        } else {
            return false;
        }
    }

    function update($post_vars) {
        global $oUser;
        global $oLanguages;

        if($post_vars)
            extract($post_vars);

        if(!isset($lang_id))
            $lang_id = $oLanguages->id;

        if(!isset($created_by))
            $created_by = $oUser->id;

        if(!isset($updated_by))
            $updated_by = $oUser->id;

        if(!isset($is_active))
            $is_active = "Y";

        $name = DB::escape($name);
        $room_type = DB::escape($room_type);
        $short_description = addslashes($short_description);
        $description = addslashes($description);
        $meta_title = DB::escape($meta_title);
        $meta_keyword = DB::escape($meta_keyword);
        $meta_description = DB::escape($meta_description);
        $link = DB::escape($link);
        $open_hours = DB::escape($open_hours);
		
        $ft_roomtype = DB::escape($ft_roomtype);
        $ft_view = DB::escape($ft_view);
        $ft_bedrooms = DB::escape($ft_bedrooms);

        $f = " name = '$name' ";

        if($parent_id) {
            $f.= ", parent_id = '$parent_id' ";
        } else {
            $f.= ", parent_id = '0' ";
        }
        if($filename)
            $f.= ", filename = '$filename' ";
        if($room_type)
            $f.= ", room_type = '$room_type' ";
        if($short_description)
            $f.= ", short_description = '$short_description' ";
        if($description)
            $f.= ", description = '$description' ";

        if($ft_roomtype)
            $f.= ", ft_roomtype = '$ft_roomtype' ";
        if($ft_view)
            $f.= ", ft_view = '$ft_view' ";
        if($ft_bedrooms)
            $f.= ", ft_bedrooms = '$ft_bedrooms' ";

        if($photoname)
            $f.= ", photoname = '$photoname' ";
        if($sort)
            $f.= ", sort = '$sort' ";
        if($meta_title)
            $f.= ", meta_title = '$meta_title' ";
        if($meta_keyword)
            $f.= ", meta_keyword = '$meta_keyword' ";
        if($meta_description)
            $f.= ", meta_description = '$meta_description' ";
        if($created_by)
            $f.= ", created_by = '$created_by' ";
        if($updated_by)
            $f.= ", updated_by = '$updated_by' ";

        $f.= ", updated_date = NOW() ";
        if($flag_homepage) {
            $f.= ", flag_homepage = '$flag_homepage' ";
        } else {
            $f.= ", flag_homepage = 'N' ";
        }
        if($is_active)
            $f.= ", is_active = '$is_active' ";


        if($update_all_lang != "Y")
            $cnt =  "AND `$this->tbSleep`.`lang_id` = '$lang_id'";

        $sql = "UPDATE $this->tbSleep SET $f WHERE $this->tbSleep.id = '$itemid' ".$cnt;
        $data = DB::query($sql);
        if($data){
            //Loop for insert all languages
            global $languages;
            $data_languages = $languages->select(NULL, NULL, "Y");
            //PRE($data_languages);
            if($data_languages) {
                foreach($data_languages as $lang_item):
                    $duplicate_check = $this->duplicate($lang_item['id'], $itemid);
                    //PRE($duplicate_check);
                    if($duplicate_check){
                        //do nothing
                    }else{
                        $value = "'$itemid', '$lang_item[id]', '$parent_id', '$name', '$room_type', '$short_description', '$description', '$filename', '$photoname', '$meta_title', '$meta_keyword', '$meta_description', '$sort', '$created_by', '$updated_by', NOW(), '$flag_homepage', '$is_active'";
                        $sql = "INSERT INTO $this->tbSleep ($field) VALUES ($value)";
                        $processok = DB::query($sql, "lastInsertId");
                    }
                endforeach;
                unset($data_languages);
            }

            //Photo Upload
            if($_FILES['photoname']['name']){
                // images upload
                $photoname = $_FILES['photoname'];
                //print_r($photoname);
                if(count($photoname['name']) > 0) {
                    $photoname_name = uploadFile($this->module_name, $itemid, $photoname, "photo");
                    if($photoname_name)
                        $this->update_photoname($itemid, $photoname_name);
                    if($old_photoname)
                        removeFile($itemid, $this->module_name, $old_photoname);
                }
                unset($photoname_name, $photoname);
            }

            return $data;
        } else {
            return false;
        }
    }

    function update_photoname($id, $photoname) {
        $field = "photoname = '$photoname' ";
        $sql = "UPDATE $this->tbSleep SET $field WHERE id = '$id'";
        return $data = DB::query($sql);
    }

    function update_status($id, $is_active) {
        $field = "is_active = '$is_active' ";
        $sql = "UPDATE $this->tbSleep SET $field WHERE id = '$id'";
        return $data = DB::query($sql);
    }

    function delete($post_vars) {
        if($post_vars)
            extract($post_vars);

        //Delete all lang
        //if($lang_id)
        //    $cnt =  "AND `$this->tbSleep`.`lang_id` = '$lang_id'";
        if($itemid) {
            $sql = "DELETE FROM `$this->tbSleep` WHERE `$this->tbSleep`.`id` = '$itemid' " . $cnt;
            $data = DB::query($sql);
            if($data) {
                //delete
                //removeFile($itemid, $this->module_name, "profile_photo.png");
                //delete all files and folders belong to this ID
                return delete_directory(PATH_UPLOAD_ROOT.$this->module_name.'/'.$itemid.'/');
            }
        } else {
            return false;
        }
    }
	
    function get_by_filename($lang_id = NULL, $filename = NULL , $query_type = NULL) {
        global $oLanguages;

        if(!isset($query_type))
            $query_type = "fetchAssoc";

        if(!isset($lang_id))
            $lang_id = $oLanguages->id;

        $sql = "SELECT * FROM `$this->tbSleep`";

        if($lang_id)
            $ct[] = "`$this->tbSleep`.`lang_id` = '$lang_id' ";

        if($filename)
            $ct[] = "`$this->tbSleep`.`filename` = '$filename' ";

        if(is_array($ct))
            $sql = $sql." WHERE ".implode(" AND ", $ct);

        //PRE($sql);
        $data = DB::query($sql, $query_type);
        return $data;
    }
	
    function delete_photo($post_vars) {
        if($post_vars)
            extract($post_vars);

        if($itemid) {
            //delete
            //$delete_result = removeFile($itemid, $this->module_name, "header_photo.png");
            $delete_result = removeFile($itemid, $this->module_name, $filename);

            if($delete_result) {
                $field = "photoname = '' ";
                $sql = "UPDATE $this->tbSleep SET $field WHERE id = '$itemid'";
                return $data = DB::query($sql);
            }
        } else {
            return false;
        }
    }


    //Upload summernote photo
    function summernoteUpload($post_vars) {
        if($post_vars)
            extract($post_vars);

        //PRE($post_vars);
        if ($_FILES['file']['name']) {
            if (!$_FILES['file']['error']) {
                $name = "smn_".$this->module_name."_".date("YmdHis");
                $ext = explode('.', $_FILES['file']['name']);
                $filename = $name . '.' . $ext[1];

                //$destination = '/saturdaysresidence.com/media/summernote/' . $filename; //change this directory
                if($itemid) {
                    $target_dir = PATH_UPLOAD_ROOT.$this->module_name."/".$itemid."/";
                    $target_url = BASE_UPLOAD.$this->module_name."/".$itemid."/";
                } else {
                    $target_dir = PATH_UPLOAD_ROOT.$this->module_name."/summernote/";
                    $target_url = BASE_UPLOAD.$this->module_name."/summernote/";
                }
                //create folder
                if (!file_exists($target_dir)) {
                    MKDIRS($target_dir);
                }
                $destination =  $target_dir.$filename;
                $location = $_FILES["file"]["tmp_name"];
                $uploadok = move_uploaded_file($location, $destination);
                if($uploadok){
                    //insert in to database
                    //$page_content = $$module_class->summernoteUpload($module, $action, $task, $itemid, $filename);

                    //$field = "is_active = '$is_active' ";
                    //$sql = "UPDATE $this->tbContents SET $field WHERE id = '$id'";
                    //return $data = DB::query($sql);
                }
                echo $target_url.$filename;//return image to editor
            } else {
                echo  $message = 'Ooops! Your upload triggered the following error:  '.$_FILES['file']['error'];
            }
        }
    }

    //Delete summernote photo
    function summernoteDelete($post_vars) {
        if($post_vars)
            extract($post_vars);

        //PRE($post_vars);
        //$file_name_array = explode('/',$file);
        //$file_name = end($file_name_array);
        //echo "<br />file name: ".$file_name;

        $file_path = str_replace(BASE_UPLOAD, PATH_UPLOAD_ROOT, $file);
        //echo "<br />file name: ".$file;
        //echo "<br />file path: ".$file_path;

        $delete_result = false;
        if(isset($file_path) && is_file($file_path)) {
            $delete_result = unlink($file_path);
        }

        if($delete_result) {
            //delete data from database
            return true;
        } else {
            echo 'Uh oh, error :(';
            return false;
        }
    }


}
?>