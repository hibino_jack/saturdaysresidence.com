<?php
if($action == "update")    {
    if($$module_class->itemid) {
        $data = $$module_class->select($$module_class->itemid);
        $itemid = $$module_class->itemid;
        if($data){
            extract($data[0]);
            //PRE($data);
        }
    }
}
?>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?php echo $module_class." ".$action;?></h3>
            </div>
            <div class="panel-body">
                <form name="create-form" id="create-form" class="form-horizontal" role="form" data-parsley-validate novalidate enctype="multipart/form-data">
                    <div class="alert alert-danger alert-dismissable hidden">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <h4>Oh snap!</h4>
                        <p>This form seems to be invalid :(</p>
                    </div>
                    <div class="alert alert-success alert-dismissable hidden">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <h4>Yay!</h4>
                        <p>Everything seems to be ok., Re-submit your form then. :)</p>
                    </div>

                    <div class="row">
                        <div class="col-md-12">

                            <div class="form-group">
                                <label class="col-md-2 control-label">Name</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="name" parsley-trigger="change" required placeholder="Enter Name" value="<?php echo $name;?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Email</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="email" parsley-trigger="change" required placeholder="Enter Email" value="<?php echo $email;?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Active</label>
                                <div class="col-sm-10">
                                    <?php
                                    $is_active_data = array("Y" => "Enable", "N" => "Disabled");
                                    echo RADIOITEMMANUAL("is_active", $is_active, $is_active_data);
                                    ?>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group pull-right">
                                <input type="hidden" id="module" name="module" value="<?php echo $module; ?>">
                                <input type="hidden" id="action" name="action" value="<?php echo $action; ?>">
                                <input type="hidden" id="lang_id" name="lang_id" value="<?php echo $lang_id; ?>">
                                <input type="hidden" id="itemid" name="itemid" value="<?php echo $itemid; ?>">
                                <input type="hidden" id="path" name="path" value="cp">

                                <button type="submit" class="btn btn-default waves-effect waves-light btn-lg">Submit</button>
                                <button type="reset" class="btn btn-white waves-effect waves-light btn-lg">Cancel</button>
                            </div>
                        </div>
                    </div>
                </form>

            </div>
        </div>

    </div>
</div>