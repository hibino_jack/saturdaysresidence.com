<?php
    if($module == 'chats') {
        $module_class = ucfirst($module);
        require_once('class.'.$module.'.php');
        $$module_class = new $module_class;
        switch ( $action ) {
            case 'load_data':
                $page_content = $$module_class->load_data($post_vars);
                break;
            case 'new':
                $page_content = $$module_class->insert($post_vars);
                break;
            case 'update':
                $page_content = $$module_class->update($post_vars);
                break;
            case 'delete':
                $page_content = $$module_class->delete($post_vars);
                break;
        }
	echo $page_content;
	exit();
	}
?>