<!--header-->
<header> 
    <div class="page-heading">
        <div class="container">
            <div class="row">
            	<div class="col-md-offset-2 col-md-8 text-center">
            	<h1><?php echo $data_content[0]['title']; ?></h1>
                <?php if(!empty($data_content[0]['subtitle'])){ ?>           
                    <h3 style="font-style:italic"><?php echo $data_content[0]['subtitle']; ?></h3>
				<?php }
				if(!empty($data_content[0]['short_description'])){ ?>                                
                    <p><?php echo $data_content[0]['short_description']; ?></p>
				<?php } ?>       
				</div>
            </div>
		</div>
    </div>
    
</header>

<section class="content-area">
      <div class="reservation-policy">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                	<div class="row content-box">
						<?php echo $data_content[0]['description']; ?>
                    </div>
                </div>
         	</div>
         </div>
      </div>
    </section>