<?php
class Dos extends DB {

    var $tbDo = "pws_do";

    function __construct() {
        $this->Page = new Page();
        $this->Page->module_name = "Do"; // ucfirst
        $this->module_name = strtolower($this->Page->module_name); // strtolower
    }

    ///Additional Functions
    function duplicate($lang_id = NULL, $id = NULL) {
        if(empty($lang_id))
            $lang_id = "en";

        if($lang_id)
            $ct[] = "$this->tbDo.lang_id = '$lang_id' ";
        if($id)
            $ct[] = "$this->tbDo.id = '$id' ";

        if(is_array($ct))
        {
            $condition = implode("AND ", $ct);
            $condition = "WHERE ".$condition;
        }

        $field = "*";
        $sql = "SELECT $field FROM $this->tbDo ";
        $sql.= $condition;
        return $data = DB::query($sql, "count");
    }

    function select($lang_id = NULL, $id = NULL, $parent_id = NULL, $is_active = NULL, $query_type = NULL) {
        global $oLanguages;

        if(!isset($query_type))
            $query_type = "fetchAssoc";

        if(!isset($lang_id))
            $lang_id = $oLanguages->id;

        $sql = "SELECT * FROM `$this->tbDo`";

        if($lang_id)
            $ct[] = "`$this->tbDo`.`lang_id` = '$lang_id' ";

        if(empty($id)) {
            if($parent_id != "parent") {
                $ct[] = "$this->tbDo.`parent_id` = '$parent_id' ";
            } else {
                $ct[] = "$this->tbDo.`parent_id` = '0' ";
            }
        }

        if($id)
            $ct[] = "$this->tbDo.`id` = '$id' ";
        if($is_active)
            $ct[] = "`$this->tbDo`.`is_active` = '$is_active' ";

        if(is_array($ct))
            $sql = $sql." WHERE ".implode(" AND ", $ct);
		$sql .= " ORDER BY updated_date DESC";
        //PRE($sql);
        $data = DB::query($sql, $query_type);
        return $data;
    }

    function insert($post_vars) {
        global $oUser;
        global $oLanguages;

        if($post_vars)
            extract($post_vars);

        if(!isset($lang_id))
            $lang_id = $oLanguages->id;

        if(!isset($created_by))
            $created_by = $oUser->id;

        if(!isset($updated_by))
            $updated_by = $oUser->id;

        if(!isset($is_active))
            $is_active = "Y";

        $name = DB::escape($name);
        $description = addslashes($description);
        $meta_title = DB::escape($meta_title);
        $meta_keyword = DB::escape($meta_keyword);
        $meta_description = DB::escape($meta_description);
        $link = DB::escape($link);

        if(!isset($flag_homepage))
            $flag_homepage = 'N';
        if(!isset($is_active))
            $is_active = "Y";

        //INSERT INTO `pws_do` (`id`, `lang_id`, `parent_id`, `name`, `description`, `link`, `link_caption`, `filename`, `meta_title`, `meta_keyword`, `meta_description`, `sort`, `created_by`, `updated_by`, `updated_date`, `is_active`)
        $field = "`id`, `lang_id`, `parent_id`, `name`, `description`, `link`, `link_caption`, `filename`, `meta_title`, `meta_keyword`, `meta_description`, `sort`, `created_by`, `updated_by`, `updated_date`, `flag_homepage`, `is_active`";
        $value = "'$itemid', '$lang_id', '$parent_id', '$name', '$description', '$link', '$link_caption', '$filename', '$meta_title', '$meta_keyword', '$meta_description', '$sort', '$created_by', '$updated_by', NOW(), '$flag_homepage', '$is_active'";
        $sql = "INSERT INTO $this->tbDo ($field) VALUES ($value)";
        $id = DB::query($sql, "lastInsertId");

        if($id) {
            //Loop for insert all languages
            global $languages;
            $data_languages = $languages->select(NULL, NULL, "Y");
            //PRE($data_languages);
            if($data_languages) {
                foreach($data_languages as $lang_item):
                    $duplicate_check = $this->duplicate($lang_item['id'], $id);
                    if($duplicate_check){
                        //do nothing
                        $post_vars = array(
                            "id" => $id,
                            "lang_id" => $lang_item['id'],
                            "parent_id" => $parent_id,
                            "name" => $name,
                            "description" => $description,
                            "link" => $link,
                            "link_caption" => $link_caption,
                            "filename" => $filename,
                            "meta_title" => $meta_title,
                            "meta_keyword" => $meta_keyword,
                            "meta_description" => $meta_description,
                            "sort" => $sort,
                            "created_by" => $created_by,
                            "updated_by" => $updated_by,
                            "updated_date" => $updated_date,
                            "flag_homepage" => $flag_homepage,
                            "is_active" => $is_active
                        );
                        //$this->update($post_vars);
                    }else{
                        $value = "'$id', '$lang_item[id]', '$parent_id', '$name', '$description', '$link', '$link_caption', '$filename', '$meta_title', '$meta_keyword', '$meta_description', '$sort', '$created_by', '$updated_by', NOW(), '$flag_homepage', '$is_active'";
                        $sql = "INSERT INTO $this->tbDo ($field) VALUES ($value)";
                        $processok = DB::query($sql, "lastInsertId");
                    }
                endforeach;
                unset($data_languages);
            }

            //Photo Upload
            if($_FILES['filename']['name']){
                // images upload
                $filename = $_FILES['filename'];
                //print_r($filename);
                if(count($filename['name']) > 0) {
                    $filename_name = uploadFile($this->module_name, $id, $filename, "photo");
                    if($filename_name)
                        $this->update_filename($id, $filename_name);
                }
                unset($filename_name, $filename);
            }
            return $id;
        } else {
            return false;
        }
    }

    function update($post_vars) {
        global $oUser;
        global $oLanguages;

        if($post_vars)
            extract($post_vars);

        if(!isset($lang_id))
            $lang_id = $oLanguages->id;

        if(!isset($created_by))
            $created_by = $oUser->id;

        if(!isset($updated_by))
            $updated_by = $oUser->id;

        if(!isset($is_active))
            $is_active = "Y";

        $name = DB::escape($name);
        $description = addslashes($description);
        $meta_title = DB::escape($meta_title);
        $meta_keyword = DB::escape($meta_keyword);
        $meta_description = DB::escape($meta_description);
        $link = DB::escape($link);

        $f = " name = '$name' ";

        //INSERT INTO `pws_do` (`id`, `lang_id`, `parent_id`, `name`, `description`, `link`, `link_caption`
        //, `filename`, `meta_title`, `meta_keyword`, `meta_description`, `sort`
        //, `created_by`, `updated_by`, `updated_date`, `is_active`)


        if($parent_id) {
            $f.= ", parent_id = '$parent_id' ";
        } else {
            $f.= ", parent_id = '0' ";
        }
        if($description)
            $f.= ", description = '$description' ";
        if($link)
            $f.= ", link = '$link' ";
        if($link_caption)
            $f.= ", link_caption = '$link_caption' ";
        if($filename)
            $f.= ", filename = '$filename' ";
        if($sort)
            $f.= ", sort = '$sort' ";
        if($meta_title)
            $f.= ", meta_title = '$meta_title' ";
        if($meta_keyword)
            $f.= ", meta_keyword = '$meta_keyword' ";
        if($meta_description)
            $f.= ", meta_description = '$meta_description' ";
        if($created_by)
            $f.= ", created_by = '$created_by' ";
        if($updated_by)
            $f.= ", updated_by = '$updated_by' ";

        $f.= ", updated_date = NOW() ";
        if($flag_homepage) {
            $f.= ", flag_homepage = '$flag_homepage' ";
        } else {
            $f.= ", flag_homepage = 'N' ";
        }
        if($is_active)
            $f.= ", is_active = '$is_active' ";


        if($update_all_lang != "Y")
            $cnt =  "AND `$this->tbDo`.`lang_id` = '$lang_id'";

        $sql = "UPDATE $this->tbDo SET $f WHERE $this->tbDo.id = '$itemid' ".$cnt;
        $data = DB::query($sql);
        if($data){
            //Loop for insert all languages
            global $languages;
            $data_languages = $languages->select(NULL, NULL, "Y");
            //PRE($data_languages);
            if($data_languages) {
                foreach($data_languages as $lang_item):
                    $duplicate_check = $this->duplicate($lang_item['id'], $itemid);
                    //PRE($duplicate_check);
                    if($duplicate_check){
                        //do nothing
                    }else{
                        $value = "'$itemid', '$lang_item[id]', '$parent_id', '$name', '$description', '$link', '$link_caption', '$filename', '$meta_title', '$meta_keyword', '$meta_description', '$sort', '$created_by', '$updated_by', NOW(), '$flag_homepage', '$is_active'";
                        $sql = "INSERT INTO $this->tbDo ($field) VALUES ($value)";
                        $processok = DB::query($sql, "lastInsertId");
                    }
                endforeach;
                unset($data_languages);
            }

            //Photo Upload
            if($_FILES['filename']['name']){
                // images upload
                $filename = $_FILES['filename'];
                //print_r($filename);
                if(count($filename['name']) > 0) {
                    $filename_name = uploadFile($this->module_name, $itemid, $filename, "photo");
                    if($filename_name)
                        $this->update_filename($itemid, $filename_name);
                    if($old_filename)
                        removeFile($itemid, $this->module_name, $old_filename);
                }
                unset($filename_name, $filename);
            }

            return $data;
        } else {
            return false;
        }
    }

    function update_filename($id, $filename) {
        $field = "filename = '$filename' ";
        $sql = "UPDATE $this->tbDo SET $field WHERE id = '$id'";
        return $data = DB::query($sql);
    }

    function update_status($id, $is_active) {
        $field = "is_active = '$is_active' ";
        $sql = "UPDATE $this->tbDo SET $field WHERE id = '$id'";
        return $data = DB::query($sql);
    }

    function delete($post_vars) {
        if($post_vars)
            extract($post_vars);

        //Delete all lang
        //if($lang_id)
        //    $cnt =  "AND `$this->tbDo`.`lang_id` = '$lang_id'";
        if($itemid) {
            $sql = "DELETE FROM `$this->tbDo` WHERE `$this->tbDo`.`id` = '$itemid' " . $cnt;
            $data = DB::query($sql);
            if($data) {
                //delete
                //removeFile($itemid, $this->module_name, "profile_photo.png");
                //delete all files and folders belong to this ID
                return delete_directory(PATH_UPLOAD_ROOT.$this->module_name.'/'.$itemid.'/');
            }
        } else {
            return false;
        }
    }
	
    function delete_photo($post_vars) {
        if($post_vars)
            extract($post_vars);

        if($itemid) {
            //delete
            //$delete_result = removeFile($itemid, $this->module_name, "header_photo.png");
            $delete_result = removeFile($itemid, $this->module_name, $filename);

            if($delete_result) {
                $field = "filename = '' ";
                $sql = "UPDATE $this->tbDo SET $field WHERE id = '$itemid'";
                return $data = DB::query($sql);
            }
        } else {
            return false;
        }
    }


    //Upload summernote photo
    function summernoteUpload($post_vars) {
        if($post_vars)
            extract($post_vars);

        //PRE($post_vars);
        if ($_FILES['file']['name']) {
            if (!$_FILES['file']['error']) {
                $name = "smn_".$this->module_name."_".date("YmdHis");
                $ext = explode('.', $_FILES['file']['name']);
                $filename = $name . '.' . $ext[1];

                //$destination = '/saturdaysresidence.com/media/summernote/' . $filename; //change this directory
                if($itemid) {
                    $target_dir = PATH_UPLOAD_ROOT.$this->module_name."/".$itemid."/";
                    $target_url = BASE_UPLOAD.$this->module_name."/".$itemid."/";
                } else {
                    $target_dir = PATH_UPLOAD_ROOT.$this->module_name."/summernote/";
                    $target_url = BASE_UPLOAD.$this->module_name."/summernote/";
                }
                //create folder
                if (!file_exists($target_dir)) {
                    MKDIRS($target_dir);
                }
                $destination =  $target_dir.$filename;
                $location = $_FILES["file"]["tmp_name"];
                $uploadok = move_uploaded_file($location, $destination);
                if($uploadok){
                    //insert in to database
                    //$page_content = $$module_class->summernoteUpload($module, $action, $task, $itemid, $filename);

                    //$field = "is_active = '$is_active' ";
                    //$sql = "UPDATE $this->tbContents SET $field WHERE id = '$id'";
                    //return $data = DB::query($sql);
                }
                echo $target_url.$filename;//return image to editor
            } else {
                echo  $message = 'Ooops! Your upload triggered the following error:  '.$_FILES['file']['error'];
            }
        }
    }

    //Delete summernote photo
    function summernoteDelete($post_vars) {
        if($post_vars)
            extract($post_vars);

        //PRE($post_vars);
        //$file_name_array = explode('/',$file);
        //$file_name = end($file_name_array);
        //echo "<br />file name: ".$file_name;

        $file_path = str_replace(BASE_UPLOAD, PATH_UPLOAD_ROOT, $file);
        //echo "<br />file name: ".$file;
        //echo "<br />file path: ".$file_path;

        $delete_result = false;
        if(isset($file_path) && is_file($file_path)) {
            $delete_result = unlink($file_path);
        }

        if($delete_result) {
            //delete data from database
            return true;
        } else {
            echo 'Uh oh, error :(';
            return false;
        }
    }


}
?>