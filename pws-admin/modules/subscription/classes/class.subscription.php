<?php
class Subscription extends DB {

    var $tbSubscription = "pws_subscription";

    function __construct()
    {
        $this->Page = new Page();
        $this->Page->module_name = "Subscription"; // ucfirst
        $this->module_name = strtolower($this->Page->module_name); // strtolower
    }

    function duplicate($id = NULL, $name = NULL)
    {
        if ($id)
            $ct[] = "$this->tbSubscription.id = '$id' ";
        if ($name)
            $ct[] = "$this->tbSubscription.name = '$name' ";

        if (is_array($ct)) {
            $condition = implode("AND ", $ct);
            $condition = "WHERE " . $condition;
        }

        $field = "*";
        $sql = "SELECT $field FROM $this->tbSubscription ";
        $sql .= $condition;
        return $data = DB::query($sql, "count");
    }

    function select($id = NULL, $email = NULL, $is_active = NULL, $query_type = NULL)
    {
        if (!isset($query_type))
            $query_type = "fetchAssoc";

        $sql = "SELECT * FROM `$this->tbSubscription`";

        if ($id)
            $ct[] = "$this->tbSubscription.`id` = '$id' ";
        if ($email)
            $ct[] = "`$this->tbSubscription`.`email` = '$email' ";
        if ($is_active)
            $ct[] = "`$this->tbSubscription`.`is_active` = '$is_active' ";
        if (is_array($ct))
            $sql = $sql . " WHERE " . implode(" AND ", $ct);

        $data = DB::query($sql, $query_type);
        return $data;
    }

    //INSERT INTO `pws_subscription` (`id`, `name`, `email`, `is_active`) VALUES (1, 'aaa', 'aaaa', 'Y');

    function insert($post_vars)
    {
        if ($post_vars)
            extract($post_vars);

        $name = DB::escape($name);
        $email = DB::escape($email);

        $field = "`id`, `name`, `email`, `is_active`";
        $value = "NULL, '$name', '$email', '$is_active'";
        $sql = "INSERT INTO `$this->tbSubscription` ($field) VALUES ($value)";
        $id = DB::query($sql, "lastInsertId");
        if ($id) {
            return $id;
        } else {
            return false;
        }
    }

    function update($post_vars)
    {
        if ($post_vars)
            extract($post_vars);

        $name = DB::escape($name);
        $email = DB::escape($email);

        $f = " `$this->tbSubscription`.`name` = '$name' ";
        if ($email)
            $f .= ", `$this->tbSubscription`.`email` = '$email' ";
        if ($is_active)
            $f .= ", `$this->tbSubscription`.`is_active` = '$is_active' ";

        $sql = "UPDATE $this->tbSubscription SET $f WHERE `$this->tbSubscription`.`id` = '$itemid'";
        return $data = DB::query($sql);
    }

    function update_status($id, $is_active)
    {
        $field = "is_active = '$is_active' ";
        $sql = "UPDATE $this->tbSubscription SET $field WHERE id = '$id'";
        return $data = DB::query($sql);
    }

    function delete($id)
    {
        $sql = "DELETE FROM $this->tbSubscription WHERE id = '$id' ";
        $data = DB::query($sql);
        if ($data) {
            return true;
        } else {
            return false;
        }
    }
	
    function subscribe($email)
    {
        if ($post_vars)
            extract($post_vars);

        $name = DB::escape($name);
        $email = DB::escape($email);

        $field = "`id`, `name`, `email`, `is_active`";
        $value = "NULL, '$email', '$email', 'Y'";
        $sql = "INSERT INTO `$this->tbSubscription` ($field) VALUES ($value)";
        $id = DB::query($sql, "lastInsertId");
        if ($id) {
            return $id;
        } else {
            return false;
        }
    }
	

}
?>