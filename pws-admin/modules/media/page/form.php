<?php
if($action == "update")    {
    if($$module_class->itemid) {
        $data = $$module_class->select($$module_class->itemid);
        $itemid = $$module_class->itemid;
        if($data){
            extract($data[0]);
            //PRE($data);
        }
    }
}
?>

<div class="row">
    <div class="col-md-12">
        <?php if($action == "update") { ?>
            <div class="btn-group pull-right m-t-5 m-r-5">
                <button type="button" class="btn btn-default dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Languages <span class="m-l-5"><i class="fa fa-cog"></i></span></button>
                <ul class="dropdown-menu drop-menu-right" role="menu">
                    <?php
                    $data_lang = $languages->select(NULL, NULL, "Y");
                    if($data_lang && ($action == "update")) {
                        foreach ($data_lang as $lang_item) {
                            ?>
                            <li>
                                <a href="index.php?lang_id=<?php echo $lang_item['id']; ?>&module=<?php echo $module; ?>&action=<?php echo $action; ?>&path=cp&itemid=<?php echo $itemid; ?>">
                                    <span class="flag-icon flag-icon-<?php echo $lang_item['icon']; ?>"></span> Switch to <?php echo $lang_item['name']; ?>
                                </a>
                            </li>
                            <?php
                            unset($li_active);
                        }
                        unset($data_lang);
                    } ?>
                </ul>
            </div>
        <?php } ?>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?php echo $module_class." ".$action;?></h3>
            </div>
            <div class="panel-body">
                <form name="create-form" id="create-form" class="form-horizontal" role="form" data-parsley-validate novalidate enctype="multipart/form-data">
                    <div class="alert alert-danger alert-dismissable hidden">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <h4>Oh snap!</h4>
                        <p>This form seems to be invalid :(</p>
                    </div>
                    <div class="alert alert-success alert-dismissable hidden">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <h4>Yay!</h4>
                        <p>Everything seems to be ok., Re-submit your form then. :)</p>
                    </div>

                    <div class="row">
                        <div class="col-md-12">

                            <div class="form-group">
                                <label class="col-md-2 control-label">Language Code</label>
                                <div class="col-md-10">
                                    <?php
                                    if($action == "update") {
                                        echo '<input type="text" class="form-control" name="id" parsley-trigger="change" required placeholder="Enter Language Code" value="'.$id.'" readonly>';
                                    } else {
                                        echo '
                                            <input class="form-control" name="id" type="text" minlength="2" maxlength="2"
                                           data-parsley-remote data-parsley-remote-options=\'{ "type": "POST", "data": { "token": "value" } }\'
                                           data-parsley-remote-validator="CheckLangID"
                                           data-parsley-errors-container="#errorID"
                                           data-parsley-remote-message="This language code already added."
                                           value="" required />
                                    <span id="errorID"></span>';
                                    }
                                    ?>

                                    <span class="font-13 text-muted">e.g. EN - TH, see more codes here "http://data.okfn.org/data/core/country-list"</span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Name</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="name" parsley-trigger="change" required placeholder="Enter Name" value="<?php echo $name;?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Icon Code</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="icon" parsley-trigger="change" required placeholder="Enter Icon Code" value="<?php echo $icon;?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Default</label>
                                <div class="col-sm-10">
                                    <?php
                                    $default_data = array("Y" => "Yes", "N" => "No");
                                    echo RADIOITEMMANUAL("default", $default, $default_data);
                                    ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Active</label>
                                <div class="col-sm-10">
                                    <?php
                                    $is_active_data = array("Y" => "Enable", "N" => "Disabled");
                                    echo RADIOITEMMANUAL("is_active", $is_active, $is_active_data);
                                    ?>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group pull-right">
                                <input type="hidden" id="module" name="module" value="<?php echo $module; ?>">
                                <input type="hidden" id="action" name="action" value="<?php echo $action; ?>">
                                <input type="hidden" id="lang_id" name="lang_id" value="<?php echo $lang_id; ?>">
                                <input type="hidden" id="itemid" name="itemid" value="<?php echo $itemid; ?>">
                                <input type="hidden" id="path" name="path" value="cp">

                                <button type="submit" class="btn btn-default waves-effect waves-light btn-lg">Submit</button>
                                <button type="reset" class="btn btn-white waves-effect waves-light btn-lg">Cancel</button>
                            </div>
                        </div>
                    </div>
                </form>

            </div>
        </div>

    </div>
</div>