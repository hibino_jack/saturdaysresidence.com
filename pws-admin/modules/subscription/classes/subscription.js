var Subscription = function () {
    var module_is = 'Subscription';
    var moduleName = module_is.toLowerCase();

    var handleCheckLangID = function() {
        window.Parsley.addAsyncValidator('CheckLangID', function (data) {
            var myResponseText = data.responseText;
            var obj = jQuery.parseJSON(myResponseText);
            //console.log(obj);
            if((obj.result > 0) && (obj.message == "Duplicated")) {
                return false;
            } else {
                return true;
            }
        }, base_url + 'oc_ajax.php?module=subscription&action=check_lang_id&path=cp');
    };

    return {
        //main function to initiate the module
        init: function () {
        },
        initList:function(){
        },
        initForm: function () {
            handleCheckLangID();
        }
    };

}();