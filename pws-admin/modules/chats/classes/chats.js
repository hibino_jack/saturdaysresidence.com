var Chats = function () {
    var module_is = 'Chats';
    var moduleName = module_is.toLowerCase();

    var ChatApp = function () {
        this.$body = $("body"),
            this.$chatInput = $('.chat-input'),
            this.$chatSenderID = $('#sender_id'),
            this.$chatName = $('#chat-name'),
            this.$chatSeenDate = $('#seen_date'),
            this.$chatList = $('.conversation-list'),
            this.$chatSendBtn = $('.chat-send .btn')
    };

    //saves chat entry - You should send ajax call to server in order to save chat enrty
    ChatApp.prototype.save = function () {
        var chatText = this.$chatInput.val();
        var chatName = this.$chatName.val();
        var chatTime = moment().format('Do MMMM YYYY, h:mm:ss a'); // December 2nd 2016, 12:27:53 pm

        var chatSeenDate = this.$chatSeenDate.val();
        var chatSenderID = this.$chatSenderID.val();

        if (chatText == "") {
            sweetAlert("Oops...", "You forgot to enter your chat message", "error");
            this.$chatInput.focus();
        } else {
            //save chat msg;
            initChatSendMsg();

            if (chatSenderID == "") {
                var sender_id = ($("#sender_id").val()) ? $("#sender_id").val() : '';
            } else {
                var sender_id = chatSenderID;
            }
            var sender_photo = base_url + "media/uploads/users/" + sender_id + "/profile_photo.png";

            $('<li class="clearfix"><div class="chat-avatar"><img src="' + sender_photo + '" alt="male"></div><div class="conversation-text"><div class="ctext-wrap"><i>' + chatName + '</i><p>' + chatText + '</p></div><br><i>' + chatTime + '</i></div></li>').appendTo('.conversation-list');
            this.$chatInput.val('');
            this.$chatInput.focus();
            this.$chatList.scrollTo('100%', '100%', {
                easing: 'swing'
            });
        }
    }, ChatApp.prototype.init = function () {
        var $this = this;
        //binding keypress event on chat input box - on enter we are adding the chat into chat list -
        $this.$chatInput.keypress(function (ev) {
            var p = ev.which;
            if (p == 13) {
                $this.save();
                return false;
            }
        });

        //binding send button click
        $this.$chatSendBtn.click(function (ev) {
            $this.save();
            return false;
        });
    }, $.ChatApp = new ChatApp; //init ChatApp
    $.ChatApp.Constructor = ChatApp;

    var initChatSendMsg = function () {
        var formData = new FormData($("#dashboard-chat")[0]);

        // var module = ($("#module").val()) ? $("#module").val() : '';
        // var action = ($("#action").val()) ? $("#action").val() : '';
        // var task = ($("#task").val()) ? $("#task").val() : '';
        // var lang_id = ($("#lang_id").val()) ? $("#lang_id").val() : '';
        // var path = ($("#path").val()) ? $("#path").val() : '';
        //
        // var room_id = ($("#room_id").val()) ? $("#room_id").val() : '';
        // var sender_id = ($("#sender_id").val()) ? $("#sender_id").val() : '';
        // var receiver_id = ($("#receiver_id").val()) ? $("#receiver_id").val() : '';
        // var message = ($("#message").val()) ? $("#message").val() : '';

        //alert(" M:" + module + " A:" + action + " T:" + task + " L:" + lang_id + " P:" + path + " I:" + itemid);

        $.ajax({
            type: "POST",
            url: base_url + "oc_ajax.php",
            data: formData,
            cache: false,
            contentType: false,
            processData: false
        }).done(function (data) {
            //console.log(data);  // this is currently returning FALSE // Which is totally correct!
            if (data == true || data > 0) {
                //Loading Sweet Alert
                swal({
                    title: "Your data was successfully submitted",
                    text: "Please waiting for a moment.",
                    type: "success",
                    timer: 4000,
                    showConfirmButton: false
                });
            } else {
                //Loading Sweet Alert
                swal({
                    title: "OPP!! Something wrong",
                    text: "Please checking your data again.",
                    type: "warning",
                    timer: 4000,
                    showConfirmButton: false
                });
            }

        });
    };

    var initChooseChatWith = function () {
        $(document).on("click", ".btn-chat-with", function (e) {
            e.preventDefault();
            var sender_id = $(this).data('sender_id');
            var receiver_id = $(this).data('receiver_id');
            var room_id = $("#room_id").val();

            //set to input
            $("#sender_id").val(sender_id);
            $("#receiver_id").val(receiver_id);
            $("#room_id").val(room_id);

            //Load Msgs
            $('#conversation-list').empty(); //clear old chat
            $('#more_button').empty(); //clear load more button
            $('#notice_msg').empty(); //clear notice msg

            initLoadChatMsg(room_id, sender_id, receiver_id);
        });
    };

    var initLoadChatMsg = function (room_id, sender_id, receiver_id) {
        var module = moduleName;
        var action = "load_data";
        var task = "list";
        var path = "cp";
        var lang_id = $("#lang_id").val();
        if (typeof track_page === 'undefined' || !track_page) {
            var track_page = 1;
        }

        //alert(" M:" + module + " A:" + action + " T:" + task + " L:" + lang_id + " P:" + path + " S:" + sender_id + " R:" + receiver_id);
        axios.get(base_url + "oc_ajax.php?module=" + module + "&action=" + action + "&task=" + task + "&path=" + path + "&lang_id=" + lang_id + "&room_id=" + room_id + "&sender_id=" + sender_id + "&receiver_id=" + receiver_id + "&page=" + track_page)
            .then(function (response) {
                if(response.data) {
                    $('#conversation-list').prepend(response.data);

                    //add load btn
                    $("#conversation-list").after("<p id='more_button' align='center'><button class='btn btn-info btn-custom btn-rounded waves-effect waves-light' id='load_more_button'>Load More</button></p>");
                    //console.log(response);
                } else {
                    //add notice alert
                    $("#notice_msg").append("<p style='font-size: 0.8em; color: #0f556f' align='center'>--- Opp!! You have no more chat history messages. ---</p>");

                }
            })
            .catch(function (error) {
                //console.log(error);
            });
    };

    var initBtnLoadMoreMsg = function () {
        if(track_page == "")
            var track_page = 1;

        var track_page = 1; //track user click as page number, righ now page number 1
        //initLoadMoreMsg(track_page); //load content

        $(document).on("click","#load_more_button", function(e) {
            track_page++; //page number increment everytime user clicks load button
            initLoadMoreMsg(track_page); //load content
        });
    };

    var initLoadMoreMsg = function(track_page) {
        var module = moduleName;
        var action = "load_data";
        var task = "list";
        var path = "cp";
        var lang_id = $("#lang_id").val();
        var room_id = $("#room_id").val();
        var sender_id = $("#sender_id").val();
        var receiver_id = $("#receiver_id").val();

        $.post(
            base_url + 'oc_ajax.php', {'module': module, 'action': action, 'task': task, 'path': path, 'lang_id': lang_id, 'sender_id': sender_id, 'room_id': room_id, 'receiver_id': receiver_id, 'page': track_page}, function(data){

                if(data.trim().length == 0){
                    //display text and disable load button if nothing to load
                    $("#load_more_button").text("You have reached end of the history logs record!").prop("disabled", true);
                }

                $("#conversation-list").prepend(data); //prepend data into #conversation-list element
                //$("#conversation-list").append(data); //append data into #conversation-list element

                //scroll page to button element
                $("html, body").animate({scrollTop: $("#load_more_button").offset().top}, 800);
            });
    };


    return {
        //main function to initiate the module
        init: function () {
            //initialize here something.
            $.ChatApp.init();
        },
        initList:function(){
            initChooseChatWith();
            initBtnLoadMoreMsg();
        }
    };

}();