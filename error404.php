<?php
//defined("ACTIVE_MODULE") or die("Access Denied");
//print_r($oConfig);
?>
<div class="wrapper-page">
    <div class="ex-page-content text-center">
        <div class="text-error">
            <span class="text-primary">4</span><i class="ti-face-sad text-pink">0</i><span class="text-info">4</span>
        </div>
        <h2>Who0ps! Page not found</h2>
        <br>
        <p class="text-muted">
            This page cannot found or is missing.
        </p>
        <p class="text-muted">
            Use the navigation above or the button below to get back and track.
        </p>
        <br>
        <a class="btn btn-default waves-effect waves-light" href="index.php"> Return Home</a>
        <a class="btn btn-info waves-effect waves-light" href="mailto:<?php echo $oConfig->email; ?>"> Contact to <?php echo $oConfig->name; ?> Support</a>

    </div>
</div>
<!-- end wrapper page -->