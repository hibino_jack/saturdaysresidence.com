<?php
$extend_url_path = "&path=cp";
if($lang_id)
    $extend_url_path.= "&lang_id=".$lang_id;
if($task) {
    $extend_url_path .= "&task=" . $task;
}
?>
<div class="left side-menu">
    <div class="sidebar-inner slimscrollleft">
        <!--- Divider -->
        <div id="sidebar-menu">
            <ul>

                <li class="text-muted menu-title">Navigation</li>

                <li>
                    <a href="index.php" class="waves-effect"><i class="ti-package"></i> <span> Dashboard </span> <span class="menu-arrow"></span></a>
                </li>

                <!--<li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect">
                        <i class="ti-files"></i>
                        <?php
                        if(file_exists(PATH_ADMIN_MODULES_ROOT.'contents/classes/class.contents.php')) {
                            require_once(PATH_ADMIN_MODULES_ROOT.'contents/classes/class.contents.php');
                            $oContents = new Contents();
                            $total_contents = $oContents->select($lang_id, NULL, "all", NULL, "count");
                            //PRE($total_contents);
                        }
                        ?>
                        <span class="label label-primary pull-right"><?php echo $total_contents; unset($oContents, $total_contents);?></span>
                        <span> Contents </span> </a>
                    <ul class="list-unstyled">
                        <li><a href="index.php?module=contents&action=list<?php echo $extend_url_path;?>">All Contents</a></li>
                        <li><a href="index.php?module=contents&action=new<?php echo $extend_url_path;?>">Create new</a></li>
                    </ul>
                </li>-->
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="ti-home"></i> <span> Home </span> <span class="menu-arrow"></span> </a>
                    <ul class="list-unstyled">
                        <li><a href="index.php?module=contents&action=update&itemid=1&task=<?php echo $extend_url_path;?>">Content</a></li>
                        <li><a href="index.php?module=contents&action=gallery&task=list&itemid=1&path=cp&lang_id=en">Image
                                Slider</a></li>
                    </ul>
                </li>

                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="ti-layout-grid2-alt"></i> <span> Sleep </span> <span class="menu-arrow"></span> </a>
                    <ul class="list-unstyled">
                        <li><a href="index.php?module=contents&action=update&itemid=2&task=<?php echo $extend_url_path;?>">Content</a></li>
                        <li><a href="index.php?module=sleep&action=list<?php echo $extend_url_path;?>">All Listing</a></li>
                        <li><a href="index.php?module=sleep&action=new<?php echo $extend_url_path;?>">Create new</a></li>
                    </ul>
                </li>
                
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="ti-paint-bucket"></i> <span> Taste </span> <span class="menu-arrow"></span> </a>
                    <ul class="list-unstyled">
                        <li><a href="index.php?module=contents&action=update&itemid=3&task=<?php echo $extend_url_path;?>">Content</a></li>
                        <li><a href="index.php?module=taste&action=list<?php echo $extend_url_path;?>">All Listing</a></li>
                        <li><a href="index.php?module=taste&action=new<?php echo $extend_url_path;?>">Create new</a></li>
                    </ul>
                </li>    
                
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="ti-direction-alt"></i> <span> Do(s) </span> <span class="menu-arrow"></span> </a>
                    <ul class="list-unstyled">
                        <li><a href="index.php?module=contents&action=update&itemid=4&task=<?php echo $extend_url_path;?>">Content</a></li>
                        <li><a href="index.php?module=do&action=list<?php echo $extend_url_path;?>">All Listing</a></li>
                        <li><a href="index.php?module=do&action=new<?php echo $extend_url_path;?>">Create new</a></li>
                    </ul>
                </li>

                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="ti-receipt"></i> <span> Gather </span> <span class="menu-arrow"></span> </a>
                    <ul class="list-unstyled">
                        <li><a href="index.php?module=contents&action=update&itemid=5&task=<?php echo $extend_url_path;?>">Content</a></li>
                        <li><a href="index.php?module=gather&action=list<?php echo $extend_url_path;?>">All Listing</a></li>
                        <li><a href="index.php?module=gather&action=new<?php echo $extend_url_path;?>">Create new</a></li>
                    </ul>
                </li>

                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="ti-crown"></i> <span> Ownership </span> <span class="menu-arrow"></span> </a>
                    <ul class="list-unstyled">
                        <li><a href="index.php?module=contents&action=update&itemid=25&task=<?php echo $extend_url_path;?>">Content</a></li>
                    </ul>
                </li>
                         
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="ti-money"></i> <span> Offers </span> <span class="menu-arrow"></span> </a>
                    <ul class="list-unstyled">
                        <li><a href="index.php?module=contents&action=update&itemid=7&task=<?php echo $extend_url_path;?>">Content</a></li>
                        <li><a href="index.php?module=offers&action=list<?php echo $extend_url_path;?>">All Listing</a></li>
                        <li><a href="index.php?module=offers&action=new<?php echo $extend_url_path;?>">Create new</a></li>
                    </ul>
                </li>

                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="ti-image"></i> <span> Gallery </span> <span class="menu-arrow"></span> </a>
                    <ul class="list-unstyled">
                        <li><a href="index.php?module=contents&action=update&itemid=6&task=<?php echo $extend_url_path;?>">Content</a></li>
                        <li><a href="index.php?module=contents&action=gallery&task=list&itemid=6<?php echo $extend_url_path;?>">Image Gallery</a></li>
                    </ul>
                </li>

                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="ti-location-pin"></i> <span> Field Guide </span> <span class="menu-arrow"></span> </a>
                    <ul class="list-unstyled">
                        <li><a href="index.php?module=contents&action=update&itemid=8&task=<?php echo $extend_url_path;?>">Content</a></li>
                        <li><a href="index.php?module=fieldguide&action=list<?php echo $extend_url_path;?>">All Listing</a></li>
                        <li><a href="index.php?module=fieldguide&action=new<?php echo $extend_url_path;?>">Create new</a></li>
                    </ul>
                </li>
                
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="ti-pulse"></i> <span> Our Story </span> <span class="menu-arrow"></span> </a>
                    <ul class="list-unstyled">
                        <li><a href="index.php?module=contents&action=update&itemid=9&task=<?php echo $extend_url_path;?>">Content</a></li>
                    </ul>
                </li>    

                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="ti-palette"></i> <span> Press </span> <span class="menu-arrow"></span> </a>
                    <ul class="list-unstyled">
                        <li><a href="index.php?module=contents&action=update&itemid=10&task=<?php echo $extend_url_path;?>">Content</a></li>
                        <li><a href="index.php?module=press&action=list<?php echo $extend_url_path;?>">All Listing</a></li>
                        <li><a href="index.php?module=press&action=new<?php echo $extend_url_path;?>">Create new</a></li>
                    </ul>
                </li>                            

                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="ti-briefcase"></i> <span> Careers </span> <span class="menu-arrow"></span> </a>
                    <ul class="list-unstyled">
                        <li><a href="index.php?module=contents&action=update&itemid=11&task=<?php echo $extend_url_path;?>">Content</a></li>
                        <li><a href="index.php?module=careers&action=list<?php echo $extend_url_path;?>">All Listing</a></li>
                        <li><a href="index.php?module=careers&action=new<?php echo $extend_url_path;?>">Create new</a></li>
                    </ul>
                </li>
                
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="ti-pencil-alt"></i> <span>Reservation Policy</span> <span class="menu-arrow"></span> </a>
                    <ul class="list-unstyled">
                        <li><a href="index.php?module=contents&action=update&itemid=12&task=<?php echo $extend_url_path;?>">Content</a></li>
                    </ul>
                </li>  
                  
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="ti-pencil-alt"></i> <span>Features</span> <span class="menu-arrow"></span> </a>
                    <ul class="list-unstyled">
                        <li><a href="index.php?module=contents&action=update&itemid=13&task=<?php echo $extend_url_path;?>">Content</a></li>
                    </ul>
                </li>    
                <?php /*
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="ti-pencil-alt"></i> <span>Terms & Conditions</span> <span class="menu-arrow"></span> </a>
                    <ul class="list-unstyled">
                        <li><a href="index.php?module=contents&action=update&itemid=14<?php echo $extend_url_path;?>">Content</a></li>
                    </ul>
                </li>    

                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="ti-pencil-alt"></i> <span>Privacy Policy</span> <span class="menu-arrow"></span> </a>
                    <ul class="list-unstyled">
                        <li><a href="index.php?module=contents&action=update&itemid=15<?php echo $extend_url_path;?>">Content</a></li>
                    </ul>
                </li>    
                */ ?>
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="ti-pencil-alt"></i> <span>Contact Us</span> <span class="menu-arrow"></span> </a>
                    <ul class="list-unstyled">
                        <li><a href="index.php?module=contents&action=update&itemid=16&task=<?php echo $extend_url_path;?>">Content</a></li>
                    </ul>
                </li>    

                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="ti-email"></i><span> Subscription </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="index.php?module=subscription&action=list<?php echo $extend_url_path;?><?php echo $extend_url_path;?>">All Subscription</a></li>
                        <li><a href="index.php?module=subscription&action=new">Create new</a></li>
                    </ul>
                </li>

                <li>
                    <a href="index.php?module=config&action=update&itemid=1<?php echo $extend_url_path;?>" class="waves-effect"><i class="ti-settings"></i> <span> Configuration </span> <span class="menu-arrow"></span> </a>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>