<?php
if($action == "gallery")    {
    if($$module_class->itemid) {
        $data = $$module_class->select($lang_id, $$module_class->itemid);
        $itemid = $$module_class->itemid;
        if($data){
            extract($data[0]);
            //PRE($data);
        }
    }

    /*
     *  Get Media items
     */
    $data_gallery = $$module_class->Media->select(NULL, $$module_class->itemid);
}
?>


<div class="row">
    <div class="col-lg-12">
        <div class="portlet">
            <!-- /primary heading -->
            <div class="portlet-heading bg-custom">
                <h3 class="portlet-title text-uppercase">
                    <?php echo $title; ?> : Photos List
                </h3>
                <div class="portlet-widgets">
                    <a href="javascript:void(0);" data-toggle="reload"><i class="ion-refresh"></i></a>
                    <span class="divider"></span>
                    <a data-toggle="collapse" data-parent="#accordion1" href="#portlet1"><i class="ion-minus-round"></i></a>
                    <span class="divider"></span>
                    <a href="javascript:void(0);" data-toggle="remove"><i class="ion-close-round"></i></a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div id="portlet1" class="panel-collapse collapse in">
                <div class="portlet-body">
                    <div id="actions" class="row">
                        <div class="col-lg-7">
                            <!-- The fileinput-button span is used to style the file input field as button -->
                            <span class="btn btn-success fileinput-button">
                                <i class="glyphicon glyphicon-plus"></i>
                                <span>Add files...</span>
                            </span>
                            <button type="submit" class="btn btn-primary start">
                                <i class="glyphicon glyphicon-upload"></i>
                                <span>Start upload</span>
                            </button>
                            <!--
                            <button type="reset" class="btn btn-warning cancel">
                                <i class="glyphicon glyphicon-ban-circle"></i>
                                <span>Cancel upload</span>
                            </button>
                            -->
                        </div>

                        <div class="col-lg-5">
                            <!-- The global file processing state -->
                            <span class="fileupload-process">
                                <div id="total-progress" class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                                    <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
                                </div>
                            </span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table table-striped files" id="previews">

                                <div id="template" class="file-row">
                                    <!-- This is used as the file preview template -->
                                    <div>
                                        <span class="preview"><img data-dz-thumbnail /></span>
                                    </div>
                                    <div>
                                        <p class="name" data-dz-name></p>
                                        <strong class="error text-danger" data-dz-errormessage></strong>
                                    </div>
                                    <div>
                                        <p class="size" data-dz-size></p>
                                        <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                                            <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
                                        </div>
                                    </div>
                                    <div>
                                        <button class="btn btn-primary start"><i class="glyphicon glyphicon-upload"></i><span>Start</span></button>
                                        <button data-dz-remove class="btn btn-warning cancel"><i class="glyphicon glyphicon-ban-circle"></i><span>Cancel</span></button>
                                        <button data-dz-remove class="btn btn-danger delete"><i class="glyphicon glyphicon-trash"></i><span>Delete</span></button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div> <!-- end col -->


</div>

<!-- end row -->



<!--- Show the photo gallery list --->
<div class="row">
    <div class="col-lg-12">
        <div class="portlet"><!-- /primary heading -->
            <div class="portlet-heading bg-pink">
                <h3 class="portlet-title text-uppercase">
                    Photos List
                </h3>
                <div class="portlet-widgets">
                    <a href="javascript:;" data-toggle="reload"><i class="ion-refresh"></i></a>
                    <span class="divider"></span>
                    <a data-toggle="collapse" data-parent="#accordion2" href="#portlet2"><i class="ion-minus-round"></i></a>
                    <span class="divider"></span>
                    <a href="#" data-toggle="remove"><i class="ion-close-round"></i></a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div id="portlet2" class="panel-collapse collapse in">
                <div class="portlet-body">
                    <!-- SECTION FILTER
                    ================================================== -->
                    <!--
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 ">
                            <div class="portfolioFilter">
                                <a href="#" data-filter="*" class="current">All</a>
                                <a href="#" data-filter=".webdesign">Web Design</a>
                                <a href="#" data-filter=".graphicdesign">Graphic Design</a>
                                <a href="#" data-filter=".illustrator">Illustrator</a>
                                <a href="#" data-filter=".photography">Photography</a>
                            </div>
                        </div>
                    </div>
                    -->
                    <?php if($data_gallery){ //PRE($data_gallery); ?>
                        <div class="pull-right m-r-10">
                            <button type="button" class="btn btn-danger delete-all-items-img"
                                    data-module="<?php echo $module;?>"
                                    data-action="<?php echo $action;?>"
                                    data-task="delete_all_gallery_item"
                                    data-lang_id="<?php echo $lang_id;?>"
                                    data-path="<?php echo $path;?>"
                                    data-itemid="<?php echo $itemid;?>">
                                <i class="glyphicon glyphicon-trash"></i>
                                <span>Delete all photos</span>
                            </button>
                        </div>
                        <div class="clearfix"></div>
                        <div class="row port">
                            <div class="portfolioContainer">
                                <?php
                                $no = 1;
                                for($i=0; $i < count($data_gallery); $i++) { ?>
                                    <div class="col-sm-6 col-lg-3 col-md-4 category<?php echo $data_gallery[$i]['gallery_categories_id']; ?> content_<?php echo $data_gallery[$i]['content_id']; ?>">
                                        <div class="gal-detail thumb photo<?php echo $data_gallery[$i]['id']; ?>">
                                            <?php
                                            $pic = BASE_UPLOAD.$module."/".$itemid."/".$data_gallery[$i]['filename'];
                                            $pic_realpath = PATH_UPLOAD_ROOT.$module."/".$itemid."/".$data_gallery[$i]['filename'];
                                            if(is_file($pic_realpath)) { ?>
                                                <a href="<?php echo $pic; ?>" class="image-popup" title="<?php echo $data_gallery[$i]['title']; ?>">
                                                    <img src="<?php echo $pic; ?>" class="thumb-img" alt="work-thumbnail">
                                                </a>
                                            <?php }else{ ?>
                                                <img src="<?php echo BASE_IMAGES; ?>nophoto.jpg" alt="<?php echo $data_gallery[$i]['title']; ?>" class="thumb-img">
                                            <?php } ?>
                                            <h4><?php echo $data_gallery[$i]['title']." - No.".$no; ?></h4>
                                            <div class="btn-action">
                                                <!--<a href="#" class="btn btn-success btn-sm"><i class="md md-mode-edit"></i></a>-->

                                                <a href="javascript:void(0);" class="btn btn-success btn-sm update-item-img"
                                                   data-module="<?php echo $module;?>"
                                                   data-action="<?php echo $action;?>"
                                                   data-task="update"
                                                   data-lang_id="<?php echo $lang_id;?>"
                                                   data-path="<?php echo $path;?>"
                                                   data-filename="<?php echo $data_gallery[$i]['filename'];?>"
                                                   data-content_id="<?php echo $itemid;?>"
                                                   data-itemid="<?php echo $data_gallery[$i]['id'];?>"><i class="md md-mode-edit"></i>
                                                </a>

                                                <a href="javascript:void(0);" class="btn btn-danger btn-sm delete-item-img"
                                                   data-module="<?php echo $module;?>"
                                                   data-action="<?php echo $action;?>"
                                                   data-task="delete_gallery_item"
                                                   data-lang_id="<?php echo $lang_id;?>"
                                                   data-path="<?php echo $path;?>"
                                                   data-filename="<?php echo $data_gallery[$i]['filename'];?>"
                                                   data-content_id="<?php echo $itemid;?>"
                                                   data-itemid="<?php echo $data_gallery[$i]['id'];?>"><i class="md md-close"></i></a>
                                            </div>
                                        </div>
                                    </div>

                                    <?php $no++; } ?>
                            </div>
                        </div> <!-- End row -->
                    <?php } else {
                        echo "<p>No Photo.</p>";
                    }
                    ?>



                </div>
            </div>
        </div>
    </div> <!-- end col -->


</div>

<!-- end row -->




<!-- Set data attr -->
<div class="div_data_controller"
     data-module="<?php echo $module;?>"
     data-action="<?php echo $action;?>"
     data-task="insert_gallery"
     data-lang_id="<?php echo $lang_id;?>"
     data-path="<?php echo $path;?>"
     data-itemid="<?php echo $itemid;?>"></div>