<!--Secondary-menu-->
<nav class="secondary-nav">
    <div class="container">

        <ul>
            <li><a href="http://www.fotohotelphuket.com/" target="_blank">Foto Hotel</a></li>
            <li><a href="http://www.blumonkeyhotels.com/" target="_blank">blu monkey</a></li>
            <li><a href="http://www.twovillasholiday.com/" target="_blank">two villa holidays</a></li>
        </ul>
        <a class="phone-number" href="tel:<?php echo preg_replace("/[^0-9]/", "", $oConfig->telephone);?>"><?php echo $oConfig->telephone; ?></a>

    </div>
</nav>
<!--//Secondary-menu-->
<nav class="navbar navbar-default navbar-custom navbar-fixed-top"  role="navigation" id="slide-nav">
<?php //PRE($oConfig); ?>
    <!-- Navigation -->
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

        </div>
        <a class="navbar-brand" href="<?php echo BASE_URL; ?>"><img src="<?php echo $logo; ?>" class="img-responsive"></a>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div id="slidemenu">

            <ul class="nav navbar-nav navbar-left">
                <li><a href="<?php echo BASE_URL.$lang_id; ?>/sleep/">Sleep</a></li>
                <li><a href="<?php echo BASE_URL.$lang_id; ?>/taste/">Taste</a></li>
                <li><a href="<?php echo BASE_URL.$lang_id; ?>/do/">do</a></li>
                <li><a href="<?php echo BASE_URL.$lang_id; ?>/gather/">Gather</a></li>
                <li><a href="<?php echo BASE_URL.$lang_id; ?>/ownership/">Ownership</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="<?php echo BASE_URL.$lang_id; ?>/offers/">Offers</a></li>
                <li><a href="<?php echo BASE_URL.$lang_id; ?>/gallery/">Gallery</a></li>

                <!--<li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <b class="caret"></b></a>
                  <ul class="dropdown-menu">
                    <li><a href="#">Action</a></li>
                    <li><a href="#">Another action</a></li>
                    <li><a href="#">Something else here</a></li>
                    <li class="divider"></li>
                    <li><a href="#">Separated link</a></li>
                  </ul>
                </li>-->

                <li><a href="#" class="btn btn-booknow" data-toggle="modal" data-target="#booking-modal"><i class="icon-calendar"></i>Book now</a></li>
            </ul>
        </div><!-- /.slidemenu -->
    </div>
</nav>