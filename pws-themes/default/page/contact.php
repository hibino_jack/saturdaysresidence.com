<?php
$data_content[0]['description'] = str_ireplace(array("\r","\n",'\r','\n'),'', $data_content[0]['description']);
?>
<!--header-->
<header> 
    <div class="page-heading">
        <div class="container">
            <div class="row">
            	<div class="col-md-offset-3 col-md-6 text-center">
            	<h1><?php echo $data_content[0]['title']; ?></h1>
                <?php if(!empty($data_content[0]['subtitle'])){ ?>           
                    <h3 style="font-style:italic"><?php echo $data_content[0]['subtitle']; ?></h3>
				<?php }
				if(!empty($data_content[0]['short_description'])){ ?>                                
                    <p><?php echo $data_content[0]['short_description']; ?></p>
				<?php } ?>       
				</div>
            </div>
            <!--            --><?php //if(!empty($data_content[0]['description'])){ ?>
            <!--            <div class="row">-->
            <!--                <div class="col-md-12">-->
            <!--					--><?php //echo $data_content[0]['description']; ?>
            <!--                </div>-->
            <!--            </div>-->
            <!--            --><?php //} ?><!--                                    -->
		</div>
    </div>
    
    <!-- room header Section -->
    <div class="inner-header">
        <div class="container">
                <div class="row bg-map">
                   <img src="<?php echo $map_image ?>" class="img-responsive" />
                </div>
         </div>
    </div>
</header>

<section class="content-area">
  
    <div class="contact-form">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                	<div class="row innerpage_intro">
                    <h3>Get in contact with us</h3>
					<form name="contact" class="contact-form" action="post" style="overflow:hidden">
                        <div class="col-md-6">
                        		<div class="row form-group">
                                <label for="fname" class="col-sm-4 control-label">First Name *</label>
                                <div class="col-sm-8">
                                  <input type="text" class="form-control" id="fname" name="fname" placeholder="First Name" required />
                                </div>
                              </div>
                              
                              <div class="row form-group">
                                <label for="lname" class="col-sm-4 control-label">Last Name *</label>
                                <div class="col-sm-8">
                                  <input type="text" class="form-control" id="lname" name="lname" placeholder="Last Name" required />
                                </div>
                              </div>
                              
                              <div class="row form-group">
                                <label for="email" class="col-sm-4 control-label">Email *</label>
                                <div class="col-sm-8">
                                  <input type="email" class="form-control" id="email" name="email" placeholder="Email Address" required />
                                </div>
                              </div>
                              
                               <div class="row form-group">
                                <label for="phone" class="col-sm-4 control-label">Phone</label>
                                <div class="col-sm-8">
                                  <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone">
                                </div>
                              </div>
                        </div>
                        
                        <div class="col-md-6">
                        	<div class="row form-group">
                                <label for="msg" class="col-sm-4 control-label">Message *</label>
                                <div class="col-sm-8">
                                  <textarea class="form-control" rows="4" id="msg" name="msg" required></textarea>
                                </div>
                              </div>
                              
                              <div class="row form-group">
                                <div class="col-sm-offset-4 col-sm-8">
                                  <input type="hidden" name="module" value="contact-form" />
                                  <button type="submit" class="btn btn-default">Submit</button>
                                </div>
                              </div>
  
                        </div>
                        
                        
                    
                    </form>
                    </div>
                </div>
         	</div>
         </div>
      </div>
      
      <div></div>
      
      <!-- paragraphs-box Section -->
    <div class="paragraphs-box">
        <div class="container">
            <div class="row paragraphs-items">
            	<div class="col-md-6">
                	<div class="row paragraphs-image">
                		 <?php echo $oConfig->googlemap_embed; ?>
                    </div>
                </div>
               	<div class="col-md-6">
                	<div class="row paragraphs-content">
                        <h3>Contact Information</h3>
                        <?php
                        if (!empty($data_content[0]['description'])) {
                            echo $data_content[0]['description'];
                        }
                        ?>
                        <!--                        <p>--><?php //echo $oConfig->address; ?><!--</p>-->
                        <!--                        <p>TELEPHONE: <br/>-->
                        <?php //echo $oConfig->telephone; ?><!--</p>-->
                        <!--                        <p>EMAIL: <br/>--><?php //echo $oConfig->email; ?><!--</p>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
      
</section>