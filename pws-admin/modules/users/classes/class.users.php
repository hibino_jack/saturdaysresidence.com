<?php
class Users extends DB
{
    //var $session_name = "pws_session";
    var $session_name = "oUser";
    var $tbUsers = "pws_users";

    function __construct() {
        $this->Page = new Page();
        $this->Page->module_name = "Users"; // ucfirst
        $this->module_name = strtolower($this->Page->module_name); // strtolower
    }

    function get_session_name($session_name='')
    {
        if(empty($session_name))
            return "oUser";
        else
            return $session_name;
    }

    function login($username, $passwd, $remember = NULL, $query_type = NULL) {
        if(!isset($query_type))
            $query_type = "fetchAssoc";

        $sql = "SELECT $this->tbUsers.* FROM $this->tbUsers ";
        $sql .= "WHERE username = '$username' AND password = MD5('$passwd')";
        $data_u = DB::query($sql, $query_type);

        if(empty($data_u)) {
            $sql = "SELECT $this->tbUsers.* FROM $this->tbUsers ";
            $sql .= "WHERE email = '$username' AND password = MD5('$passwd')";
            $data_e = DB::query($sql, $query_type);
            if($data_e)
                $data =$data_e;
        } else {
            $data = $data_u;
        }

        if($data) {
            //set $oUser and session
            $this->oUser($data[0]['id']);
            //print_r($_SESSION[$this->get_session_name()]);
            //exit;
            if($remember=="Yes") {
                // clear cookies first
                setcookie($this->get_session_name()."[sessid]", '', time()-604800);  /* expire in 9 hour */
                setcookie($this->get_session_name()."[user_id]", '', time()-604800);  /* expire in 9 hour */
                setcookie($this->get_session_name()."[group_id]", '', time()-604800);  /* expire in 9 hour */
                setcookie($this->get_session_name()."[username]", '', time()-604800);  /* expire in 9 hour */

                // set the cookies
                setcookie($this->get_session_name()."[sessid]", session_id(), time()+604800);  /* expire in 9 hour */
                setcookie($this->get_session_name()."[user_id]", $data[0]['id'], time()+604800);  /* expire in 9 hour */
                setcookie($this->get_session_name()."[group_id]", $data[0]['id'], time()+604800);  /* expire in 9 hour */
                setcookie($this->get_session_name()."[username]", $data[0]['username'], time()+604800);  /* expire in 9 hour */
            }
            return true;
        } else {
            return false;
        }
    }

    function oUser($id) {
        if($id)
            $data = $this->select($id);

        if($data) {
            $GLOBALS['oUser'] = array(
                "sessid" => session_id(),
                "id" => $data[0]['id'],
                "group_id" => $data[0]['group_id'],
                "username" => $data[0]['username'],
                "password" => $data[0]['password'],
                "email" => $data[0]['email'],
                "first_name" => $data[0]['first_name'],
                "last_name" => $data[0]['last_name'],
                "profile_photo" => $data[0]['profile_photo'],
                "last_login" => $data[0]['last_login'],
                "ip_address" => $data[0]['ip_address'],
                "status" => $data[0]['status'],
            );
            $_SESSION['oUser'] = $GLOBALS['oUser'];

            if($_SESSION['oUser']) {
                return $_SESSION['oUser'];
            }
        }
    }

    function generate_api_token_key($post_vars) {
        global $oUser;
        if($post_vars)
            extract($post_vars);

        //PRE($post_vars);

        if ($oUser->username && $oUser->password) {
            $authenticator['parameter'] = array(
                "user" => $oUser->username,
                "pass" => $oUser->password
            );

            require_once(PATH_API_ROOT.'auth.php');
            $auth = new PHP_API_AUTH(array(
                'secret'=> 'a8c1556991f48ad6cac4244d1b6a6f5812f72a60',
                'username'=> $oUser->username,
                'password'=> $oUser->password,
                'authenticator'=> $authenticator,
                'method' => "POST",
                'ttl'=>30
            ));
            $auth->executeCommand();
            if (empty($_SESSION['csrf'])) {
                exit(header('HTTP/1.1 403 Access denied'));
            } else {
                $_SESSION[$this->get_session_name()]['csrf'] = $_SESSION['csrf'];
                $this->generate_token_txt_file($_SESSION['csrf']);
                echo $_SESSION['csrf'];
            }
        }else{
            $authenticator = '';
            $_SESSION['user'] = '';
            return false;
        }
    }

    function generate_token_txt_file($csrf) {
        global $oUser;
        if($csrf) {
            $tmp_fname = PATH_CACHE_ROOT . "token_" . $csrf . ".txt";
            $file = fopen($tmp_fname, "w") or die("Unable to open file!");
            $txt = "";
            if ($oUser->sessid)
                $txt .= $oUser->sessid . "\n";
            if ($oUser->id)
                $txt .= $oUser->id . "\n";
            if ($oUser->username)
                $txt .= $oUser->username . "\n";
            if ($oUser->password)
                $txt .= $oUser->password . "\n";
            if ($oUser->email)
                $txt .= $oUser->email . "\n";
            if ($oUser->first_name)
                $txt .= $oUser->first_name . "\n";
            if ($oUser->last_name)
                $txt .= $oUser->last_name . "\n";
            if ($csrf)
                $txt .= $csrf. "\n";

            fwrite($file, $txt);
            fclose($file);
        }
    }

    function read_token_txt_file($tmp_fname) {
        // Get a file into an array.  In this example we'll go through HTTP to get
        // the HTML source of a URL.
        //$lines = file($tmp_fname);

        // Loop through our array, show HTML source as HTML source; and line numbers too.
        //foreach ($lines as $line_num => $line) {
        //    echo "Line #<b>{$line_num}</b> : " . htmlspecialchars($line) . "<br />\n";
        //}

        // Another example, let's get a web page into a string.  See also file_get_contents().
        //$html = implode('', file($tmp_fname));

        // Using the optional flags parameter since PHP 5
        return $trimmed = file($tmp_fname, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
    }

    function login_recheck() {
        //Cookie check!!
        if(isset($_COOKIE[$this->get_session_name()])) {
            $arr = array(
                "sessid" => $_COOKIE[$this->get_session_name()]['sessid'],
                "user_id" => $_COOKIE[$this->get_session_name()]['user_id'],
                "group_id" => $_COOKIE[$this->get_session_name()]['group_id'],
                "username" => $_COOKIE[$this->get_session_name()]['username']
            );
            $_SESSION[$this->get_session_name()] = $arr;
            $data = true;
        }

        if(isset($_SESSION[$this->get_session_name()]))
            if ($_SESSION[$this->get_session_name()]->sessid == session_id()) //if ($_SESSION[$this->get_session_name()]['sessid'] == session_id())
                $data = true;
				
        if(!$data) {
            //redirect(PATH_ADMIN."login.php");
            //exit;
        }
        //java_goback(); die();
    }

    function permission_check($group_data = NULL)
    {
        if ($_SESSION[$this->get_session_name()]['group_id'] == "") {
            //if ($_SESSION[$this->get_session_name()]['status'] == "" || $_SESSION[$this->get_session_name()]['userposition'] == "") {
            java_goback();
            die();
        } else {
            foreach ($group_data as $value) {
                if($_SESSION[$this->get_session_name()]['group_id'] == $value) {
                    java_goback();
                    die();
                }
            }
        }
    }

    function logout()
    {
        // set the expiration date to one hour ago
        if(isset($_COOKIE[$this->get_session_name()])) {
            // clear cookies first
            setcookie("oUser[sessid]", '', time()-604800);  /* expire in 9 hour */
            setcookie("oUser[user_id]", '', time()-604800);  /* expire in 9 hour */
            setcookie("oUser[group_id]", '', time()-604800);  /* expire in 9 hour */
            setcookie("oUser[username]", '', time()-604800);  /* expire in 9 hour */
        }

        //delete csrf token key
        $tmp_fname = PATH_CACHE_ROOT."token_".$_SESSION['csrf'].".txt";
        if(is_file($tmp_fname)) { unlink($tmp_fname); }

        // Unset all of the session variables.
        //session_start();
        session_unset();
        $_SESSION = array();

        // If it's desired to kill the session, also delete the session cookie.
        // Note: This will destroy the session, and not just the session data!
        /*
        if (ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 604800,
                $params["path"], $params["domain"],
                $params["secure"], $params["httponly"]
            );
        }
        */

        /*if(ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();
            setcookie(
                session_name(),
                '',
                time() - 604800,
                $params["path"],
                $params["domain"],
                $params["secure"],
                $params["httponly"]
            );
        }*/

        // Finally, destroy the session.
        session_destroy();
    }

    function select($id = NULL, $email = NULL, $is_active = NULL, $query_type = NULL) {
        if (!isset($query_type))
            $query_type = "fetchAssoc";

        $sql = "SELECT * FROM $this->tbUsers";

        if($id)
            $ct[] = "$this->tbUsers.id = '$id' ";
        if($email)
            $ct[] = "$this->tbUsers.email = '$email' ";
        if($is_active)
            $ct[] = "$this->tbUsers.is_active = '$is_active' ";
        if(is_array($ct))
            $sql = $sql." WHERE ".implode(" AND ", $ct);

        return $data = DB::query($sql, $query_type);
    }

    function check_duplicate_username($username) {
        $sql = "SELECT * FROM $this->tbUsers WHERE username = '$username'";
        $data = DB::query($sql, "count");
        if($data > 0) {
            $json_data = array('message' => 'Duplicated', 'result' => $data);
        }else{
            $json_data = array('message' => 'OK', 'result' => $data);
        }
        echo json_encode($json_data);
    }

    function check_duplicate_email($email) {
        $sql = "SELECT * FROM $this->tbUsers WHERE email = '$email'";
        $data = DB::query($sql, "count");
        if($data > 0) {
            $json_data = array('message' => 'Duplicated', 'result' => $data);
        }else{
            $json_data = array('message' => 'OK', 'result' => $data);
        }
        echo json_encode($json_data);
    }

    function insert($post_vars) {
        if($post_vars)
            extract($post_vars);

        $first_name = DB::escape($first_name);
        $last_name = DB::escape($last_name);
        $email = DB::escape($email);

        $field = "`id`, `group_id`, `username`, `password`, `email`, `salt`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `first_name`, `last_name`, `profile_photo`, `created_on`, `last_login`, `ip_address`, `status`, `is_active`";
        $value = "'$id', '$group_id', '$username', '".MD5($password)."', '$email', '$salt', '$activation_code', '$forgotten_password_code', '$forgotten_password_time', '$remember_code', '$first_name', '$last_name', '$profile_photo', NOW(), '$last_login', '".get_ipaddress()."', '$status', '$is_active'";
        $sql = "INSERT INTO $this->tbUsers ($field) VALUES ($value)";

        $id = DB::query($sql, "lastInsertId");
        if($id) {
            //Profile Photos Upload
            if($_FILES['profile_photo']['name']){
                // images upload
                $profile_photo = $_FILES['profile_photo'];
                $file_count = count($profile_photo['name']);

                //print_r($profile_photo);
                for($i= 0 ; $i <= $file_count; $i++) {
                    $profile_photo_name = uploadFile($this->module_name, $id, $profile_photo, "photo", "profile_photo.png");
                    if($profile_photo_name)
                        $this->update_profile_photo($id, $profile_photo_name);
                }
                unset($profile_photo, $file_count);
            }
            return $id;
        }
    }

    //INSERT INTO `pws_users` (`id`, `group_id`, `username`, `password`, `email`, `salt`, `activation_code`,
    // `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `first_name`, `last_name`, `profile_photo`,
    // `created_on`, `last_login`, `ip_address`, `status`, `is_active`) VALUES
    function update($post_vars) {

        if($post_vars)
            extract($post_vars);

        $first_name = DB::escape($first_name);
        $last_name = DB::escape($last_name);
        $email = DB::escape($email);

        if($group_id)
            $ct[] = "$this->tbUsers.group_id = '$group_id' ";
        if($username)
            $ct[] = "$this->tbUsers.username = '$username' ";
        //if($password)
        //    $ct[] = "$this->tbUsers.password = '$password' ";

        if($password) {
            $ct[] = "$this->tbUsers.password = MD5('$password') ";
        } else {
            $ct[] = "$this->tbUsers.password = '$old_password' ";
        }

        if($email)
            $ct[] = "$this->tbUsers.email = '$email' ";
        if($salt)
            $ct[] = "$this->tbUsers.salt = '$salt' ";
        if($activation_code)
            $ct[] = "$this->tbUsers.activation_code = '$activation_code' ";
        if($forgotten_password_code)
            $ct[] = "$this->tbUsers.forgotten_password_code = '$forgotten_password_code' ";
        if($forgotten_password_time)
            $ct[] = "$this->tbUsers.forgotten_password_time = '$forgotten_password_time' ";
        if($remember_code)
            $ct[] = "$this->tbUsers.remember_code = '$remember_code' ";
        if($first_name)
            $ct[] = "$this->tbUsers.first_name = '$first_name' ";
        if($last_name)
            $ct[] = "$this->tbUsers.last_name = '$last_name' ";
        if($profile_photo)
            $ct[] = "$this->tbUsers.profile_photo = '$profile_photo' ";
        if($created_on)
            $ct[] = "$this->tbUsers.created_on = '$created_on' ";
        //if($last_login)
        //    $ct[] = "$this->tbUsers.last_login = '$last_login' ";

        $ct[] = "$this->tbUsers.last_login = NOW() ";
        $ct[] = "$this->tbUsers.ip_address = '".get_ipaddress()."' ";

        if($status)
            $ct[] = "$this->tbUsers.status = '$status' ";
        if($is_active)
            $ct[] = "$this->tbUsers.is_active = '$is_active' ";

        if(is_array($ct))
            $field = implode(", ", $ct);

        $sql = "UPDATE $this->tbUsers SET $field WHERE id = '$itemid'";
        //return $data = DB::query($sql, "fetchAssoc");
        $data = DB::query($sql);
        if($data){

            //Multiple Photos Upload
            /*
            if($_FILES['profile_photo']['name']){
                $profile_photo = $_FILES['profile_photo'];
                $profile_photo_name = uploadMultipleFile($this->module_name, $itemid, $profile_photo, "photo");
                if($profile_photo_name) {
                    //print_r($profile_photo_name);
                    foreach ($profile_photo_name as $v):
                        $this->update_photo($itemid, $v);
                    endforeach;
                }
                unset($profile_photo, $file_count);
            }
            */

            //Profile Photos Upload
            if($_FILES['profile_photo']['name']){
                // images upload
                $profile_photo = $_FILES['profile_photo'];
                //print_r($profile_photo);
                if(count($profile_photo['name']) > 0) {
                    $profile_photo_name = uploadFile($this->module_name, $itemid, $profile_photo, "photo", "profile_photo.png");
                    if($profile_photo_name)
                        $this->update_profile_photo($itemid, $profile_photo_name);
                }
                unset($profile_photo, $file_count);
            }

            return $data;
        } else {
            return false;
        }
    }

    function update_profile_photo($id, $profile_photo) {
        $field = "profile_photo = '$profile_photo' ";
        $sql = "UPDATE $this->tbUsers SET $field WHERE id = '$id'";
        return $data = DB::query($sql, "fetchAssoc");
    }

    function update_status($id, $status) {
        $field = "status = '$status' ";
        $sql = "UPDATE $this->tbUsers SET $field WHERE id = '$id'";
        return $data = DB::query($sql, "fetchAssoc");
    }

    function update_password($id, $password) {
        if(isset($id) && isset($password)) {
            $field = "password = MD5('$password') ";
            $sql = "UPDATE $this->tbUsers SET $field WHERE id = '$id'";
            return $data = DB::query($sql, "fetchAssoc");
        }
    }

    function delete($post_vars) {
        if($post_vars)
            extract($post_vars);

        if($itemid != "1") {
            $sql = "DELETE FROM `$this->tbUsers` WHERE `$this->tbUsers`.`id` = '$itemid' ";
            $data = DB::query($sql);
            if($data) {
                //delete
                //removeFile($itemid, $this->module_name, "profile_photo.png");
                //delete all files and folders belong to this ID
                return delete_directory(PATH_UPLOAD_ROOT.$this->module_name.'/'.$itemid.'/');
            }
        } else {
            return false;
        }
    }

    function reset_password($email) {
        global $oConfig;
        //PRE($oConfig);

        if (!isset($query_type))
            $query_type = "fetchAssoc";

        if ($email) {
            $sql = "SELECT * FROM $this->tbUsers WHERE email = '$email'";
            $data = DB::query($sql, "fetchAssoc");
            //PRE($data);
            if($data) {

                //recipient
                if($data[0]['first_name']) {
                    $to = $data[0]['first_name']." ".$data[0]['last_name'].' <'.$email.'>';
                } else {
                    $to = $email.' <'.$email.'>';
                }

                //sender
                if(empty($oConfig->contact_email)) {
                    $from = 'No-Reply <no-reply@phuketwebstudio.com>';
                }else{
                    $from = $oConfig->contact_name." <".$oConfig->contact_email.">";
                }

                $new_password = generatePassword(8,4);

                //subject and the html message
                $subject = 'Password recovery email to ' . $data[0]['first_name']." ".$data[0]['last_name'];
                $message = '
                    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
                    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                    <html xmlns="http://www.w3.org/1999/xhtml">
                    <head></head>
                    <body>
                    <table>
                        <tr><td>Hi ,</td><td>' . $data[0]['first_name']." ".$data[0]['last_name'] . '</td></tr>
                        <tr><td colspan=2><br><br>Yor were asked for reset the password fron us,<br>So here you are:<br><br></td></tr>
                        <tr><td>Username :</td><td>' . $data[0]['username'] . '</td></tr>
                        <tr><td>Email :</td><td>' . $data[0]['email'] . '</td></tr>
                        <tr><td>Password :</td><td>' .$new_password. '</td></tr>
                    </table>
                    </body>
                    </html>';

                //send the mail
                $result = quickmail($from, $to, $subject, $message ,$cc = NULL);
                if ($result) {
                    $this->update_password($data[0]['id'], $new_password);
                    return true;
                } else {
                    return false;
                }
            }
        }
    }

}