<?php
$module = "Fieldguide";
$module_class = strtolower($module);

if(file_exists(PATH_ADMIN_MODULES_ROOT.$module_class.'/classes/class.'.$module_class.'.php')) {    
	require_once(PATH_ADMIN_MODULES_ROOT.$module_class . '/classes/class.'.$module_class.'.php');    
	$$module_class = new $module();

    if (empty($_GET['pageno'])) {
        $pageno = "1";
    } else {
        $pageno = $_GET['pageno'];
    }
    if (empty($data_total))
        $data_total = count($$module_class->select());

    $$module_class->Page->set_data_total($data_total);
    $$module_class->Page->set_perpage(3);
    $$module_class->Page->set_pageno($pageno);
    $$module_class->Page->set_sortby("id");
    $$module_class->Page->set_orderby("DESC");

    $data = $$module_class->select();
    //PRE($data);
}
?>
	<section class="content-area">

    	<div class="field-guide">
            <div class="container">
            	<div class="row bg-white">
                	<div class="col-md-12">

							<!-- leftside-->
                            <div class="field-guide-leftside col-md-8">
                            
                            	<!--page-heading-->
                                 <div class="row page-heading">
                                           	<div class="col-lg-offset-2 col-lg-8 text-center">
                                                    <h1><span>the</span> Field Guide</h1>
                                                    <p>It's time to share with you what to do in Phuket beyond , top attractions, how to get around ...</p>
                                             </div>
                                 </div>
                                 
								<?php if(is_array($data) && (count($data) > 0)){ 
                                	//PRE($data[0]);
                                    $hilight_image_header = "http://placehold.it/800x550&text=".$data[0]['title'];	
                                    if($data[0]['header_photo'] != "")
                                    {
                                        $pic = BASE_UPLOAD.$module_class."/".$data[0]['id']."/".$data[0]['header_photo'];
                                        $pic_realpath = PATH_UPLOAD_ROOT.$module_class."/".$data[0]['id']."/".$data[0]['header_photo'];
                                        if(is_file($pic_realpath)) {
                                            $hilight_image_header = PATH_ROOT."timthumb.php?src=".$pic."&w=800&h=550";
                                        }
                                    }
                                
                                ?>
                                 <!--article-item-->
                                 <div class="row article-item">
                                 	<div class="col-md-12 article-img">
                                    	<div class="row">
                                    	<img src="<?php echo $hilight_image_header; ?>" class="img-responsive">
                                        </div>
                                    </div>
                                    <div class="col-md-12 article-shortdesc">
                                    	<h3><?php echo $data[0]["title"]; ?></h3>
                                        <p><?php echo substr(strip_tags($data[0]["description"]),0,150); ?>...</p>
                                        <p>
                                            <a href="<?php echo BASE_URL . $lang_id . "/field-guide/" . $data[0]["filename"]; ?>.html">READ
                                                MORE ></a></p>
                                       
                                    </div>
                                    <div class="col-md-12">
                                        <div class="row"><hr></div>
                                    </div>
                                 </div>
								 <?php } ?>
                                
								<?php if(is_array($data) && (count($data) > 0)){ 
                                $is_first_loop = true;
                                foreach($data as $item){ 
                                
                                    $image_header = "http://placehold.it/800x550&text=".$item['title'];	
                                    if($item['header_photo'] != "")
                                    {
                                        $pic = BASE_UPLOAD.$module_class."/".$item['id']."/".$item['header_photo'];
                                        $pic_realpath = PATH_UPLOAD_ROOT.$module_class."/".$item['id']."/".$item['header_photo'];
                                        if(is_file($pic_realpath)) {
                                            $image_header = PATH_ROOT."timthumb.php?src=".$pic."&w=800&h=550";
                                        }
                                    }
                                
                                    if(!$is_first_loop) {
                                ?>
                                  <!--article-item-->
                                 <div class="row article-item">
                                 	<div class="col-md-6 article-img">
                                    	<div class="row">
                                    	<img src="<?php echo $image_header; ?>" class="img-responsive">
                                        </div>
                                    </div>
                                    <div class="col-md-6 article-shortdesc">
                                    	
                                    	<h3><?php echo $item["title"]; ?></h3>
                                        <p><?php echo substr(strip_tags($item["description"]),0,150); ?>...</p>
                                        <p>
                                            <a href="<?php echo BASE_URL . $lang_id . "/field-guide/" . $item["filename"]; ?>.html">READ
                                                MORE ></a></p>
                                      
                                    </div>
                                    <div class="col-md-12">
                                    	<div class="row"><hr></div>
                                    </div>
                                 </div>
								<?php 
                                    } //if 
                                     $is_first_loop = false;
                                } //foreach
								}//if ?>


                                <!--pagination-->
                                <nav class="row  article-item" aria-label="...">
                                    <?php
                                    echo $$module_class->Page->page_navigation($data_total, BASE_URL . $lang_id . "/field-guide");
                                    ?>
                                    <!--
                                    <ul class="pager">
                                    <li class="previous"><a href="#">< prev article</a></li>
                                    <li class="next"><a href="#">next article > </a></li>
                                    </ul>
                                    -->
                                </nav>
                            </div>
                            
                            <!-- rightside-->
                			<?php include(PATH_THEMES_ROOT."inc/nav-right.php"); ?>
                

                    </div>
                </div>
                
            </div>
        
        </div>
    
    
    
    </section>