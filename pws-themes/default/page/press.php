<?php
$module = "Press";
$module_class = strtolower($module);

if(file_exists(PATH_ADMIN_MODULES_ROOT.$module_class.'/classes/class.'.$module_class.'.php')) {    
	require_once(PATH_ADMIN_MODULES_ROOT.$module_class . '/classes/class.'.$module_class.'.php');    
	$$module_class = new $module();

    if (empty($_GET['pageno'])) {
        $pageno = "1";
    } else {
        $pageno = $_GET['pageno'];
    }
    if (empty($data_total))
        $data_total = count($$module_class->select());

    $$module_class->Page->set_data_total($data_total);
    $$module_class->Page->set_perpage(3);
    $$module_class->Page->set_pageno($pageno);
    $$module_class->Page->set_sortby("id");
    $$module_class->Page->set_orderby("DESC");

    $data = $$module_class->select();
	//PRE($data);
}

$data_content[0]['description'] = str_ireplace(array("\r","\n",'\r','\n'),'', $data_content[0]['description']);
//$image_header = "http://placehold.it/1450x500&text=".$data_content[0]['title'];
if($data_content[0]['header_photo'] != "")
{
	$pic = BASE_UPLOAD."contents/".$data_content[0]['id']."/".$data_content[0]['header_photo'];
	$pic_realpath = PATH_UPLOAD_ROOT."contents/".$data_content[0]['id']."/".$data_content[0]['header_photo'];
	if(is_file($pic_realpath)) {
		$image_header = PATH_ROOT."timthumb.php?src=".$pic."&w=1450&h=500";
	}
}

?>
<header> 
    <div class="page-heading">
        <div class="container">
            <div class="row">
                <div class="col-md-offset-2 col-md-8 text-center">
            	<h1><?php echo $data_content[0]['title']; ?></h1>
                <?php if(!empty($data_content[0]['subtitle'])){ ?>           
                    <h3 style="font-style:italic"><?php echo $data_content[0]['subtitle']; ?></h3>
				<?php }
				if(!empty($data_content[0]['short_description'])){ ?>                                
                    <p><?php echo $data_content[0]['short_description']; ?></p>
				<?php } ?>       
                </div>
            </div>
        </div>
    </div>
    <?php if($image_header){ ?>
    <!-- room header Section -->
    <div class="inner-header">
        <div class="container">
                <div class="row">
                    <div class="inner-header-fill" style="background-image:url('<?php echo $image_header; ?>');"></div>
                </div>
         </div>
    </div>
    <?php } ?>    
</header>

<section class="content-area page-press">
	<?php if($data_content[0]['description']) { ?>
    <div id="innerpage_intro">
        <div class="container">
            <div class="row">
                <div class="col-lg-offset-1 col-lg-10">
                	<div class="row innerpage_intro <?php echo ($image_header) ? "":"no-margin"; ?>">
                    <?php echo $data_content[0]['description']; ?>
                    </div><!--//saturdays_intro-->
                </div><!--//col-md-6-->
         	</div>
         </div>
      </div>
    <?php } ?>
    <?php if(is_array($data) && (count($data) > 0)){ 
	
		$hilight_image_header = "http://placehold.it/800x550&text=".$data[0]['name'];	
		if($data[0]['photoname'] != "")
		{
			$pic = BASE_UPLOAD.$module_class."/".$data[0]['id']."/".$data[0]['photoname'];
			$pic_realpath = PATH_UPLOAD_ROOT.$module_class."/".$data[0]['id']."/".$data[0]['photoname'];
			if(is_file($pic_realpath)) {
				$hilight_image_header = PATH_ROOT."timthumb.php?src=".$pic."&w=800&h=550";
			}
		}
	
	?>
    <!-- paragraphs-box Section -->
    <div class="paragraphs-box" style="margin-top:30px;">
        <div class="container">
            <div class="row paragraphs-items">
            	<div class="col-md-6">
                	<div class="row paragraphs-image">
                		<img src="<?php echo $hilight_image_header; ?>" class="img-responsive">
                    </div>
                </div>
               	<div class="col-md-6">
                	<div class="row paragraphs-content">
                    		<span class="date"><?php echo date("d/m/Y",strtotime($data[0]["updated_date"])); ?></span>
                            <h3><?php echo $data[0]["name"]; ?></h3>
                            <p><?php echo substr(strip_tags($data[0]["description"]),0,150); ?>...</p>
                        <p>
                            <a href="<?php echo BASE_URL . $lang_id . "/" . $module_class . "/" . $data[0]["filename"]; ?>.html"
                               class="btn btn-default">view article</a></p>
                       
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>
    
	<?php if(is_array($data) && (count($data) > 0)){ 
	$is_first_loop = true;
	?>
    <!-- paragraphs-3box Section -->
    <div class="paragraphs-3box">
        <div class="container">
            <div class="row paragraphs-3box-items">
            	<?php foreach($data as $item){ 
				
					$image_header = "http://placehold.it/800x550&text=".$item['name'];	
					if($item['photoname'] != "")
					{
						$pic = BASE_UPLOAD.$module_class."/".$item['id']."/".$item['photoname'];
						$pic_realpath = PATH_UPLOAD_ROOT.$module_class."/".$item['id']."/".$item['photoname'];
						if(is_file($pic_realpath)) {
							$image_header = PATH_ROOT."timthumb.php?src=".$pic."&w=800&h=550";
						}
					}
				
					if(!$is_first_loop) {
				?>
                    <!--box-1-->
                    <div class="col-md-4 paragraphs-item">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="paragraphs-image">
                                    <img src="<?php echo $image_header; ?>" class="img-responsive">
                                </div>
                                <div class="paragraphs-content">
                                    <span class="date"><?php echo date("d/m/Y",strtotime($item["updated_date"])); ?></span>
                                    <h3><?php echo $item["name"]; ?></h3>
                                    <p>
                                        <a href="<?php echo BASE_URL . $lang_id . "/" . $module_class . "/" . $item["filename"]; ?>.html"
                                           class="btn btn-default">view article</a></p>
                               
                                </div>
                                
                            </div>
                        </div>
                    </div>
                <?php 
					} //if 
					 $is_first_loop = false;
				} //foreach ?>

                <!--pagination-->
                <nav class="row"  aria-label="...">
                    <?php
                    echo $$module_class->Page->page_navigation($data_total, BASE_URL . $lang_id . "/press");
                    ?>
                    <!--<ul class="col-md-12 pager">
                        <li class="previous"><a href="#">< prev article</a></li>
                        <li class="next"><a href="#">next article > </a></li>
                     </ul>-->
                </nav>
                
            </div>
        </div>
    </div>
    <?php } ?>
    
    
    
    
    </section>
