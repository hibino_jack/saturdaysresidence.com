<?php
/*
 * Cupid MVC CMS System
 * Main Function Control
 * JaCK Peerawat Promrit
 */

    //java script function
	function redirect($url='.')
	{
		echo "<meta http-equiv='refresh' content='0;url=$url'>";
	}
	
	function java_goback()
	{
		echo "<script language='javascript'>history.go(-1);</script>";
	}
	
	function alert($msg = NULL)
	{
		echo "<script language='javascript'>alert('$msg');</script>";
	}

    function generateUsername($length=8, $strength=8) {
        $vowels = 'aeiou';
        $consonants = 'abcdefghijklmnopqrstuvwxyz123456789@#$%';
        if ($strength & 1) {
            $consonants .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        }
        if ($strength & 2) {
            $vowels .= "AEUIOU";
        }
        if ($strength & 4) {
            $consonants .= '123456789';
        }
        if ($strength & 8) {
            $consonants .= '@#$%';
        }

        $username = '';
        $alt = time() % 2;
        for ($i = 0; $i < $length; $i++) {
            if ($alt == 1) {
                $username .= $consonants[(rand() % strlen($consonants))];
                $alt = 0;
            } else {
                $username .= $vowels[(rand() % strlen($vowels))];
                $alt = 1;
            }
        }
        return $username;
    }

    function generatePassword($length=4, $strength=4) {
        $vowels = 'aeiou';
        $consonants = 'abcdefghijklmnopqrstuvwxyz';
        if ($strength & 1) {
            $consonants .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        }
        if ($strength & 2) {
            $vowels .= "AEUIOU";
        }
        if ($strength & 4) {
            $consonants .= '123456789';
        }
        if ($strength & 8) {
            $consonants .= '@#$%';
        }

        $password = '';
        $alt = time() % 2;
        for ($i = 0; $i < $length; $i++) {
            if ($alt == 1) {
                $password .= $consonants[(rand() % strlen($consonants))];
                $alt = 0;
            } else {
                $password .= $vowels[(rand() % strlen($vowels))];
                $alt = 1;
            }
        }
        return $password;
    }

    function getColorRandom() {
        $randomString = md5(time().rand(0,999)); //like "d73a6ef90dc6a ..."
        $r = substr($randomString,0,2); //1. and 2.
        $g = substr($randomString,2,2); //3. and 4.
        $b = substr($randomString,4,2); //5. and 6.
        $color = "#".$r.$g.$b;
        return $color;
    }

    /**
     * @param $string
     * @return string
     */
    function seo_friendly_url($string){
        global $oDB;
        $string = str_replace(array('[\', \']'), '', $string);
        $string = preg_replace('/\[.*\]/U', '', $string);
        $string = preg_replace('/&(amp;)?#?[a-z0-9]+;/i', '-', $string);
        $string = htmlentities($string, ENT_COMPAT, 'utf-8');
        $string = preg_replace('/&([a-z])(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig|quot|rsquo);/i', '\\1', $string );
        $string = preg_replace(array('/[^a-z0-9]/i', '/[-]+/') , '-', $string);
        $string = str_replace('amp','and', $string);

        return strtolower(trim($string, '-'));
    }

    /*
     * this has been changed and no longer sends messages using the admin setting for batch control
     * SET WITH PHP MAILER
     */
    function sendEmail($msg_array) {
        global $Page;
        global $oConfig;

        date_default_timezone_set('Etc/UTC');
        require_once(PATH_CLASSES_ROOT.'PHPMailer/PHPMailerAutoload.php');

    //    $mail = new PHPMailer;
    //    $mail->CharSet = 'UTF-8';
    //    //Enable SMTP debugging.
    //    $mail->SMTPDebug = 0;
    //    //Set PHPMailer to use SMTP.
    //    $mail->isSMTP();
    //    //Set SMTP host name
    //    $mail->Host = $oConfig->smtp_srv;
    //    //Set this to true if SMTP host requires authentication to send email
    //    $mail->SMTPAuth = true;
    //    //Provide username and password
    //    $mail->Username = $oConfig->smtp_usr;
    //    $mail->Password = $oConfig->smtp_pass;
    //    //If SMTP requires TLS encryption then set it
    //    $mail->SMTPSecure = "tls";
    //    //Set TCP port to connect to
    //    $mail->Port = $oConfig->smtp_port;


        $mail = new PHPMailer;
        $mail->CharSet = 'UTF-8';
        $mail->SMTPDebug = 0;
        $mail->isSMTP();
        $mail->Host = 'smtp.mailgun.org';
        $mail->SMTPAuth = true;
        $mail->Username = 'postmaster@sandbox7bcb8f950a994961bf45350e62d5f7d5.mailgun.org';
        $mail->Password = '912418828a27458f454578ea7f0c8957';
        $mail->SMTPSecure = "tls";
        $mail->Port = 587;

        $mail->From = $msg_array['email_from'] ? $msg_array['email_from'] : $oConfig->smtp_usr;

    //    $mail->From = $oConfig->smtp_usr;
        $mail->FromName = $msg_array['from_name'];

        $mail->addAddress($msg_array['email_to'],$msg_array['to_name']);

        $mail->isHTML(true);
        $mail->Subject = $msg_array['subject'];

        if ( $msg_array['file_path'] )
            $mail->addAttachment($msg_array['file_path']);

        if ( $msg_array['multi_file'] ){
            foreach ($msg_array['multi_file'] as $k => $v){
                $mail->addAttachment($msg_array['multi_file'][$k]);
            }
        }

        if ( $msg_array['template']) {
            $html_msg = $Page->render($msg_array['template']);
            $mail->msgHTML($html_msg, dirname(__FILE__));
        }
        else       {
            $mail->msgHTML($msg_array['msg_to_send']);
        }


        if(!$mail->send())
        {
            $return = "Error: " . $mail->ErrorInfo;
            $date = date("Y-m-d");
            $field = "`email_address`, `subject`, `message`, `date_sent`, `status`";
            $value = "'".$msg_array['email_to']."','".$msg_array['subject']."','".$msg_array['msg_to_send']."','$date','waiting'";
            //$value = "'".$msg_array['email_to']."','".$msg_array['subject']."','".$return."','$date','waiting'";

            INSERT('site_outgoing_emails',$field,$value);
        }
        else
        {
            $return = "Success";
        }

        return $return;

    }


//Simple mail function with HTML header
	function quickmail($from, $to, $subject, $content ,$cc = NULL) {
		$newline = "\n";
		$headers  = "MIME-Version: 1.0".$newline;
		$headers .= "Content-type: text/html; charset=utf-8 ".$newline; 
		$headers .= "From: $from".$newline;
		if(is_array($cc)) {
			for($i=0; $i < count($cc); $i++) {
				$ccmail = $cc[$i];
				if(format_email($ccmail))
					$headers .= "CC: $ccmail".$newline;
			}
		} else {
			if(format_email($cc))
				$headers .= "CC: $ccmail".$newline;
		}
		$headers .= "Reply-To: $from".$newline;
		$headers .= "X-Sender: $from".$newline;
		$headers .= "X-Mailer: PWS".$newline; //mailer
		$headers .= "X-Priority: 3".$newline; //1 UrgentMessage, 3 Normal
		$headers .= "Return-Path: $from".$newline;
		//echo nl2br($headers);
	    $result =  mail($to,$subject,$content,$headers);
	    //return $result;
        if($result){
            return true;
        } else {
            return false;
        }
    }

    function format_email($data) {
        $flag = preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])+@([a-zA-Z0-9_-])+(\.[a-zA-Z0-9_-]+)+/", $data);
        return $flag;
    }

//------------------------------------------- Upload File Function by ~ J ----------------------------------------------------------//
    //string
    function remove_whitespace($str, $option = nulll) {
        $str = str_replace(' ', '', $str);
        switch($option)
        {
            case "small" :
                $str = strtolower($str);
                break;
        }
        preg_replace('/[^a-zA-Z0-9_%\[().\]\\/-]/s', '', $str);
        return $str;
    }

    function change_filename($filename, $new_name) {
        $name = explode(".",$filename);
        $i = count($name) - 1;
        if($i >= 1)
            return $new_name.".".$name[$i];
    }

    //check & make new directory
    function MKDIRS($dir, $mode = 0777, $recursive = true) {
        if(is_null($dir) || $dir === ""){
            return FALSE;
        }
        if(is_dir($dir) || $dir === "/"){
            return TRUE;
        }
        if(mkdirs(dirname($dir), $mode, $recursive)){
            return mkdir($dir, $mode, $recursive);
        }
        return FALSE;
    }

    function removeFile($id, $module_name, $filename, $delete_mode = 'each') {
        $filePath = PATH_UPLOAD_ROOT.$module_name.'/'.$id.'/';
        //echo "<br />Delete: <br>".$filePath.$filename;
        if ($delete_mode == "all") {
            delete_directory($filePath);
        } else {
            if (is_file($filePath.$filename)) {
                //echo "<br />Isfile: ";
                $deleteok = unlink($filePath.$filename);
                //updateFileItem($id, '', '');
                if($deleteok) {
                    return true;
                } else {
                    return false;
                }
            }
        }
    }

    //Recursive Delete Folder.
    function delete_directory($dir) {
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (filetype($dir."/".$object) == "dir") delete_directory($dir."/".$object); else unlink($dir."/".$object);
                }
            }
            reset($objects);
            $deleteok = rmdir($dir);
            if($deleteok) {
                return true;
            } else {
                return false;
            }
        }
    }

    function delete_old_tmp($folderName = NULL){
    if(empty($folderName))
        $folderName = PATH_CACHE_ROOT;
    if (file_exists($folderName)) {
        foreach (new DirectoryIterator($folderName) as $fileInfo) {
            if ($fileInfo->isDot()) {
                continue;
            }

            $twoday = 2*24*60*60;
            //if (time() - $fileInfo->getCTime() >= 2*24*60*60) {
            if (time() - $fileInfo->getCTime() >= $twoday) {
                //PRE(time() - $fileInfo->getCTime()." = ".$twoday);
                unlink($fileInfo->getRealPath());
            }
        }
    }
}

    function uploadFile($module_name, $id, $data_file = NULL, $filetype = NULL, $set_filename = NULL, $target_path = NULL) {
        //set path
        if($target_path) {
            $target_dir = $target_path;
        } else {
            $target_dir = PATH_UPLOAD_ROOT.$module_name."/".$id."/";
        }
        //echo "<br />".$target_dir; exit;
        //echo "<br />";
        //$maxsize_upload = 8000000; //8MB
        //$maxsize_upload = 10000000; //10MB
        //$maxsize_upload = MAXSIZE_UPLOAD; //site.config

        //create folder
        if (!file_exists($target_dir)) {
            MKDIRS($target_dir);
        }

        //upload
        if($data_file) {
            $file = $data_file;
        }else{
            $file = $_FILES['file'];
        }

        //print_r($file);
        $file_total = count($file['name']);
        if($file_total > 0) {
            //for($i=0; $i < $file_total; $i++)
            //check file type
            switch($filetype) {
                case "photo" :
                    $array_filetype = array('image/jpeg', 'image/gif', 'image/pjpeg', 'image/png');
                    break;
                case "docs" :
                    $array_filetype = array('application/pdf', 'application/x-pdf', 'application/vnd.pdf', 'application/msword', 'application/vnd.ms-excel', 'application/vnd.ms-powerpoint', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/vnd.openxmlformats-officedocument.presentationml.presentation', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                    break;
                case "both" :
                default:
                    $array_filetype = array('image/jpeg', 'image/gif', 'image/pjpeg', 'image/png', 'application/pdf', 'application/x-pdf', 'application/vnd.pdf', 'application/msword', 'application/vnd.ms-excel', 'application/vnd.ms-powerpoint', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/vnd.openxmlformats-officedocument.presentationml.presentation', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                    break;
            }

            if(in_array($file['type'], $array_filetype)) {
                //check file size
                if($file['size'] <= MAXSIZE_UPLOAD) {
                    if($set_filename) {
                        $filename = remove_whitespace($set_filename, 'small');
                    }else{
                        $filename = $id."_".date("ymdHis")."_".remove_whitespace($file['name'], 'small'); // With date postfix
                    }

                    if(copy($file['tmp_name'], $target_dir.$filename)) {
                        return $filename;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                    //$error_upload = "Your file should not be over 8 MB.";
                }
            }
        } else {
            return false;
        }
    }

    function uploadMultipleFile($module_name, $id, $data_file = NULL, $filetype = NULL, $set_filename = NULL, $target_path = NULL) {
        //set path
        if($target_path) {
            $target_dir = $target_path;
        } else {
            $target_dir = PATH_UPLOAD_ROOT.$module_name."/".$id."/";
        }

        //create folder
        if (!file_exists($target_dir)) {
            MKDIRS($target_dir);
        }

        //set data $_FILES
        if($data_file) {
            $file = $data_file;
        }else{
            $file = $_FILES['file'];
        }

        //print_r($file);
        $file_total = count($file['name']);
        if($file_total > 0) {
            for($i=0; $i < $file_total; $i++) {
                //check file type
                switch ($filetype) {
                    case "photo" :
                        $array_filetype = array('image/jpeg', 'image/gif', 'image/pjpeg', 'image/png');
                        break;
                    case "docs" :
                        $array_filetype = array('application/pdf', 'application/x-pdf', 'application/vnd.pdf', 'application/msword', 'application/vnd.ms-excel', 'application/vnd.ms-powerpoint', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/vnd.openxmlformats-officedocument.presentationml.presentation', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                        break;
                    case "both" :
                    default:
                        $array_filetype = array('image/jpeg', 'image/gif', 'image/pjpeg', 'image/png', 'application/pdf', 'application/x-pdf', 'application/vnd.pdf', 'application/msword', 'application/vnd.ms-excel', 'application/vnd.ms-powerpoint', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/vnd.openxmlformats-officedocument.presentationml.presentation', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                        break;
                }

                if (in_array($file['type'][$i], $array_filetype)) {
                    //check file size
                    if ($file['size'][$i] <= MAXSIZE_UPLOAD) {
                        if ($set_filename) {
                            $filename = remove_whitespace($set_filename, 'small');
                        } else {
                            $filename = $id . "_" . date("ymdHis") . "_" . remove_whitespace($file['name'][$i], 'small'); // With date postfix
                        }

                        if (copy($file['tmp_name'][$i], $target_dir . $filename)) {
                            //return $filename;
                            $profile_photo_data[] = $filename;
                        } else {
                            return false;
                        }
                    } else {
                        return false;
                        //$error_upload = "Your file should not be over 8 MB.";
                    }
                }
            }
            if($profile_photo_data)
                return $profile_photo_data;
        } else {
            return false;
        }
    }

    /*
     * Used to create an image from base64
     *  This was added for image crop/upload
     */
    function data_to_img($match) {
        return $match;
        list(, $img, $type, $base64, $end) = $match;

        $bin = base64_decode($base64);
        $md5 = md5($bin);   // generate a new temporary filename
        $fn = "tmp/img/$md5.$type";
        file_exists($fn) or file_put_contents($fn, $bin);

        return "$img$fn$end";  // new <img> tag
    }

	//ipaddress
	function get_ipaddress() 
	{
		if (isset($_SERVER)) {
			if (isset($_SERVER["HTTP_X_FORWARDED_FOR"]))
				return $_SERVER["HTTP_X_FORWARDED_FOR"];
			if (isset($_SERVER["HTTP_CLIENT_IP"]))
				return $_SERVER["HTTP_CLIENT_IP"];
			return $_SERVER["REMOTE_ADDR"];
		}

		if (getenv('HTTP_X_FORWARDED_FOR'))
			return getenv('HTTP_X_FORWARDED_FOR');
		if (getenv('HTTP_CLIENT_IP'))
			return getenv('HTTP_CLIENT_IP');
		return getenv('REMOTE_ADDR');
	}

	function format_string($value)
	{
		$data = false;
		$tagBlacklist = array('applet', 'body', 'bgsound', 'base', 'basefont', 'embed', 'frame', 'frameset', 'head', 'html', 'iframe', 'ilayer', 'layer', 'link', 'meta', 'name', 'object', 'script', 'style', 'xml');
		if($value)
		{
			$value = strtolower($value);
			for($i=0; $i < count($tagBlacklist); $i++)
			{
				$blacktag = "<".$tagBlacklist[$i].">";
				$blacktag_status = preg_match($blacktag, $value);
				if($blacktag_status)
				{
					//echo htmlentities($blacktag);
					$data = false;
					break;
				}
				$data = true;
			}
		}
		return $data;
	}

	//convert 
	function string2number($value)
	{
		$data = "";
		if($value)
		{
			$value_new = str_replace(",","",$value);
			if(is_numeric($value_new))
				$data = $value_new;
		}
		return $data;	
	}

	//calulate age
	function date2age($a_certain_date) 
	{
		list ( $year, $month, $day ) = explode('-', $a_certain_date);
		$year_diff = date('Y') - $year;
		if ( date("m") < $month || (date("m") == $month && date("d") < $day))
		$year_diff--;
		return $year_diff;
	}

	//time
	function timeminbox($name, $default = NULL, $css = NULL)
	{
		$data = array(
				"", "00", 
				"01", "02", "03", "04", "05", "06", "07", "08", "09", "10",
				"11", "12", "13", "14", "15", "16", "17", "18", "19", "20",
				"21", "22", "23", "24", "25", "26", "27", "28", "29", "30",
				"31", "32", "33", "34", "35", "36", "37", "38", "39", "40",
				"41", "42", "43", "44", "45", "46", "47", "48", "49", "50",
				"51", "52", "53", "54", "55", "56", "57", "58", "59"
		);
		
		echo "<select name='$name' $css >\n";
		//echo "<option value=''>MM</option>";
		for($i=0; $i < count($data); $i++)
		{
			$select = "";
			if($data[$i] == $default)
			{
				$select = "selected";
			}
			echo "<option value='".$data[$i]."' $select >".$data[$i]."</option>";
		}
		echo "</select>\n";
	}
	
	function timehourbox($name, $default = NULL, $css = NULL)
	{
		$data = array(
				"", "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", 
				"10", "11", "12", "13", "14", "15", "16", "17", "18", "19", 
				"20", "21", "22", "23"
		);
		
		echo "<select name='$name' $css>\n";
		//echo "<option value=''>HH</option>";
		for($i=0; $i < count($data); $i++)
		{
			$select = "";
			if($data[$i] == $default)
			{
				$select = "selected";
			}
			echo "<option value='".$data[$i]."' $select >".$data[$i]."</option>";
		}
		echo "</select>\n";
	}
	
	//format date
	function date_check($date_value)
	{
		$data = explode("-",$date_value);
		return checkdate($data[1], $data[2], $data[0]);
	}
	
	function day_diff($date_start, $date_end)
	{
		date_default_timezone_set("Asia/Bangkok");
		$days = false;
		$date_start_check = $this->date_check($date_start);
		$date_end_check = $this->date_check($date_end);
		if($date_start_check && $date_end_check)
		{
			$diff = strtotime($date_end) - strtotime($date_start);
			$days = floor($diff/60/60/24);
			if(strtotime($date_start) == strtotime($date_end)){
				$days = 1;
			} else {
				$days = $days + 1;
			}
		}
		return $days;
	}

	function number2month($value)
	{
		$data = false;
		if($value )
		{
			$data_month = array(
				"January", "February", "March", "April", "May", "June", 
				"July", "August", "September", "October", "November", "December"
				);
			$value = $value - 1;
			if($value < 12)
			{
				$data = $data_month[$value];
			}
		}
		return $data;
	}
	
	function date_show($date_value)
	{
		// YYYY-MM-DD
		$data = false;
		if($date_value)
		{
			$data_tmp = explode("-",$date_value);
			$data = $data_tmp[2]." ".$this->number2month($data_tmp[1])." ".$data_tmp[0];
		}
		return $data;
	}

	function format_date($value, $format, $operator)
	{
        $data = "";
		if($format == "dmy")
		{
			// DD/MM/YYYY format
			if($operator == "/")
			{
				$value = explode("/", $value);
				$data = $value[2]."-".$value[1]."-".$value[0];
			}
		}
		return $data;
	}

    //check date expire
    function check_date_expire($date_expire)
    {
        $today = strtotime(date("Y-m-d"));
        $expire = strtotime($date_expire);
        if($today <= $expire)
            return true;
    }

    //date day expire
    function check_day_expire($numday)
    {
        if(is_numeric($numday))
        {
            $day_msg = "+".$numday." day";
            $today = strtotime($day_msg);
            return date("Y-m-d", $today);
        }
    }

	//swap
	function swap($str1, $str2, $i)
	{
		return ($i % 2)? $str2 : $str1;
	}

	//html
	function html_selectbox_data($data, $name, $default = NULL, $default_name = NULL, $option = NULL)
	{
		//echo "default = $default";
		if(!$default_name)
			$default_name = "--- Please Select ---";
		echo "<select name='$name' id='$name' $option>\n";
		echo "<option value=''>$default_name</option>\n";
		for($i=0; $i < count($data); $i++)
		{
			$select = "";
			if($default == $data[$i]['id'])
				$select = "selected";
			echo "<option value='{$data[$i]['id']}' $select>{$data[$i]['name']}</option>\n";
		}
		echo "</select>";
	}
	
	//html country
	function html_select_country($name, $default = NULL, $css = NULL)
	{
		$country = array(
		"Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla","Antarctica","Antigua and Barbuda","Argentina","Armenia","Aruba","Australia","Austria","Azerbaijan",
		"Bahamas", "Bahrain", "Bangladesh", "Barbados","Belarus","Belgium","Belize","Benin","Bermuda","Bhutan","Bolivia","Bosnia and Herzegovina", "Botswana","Bouvet Island","Brazil","British Indian Ocean Territory","Brunei Darussalam","Bulgaria","Burkina Faso","Burundi",
		"Cambodia","Cameroon","Canada","Cape Verde","Cayman Islands","Central African Republic","Chad","Chile","China","Christmas Island","Cocos Islands","Colombia","Comoros","Congo","Congo, Democratic Republic of the","Cook Islands","Costa Rica","Cote d Ivoire","Croatia","Cuba","Cyprus","Czech Republic",
		"Denmark","Djibouti","Dominica","Dominican Republic",
		"Ecuador","Egypt","El Salvador","Equatorial Guinea","Eritrea","Estonia","Ethiopia",
		"Falkland Islands","Faroe Islands","Fiji","Finland","France","French Guiana","French Polynesia",
		"Gabon","Gambia","Georgia","Germany","Ghana","Gibraltar","Greece","Greenland","Grenada","Guadeloupe","Guam","Guatemala","Guinea","Guinea-Bissau","Guyana",
		"Haiti","Heard Island and McDonald Islands","Honduras","Hong Kong","Hungary",
		"Iceland","India","Indonesia","Iran","Iraq","Ireland","Israel","Italy",
		"Jamaica","Japan","Jordan",
		"Kazakhstan","Kenya","Kiribati","Kuwait","Kyrgyzstan",
		"Laos","Latvia","Lebanon","Lesotho","Liberia","Libya","Liechtenstein","Lithuania","Luxembourg",
		"Macao","Macedonia","Madagascar","Malawi","Malaysia","Maldives","Mali","Malta","Marshall Islands","Martinique","Mauritania","Mauritius","Mayotte","Mexico","Micronesia","Moldova","Monaco","Mongolia","Montserrat","Morocco","Mozambique","Myanmar",
		"Namibia","Nauru","Nepal","Netherlands","Netherlands Antilles","New Caledonia","New Zealand","Nicaragua","Niger","Nigeria","Norfolk Island","North Korea","Norway",
		"Oman",
		"Pakistan","Palau","Palestinian Territory","Panama","Papua New Guinea","Paraguay","Peru","Philippines","Pitcairn","Poland","Portugal","Puerto Rico",
		"Qatar",
		"Romania","Russian Federation","Rwanda",
		"Saint Helena","Saint Kitts and Nevis","Saint Lucia","Saint Pierre and Miquelon","Saint Vincent and the Grenadines","Samoa","San Marino","Sao Tome and Principe","Saudi Arabia","Senegal","Serbia and Montenegro","Seychelles","Sierra Leone","Singapore","Slovakia","Slovenia","Solomon Islands","Somalia","South Africa","South Georgia","South Korea","Spain","Sri Lanka","Sudan","Suriname","Svalbard and Jan Mayen","Swaziland","Sweden","Switzerland","Syrian Arab Republic",
		"Taiwan","Tajikistan","Tanzania","Thailand","Timor-Leste","Togo","Tokelau","Tonga","Trinidad and Tobago","Tunisia","Turkey",
"Turkmenistan","Tuvalu",
		"Uganda","Ukraine","United Arab Emirates","United Kingdom","United States","United States Minor Outlying Islands","Uruguay","Uzbekistan",
		"Vanuatu","Vatican City","Venezuela","Vietnam","Virgin Islands, British","Virgin Islands, U.S.",
		"Wallis and Futuna","Western Sahara",
		"Yemen",
		"Zambia","Zimbabwe"
		);
		
		echo "<select name='$name' $css>\n";
		echo "<option value=''>--- Please Select ---</option>\n";
		for($i=0; $i < count($country); $i++)
		{
			$selected = "";
			if($default == $country[$i])
			{
				echo $selected = "selected";
			}
			echo "<option value='$country[$i]' $selected >$country[$i]</option>\n";
		}
		echo "</select>";
	}
	
	//html country
	function html_select_language($name, $default = NULL, $css = NULL)
	{

        $languageCodes = array(
        "aa" => "Afar",
        "ab" => "Abkhazian",
        "ae" => "Avestan",
        "af" => "Afrikaans",
        "ak" => "Akan",
        "am" => "Amharic",
        "an" => "Aragonese",
        "ar" => "Arabic",
        "as" => "Assamese",
        "av" => "Avaric",
        "ay" => "Aymara",
        "az" => "Azerbaijani",
        "ba" => "Bashkir",
        "be" => "Belarusian",
        "bg" => "Bulgarian",
        "bh" => "Bihari",
        "bi" => "Bislama",
        "bm" => "Bambara",
        "bn" => "Bengali",
        "bo" => "Tibetan",
        "br" => "Breton",
        "bs" => "Bosnian",
        "ca" => "Catalan",
        "ce" => "Chechen",
        "ch" => "Chamorro",
        "co" => "Corsican",
        "cr" => "Cree",
        "cs" => "Czech",
        "cu" => "Church Slavic",
        "cv" => "Chuvash",
        "cy" => "Welsh",
        "da" => "Danish",
        "de" => "German",
        "dv" => "Divehi",
        "dz" => "Dzongkha",
        "ee" => "Ewe",
        "el" => "Greek",
        "en" => "English",
        "eo" => "Esperanto",
        "es" => "Spanish",
        "et" => "Estonian",
        "eu" => "Basque",
        "fa" => "Persian",
        "ff" => "Fulah",
        "fi" => "Finnish",
        "fj" => "Fijian",
        "fo" => "Faroese",
        "fr" => "French",
        "fy" => "Western Frisian",
        "ga" => "Irish",
        "gd" => "Scottish Gaelic",
        "gl" => "Galician",
        "gn" => "Guarani",
        "gu" => "Gujarati",
        "gv" => "Manx",
        "ha" => "Hausa",
        "he" => "Hebrew",
        "hi" => "Hindi",
        "ho" => "Hiri Motu",
        "hr" => "Croatian",
        "ht" => "Haitian",
        "hu" => "Hungarian",
        "hy" => "Armenian",
        "hz" => "Herero",
        "ia" => "Interlingua",
        "id" => "Indonesian",
        "ie" => "Interlingue",
        "ig" => "Igbo",
        "ii" => "Sichuan Yi",
        "ik" => "Inupiaq",
        "io" => "Ido",
        "is" => "Icelandic",
        "it" => "Italian",
        "iu" => "Inuktitut",
        "ja" => "Japanese",
        "jv" => "Javanese",
        "ka" => "Georgian",
        "kg" => "Kongo",
        "ki" => "Kikuyu",
        "kj" => "Kwanyama",
        "kk" => "Kazakh",
        "kl" => "Kalaallisut",
        "km" => "Khmer",
        "kn" => "Kannada",
        "ko" => "Korean",
        "kr" => "Kanuri",
        "ks" => "Kashmiri",
        "ku" => "Kurdish",
        "kv" => "Komi",
        "kw" => "Cornish",
        "ky" => "Kirghiz",
        "la" => "Latin",
        "lb" => "Luxembourgish",
        "lg" => "Ganda",
        "li" => "Limburgish",
        "ln" => "Lingala",
        "lo" => "Lao",
        "lt" => "Lithuanian",
        "lu" => "Luba-Katanga",
        "lv" => "Latvian",
        "mg" => "Malagasy",
        "mh" => "Marshallese",
        "mi" => "Maori",
        "mk" => "Macedonian",
        "ml" => "Malayalam",
        "mn" => "Mongolian",
        "mr" => "Marathi",
        "ms" => "Malay",
        "mt" => "Maltese",
        "my" => "Burmese",
        "na" => "Nauru",
        "nb" => "Norwegian Bokmal",
        "nd" => "North Ndebele",
        "ne" => "Nepali",
        "ng" => "Ndonga",
        "nl" => "Dutch",
        "nn" => "Norwegian Nynorsk",
        "no" => "Norwegian",
        "nr" => "South Ndebele",
        "nv" => "Navajo",
        "ny" => "Chichewa",
        "oc" => "Occitan",
        "oj" => "Ojibwa",
        "om" => "Oromo",
        "or" => "Oriya",
        "os" => "Ossetian",
        "pa" => "Panjabi",
        "pi" => "Pali",
        "pl" => "Polish",
        "ps" => "Pashto",
        "pt" => "Portuguese",
        "qu" => "Quechua",
        "rm" => "Raeto-Romance",
        "rn" => "Kirundi",
        "ro" => "Romanian",
        "ru" => "Russian",
        "rw" => "Kinyarwanda",
        "sa" => "Sanskrit",
        "sc" => "Sardinian",
        "sd" => "Sindhi",
        "se" => "Northern Sami",
        "sg" => "Sango",
        "si" => "Sinhala",
        "sk" => "Slovak",
        "sl" => "Slovenian",
        "sm" => "Samoan",
        "sn" => "Shona",
        "so" => "Somali",
        "sq" => "Albanian",
        "sr" => "Serbian",
        "ss" => "Swati",
        "st" => "Southern Sotho",
        "su" => "Sundanese",
        "sv" => "Swedish",
        "sw" => "Swahili",
        "ta" => "Tamil",
        "te" => "Telugu",
        "tg" => "Tajik",
        "th" => "Thai",
        "ti" => "Tigrinya",
        "tk" => "Turkmen",
        "tl" => "Tagalog",
        "tn" => "Tswana",
        "to" => "Tonga",
        "tr" => "Turkish",
        "ts" => "Tsonga",
        "tt" => "Tatar",
        "tw" => "Twi",
        "ty" => "Tahitian",
        "ug" => "Uighur",
        "uk" => "Ukrainian",
        "ur" => "Urdu",
        "uz" => "Uzbek",
        "ve" => "Venda",
        "vi" => "Vietnamese",
        "vo" => "Volapuk",
        "wa" => "Walloon",
        "wo" => "Wolof",
        "xh" => "Xhosa",
        "yi" => "Yiddish",
        "yo" => "Yoruba",
        "za" => "Zhuang",
        "zh" => "Chinese",
        "zu" => "Zulu"
        );
		
		echo "<select name='$name' $css>\n";
		echo "<option value=''>--- Please Select ---</option>\n";
		for($i=0; $i < count($languageCodes); $i++)
		{
			$selected = "";
			if($default == $languageCodes[$i])
			{
				echo $selected = "selected";
			}
			echo "<option value='$languageCodes[$i]' $selected >$languageCodes[$i]</option>\n";
		}
		echo "</select>";
	}

	function home_url() {
	    return (((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS']!=='off') || $_SERVER['SERVER_PORT']==443) ? 'https://':'http://' ).$_SERVER['HTTP_HOST'];
    }

    //Returns Base URL
	function base_url()
	{
		$base_url = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https" : "http");
		$base_url .= "://".$_SERVER['HTTP_HOST'];
		$base_url .= str_replace(basename($_SERVER['SCRIPT_NAME']),"",$_SERVER['SCRIPT_NAME']);
		
		$base_url = rtrim($base_url, "/"); //remove '/'
		$base_url .= "/";
		return $base_url; 
	}

    //Returns Path URL
	function base_path()
	{
        $base_path = "";
        $base_path .= str_replace(basename($_SERVER['SCRIPT_NAME']),"",$_SERVER['SCRIPT_NAME']);

        $base_path = rtrim($base_path, "/"); //remove '/'
        $base_path .= "/";
		return $base_path;
	}

    //Returns current page URL
    function curPageURL() {
        $pageURL = 'http';
        if ( isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
        $pageURL .= "://";
        if ($_SERVER["SERVER_PORT"] != "80") {
            $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
        } else {
            $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
        }
        return $pageURL;
    }

    function CovertObjecttoArray($obj)
    {
        if (is_object($obj)) $obj = (array)$obj;
        if (is_array($obj)) {
            $new = array();
            foreach ($obj as $key => $val) {
                $new[$key] = CovertObjecttoArray($val);
            }
        } else {
            $new = $obj;
        }

        return $new;
    }
/*
    function readAPIFile($url, $tbName){
        global $oUser;
        $data = http_build_query(
            array('username' => $oUser->username, 'password' => $oUser->password)
        );

        $opts = array('http' =>
            array(
                'method'  => 'POST',
                'header'  => 'Content-type: application/x-www-form-urlencoded',
                'content' => $data
            )
        );

        $output = json_decode(file_get_contents($url."&csrf=".$oUser->csrf, false, stream_context_create($opts)));
        if($output)
            echo $output;
    }
*/

    //function readAPIFile($url, $tbName, $return_type = NULL) {
    function readAPIFile($tbName, $transform = 1, $return_type = NULL, $conditions = NULL) {
        global $oUser;
        if($conditions)
            $conditions = "&".$conditions;
        if($tbName)
            $url = BASE_API."curl_api.php/".$tbName."?transform=".$transform."&return_type=".$return_type.$conditions;

        if($url && $oUser->csrf) {
            $url = $url."&sessid=".$_COOKIE['PHPSESSID']."&csrf=".$oUser->csrf;
            //create folder
            if (!file_exists(PATH_CACHE_ROOT)) {
                MKDIRS(PATH_CACHE_ROOT);
            }
            //$tmp_fname = tempnam(PATH_CACHE_ROOT,"COOKIE".$oUser->csrf);
            $tmp_fname = PATH_CACHE_ROOT."token_".$oUser->csrf.".txt";

            if($oUser->username && $oUser->password) {
                $authenticator['parameter'] = array(
                    "user" => $oUser->username,
                    "pass" => $oUser->password
                );
            }

            $postfields = array(
                'module'=> 'users',
                'action'=> 'token',
                'path'=> 'cp',
                'username'=> $oUser->username,
                'password'=> $oUser->password,
                'authenticator' => $authenticator,
                'method' => "POST",
                'csrf' => $oUser->csrf,
                'ttl'=>30
            );
            $post_data = http_build_query($postfields);
            $userAgent = $_SERVER['HTTP_USER_AGENT'];
            $headers = array(
                'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8, image/gif, image/x-bitmap, image/jpeg, image/pjpeg',
                'Connection: Keep-Alive',
                'Content-type: application/x-www-form-urlencoded;charset=UTF-8'
            );

            // account variables
            $token = $oUser->csrf;
            $username = $oUser->password;
            $password = $oUser->password;

            // encode json data
            $data = json_encode($postfields);

            // initialize cURL
            $ch = curl_init();

            // set options
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
            //curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($data))
            );
            curl_setopt($ch, CURLOPT_COOKIEJAR, $tmp_fname);
            curl_setopt($ch, CURLOPT_COOKIESESSION, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

            // make the request
            $output = curl_exec($ch);
            //PRE($output);

            // convert response
            $output = json_decode($output);
            curl_close($ch);
        }
        if($output) {
            $data = CovertObjecttoArray($output->$tbName);
            //PRE($data);
        }
        return $data;
    }
    
//------------------------------------------- Begin Global Function ----------------------------------------------------------//

//===========DEBUG CODE===============
function PRE($val){
    echo "<pre>";
    print_r($val);
    echo "</pre>";
}

//============== SORTING ARRAY BY SUBKEY ==============
function SK_SORT(&$array, $subkey = "id", $sort_ascending = false) {

    if (count($array))
        $temp_array[key($array)] = array_shift($array);

    foreach($array as $key => $val){
        $offset = 0;
        $found = false;
        foreach($temp_array as $tmp_key => $tmp_val)
        {
            if(!$found and strtolower($val[$subkey]) > strtolower($tmp_val[$subkey]))
            {
                $temp_array = array_merge((array)array_slice($temp_array,0,$offset),
                    array($key => $val),
                    array_slice($temp_array,$offset)
                );
                $found = true;
            }
            $offset++;
        }
        if(!$found)
            $temp_array = array_merge($temp_array, array($key => $val));
    }

    if ($sort_ascending)
        $array = array_reverse($temp_array);
    else
        $array = $temp_array;
}

/**
 * @description - it fetches Currency conversion from the OANDA website. (To be used by the APPiC)
 *
 * @param $from - the Currency to be converted
 * @param $to - the Currency to be converted to
 * @param int $amount - the amount to be converted to
 * @param null $date - the date of the currency
 * @return array - returns the array of the currency to be converted to Array ( [0] => Full Name [1] => character code [2] => the amount [3] => converted)
 */

function convertCurrency($from, $to, $amount=1, $date=NULL){
    $to = trim($to);
    if($date == NULL)
        $date = date("m/d/y");
    $data = file_get_contents("https://www.oanda.com/currency/table?date=".$date."&date_fmt=us&exch=".$from."&sel_list=".$to."&value=".$amount."&format=HTML&redirected=1");

    preg_match_all('/<table.*id="converter_table".*>.*<table.*>.*<tr bgcolor=#efefef>(.*)<\/tr>.*<\/table>.*<\/table>/isU',$data, $converted);

    $dom = new DOMDocument;
    $dom->loadHTML( $converted[1][0] );
    $cells = array();
    foreach( $dom->getElementsByTagName( 'td' ) as $td ) {
        $cells[] = $td->nodeValue;
    }
    $data_amount = array('from' => $from, 'to' => $to, "amount" => $cells[2], 'base' => $cells[3]);
    return $data_amount;
}

//------------------------------------------- End Global Function ----------------------------------------------------------//

/*
 * SELECT, CHECKBOX, RADIOBOX, DROPDOWN Functions
 */
//============== SELECT RETURN JSON array,json || SELECT Return 1 Row returnobject = '1' ==============
function SELECT($table_name, $field = NULL, $where = NULL, $group_by = NULL, $order_by = NULL, $return = NULL, $query_type =  NULL, $debug_mode = NULL){
    $condition = ' WHERE 1=1';
    if(!isset($query_type))
        $query_type = "fetchAssoc";

    if(empty($field))
        $field = '*';
    if(!empty($where))
        $condition .= " AND ".$where;
    if(!empty($group_by))
        $condition .= " GROUP BY ".$group_by;
    if(!empty($order_by))
        $condition .= " ORDER BY ".$order_by;

    $sql = "SELECT ".$field." FROM ".$table_name.' '.$condition;

    if($debug_mode)
        echo "<br/>SQL = $sql<br/><br/>";

    $data = DB::query($sql, $query_type);

    if($return=='json'){
        return json_encode($data);
    }else{
        return $data;
    }
}

//============== UPDATE ==============
function UPDATE($table_name, $field, $where = NULL, $debug_mode = NULL){
    $condition = ' WHERE 1=1 ';
    if (!empty($where))
        $condition .= " AND " . $where;

    $sql = "UPDATE ".$table_name." SET ".$field." ".$condition;
    if($debug_mode)
        echo "<br />SQL: ".$sql;

    $stmt = DB::prepare($sql);
    $results = DB::execute($stmt,false);
    DB::close();
    return $results;
}


//============== DROPDOWNITEM  $field multi ("value1,value2,value3,value4")// multiple NULL,1 id=["value1","value2","value3"] ==============
function DROPDOWNITEM($name, $id = NULL, $table_name, $key_id, $field, $where = NULL, $disable = NULL, $style = NULL, $multiple = NULL) {
    $data = SELECT($table_name, $key_id.",".$field, $where);
    $return_dd = "";

    if(empty($css ))
        $css = " class='form-control select2 $style'";

    $multiplestyle = "";
    if($multiple) {
        $multiplestyle = "multiple='multiple'";
        $decode_key = json_decode($id);
    }

    if($disable) {
        $disable = "disabled='disabled'";
        $return_dd.= "<input type='hidden' name='".$name."' value='".$id."' />";
    }

    $return_dd .= "<select name='".$name."' id='".$name."' $css $disable $multiplestyle>\n";
    if(!$multiple) {
        $return_dd .= "<option value=''>--- ((Please Select)) ---</option>\n";
    }
    if (count($data) > 0) {
        $addr = explode(',',$field);
        foreach ($data as $key => $val) {
            $value = $val["$key_id"];
            //$text = $val["$field"];
            $text = "";
            foreach ($addr as $valuefield) :
                $text .= $val[$valuefield]." ";
            endforeach;
            if(trim($text) == "")
                $text = $val["$field"];

            // $text = ucwords(strtolower($text));

            if($decode_key) {
                $selectVal = in_array($value, $decode_key) ? "selected='selected'" : "";
            }else{
                $selectVal = ($value == $id) ? "selected='selected'" : "";
            }

            $return_dd .=  "<option value='$value' $selectVal>$text</option>\n";
        }
    }
    $return_dd .= "</select>\n";
    return $return_dd;
}

//============== DROPDOWN MANUAL $arroption @vars array("name" => "value1", "name2" => "value2", "name3" => "value3") Or @string ==============
function DROPDOWNMANUAL($name, $default_id = NULL, $arroption = NULL, $disable = NULL, $css = NULL, $attribute = NULL,$disable_default = NULL) {
    $return_dd = "";
    if(empty($css))
        $css = " class='form-control select2'";
    if($disable)
        $disable = "disabled='disabled'";
    if(empty($attribute))
        $attribute = " id='".$name."'";

    if(is_array($arroption)) {
        $options_set = '';
        foreach ($arroption as $key => $value) :
            if($default_id !=  NULL)
                $selectVal = ($key == $default_id) ? "selected='selected'" : "";

            $options_set .=  "<option value='$key' $selectVal>$value</option>\n";
        endforeach;
    } else {
        $options_set = $arroption;
    }
    $return_dd .= "<select name='".$name."' $css $disable $attribute>\n";
	if(!$disable_default)
		$return_dd .=  "<option value=''>--- ((Please Select)) ---</option>\n";
    $return_dd .=  $options_set."\n";
    $return_dd .= "</select>";
    return $return_dd;
}

//============== CHECKBOXITEM USE DATABASE ==============
//$keyvalue = "keyid",$fieldtext = "value1,value2,value3", $keyarray = '["value1","value2","value3"]', $css = "col-md-(int)", $column = "1 between 20"
function CHECKBOXITEM($table_name, $where = NULL, $keyvalue, $fieldtext, $inputname,$keyarray = NULL,  $css = NULL, $column = NULL,$enabledDisabled = null) {
    $data = SELECT($table_name,"",$where);
    if(empty($css)){$css = "col-md-12";}
    if($column>20){
        $style = "col_checkbox_20";
    }else {
        $style = "col_checkbox_$column";
    }
    $inputname = $inputname."[]";
    $decode_key = json_decode($keyarray);
    $return_dd = "";
    $return_dd .= "<div class=\"$css\">\n";
    $return_dd .= "<ul class=\"all-listing-functions\">\n";
    if($data) {
        $addr = explode(',',trim($fieldtext));

        foreach ($data as $key => $val):
            $text = "";$disabledNi = " ";
            foreach ($addr as $valuefield) :
                $text .= "((".$val["$valuefield"]."))  ";
            endforeach;

            if($decode_key) {
                $CheckedVal = in_array($val[$keyvalue], $decode_key) ? "checked='checked'" : "";
            }

            if(!empty($enabledDisabled)){ // this is to disabled checkbox if you want
                // $enabledDisabled[0] = the table field key
                // $enabledDisabled[1] = the table field value
                if( $val[$enabledDisabled[0]] == $enabledDisabled[1]){
                    $disabledNi = "disabled";
                }
            }
            $return_dd .=  "<li class='$style'><label class=\"radio-inline\"><input type=\"checkbox\" name=\"$inputname\" value=\"$val[$keyvalue]\" $CheckedVal $disabledNi />$text</label></li>\n";
        endforeach;
    }
    $return_dd .= "</ul>\n</div>\n";
    return $return_dd;
}
//============== RADIOITEM NO DATABASE ============== ~ J
/**
 * @param (string)$name, (array)$data["key1" => "value1", "key2" => "value2", "key3" => "value3"], (int)$default
 * @return radioset
 */
function CHECKBOXITEMMANUAL($name, $default = NULL, $data = NULL, $attribute = NULL, $css = NULL) {
    if(empty($css))
        $css = "checkbox checkbox-inline";
    if(empty($attribute))
        $attribute = "required data-parsley-errors-container='#span_error_".$name."'";

    $return_dd = "";
    if(is_array($data)) {
        foreach ($data as $key => $val):
            if($key == $default) { $CheckedVal = " checked = 'checked'"; } else { $CheckedVal = ''; }
            if($key == "Y") {
                $css_label = "checkbox-success";
            } elseif($key == "N") {
                $css_label = "checkbox-danger";
            } else {
                $css_label = "checkbox-default";
            }

            $return_dd .= "
                <div class='$css $css_label'>";
            $return_dd .= "
                <input type='checkbox' name='".$name."' id='".$name."_".$key."' value='".$key."' ".$attribute." ".$CheckedVal.">
                <label for='".$name."_".$key."'>".$val."</label>";
            $return_dd .= "
                </div>\n";
            unset($CheckedVal);
        endforeach;
    }

    $return_dd .= "
                <span id='span_error_".$name."'></span>\n";
    return $return_dd;
}







//============== RADIOITEM USE DATABASE ==============
//$keyvalue = "keyid",$fieldtext = "value1,value2,value3", $key = '$keyid', $css = "col-md-(int)", $column = "1 between 20"
function RADIOITEM($table_name,$where = NULL,$keyvalue, $fieldtext, $inputname,$keydb = NULL,  $css = NULL, $column = NULL) {
    $data = SELECT($table_name,"",$where);
    if(empty($css)){$css = "col-md-12";}
    if($column>20){
        $style = "col_checkbox_20";
    }else {
        $style = "col_checkbox_$column";
    }
    $return_dd = "";
    $return_dd .= "<div class=\"$css\">\n";
    $return_dd .= "<ul class=\"all-listing-functions\">\n";
    if($data) {
        $addr = explode(',',trim($fieldtext));
        foreach ($data as $key => $val):
            $text = "";
            foreach ($addr as $valuefield) :
                $text .= $val["$valuefield"]."  ";
            endforeach;
            if($val[$keyvalue] == $keydb)$CheckedVal = "checked = \"checked\"";
            $return_dd .=  "<li class='$style'><label class=\"radio-inline\"><input type=\"radio\" name=\"$inputname\" value=\"$val[$keyvalue]\" $CheckedVal/>$text</label></li>\n";
            unset($CheckedVal);
        endforeach; //Endforeach
    }
    $return_dd .= "</ul>\n</div>\n";
    return $return_dd;
}

//============== RADIOITEM NO DATABASE ============== ~ J
/**
 * @param (string)$name, (array)$data["key1" => "value1", "key2" => "value2", "key3" => "value3"], (int)$default
 * @return radioset
 */
function RADIOITEMMANUAL($name, $default = NULL, $data = NULL, $attribute = NULL, $css = NULL) {
    if(empty($css))
        $css = "radio radio-inline";
    if(empty($attribute))
        $attribute = "required data-parsley-errors-container='#span_error_".$name."'";

    $return_dd = "";
    if(is_array($data)) {
        foreach ($data as $key => $val):
            if($key == $default) { $CheckedVal = " checked = 'checked'"; } else { $CheckedVal = ''; }
            if($key == "Y") {
                $css_label = "radio-success";
            } elseif($key == "N") {
                $css_label = "radio-danger";
            } else {
                $css_label = "radio-default";
            }

            $return_dd .= "
                <div class='$css $css_label'>";
            $return_dd .= "
                <input type='radio' name='".$name."' id='".$name."_".$key."' value='".$key."' ".$attribute." ".$CheckedVal.">
                <label for='".$name."_".$key."'>".$val."</label>";
            $return_dd .= "
                </div>\n";
            unset($CheckedVal);
        endforeach;
    }

    $return_dd .= "
                <span id='span_error_".$name."'></span>\n";
    return $return_dd;
}

//=============== GETFIELDVALUE ==================
function GETVALUE($table_name, $field ,$where = NULL , $group_by = NULL , $order_by = NULL) {

    $condition = 'WHERE 1=1';
    if(!empty($where)){
        $condition .= " AND ".$where;
    }
    if(!empty($group_by)){
        $condition .= " GROUP BY ".$group_by;
    }
    if(!empty($order_by)){
        $condition .= " ORDER BY ".$order_by;
    }
    $sql = "SELECT $field FROM ".$table_name.'  '.$condition;
    //echo "<br />".$sql."<br />";
    //exit;

    $stmt = DB::prepare($sql);
    DB::execute($stmt,false);
    $fieldvalue = DB::fetchColumn($stmt);
    DB::close();
    return $fieldvalue;
}


//=============== SETLIMIT :: Page navigation Control ====================== ~ J :: Test Drive
/*
function set_pageno($pageno) {
    $this->pageno = $pageno;
}

function set_perpage($perpage) {
    $this->perpage = $perpage;
}

function set_sortby($sortby) {
    $this->sortby = $sortby;
}

function set_orderby($orderby) {
    $this->orderby = $orderby;
}

function set_data_total($data_total) {
    $this->data_total = $data_total;
}

function order_page() {
    if($this->sortby)
        $data =" ORDER BY $this->sortby ".$this->orderby ;
    return $data;
}

function limit_page() {
    if($this->perpage)
    {
        $record_start = page_record_start($this->perpage, $this->pageno);
        return " LIMIT $record_start, $this->perpage ";
    }
}

function page_record_start($per_page, $page_no) {
    if($page_no == "" || $page_no == 0)
        $record_no = 0;
    else
        $record_no = $per_page * ($page_no-1);
    return $record_no;
}

function total_page($data_total,$per_page) {
    return ceil($data_total/$per_page);
}

function page_navigation($data_total, $link_string = NULL) {
    $page_link = "";
    echo "<br />page_number: ".$page_number = $this->pageno;
    echo "<br />per_page: ".$per_page = $this->perpage;
    echo "<br />total_page: ".$total_page = total_page($data_total, $per_page);
    if($link_string)
        $link_string = $link_string."&";

    if($total_page > 1)
    {
        $page_link = "Page : ";
        for($page_no=1; $page_no <= $total_page; $page_no++)
        {
            //$q = $querystring."-".$page;
            $q = $link_string."&amp;-".$page_no;
            if($this->pageno == $page_no)
                $page_link.= "<b>$page_no</b>&nbsp;&nbsp;";
            else
                $page_link.= "<a href='".$q."'>$page_no</a>&nbsp;&nbsp;";
        }
    }
    return $page_link;
}
*/


?>