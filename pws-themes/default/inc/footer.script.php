<!-- Modal -->
<div class="modal fade" id="booking-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content modal-booking">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i></button>
            </div>
            <div class="modal-body ">
                <div class="row">
                    <div class="col-lg-7">
                        <h4><span class="selectdate-title">Select Your Dates</span></h4>
                        <div class="datepicker"></div>
                    </div>

                    <div class="col-lg-5 ">
                        <div class="row">
                            <div class="col-xs-6">
                                <label>Check-in</label>
                                <input type="text" id="input1" size="10">
                            </div>
                            <div class="col-xs-6">
                                <label>Check-out</label>
                                <input type="text" id="input2" size="10">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4 numbers-row">

                                <label for="rooms"><span class="label-number">Room(s)</span>
                                    <input type="text" name="rooms" id="rooms" value="1">
                                </label>

                            </div>

                            <div class="col-sm-4 numbers-row">

                                <label for="adults"><span class="label-number">adult(s)</span>
                                    <input type="text" name="adults" id="adults" value="1">
                                </label>

                            </div>


                            <div class="col-sm-4 numbers-row">

                                <label for="children"><span class="label-number">Child(ren)</span>
                                    <input type="text" name="children" id="children" value="0">
                                </label>

                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <a href="javascript:void(0);" id="promo-show">Apply Special Code ></a>
                                <div class="row promo-box" style="display:none;">
                                    <div class="col-xs-6">
                                        <input type="text" name="promo-code" id="promo-code">
                                    </div>
                                    <div class="col-xs-6">
                                        <button class="btn btn-primary">Apply</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <button class="btn btn-primary btn-lg">check availability</button>
                            </div>
                        </div>


                    </div>
                </div>

            </div>

        </div>
    </div>
</div>

<base_url href="<?php echo PATH_ROOT; ?>"></base_url>

<script type='text/javascript'>//<![CDATA[
    //set root path URL :: donot remove this
    var base_url = $('base_url').attr('href');

    $( document ).ready(function() {
	
	$("#galleryload").bind("click",function(){
		$.ajax({
			type: "POST",
			url: "../../ajax_operation.php",
			data:  { module: "gallery" , content_id : <?php echo $data_content[0]['id']; ?> },
		}).done(function( msg ) {
			$("#gallery-list").append(msg);
			$("#galleryload").hide();
		});
	});

	$("form.footer-subscribe").validate({
		errorPlacement: function(error,element) {
			return true;
		},
		submitHandler: function (form) {
			var formData = new FormData(form);
			$.ajax({
				type: "POST",
				url: "../../ajax_operation.php",
				data:  formData,
				//mimeType:"multipart/form-data",
				contentType: false,
				cache: false,
				processData:false
			}).done(function( msg ) {
				//alert(msg);
				if(msg == "done")
				{
					$("form.footer-subscribe")[0].reset();
					$("<div class='success-message'><p>Thank You For Subscription</p></div>").insertAfter("form.footer-subscribe .input-group");
					setTimeout(function() {
						$("form.footer-subscribe .success-message").hide();
					}, 5000);
				}else{
					$("<div class='success-message'><p>Something Wrong Please Try Again</p></div>").insertAfter("form.footer-subscribe .input-group");
					setTimeout(function() {
						$("form.footer-subscribe .success-message").hide();
					}, 5000);
				}
			});
		}
	});
	if($("form.contact-form").length){
	$("form.contact-form").validate({
		errorPlacement: function(error,element) {
			return true;
		},
		submitHandler: function (form) {
			var formData = new FormData(form);
			$.ajax({
				type: "POST",
				url: "../../ajax_operation.php",
				data:  formData,
				//mimeType:"multipart/form-data",
				contentType: false,
				cache: false,
				processData:false
			}).done(function( msg ) {
				if(msg == "done")
				{
					$("form.contact-form")[0].reset();
					$("form.contact-form").append("<div class='success-message' style='clear:both;text-align:center'><p>Thank You,We already get your message.</p></div>");
					setTimeout(function() {
						$("form.footer-subscribe .success-message").hide();
					}, 5000);
				}else{
					$("form.contact-form").append("<div class='success-message' style='clear:both;text-align:center'><p>Something Wrong Please Try Again</p></div>");
					setTimeout(function() {
						$("form.contact-form .success-message").hide();
					}, 5000);
				}
			});
		}
	});
	}
	
	//Home Slider INSTAGRAM
	if($("#home-instafeed").length){
		var feedHome = new Instafeed({
		  target: "home-instafeed",
		  userId: 303327941,
            clientId: '39bbf35deab34447924daff350c34cc3',
            accessToken: '7835631.ba4c844.5e0bb4f1d603402fad78628dc6f25cbc',
//		  clientId: '366948592f1a465486dcc4223a9458f6',
//		  accessToken: '90003.3669485.f77ce026408c46d39c5b1115f60e64fd',
		  get: "tagged",
		  tagName: "saturdaysresidence",
		  resolution: 'standard_resolution',
		  template: '<li><a data-fancybox="gallery" href="{{image}}" data-caption="{{caption}}"><img src="{{image}}" alt="{{caption}}" style="max-width:100%;" /></a></li>',
		  limit: 12
		});
		feedHome.run();
	}

	//NAV Gallery
	if($("#nav-instafeed").length){
		var feedNav = new Instafeed({
		  target: "nav-instafeed",
		  userId: 303327941,
            clientId: '39bbf35deab34447924daff350c34cc3',
            accessToken: '7835631.ba4c844.5e0bb4f1d603402fad78628dc6f25cbc',
//		  clientId: '366948592f1a465486dcc4223a9458f6',
//		  accessToken: '90003.3669485.f77ce026408c46d39c5b1115f60e64fd',
		  get: "tagged",
		  tagName: "saturdaysresidence",
		  resolution: 'standard_resolution',
		  template: '<li class="col-xs-4"><a data-fancybox="gallery" href="{{image}}" data-caption="{{caption}}"><img src="{{image}}" alt="{{caption}}" style="max-width:100%;" /></a></li>',
		  limit: 3
		});
		feedNav.run();
	}
	//Gallery
	if($("#instafeed").length){
		var button = document.getElementById("load");
		var feed = new Instafeed({
		  target: "instafeed",
		  userId: 303327941,
		  clientId: '39bbf35deab34447924daff350c34cc3',
		  accessToken: '7835631.ba4c844.5e0bb4f1d603402fad78628dc6f25cbc',
//		  clientId: '366948592f1a465486dcc4223a9458f6',
//		  accessToken: '90003.3669485.f77ce026408c46d39c5b1115f60e64fd',
		  get: "tagged",
		  tagName: "saturdaysresidence",
		  resolution: 'standard_resolution',
		  template: '<li class="col-md-3 col-sm-4 col-xs-6"><a data-fancybox="gallery" href="{{image}}" data-caption="{{caption}}"><img src="{{image}}" alt="{{caption}}" style="max-width:100%;" /></a></li>',
		  limit: 12,
			after: function() {
				// disable button if no more results to load
				if (!this.hasNext()) {
					//button.setAttribute('disabled', 'disabled');
					button.setAttribute('style','display:none');
				}
			}
		});
		
		button.addEventListener("click", function() {
		  feed.next();
		});	
		
		feed.run();
	}
});

    //stick in the fixed 100% height behind the navbar but don't wrap it
    $('#slide-nav.navbar-custom').after($('<div class="inverse" id="navbar-height-col"></div>'));

    $('#slide-nav.navbar-default').after($('<div id="navbar-height-col"></div>'));

    // Enter your ids or classes
    var toggler = '.navbar-toggle';
    var pagewrapper = '#page-content';
    var navigationwrapper = '.navbar-header';
    var menuwidth = '100%'; // the menu inside the slide menu itself
    var slidewidth = '60%';
    var menuneg = '-100%';
    var slideneg = '-60%';


    $("#slide-nav").on("click", toggler, function (e) {

        var selected = $(this).hasClass('slide-active');

        $('#slidemenu').stop().animate({
            left: selected ? menuneg : '0px'
        });

        $('#navbar-height-col').stop().animate({
            left: selected ? slideneg : '0px'
        });

        $(pagewrapper).stop().animate({
            left: selected ? '0px' : slidewidth
        });

        $(navigationwrapper).stop().animate({
            left: selected ? '0px' : slidewidth
        });


        $(this).toggleClass('slide-active', !selected);
        $('#slidemenu').toggleClass('slide-active');


        $('#page-content, .navbar, body, .navbar-header').toggleClass('slide-active');


    });


    var selected = '#slidemenu, #page-content, body, .navbar, .navbar-header';


    $(window).on("resize", function () {

        if ($(window).width() > 767 && $('.navbar-toggle').is(':hidden')) {
            $(selected).removeClass('slide-active');
        }


    });



    $(function () {
        var date1 = new Date();
        //date1.setDate(0); // this magically sets the date to last day of previous month
        //date1.setDate(1);
        var date2 = new Date(date1.getTime() + 24 * 60 * 60 * 1000);
        //date2.setDate(0);

        var monthNames = ["January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December"];

        var date1_month = monthNames[date1.getUTCMonth()]; //months from 1-12
        var date1_day = date1.getUTCDate();

        var date2_month = monthNames[date2.getUTCMonth()]; //months from 1-12
        var date2_day = date2.getUTCDate();

        $(".check-in .day").text(date1_day);
        $(".check-in .month").text(date1_month);

        $(".check-out .day").text(date2_day);
        $(".check-out .month").text(date2_month);

        $("#input1").val($.datepicker.formatDate($.datepicker._defaults.dateFormat, date1));
        $("#input2").val($.datepicker.formatDate($.datepicker._defaults.dateFormat, date2));

        $(".datepicker").datepicker({
            minDate: 0,

            numberOfMonths: 2,
            beforeShowDay: function(date) {
                var date1 = $.datepicker.parseDate($.datepicker._defaults.dateFormat, $("#input1").val());
                var date2 = $.datepicker.parseDate($.datepicker._defaults.dateFormat, $("#input2").val());
                return [true, date1 && ((date.getTime() == date1.getTime()) || (date2 && date >= date1 && date <= date2)) ? "dp-highlight" : ""];
            },
            onSelect: function(dateText, inst) {
                var date1 = $.datepicker.parseDate($.datepicker._defaults.dateFormat, $("#input1").val());
                var date2 = $.datepicker.parseDate($.datepicker._defaults.dateFormat, $("#input2").val());
                var selectedDate = $.datepicker.parseDate($.datepicker._defaults.dateFormat, dateText);


                if (!date1 || date2) {
                    $("#input1").val(dateText);
                    $("#input2").val("");
                    $(this).datepicker();
                } else if( selectedDate < date1 ) {
                    $("#input2").val( $("#input1").val() );
                    $("#input1").val( dateText );
                    $(this).datepicker();
                } else {
                    $("#input2").val(dateText);
                    $(this).datepicker();
                }
            }
        });


        $(".numbers-row").append('<div class="inc button">+</div><div class="dec button">-</div>');

        $(".button").on("click", function() {

            var $button = $(this);
            var oldValue = $button.parent().find("input").val();

            if ($button.text() == "+") {
                var newVal = parseFloat(oldValue) + 1;
            } else {
                // Don't allow decrementing below zero
                if (oldValue > 0) {
                    var newVal = parseFloat(oldValue) - 1;
                } else {
                    newVal = 0;
                }
            }

            $button.parent().find("input").val(newVal);

        });

    });//]]>


    $(window).load(function(){

        $('.flexslider').flexslider({
            animation: "slide",
            animationLoop: false,
            controlNav: false,
            itemWidth: 210,
            itemMargin: 40,
            minItems: 1,
            maxItems: 4,
            start: function(slider){
                $('body').removeClass('loading');
            }
        });
    });

    $("#promo-show").click(function(){
        $(".promo-box").show();
        $("#promo-show").hide();
    });
	
	
$('.room-filters').on("change keyup", function() {  
  $(".filter-div").hide().filter(function() {
    var rtnData = "";

    regRoomtype 	= new RegExp($('#room_type').val().trim(), "ig");
    regView 			= new RegExp($('#room_view').val().trim(), "ig");
    regBedrooms			= new RegExp($('#no_of_bedrooms').val().trim(), "ig");
   
    
		rtnData = (
      $(this).attr("data-roomtype").match(regRoomtype) && 
      $(this).attr("data-view").match(regView) && 
      $(this).attr("data-bedrooms").match(regBedrooms) 
    );
     
    //console.log(rtnData);
    return rtnData;
  }).show();
});	

</script>

<script src="https://static.jsbin.com/js/render/edit.js?3.40.3"></script>
<!-- Plugin JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

<!-- Contact Form JavaScript
<script src="<?php //echo PATH_THEMES; ?>js/jqBootstrapValidation.js"></script>
<script src="<?php //echo PATH_THEMES; ?>js/contact_me.js"></script> -->

<!-- Theme JavaScript -->
<script src="<?php echo PATH_THEMES; ?>js/agency.js"></script>
<!-- FlexSlider -->
<script defer src="<?php echo PATH_THEMES; ?>js/jquery.flexslider.js"></script>

<?php
if(file_exists(PATH_ASSETS_ROOT."core.js")) {
    $page_js.= "<script type='text/javascript' src='".PATH_ASSETS."core.js'></script>\n";
    $doc_ready_js.= "Global.init();\n";
}

echo $page_css;
echo $page_js;
echo '
<script type="text/javascript">
'.$doc_ready_js.'
</script>
';

?>