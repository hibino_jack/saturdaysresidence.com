<script type="text/javascript">
    var resizefunc = [];
</script>

<!-- jQuery  -->
<script src="<?php echo PATH_ASSETS; ?>js/jquery.min.js"></script>
<script src="<?php echo PATH_ASSETS; ?>js/bootstrap.min.js"></script>
<script src="<?php echo PATH_ASSETS; ?>js/detect.js"></script>
<script src="<?php echo PATH_ASSETS; ?>js/fastclick.js"></script>

<script src="<?php echo PATH_ASSETS; ?>js/jquery.slimscroll.js"></script>
<script src="<?php echo PATH_ASSETS; ?>js/jquery.blockUI.js"></script>
<script src="<?php echo PATH_ASSETS; ?>js/waves.js"></script>
<script src="<?php echo PATH_ASSETS; ?>js/wow.min.js"></script>
<script src="<?php echo PATH_ASSETS; ?>js/jquery.nicescroll.js"></script>
<script src="<?php echo PATH_ASSETS; ?>js/jquery.scrollTo.min.js"></script>

<script src="<?php echo PATH_ASSETS; ?>plugins/peity/jquery.peity.min.js"></script>

<!-- jQuery -->
<script src="<?php echo PATH_ASSETS; ?>plugins/waypoints/lib/jquery.waypoints.js"></script>
<script src="<?php echo PATH_ASSETS; ?>plugins/counterup/jquery.counterup.min.js"></script>

<script src="<?php echo PATH_ASSETS; ?>plugins/morris/morris.min.js"></script>
<script src="<?php echo PATH_ASSETS; ?>plugins/raphael/raphael-min.js"></script>

<script src="<?php echo PATH_ASSETS; ?>plugins/jquery-knob/jquery.knob.js"></script>

<!-- DataTables -->
<link href="<?php echo PATH_ASSETS; ?>plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo PATH_ASSETS; ?>plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo PATH_ASSETS; ?>plugins/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo PATH_ASSETS; ?>plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo PATH_ASSETS; ?>plugins/datatables/scroller.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo PATH_ASSETS; ?>plugins/datatables/dataTables.colVis.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo PATH_ASSETS; ?>plugins/datatables/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo PATH_ASSETS; ?>plugins/datatables/fixedColumns.dataTables.min.css" rel="stylesheet" type="text/css"/>

<script src="<?php echo PATH_ASSETS; ?>plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo PATH_ASSETS; ?>plugins/datatables/dataTables.bootstrap.js"></script>

<script src="<?php echo PATH_ASSETS; ?>plugins/datatables/dataTables.buttons.min.js"></script>
<script src="<?php echo PATH_ASSETS; ?>plugins/datatables/buttons.bootstrap.min.js"></script>
<script src="<?php echo PATH_ASSETS; ?>plugins/datatables/jszip.min.js"></script>
<script src="<?php echo PATH_ASSETS; ?>plugins/datatables/pdfmake.min.js"></script>
<script src="<?php echo PATH_ASSETS; ?>plugins/datatables/vfs_fonts.js"></script>
<script src="<?php echo PATH_ASSETS; ?>plugins/datatables/buttons.html5.min.js"></script>
<script src="<?php echo PATH_ASSETS; ?>plugins/datatables/buttons.print.min.js"></script>
<script src="<?php echo PATH_ASSETS; ?>plugins/datatables/dataTables.fixedHeader.min.js"></script>
<script src="<?php echo PATH_ASSETS; ?>plugins/datatables/dataTables.keyTable.min.js"></script>
<script src="<?php echo PATH_ASSETS; ?>plugins/datatables/dataTables.responsive.min.js"></script>
<script src="<?php echo PATH_ASSETS; ?>plugins/datatables/responsive.bootstrap.min.js"></script>
<script src="<?php echo PATH_ASSETS; ?>plugins/datatables/dataTables.scroller.min.js"></script>
<script src="<?php echo PATH_ASSETS; ?>plugins/datatables/dataTables.colVis.js"></script>
<script src="<?php echo PATH_ASSETS; ?>plugins/datatables/dataTables.fixedColumns.min.js"></script>

<link href="<?php echo PATH_ASSETS; ?>plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.css" rel="stylesheet" />
<link href="<?php echo PATH_ASSETS; ?>plugins/switchery/css/switchery.min.css" rel="stylesheet" />
<link href="<?php echo PATH_ASSETS; ?>plugins/multiselect/css/multi-select.css"  rel="stylesheet" type="text/css" />
<link href="<?php echo PATH_ASSETS; ?>plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo PATH_ASSETS; ?>plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" />
<link href="<?php echo PATH_ASSETS; ?>plugins/bootstrap-touchspin/css/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />

<script src="<?php echo PATH_ASSETS; ?>plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.min.js"></script>
<script src="<?php echo PATH_ASSETS; ?>plugins/switchery/js/switchery.min.js"></script>
<script src="<?php echo PATH_ASSETS; ?>plugins/multiselect/js/jquery.multi-select.js"></script>
<script src="<?php echo PATH_ASSETS; ?>plugins/jquery-quicksearch/jquery.quicksearch.js"></script>
<script src="<?php echo PATH_ASSETS; ?>plugins/select2/js/select2.min.js" type="text/javascript"></script>
<script src="<?php echo PATH_ASSETS; ?>plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>
<script src="<?php echo PATH_ASSETS; ?>plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js" type="text/javascript"></script>
<script src="<?php echo PATH_ASSETS; ?>plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js" type="text/javascript"></script>
<script src="<?php echo PATH_ASSETS; ?>plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>

<!--
<script type="text/javascript" src="<?php //echo PATH_ASSETS; ?>plugins/autocomplete/jquery.mockjax.js"></script>
<script type="text/javascript" src="<?php //echo PATH_ASSETS; ?>plugins/autocomplete/jquery.autocomplete.min.js"></script>
<script type="text/javascript" src="<?php //echo PATH_ASSETS; ?>plugins/autocomplete/countries.js"></script>
<script type="text/javascript" src="<?php //echo PATH_ASSETS; ?>pages/autocomplete.js"></script>
-->

<!--summernote form validation init-->
<link href="<?php echo PATH_ASSETS; ?>plugins/summernote/summernote.css" rel="stylesheet" />
<script src="<?php echo PATH_ASSETS; ?>plugins/summernote/summernote.js"></script>
<!--<script src="--><?php //echo PATH_ASSETS; ?><!--plugins/summernote/summernote.min.js"></script>-->
<script src="<?php echo PATH_ASSETS; ?>plugins/summernote/summernote-cleaner.js"></script>

<!--
<script src="<?php //echo PATH_ASSETS; ?>js/jquery.core.js"></script>-->
<script src="<?php echo PATH_ASSETS; ?>js/jquery.app.js"></script>

<!-- Parsley From Validation -->
<script type="text/javascript" src="<?php echo PATH_ASSETS; ?>plugins/parsleyjs/parsley.min.js"></script>

<!-- Sweet-Alert  -->
<link href="<?php echo PATH_ASSETS; ?>plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css">
<script src="<?php echo PATH_ASSETS; ?>plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
<script src="<?php echo PATH_ASSETS; ?>pages/jquery.sweet-alert.init.js"></script>

<!-- Venobox Lightbox-->
<link rel="stylesheet" href="<?php echo PATH_ASSETS; ?>plugins/magnific-popup/css/magnific-popup.css"/>
<script type="text/javascript" src="<?php echo PATH_ASSETS; ?>plugins/isotope/js/isotope.pkgd.min.js"></script>
<script type="text/javascript" src="<?php echo PATH_ASSETS; ?>plugins/magnific-popup/js/jquery.magnific-popup.min.js"></script>

<!-- fontawesome iconpicker -->
<link rel="stylesheet" href="<?php echo PATH_ASSETS; ?>plugins/fontawesome-iconpicker/dist/css/fontawesome-iconpicker.min.css"/>
<script type="text/javascript" src="<?php echo PATH_ASSETS; ?>plugins/fontawesome-iconpicker/dist/js/fontawesome-iconpicker.js"></script>

<base_url href="<?php echo PATH_ROOT; ?>"></base_url>
<script type="text/javascript">

    //set root path URL :: donot remove this
    var base_url = $('base_url').attr('href');

    jQuery(document).ready(function($) {
        $('.counter').counterUp({
            delay: 100,
            time: 1200
        });

        $(".knob").knob();
    });
</script>
<?php
if(file_exists(PATH_ASSETS_ROOT."core.js")) {
    $page_js.= "<script type='text/javascript' src='".PATH_ASSETS."core.js'></script>\n";
    $doc_ready_js.= "Global.init();\n";
}

echo $page_css;
echo $page_js;
echo '
<script type="text/javascript">
'.$doc_ready_js.'
</script>
';

?>
