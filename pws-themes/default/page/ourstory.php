<?php
//PRE($data);
$data_content[0]['description'] = str_ireplace(array("\r","\n",'\r','\n'),'', $data_content[0]['description']);
//$image_header = "http://placehold.it/1450x500&text=".$data_content[0]['title'];
if($data_content[0]['header_photo'] != "")
{
	$pic = BASE_UPLOAD."contents/".$data_content[0]['id']."/".$data_content[0]['header_photo'];
	$pic_realpath = PATH_UPLOAD_ROOT."contents/".$data_content[0]['id']."/".$data_content[0]['header_photo'];
	if(is_file($pic_realpath)) {
		$image_header = PATH_ROOT."timthumb.php?src=".$pic."&w=1450&h=500";
	}
}
?>
<header> 
    <div class="page-heading">
        <div class="container">
            <div class="row">
                <div class="col-md-offset-2 col-md-8 text-center">
            	<h1><?php echo $data_content[0]['title']; ?></h1>
                <?php if(!empty($data_content[0]['subtitle'])){ ?>           
                    <h3 style="font-style:italic"><?php echo $data_content[0]['subtitle']; ?></h3>
				<?php }
				if(!empty($data_content[0]['short_description'])){ ?>                                
                    <p><?php echo $data_content[0]['short_description']; ?></p>
				<?php } ?>       
                </div>
            </div>
        </div>
    </div>
    <?php if($image_header){ ?>
    <!-- room header Section -->
    <div class="inner-header">
        <div class="container">
                <div class="row">
                    <div class="inner-header-fill" style="background-image:url('<?php echo $image_header; ?>');"></div>
                </div>
         </div>
    </div>
    <?php } ?>    
</header>
<section class="content-area page-our-story">
  	<?php /*if(!empty($data_content[0]['description'])){ ?>
    <div id="innerpage_intro">
        <div class="container">
            <div class="row">
                <div class="col-lg-offset-1 col-lg-10">
                	<div class="row innerpage_intro <?php echo ($image_header) ? "":"no-margin"; ?>">
                    <?php echo $data_content[0]['description']; ?>
                    </div><!--//saturdays_intro-->
                </div><!--//col-md-6-->
         	</div>
         </div>
      </div>
      <?php }*/ ?>

    <!-- paragraphs-box Section -->
    <?php echo $data_content[0]['description']; ?>
    
    
    </section>