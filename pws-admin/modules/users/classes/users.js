var Users = function () {
    var module_is = 'Users';
    var moduleName = module_is.toLowerCase();

    var handleLogin = function() {
        $('#login-check').parsley().on('field:validated', function () {
            var ok = $('.parsley-error').length === 0;
            //$('.alert-success').toggleClass('hidden', !ok);
            $('.alert-danger').toggleClass('hidden', ok);
        }).on('form:submit', function () {
            var formData = $("#login-check").serialize();
            $.ajax({
                type: "POST",
                url: base_url + "oc_ajax.php",
                data: formData
            }).done(function (data) {
                //console.log(data);  // this is currently returning FALSE // Which is totally correct!

                if (data == true) {
                    var username = $("#username").val();
                    var password = $("#passwd").val();

                    var authenticator = {};
                    authenticator['user'] = $("#username").val();
                    authenticator['pass'] = $("#passwd").val();

                    //generate token api key
                    initAPIKey(username, password, authenticator);

                    //Loading Sweet Alert
                    swal({
                        title: "Logged In",
                        text: "Please waiting for a moment.",
                        type: "success",
                        timer: 5000,
                        showConfirmButton: false
                    });
                    window.location = base_url + 'pws-admin/index.php';
                } else {
                    //Loading Sweet Alert
                    swal({
                        title: "OPP!! Something wrong",
                        text: "Please checking username and password.",
                        type: "warning",
                        timer: 2000,
                        showConfirmButton: false
                    });
                }

            });

            return false; // Don't submit form
        });
    };

    var handleResetPassword = function() {
        $('#reset-password-form').parsley().on('field:validated', function () {
            var ok = $('.parsley-error').length === 0;
            //$('.alert-success').toggleClass('hidden', !ok);
            $('.alert-danger').toggleClass('hidden', ok);
        }).on('form:submit', function () {
            var formData = $("#reset-password-form").serialize();
            $.ajax({
                type: "POST",
                url: base_url + "oc_ajax.php",
                data: formData
            }).done(function (data) {
                //console.log(data);  // this is currently returning FALSE // Which is totally correct!

                if (data == true) {
                    //Loading Sweet Alert
                    swal({
                        title: "Email Sent!!",
                        text: "New Password was sent to your email !!.",
                        type: "success",
                        showConfirmButton: true
                    });
                } else {
                    //Loading Sweet Alert
                    swal({
                        title: "OPP!! Something wrong",
                        text: "Please checking your email again.",
                        type: "warning",
                        timer: 8000,
                        showConfirmButton: false
                    });
                }
            });
            return false; // Don't submit form
        });
    };

    var handleCheckEmail = function() {
        window.Parsley.addAsyncValidator('emailvalidation', function (data) {
            var myResponseText = data.responseText;
            var obj = jQuery.parseJSON(myResponseText);
            //console.log(obj);
            if((obj.result > 0) && (obj.message == "Duplicated")) {
                return false;
            } else {
                return true;
            }
        }, base_url + 'oc_ajax.php?module=users&action=check_email&path=cp');
    };

    var handleCheckUsername = function() {
        window.Parsley.addAsyncValidator('CheckUsername', function (data) {
            var myResponseText = data.responseText;
            var obj = jQuery.parseJSON(myResponseText);
            //console.log(obj);
            if((obj.result > 0) && (obj.message == "Duplicated")) {
                return false;
            } else {
                return true;
            }
        }, base_url + 'oc_ajax.php?module=users&action=check_username&path=cp');
    };

    //initializing API Token key
    var initAPIKey = function(username, password, authenticator) {
        axios.get(base_url + "oc_ajax.php?module=users&action=token&path=cp")
            .then(function (response) {
                //console.log(response);
            })
            .catch(function (error) {
                //console.log(error);
            });
        /*
        $.ajax({
            type: "POST",
            //url: base_url + "rest/ajax_auth.php",
            url: base_url + "oc_ajax.php",
            //data: {"module": "users", "action": "token", "path": "cp", "method": "POST", "username": username, "password": password, "authenticator": authenticator, "secret": 'a8c1556991f48ad6cac4244d1b6a6f5812f72a60'}
            data: {"module": "users", "action": "token", "path": "cp"}
        }).done(function (res) {
            //console.log(res);
        });
        */
    };

    return {
        //main function to initiate the module
        init: function () {
        },
        initLogin: function() {
            handleLogin();
            handleResetPassword();
        },
        initLockScreen: function() {
            handleLogin();
        },
        initList: function() {
        },
        initForm: function() {
            handleCheckUsername();
            handleCheckEmail();
        }
    };

}();