<?php
/*
 * Module controller
 * Include and Declare Class for Module
 */
if(!isset($module))
    $module = "chats"; //necessary value

if(!$action)
    $action = "list";

if(!$path)
    $path = "cp";

$module_class = ucfirst($module);
$module_file_class = PATH_ADMIN_MODULES_ROOT.$module."/classes/class.".$module.".php";

// Declare Main Class
if(file_exists($module_file_class)) {
    require_once($module_file_class);
    $$module_class = new $module_class;
}

//set global js
$doc_ready_js.= $oController->doc_ready_js($module);

$$module_class->module_name = $module;
$$module_class->action = $action;
$$module_class->task = $task;
$$module_class->lang_id = $lang_id;
$$module_class->itemid = $itemid;
$$module_class->path = $path;

$$module_class->module_action_page = PATH_ADMIN_MODULES_ROOT . $module . "/page/" . $$module_class->action . ".php";
if(!file_exists($$module_class->module_action_page)) {
    $$module_class->module_action_page = PATH_ADMIN_ROOT."error404.php";
}

//Select an action
switch ($$module_class->action) {
    case "new":
        require_once(PATH_ADMIN_MODULES_ROOT . strtolower($$module_class->module_name) . "/page/form.php");
        $doc_ready_js.= "Global.initForm();\n";
        $doc_ready_js.= $module_class.".initForm();";
        break;
    case "update":
        //$$module_class->itemid = $_REQUEST['itemid'];
        require_once(PATH_ADMIN_MODULES_ROOT . strtolower($$module_class->module_name) . "/page/form.php");
        $doc_ready_js.= "Global.initForm();\n";
        $doc_ready_js.= $module_class.".initForm();";
        break;
    default:
    case "list":
        require_once($$module_class->module_action_page);
        $page_css.= "";
        $page_js.= "
                <script src='".PATH_ASSETS."plugins/moment/moment.js' type='text/javascript'></script>
        
                <script src='https://unpkg.com/react@15/dist/react.min.js'></script>
                <script src='https://unpkg.com/react-dom@15/dist/react-dom.min.js'></script>
                <script src='https://unpkg.com/react@15/dist/react-with-addons.js'></script>
                <script src='https://cdnjs.cloudflare.com/ajax/libs/react-bootstrap/0.30.6/react-bootstrap.js'></script>
                <script src='https://cdnjs.cloudflare.com/ajax/libs/babel-core/5.8.23/browser.min.js'></script>
                <script src='https://cdnjs.cloudflare.com/ajax/libs/zepto/1.1.6/zepto.min.js'></script>
                <script src='https://unpkg.com/axios/dist/axios.min.js'></script>
        
                <script src='".PATH_ROOT."rest/lib/php_crud_api_transform.js'></script>
                <script type='text/babel' src='".PATH_ADMIN_MODULES."chats/classes/chats.times.js'></script>
                ";

        $doc_ready_js.= "Global.initList();\n";
        $doc_ready_js.= $module_class.".initList();\n";

        break;
}
?>