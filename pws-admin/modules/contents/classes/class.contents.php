<?php
class Contents extends DB {
    var $tbContents = "pws_contents";
    var $tbMedia = "pws_media";

    function __construct() {
        $this->Page = new Page();
        $this->Page->module_name = "Contents"; // ucfirst
        $this->module_name = strtolower($this->Page->module_name); // strtolower

        /*
         * Extend class declaring
         * Media Contents - Galleries Class
         */
        if(file_exists(PATH_ADMIN_MODULES_ROOT.$this->module_name."/classes/class.contents.media.php")) {
            require_once(PATH_ADMIN_MODULES_ROOT.$this->module_name."/classes/class.contents.media.php");
            $this->Media = new ContentsMedia();
        }
    }

    ///Additional Functions
    function duplicate($lang_id = NULL, $id = NULL) {
        if(empty($lang_id))
            $lang_id = "en";

        if($lang_id)
            $ct[] = "$this->tbContents.lang_id = '$lang_id' ";
        if($id)
            $ct[] = "$this->tbContents.id = '$id' ";

        if(is_array($ct))
        {
            $condition = implode("AND ", $ct);
            $condition = "WHERE ".$condition;
        }

        $field = "*";
        $sql = "SELECT $field FROM $this->tbContents ";
        $sql.= $condition;
        return $data = DB::query($sql, "count");
    }

    function get($lang_id = NULL, $filename = NULL, $query_type = NULL) {
        global $oLanguages;

        if(!isset($query_type))
            $query_type = "fetchAssoc";

        if(!isset($lang_id))
            $lang_id = $oLanguages->id;

        $sql = "SELECT * FROM `$this->tbContents`";

        if($lang_id)
            $ct[] = "`$this->tbContents`.`lang_id` = '$lang_id' ";

        if($filename)
            $ct[] = "$this->tbContents.`filename` = '$filename' ";

        $ct[] = "`$this->tbContents`.`is_active` = 'Y' ";

        if(is_array($ct))
            $sql = $sql." WHERE ".implode(" AND ", $ct);

        //PRE($sql);
        $data = DB::query($sql, $query_type);
        return $data;
    }

    function select($lang_id = NULL, $id = NULL, $parent_id = NULL, $is_active = NULL, $query_type = NULL) {
        global $oLanguages;

        if(!isset($query_type))
            $query_type = "fetchAssoc";

        if(!isset($lang_id))
            $lang_id = $oLanguages->id;

        $sql = "SELECT * FROM `$this->tbContents`";

        if($lang_id)
            $ct[] = "`$this->tbContents`.`lang_id` = '$lang_id' ";

        if(empty($id)) {
            if($parent_id == "all") {
                //$ct[] = "$this->tbContents.`parent_id` = '$parent_id' ";
            } elseif($parent_id == "parent") {
                $ct[] = "$this->tbContents.`parent_id` = '0' ";
            } else {
                $ct[] = "$this->tbContents.`parent_id` = '$parent_id' ";
            }
        }

        if($id)
            $ct[] = "$this->tbContents.`id` = '$id' ";
        if($is_active)
            $ct[] = "`$this->tbContents`.`is_active` = '$is_active' ";

        if(is_array($ct))
            $sql = $sql." WHERE ".implode(" AND ", $ct);

        //PRE($sql);
        $data = DB::query($sql, $query_type);
        return $data;
    }

    function insert($post_vars) {
        global $oUser;
        global $oLanguages;

        if($post_vars)
            extract($post_vars);

        if(!isset($lang_id))
            $lang_id = $oLanguages->id;

        if(!isset($created_by))
            $created_by = $oUser->id;

        if(!isset($updated_by))
            $updated_by = $oUser->id;

        if(!isset($is_active))
            $is_active = "Y";

        $title = DB::escape($title);
        $subtitle = DB::escape($subtitle);
        $short_description = DB::escape($short_description);
        $description = addslashes($description);
        //$description = DB::escape($description);
        $meta_title = DB::escape($meta_title);
        $meta_keyword = DB::escape($meta_keyword);
        $meta_description = DB::escape($meta_description);

        $field = "`id`, `lang_id`, `parent_id`, `filename`, `title`, `subtitle`, `short_description`, `description`, `header_photo`, `meta_title`, `meta_keyword`, `meta_description`, `created_by`, `updated_by`, `updated_date`, `is_active`";
        $value = "'', '$lang_id', '$parent_id', '$filename', '$title', '$subtitle', '$short_description', '$description', '$header_photo', '$meta_title', '$meta_keyword', '$meta_description', '$created_by', '$updated_by', NOW(), '$is_active'";

        $sql = "INSERT INTO $this->tbContents ($field) VALUES ($value)";
        $id = DB::query($sql, "lastInsertId");
        if($id) {
            //Loop for insert all languages
            global $languages;
            $data_languages = $languages->select(NULL, NULL, "Y");
            //PRE($data_languages);
            if($data_languages) {
                foreach($data_languages as $lang_item):
                    $duplicate_check = $this->duplicate($lang_item['id'], $id);
                    //PRE($duplicate_check);
                    if($duplicate_check){
                        //`id`, `lang_id`, `parent_id`, `filename`, `title`, `subtitle`, `short_description`, `description`,
                        // `meta_title`, `meta_keyword`, `meta_description`, `created_by`, `updated_by`, `updated_date`, `is_active`
                        $post_vars = array(
                            "id" => $id,
                            "lang_id" => $lang_item['id'],
                            "parent_id" => $parent_id,
                            "filename" => $filename,
                            "title" => $title,
                            "subtitle" => $subtitle,
                            "short_description" => $short_description,
                            "description" => $description,
                            "header_photo" => $header_photo,
                            "meta_title" => $meta_title,
                            "meta_keyword" => $meta_keyword,
                            "meta_description" => $meta_description,
                            "created_by" => $created_by,
                            "updated_by" => $updated_by,
                            "updated_date" => $updated_date,
                            "is_active" => $is_active
                        );
                        //$this->update($post_vars);
                    }else{
                        $value = "'$id', '$lang_item[id]', '$parent_id', '$filename', '$title', '$subtitle', '$short_description', '$description', '$header_photo', '$meta_title', '$meta_keyword', '$meta_description', '$created_by', '$updated_by', NOW(), '$is_active'";
                        $sql = "INSERT INTO $this->tbContents ($field) VALUES ($value)";
                        $processok = DB::query($sql, "lastInsertId");
                    }
                endforeach;
                unset($data_languages);
            }

            //Photo Upload
            if($_FILES['header_photo']['name']){
                // images upload
                $file_header_photo = $_FILES['header_photo'];
                //print_r($file_header_photo);
                if(count($file_header_photo['name']) > 0) {
                    $file_header_photo_name = uploadFile($this->module_name, $id, $file_header_photo, "photo");
                    if($file_header_photo_name)
                        $this->update_header_photo($id, $file_header_photo_name);
                }
                unset($file_header_photo_name, $file_header_photo);
            }

            return $id;
        } else {
            return false;
        }
    }

    function update($post_vars) {
        global $oUser;
        global $oLanguages;

        if($post_vars)
            extract($post_vars);

        if(!isset($lang_id))
            $lang_id = $oLanguages->id;

        if(!isset($created_by))
            $created_by = $oUser->id;

        if(!isset($updated_by))
            $updated_by = $oUser->id;

        if($description)
            $description = str_replace("<p><br></p>", "", $description);

        $title = DB::escape($title);
        $subtitle = DB::escape($subtitle);
        $short_description = DB::escape($short_description);
        $description = addslashes($description);
        //$description = DB::escape($description);
        $meta_title = DB::escape($meta_title);
        $meta_keyword = DB::escape($meta_keyword);
        $meta_description = DB::escape($meta_description);

        $f = " filename = '$filename' ";
        $f.= ", title = '$title' ";

        if($parent_id) {
            $f.= ", parent_id = '$parent_id' ";
        } else {
            $f.= ", parent_id = '0' ";
        }
        //if($subtitle)
            $f.= ", subtitle = '$subtitle' ";
        //if($short_description)
            $f.= ", short_description = '$short_description' ";
        $f.= ", description = '$description' ";

        if($header_photo)
            $f.= ", header_photo = '$header_photo' ";
        if($meta_title)
            $f.= ", meta_title = '$meta_title' ";
        if($meta_keyword)
            $f.= ", meta_keyword = '$meta_keyword' ";
        if($meta_description)
            $f.= ", meta_description = '$meta_description' ";
        if($created_by)
            $f.= ", created_by = '$created_by' ";
        if($updated_by)
            $f.= ", updated_by = '$updated_by' ";

        $f.= ", updated_date = NOW() ";
        if($is_active)
            $f.= ", is_active = '$is_active' ";

        if($update_all_lang != "Y")
            $cnt =  "AND `$this->tbContents`.`lang_id` = '$lang_id'";

        $sql = "UPDATE $this->tbContents SET $f WHERE $this->tbContents.id = '$itemid' ".$cnt;
        //echo $sql;
        //exit;
        $data = DB::query($sql);
        if($data){
            //Loop for insert all languages
            global $languages;
            $data_languages = $languages->select(NULL, NULL, "Y");
            //PRE($data_languages);
            if($data_languages) {
                foreach($data_languages as $lang_item):
                    $duplicate_check = $this->duplicate($lang_item['id'], $itemid);
                    //PRE($duplicate_check);
                    if($duplicate_check){
                        //do nothing
                    }else{
                        $value = "'$itemid', '$lang_item[id]', '$parent_id', '$filename', '$title', '$subtitle', '$short_description', '$description', '$header_photo', '$meta_title', '$meta_keyword', '$meta_description', '$created_by', '$updated_by', NOW(), '$is_active'";
                        $sql = "INSERT INTO $this->tbContents ($field) VALUES ($value)";
                        $processok = DB::query($sql, "lastInsertId");
                    }
                endforeach;
                unset($data_languages);
            }

            //Photo Upload
            if($_FILES['header_photo']['name']){
                // images upload
                $file_header_photo = $_FILES['header_photo'];
                //print_r($file_header_photo);
                if(count($file_header_photo['name']) > 0) {
                    $file_header_photo_name = uploadFile($this->module_name, $itemid, $file_header_photo, "photo");
                    if($file_header_photo_name)
                        $this->update_header_photo($itemid, $file_header_photo_name);
                    if($old_header_photo)
                        removeFile($itemid, $this->module_name, $old_header_photo);
                }
                unset($file_header_photo_name, $file_header_photo);
            }

            return $data;
        } else {
            return false;
        }
    }

    function update_header_photo($id, $header_photo) {
        $field = "header_photo = '$header_photo' ";
        $sql = "UPDATE $this->tbContents SET $field WHERE id = '$id'";
        return $data = DB::query($sql);
    }

    function update_status($id, $is_active) {
        $field = "is_active = '$is_active' ";
        $sql = "UPDATE $this->tbContents SET $field WHERE id = '$id'";
        return $data = DB::query($sql);
    }

    function delete_photo($post_vars) {
        if($post_vars)
            extract($post_vars);

        if($itemid) {
            //delete
            //$delete_result = removeFile($itemid, $this->module_name, "header_photo.png");
            $delete_result = removeFile($itemid, $this->module_name, $filename);

            if($delete_result) {
                $field = "header_photo = '' ";
                $sql = "UPDATE $this->tbContents SET $field WHERE id = '$itemid'";
                return $data = DB::query($sql);
            }
        } else {
            return false;
        }
    }

    function delete($post_vars) {
        if($post_vars)
            extract($post_vars);

        //Delete all lang
        //if($lang_id)
        //    $cnt =  "AND `$this->tbContents`.`lang_id` = '$lang_id'";
        if($itemid) {
            $sql = "DELETE FROM `$this->tbContents` WHERE `$this->tbContents`.`id` = '$itemid' ";
            $data = DB::query($sql);
            if($data) {
                //delete from media table
                $sql = "DELETE FROM `$this->tbMedia` WHERE `$this->tbMedia`.`id` = '$itemid' ";
                DB::query($sql);

                //delete
                //removeFile($itemid, $this->module_name, "profile_photo.png");
                //delete all files and folders belong to this ID
                return delete_directory(PATH_UPLOAD_ROOT.$this->module_name.'/'.$itemid.'/');
            }
        } else {
            return false;
        }
    }

    //Upload summernote photo
    function summernoteUpload($post_vars) {
        if($post_vars)
            extract($post_vars);

        //PRE($post_vars);
        if ($_FILES['file']['name']) {
            if (!$_FILES['file']['error']) {
                $name = "smn_".$this->module_name."_".date("YmdHis");
                $ext = explode('.', $_FILES['file']['name']);
                $filename = $name . '.' . $ext[1];

                //$destination = '/saturdaysresidence.com/media/summernote/' . $filename; //change this directory
                if($itemid) {
                    $target_dir = PATH_UPLOAD_ROOT.$this->module_name."/".$itemid."/";
                    $target_url = BASE_UPLOAD.$this->module_name."/".$itemid."/";
                } else {
                    $target_dir = PATH_UPLOAD_ROOT.$this->module_name."/summernote/";
                    $target_url = BASE_UPLOAD.$this->module_name."/summernote/";
                }
                //create folder
                if (!file_exists($target_dir)) {
                    MKDIRS($target_dir);
                }
                $destination =  $target_dir.$filename;
                $location = $_FILES["file"]["tmp_name"];
                $uploadok = move_uploaded_file($location, $destination);
                if($uploadok){
                    //insert in to database
                    //$page_content = $$module_class->summernoteUpload($module, $action, $task, $itemid, $filename);

                    //$field = "is_active = '$is_active' ";
                    //$sql = "UPDATE $this->tbContents SET $field WHERE id = '$id'";
                    //return $data = DB::query($sql);
                }
                echo $target_url.$filename;//return image to editor
            } else {
                echo  $message = 'Ooops! Your upload triggered the following error:  '.$_FILES['file']['error'];
            }
        }
    }

    //Delete summernote photo
    function summernoteDelete($post_vars) {
        if($post_vars)
            extract($post_vars);

        //PRE($post_vars);
        //$file_name_array = explode('/',$file);
        //$file_name = end($file_name_array);
        //echo "<br />file name: ".$file_name;

        $file_path = str_replace(BASE_UPLOAD, PATH_UPLOAD_ROOT, $file);
        //echo "<br />file name: ".$file;
        //echo "<br />file path: ".$file_path;

        $delete_result = false;
        if(isset($file_path) && is_file($file_path)) {
            $delete_result = unlink($file_path);
        }

        if($delete_result) {
            //delete data from database
            return true;
        } else {
            echo 'Uh oh, error :(';
            return false;
        }
    }

}
?>