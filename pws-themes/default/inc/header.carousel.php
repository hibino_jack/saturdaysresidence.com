<?php
if($data_content[0]['id']) {
    /*
    *  Get Media items
    */
    $data_gallery = $contents->Media->select(NULL, $data_content[0]['id']);
}
?>
<!-- Header Carousel -->
<?php if($data_gallery){ ?>
    <header id="myCarousel" class="carousel slide carousel-fade" data-interval="3000" data-ride="carousel">
    <!-- Indicators -->
    <!--<ol class="carousel-indicators">
        <?php
        for($i=0; $i < count($data_gallery); $i++) {
            if($i == "0") { $class_active = "active"; } else { $class_active = ""; }
            ?>
            <li data-target="#myCarousel" data-slide-to="<?php echo $i; ?>" class="<?php echo $class_active; ?>"></li>
        <?php } ?>
    </ol>-->

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
        <?php
        for($i=0; $i < count($data_gallery); $i++) {
            if($i == "0") { $class_active = "active"; } else { $class_active = ""; }
            ?>

        <div class="item <?php echo $class_active; ?>">
            <?php
			$image_slider = "http://placehold.it/1450x500&text=".$data_content[0]['title'];
            $pic = BASE_UPLOAD."contents/".$data_gallery[$i]['content_id']."/".$data_gallery[$i]['filename'];
            $pic_realpath = PATH_UPLOAD_ROOT."contents/".$data_gallery[$i]['content_id']."/".$data_gallery[$i]['filename'];
			if(is_file($pic_realpath)) {
				//$image_slider = PATH_ROOT."timthumb.php?src=".$pic."&w=1920&h=1080";
				$image_slider = $pic;
			}
                ?>
                <div class="fill" style="background-image:url('<?php echo $image_slider; ?>');"></div>
                <!--<div class="carousel-caption">-->
                <div class="slider-content-wrapper"><div class="slider-content-inner">
                    <h2><?php echo $data_gallery[$i]['title']?></h2>
                    <?php if($data_gallery[$i]['button_label'] && $data_gallery[$i]['button_link']){?>
                    	<a href="<?php echo $data_gallery[$i]['button_link']; ?>" title="<?php echo $data_gallery[$i]['button_label']; ?>"><?php echo $data_gallery[$i]['button_label']; ?></a>
                    <?php } ?>
                </div></div>
                <!--</div>-->
                </div>
        <?php } ?>
    </div>

    <!-- Controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
        <span class="fa fa-angle-left"></span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
        <span class="fa fa-angle-right"></span>
    </a>
</header>
<?php } ?>

<!-- //Header Carousel -->