var Contents = function () {
    var module_is = 'Contents';
    var moduleName = module_is.toLowerCase();

    var handleMagnificPopup = function () {
        var $container = $('.portfolioContainer');
        $container.isotope({
            filter: '*',
            animationOptions: {
                duration: 750,
                easing: 'linear',
                queue: false
            }
        });

        $('.portfolioFilter a').click(function(){
            $('.portfolioFilter .current').removeClass('current');
            $(this).addClass('current');

            var selector = $(this).attr('data-filter');
            $container.isotope({
                filter: selector,
                animationOptions: {
                    duration: 750,
                    easing: 'linear',
                    queue: false
                }
            });
            return false;
        });

        $('.image-popup').magnificPopup({
            type: 'image',
            closeOnContentClick: true,
            mainClass: 'mfp-fade',
            gallery: {
                enabled: true,
                navigateByImgClick: true,
                preload: [0,1] // Will preload 0 - before current, and 1 after the current image
            }
        });

    };

    var handleDropzone = function () {
        /*
        var module = $(".div_data_controller").data("module");
        var action = $(".div_data_controller").data("action");
        var task = $(".div_data_controller").data("task");
        var lang_id = $(".div_data_controller").data("lang_id");
        var path = $(".div_data_controller").data("path");
        var content_id = $(".div_data_controller").data("itemid");

        if(path == "") path = 'cp';
        */
        //alert(" M:" + module + " A:" + action + " T:" + task + " L:" + lang_id + " P:" + path + " I:" + itemid);

        // Get the template HTML and remove it from the doument
        var previewNode = document.querySelector("#template");
        previewNode.id = "";
        var previewTemplate = previewNode.parentNode.innerHTML;
        previewNode.parentNode.removeChild(previewNode);

        var myDropzone = new Dropzone(document.body, {
            // Make the whole body a dropzone
            //url: base_url + "oc_ajax.php?module=" + module + "&action=" + action + "&task=" + task + "&path=" + path + "&lang_id=" + lang_id + "&itemid=" + itemid, // Set the url
            url: base_url + "oc_ajax.php?module=" + $(".div_data_controller").data("module") + "&action=" + $(".div_data_controller").data("action") + "&task=" + $(".div_data_controller").data("task") + "&path=" + $(".div_data_controller").data("path") + "&lang_id=" + $(".div_data_controller").data("lang_id") + "&itemid=" + $(".div_data_controller").data("itemid"), // Set the url
            maxFilesize: 2, // MB
            maxFiles: 12, //change limit as per your requirements
            dictMaxFilesExceeded: "Maximum upload limit reached",
            acceptedFiles: "image/*",
            dictInvalidFileType: "upload only JPG/PNG images",
            thumbnailWidth: 80,
            thumbnailHeight: 80,
            parallelUploads: 20,
            previewTemplate: previewTemplate,
            autoQueue: false, // Make sure the files aren't queued until manually added
            previewsContainer: "#previews", // Define the container to display the previews
            clickable: ".fileinput-button" // Define the element that should be used as click trigger to select files.
        });

        myDropzone.on("addedfile", function(file) {
            // Hookup the start button
            file.previewElement.querySelector(".start").onclick = function() { myDropzone.enqueueFile(file); };
        });

        // Update the total progress bar
        myDropzone.on("totaluploadprogress", function(progress) {
            document.querySelector("#total-progress .progress-bar").style.width = progress + "%";
        });

        myDropzone.on("sending", function(file) {
            // Show the total progress bar when upload starts
            document.querySelector("#total-progress").style.opacity = "1";
            // And disable the start button
            file.previewElement.querySelector(".start").setAttribute("disabled", "disabled");
        });

        // Hide the total progress bar when nothing's uploading anymore
        myDropzone.on("queuecomplete", function(progress) {
            document.querySelector("#total-progress").style.opacity = "0";
        });

        myDropzone.on("success", function(file, serverResponse) {
            data = JSON.parse(serverResponse);
            //console.log(data);
            var fileuploded = file.previewElement.querySelector("[data-dz-name]");
            fileuploded.innerHTML = "New Filename :<br>" + data.filename;
            var btndelete = file.previewElement.querySelector(".delete");
            btndelete.setAttribute("data-module", moduleName);
            btndelete.setAttribute("data-action", "gallery");
            btndelete.setAttribute("data-lang_id", "en");
            btndelete.setAttribute("data-path", "cp");
            btndelete.setAttribute("data-itemid", data.id);
            btndelete.setAttribute("data-content_id", data.content_id);
            btndelete.setAttribute("data-filename", data.filename);
            btndelete.setAttribute("data-task", "delete_gallery_item");
            //$("button.delete").closest("button").addClass("delete-item-img");
        });

        // On removing file
        myDropzone.on("removedfile", function (file, response) {
            //console.log(response);
            //console.log(file.name);
            var name = file.previewElement.querySelector('[data-dz-name]').innerHTML;

            var btndelete = file.previewElement.querySelector(".delete");
            if(btndelete.hasAttribute("data-itemid")) {
                var module = btndelete.getAttribute("data-module");
                var action = btndelete.getAttribute("data-action");
                var lang_id = btndelete.getAttribute("data-lang_id");
                var path = btndelete.getAttribute("data-path");
                var itemid = btndelete.getAttribute("data-itemid");
                var content_id = btndelete.getAttribute("data-content_id");
                var filename = btndelete.getAttribute("data-filename");
                var task = btndelete.getAttribute("data-task");
                //console.log(module + " - " + action + " - " + lang_id + " - " + path + " - " + itemid + " - " + content_id + " - " + filename + " - " + task);
                DeletePhoto(module, action, lang_id, path, itemid, content_id, filename, task);
            }
        });

        // Setup the buttons for all transfers
        // The "add files" button doesn't need to be setup because the config
        // `clickable` has already been specified.
        document.querySelector("#actions .start").onclick = function() {
            myDropzone.enqueueFiles(myDropzone.getFilesWithStatus(Dropzone.ADDED));
        };
        /*
        document.querySelector("#actions .cancel").onclick = function() {
            //alert("Cancel");
            //DeleteAllPhoto(module, action, lang_id, path, content_id, task);
            myDropzone.removeAllFiles(true);
        };*/
    };

    var handleDeletePhoto = function () {
        $(document).on("click",".delete-item-img", function(e) {
            e.preventDefault();

            var module = $(this).data('module');
            var action = $(this).data('action');
            var lang_id = $(this).data('lang_id');
            var path = $(this).data('path');
            var itemid = $(this).data('itemid');
            var content_id = $(this).data('content_id');
            var filename = $(this).data('filename');
            var task = $(this).data('task');
            if(task == ""){
                task = "delete_item";
            }

            //alert(" M:" + module + " A:" + action + " T:" + task + " L:" + lang_id + " P:" + path + " I:" + itemid + " C:" + content_id + " F:" + filename);

            if(itemid && content_id && filename && task) {
                DeletePhoto(module, action, lang_id, path, itemid, content_id, filename, task);
            }
        });

        /*
                 $(document).on("click",".delete-item-img", function(e) {
                 e.preventDefault();

                 var module = $(this).data('module');
                 var action = $(this).data('action');
                 var lang_id = $(this).data('lang_id');
                 var path = $(this).data('path');
                 var itemid = $(this).data('itemid');
                 var content_id = $(this).data('content_id');
                 var filename = $(this).data('filename');
                 var task = $(this).data('task');
                 if(task == ""){
                 task = "delete_item";
                 }

                 //alert(" M:" + module + " A:" + action + " T:" + task + " L:" + lang_id + " P:" + path + " I:" + itemid + " C:" + content_id + " F:" + filename);

                 if(itemid) {
                 swal({
                 title: "Are you sure?",
                 text: "Are you sure that you want to delete this record?",
                 type: "warning",
                 showCancelButton: true,
                 closeOnConfirm: false,
                 confirmButtonText: "Yes, delete it!",
                 confirmButtonColor: "#ec6c62"
                 }, function() {
                 $.ajax({
                 type: "POST",
                 url: base_url + "oc_ajax.php",
                 //data: {"module": module, "action": action, "task": task, "lang_id": lang_id, "path": path, "itemid": itemid, "filename": filename, "content_id": content_id},
                 data: "module=" + module + "&action=" + action + "&task=" + task + "&path=" + path + "&lang_id=" + lang_id + "&itemid=" + itemid + "&content_id=" + content_id + "&filename=" + filename,
                 success: function(data){
                 console.log(data);  // this is currently returning FALSE // Which is totally correct!
                 }
                 }).done(function(data) {
                 swal("Done!!", "Your data ID #" + itemid + " was successfully deleted!", "success");

                 $( "div" ).remove(".photo" + itemid);
                 $(".portfolioContainer").load();

                 //document.getElementById("Table"+table_id).deleteRow(row);
                 }).error(function(data) {
                 swal("Oops", "We couldn't connect to the server!", "error");
                 });
                 });
                 }

                 });
                 */
    }

    function DeletePhoto(module, action, lang_id, path, itemid, content_id, filename, task) {
        if(itemid) {
            swal({
                title: "Are you sure?",
                text: "Are you sure that you want to delete this record?",
                type: "warning",
                showCancelButton: true,
                closeOnConfirm: false,
                confirmButtonText: "Yes, delete it!",
                confirmButtonColor: "#ec6c62"
            }, function() {
                $.ajax({
                    type: "POST",
                    url: base_url + "oc_ajax.php",
                    //data: {"module": module, "action": action, "task": task, "lang_id": lang_id, "path": path, "itemid": itemid, "filename": filename, "content_id": content_id},
                    data: "module=" + module + "&action=" + action + "&task=" + task + "&path=" + path + "&lang_id=" + lang_id + "&itemid=" + itemid + "&content_id=" + content_id + "&filename=" + filename,
                    success: function(data){
                        console.log(data);  // this is currently returning FALSE // Which is totally correct!
                    }
                }).done(function(data) {
					//alert("RETURN " + data);
					if(task == "delete_header_photo_item"){
						swal("Done!!", "Your Photo was successfully deleted!", "success");
						if(module == "contents" && content_id == 1)
						{
						window.location = base_url + 'pws-admin/index.php?module=' + module + "&action=update&itemid=1&task=" + task + "&lang_id=" + lang_id;
						}else{
						window.location = base_url + 'pws-admin/index.php?module=' + module + "&action=update&itemid=" + itemid + "&task=" + task + "&lang_id=" + lang_id;
						}
						
					}else{
						swal("Done!!", "Your data ID #" + itemid + " was successfully deleted!", "success");
	
						$( "div" ).remove(".photo" + itemid);
						$(".portfolioContainer").load();
					}
                    //document.getElementById("Table"+table_id).deleteRow(row);
                }).error(function(data) {
                    swal("Oops", "We couldn't connect to the server!", "error");
                });
            });
        }
    }

    var handleDeleteAllPhoto = function () {
        $(document).on("click",".delete-all-items-img", function(e) {
            e.preventDefault();

            var module = $(this).data('module');
            var action = $(this).data('action');
            var lang_id = $(this).data('lang_id');
            var path = $(this).data('path');
            var itemid = $(this).data('itemid');
            var task = $(this).data('task');
            if(task == ""){
                task = "delete_item";
            }

            //alert(" M:" + module + " A:" + action + " T:" + task + " L:" + lang_id + " P:" + path + " I:" + itemid + " C:" + content_id + " F:" + filename);

            if(itemid && task) {
                DeleteAllPhoto(module, action, lang_id, path, itemid, task);
            }
        });
    }

    function DeleteAllPhoto(module, action, lang_id, path, itemid, task) {
        //alert(" M:" + module + " A:" + action + " T:" + task + " L:" + lang_id + " P:" + path + " I:" + itemid);
        if(module && action && itemid && task) {
            swal({
                title: "Are you sure?",
                text: "You want to delete 'ALL' records?",
                type: "warning",
                showCancelButton: true,
                closeOnConfirm: false,
                confirmButtonText: "Yes, delete it!",
                confirmButtonColor: "#ec6c62"
            }, function() {
                $.ajax({
                    type: "POST",
                    url: base_url + "oc_ajax.php",
                    data: "module=" + module + "&action=" + action + "&task=" + task + "&path=" + path + "&lang_id=" + lang_id + "&itemid=" + itemid,
                    success: function(data){
                        //console.log(data);  // this is currently returning FALSE // Which is totally correct!
                    }
                }).done(function(data) {
                    swal("Done!!", "Your data was successfully deleted!", "success");
                    window.location = base_url + 'pws-admin/index.php?module=' + module + "&action=" + action + "&task=&lang_id=" + lang_id + "&path=" + path + "&itemid=" + itemid;
                }).error(function(data) {
                    swal("Oops", "We couldn't connect to the server!", "error");
                });
            });
        }
    }

    var handleUpdatePhotoForm = function() {
        $('#update-form').parsley().on('field:validated', function () {
            var ok = $('.parsley-error').length === 0;
            $('.alert-success').toggleClass('hidden', !ok);
            $('.alert-danger').toggleClass('hidden', ok);
        }).on('form:submit', function () {
            var formData = new FormData($("#update-form")[0]);

            var module = ($("#module").val()) ? $("#module").val() : '';
            var action = ($("#action").val()) ? $("#action").val() : '';
            var task = ($("#task").val()) ? $("#task").val() : '';
            var lang_id = ($("#lang_id").val()) ? $("#lang_id").val() : '';
            var path = ($("#path").val()) ? $("#path").val() : '';
            var itemid = ($("#itemid").val()) ? $("#itemid").val() : '';
            var content_id = ($("#content_id").val()) ? $("#content_id").val() : '';

            //alert(" M:" + module + " A:" + action + " T:" + task + " L:" + lang_id + " P:" + path + " I:" + itemid);

            $.ajax({
                type: "POST",
                url: base_url + "oc_ajax.php",
                data: formData,
                //mimeType:"multipart/form-data",
                cache: false,
                contentType: false,
                processData:false
            }).done(function (data) {
                //console.log(data);  // this is currently returning FALSE // Which is totally correct!
                if (data == true || data > 0) {
                    //Loading Sweet Alert
                    swal({
                        title: "Your data was successfully submitted",
                        text: "Please waiting for a moment.",
                        type: "success",
                        timer: 4000,
                        showConfirmButton: false
                    });
                    window.location = base_url + 'pws-admin/index.php?module=' + module + "&action=gallery" + "&task=list&lang_id=" + lang_id + "&path=" + path + "&itemid=" + content_id;
                } else {
                    //Loading Sweet Alert
                    swal({
                        title: "OPP!! Something wrong",
                        text: "Please checking your data again.",
                        type: "warning",
                        timer: 4000,
                        showConfirmButton: false
                    });
                }

            });

            return false; // Don't submit form
        });
    };

    var handleUpdatePhotoButton = function() {
        $(document).on("click",".update-item-img", function(e) {
            e.preventDefault();
            var module = $(this).data('module');
            var action = $(this).data('action');
            var task = $(this).data('task');
            var lang_id = $(this).data('lang_id');
            var path = $(this).data('path');
            var itemid = $(this).data('itemid');
            var content_id = $(this).data('content_id');
            var filename = $(this).data('filename');

            window.location = base_url + 'pws-admin/index.php?module=' + module + "&action=" + action + "&task=" + task + "&path=" + path + "&lang_id=" + lang_id + "&itemid=" + itemid + "&content_id=" + content_id + "&filename=" + filename;
        });
    };

    return {
        //main function to initiate the module
        init: function() {
        },
        initList: function(){
        },
        initForm: function() {
			handleDeletePhoto();
        },
        initMediaForm: function() {
            handleMagnificPopup();
            handleUpdatePhotoForm();
        },
        initDropzone: function() {
            handleMagnificPopup();
            handleDeletePhoto();
            handleDeleteAllPhoto();
            handleUpdatePhotoButton();
            handleDropzone();
        }
    };

}();