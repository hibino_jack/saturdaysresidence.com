<?php
//$data = $$module_class->select();
//$datatotal = count($data);
//PRE($data);

//GET $data from API
//$conditions = "filter=lang_id,eq,".$lang_id;
$data = readAPIFile($$module_class->tbUsers);

if($data) {
    $datatotal = count($data);
}
//PRE($data);
?>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default panel-border">
            <div class="panel-heading">
                <?php echo $languages->dropdown_languages($module, $action, $path, $itemid, $task); ?>
                <h4 class="header-title"><b><?php echo $module_class." ".$action;?></b></h4>
                <p class="text-muted font-13 m-b-10">Total <?php echo $datatotal; ?> record(s) on the list.</p>
            </div>
            <div class="panel-body">
                <table id="datatable-buttons" class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Photo</th>
                        <th>Username</th>
                        <th>Fullname</th>
                        <th>Email</th>
                        <th>Status</th>
                        <th>Active</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if($data) {
                        $j = 1;
                        for($i=0; $i < $datatotal; $i++) { ?>
                            <tr id="<?php echo $data[$i]['id']; ?>">
                                <td><?php echo $j; ?></td>
                                <td>
                                    <?php
                                    $pic = BASE_UPLOAD.$module."/".$data[$i]['id']."/".$data[$i]['profile_photo'];
                                    $pic_realpath = PATH_UPLOAD_ROOT.$module."/".$data[$i]['id']."/".$data[$i]['profile_photo'];
                                    if(is_file($pic_realpath)) {
                                        ?>
                                        <a class="image-popup" href="<?php echo $pic; ?>" title="<?php echo $data[$i]['first_name']." ".$data[$i]['last_name']; ?>">
                                            <img src="<?php echo PATH_ROOT; ?>timthumb.php?src=<?php echo $pic_realpath; ?>&w=80&h=80" class="thumb-md">
                                        </a>
                                    <?php }else{ ?>
                                        <img src="<?php echo BASE_IMAGES; ?>nophoto.jpg" alt="<?php echo $data[$i]['first_name']." ".$data[$i]['last_name']; ?>" class="thumb-md">
                                    <?php } ?>
                                </td>
                                <td><strong><?php echo $data[$i]['username']; ?></strong></td>
                                <td><?php echo $data[$i]['first_name']." ".$data[$i]['last_name']; ?></td>
                                <td><?php echo $data[$i]['email']; ?></td>
                                <td><?php echo $data[$i]['status']; ?></td>
                                <td><span class="label label-table label-<?php echo ($data[$i]['is_active'] == "Y") ? "success" : "inverse"; ?>"><?php echo ($data[$i]['is_active'] == "Y") ? "Active" : "Disabled"; ?></span></td>
                                <td class="actions">
                                    <button class="btn btn-default btn-xs btn-icon fa fa-pencil btn-attr-item" data-lang_id="<?php echo $lang_id;?>" data-module="<?php echo $module;?>" data-action="update" data-id="<?php echo $data[$i]['id']; ?>" data-task="<?php echo $task;?>" data-path="<?php echo $path;?>"></button>
                                    <button class="btn btn-danger btn-xs btn-icon fa fa-trash-o delete-item" data-lang_id="<?php echo $lang_id;?>" data-module="<?php echo $module;?>" data-action="delete" data-id="<?php echo $data[$i]['id']; ?>" data-task="<?php echo $task;?>" data-path="<?php echo $path;?>"></button>
                                </td>
                            </tr>
                            <?php  $j++;
                        } //for
                    }else{ ?>
                        <tr>
                            <td colspan="8">Sorry, Record not found</td>
                        </tr>
                    <?php }?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>