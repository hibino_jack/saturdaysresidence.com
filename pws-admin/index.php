<?php
require_once("../pws-core/site.config.php");
if(file_exists(PATH_ADMIN_MODULES_ROOT.'users/classes/class.users.php')) {
    require_once(PATH_ADMIN_MODULES_ROOT.'users/classes/class.users.php');
    $users = new Users();

    //LOGIN RECHECK
    $users->login_recheck();
    //define("ACTIVE_MODULE", 1);

    //if($_SESSION['pws_session']['user_id']) {
        //$oUser = (object)$users->oUser($_SESSION['pws_session']['user_id']);
        //$oUser = (object)$_SESSION['oUser'];
        //PRE($oUser);
    //}
}

if(!$path)
    $path = "cp"; // set ajax and redirect path to Control Panel : CP

//PRE($_REQUEST);
$module_include_page = $oController->controller($module, $action, $task);

// Set CSS and JS
$page_css = $oController->page_css($module);
$page_js = $oController->page_js($module);
?>
<!DOCTYPE html>
<html>
    <head>
        <?php require_once(PATH_ADMIN_INC_ROOT."header.php"); ?>
    </head>


    <body class="fixed-left">
    <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
            <?php require_once(PATH_ADMIN_INC_ROOT."top-bar.php"); ?>
            <!-- Top Bar End -->

            <!-- ========== Left Sidebar Start ========== -->
            <?php require_once(PATH_ADMIN_INC_ROOT."left-bar.php"); ?>
            <!-- Left Sidebar End -->

            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <!-- Page-Title and breadcrumb -->
                        <?php require_once(PATH_ADMIN_INC_ROOT."breadcrumb.php"); ?>

                        <!-- Row main content -->
                        <?php
                        //PRE("Token : ". $token);
                        //PRE($_SESSION);
                        include_once($module_include_page);
                        ?>
                        <!-- End row main content-->

                    </div> <!-- container -->

                </div> <!-- content -->

                <?php require_once(PATH_ADMIN_INC_ROOT."footer.php"); ?>
            </div>

            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


            <!-- Right Sidebar -->
            <?php require_once(PATH_ADMIN_INC_ROOT."right-bar.php"); ?>
            <!-- /Right-bar -->

        </div>
        <!-- END wrapper -->

        <?php require_once(PATH_ADMIN_INC_ROOT."footer-script.php"); ?>
    </body>
</html>