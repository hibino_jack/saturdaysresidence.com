<?php
class Config extends DB {

    var $tbConfig = "pws_config";
    var $tbThemes = "pws_themes";

    function __construct() {
        $this->Page = new Page();
        $this->Page->module_name = "Config"; // ucfirst
        $this->module_name = strtolower($this->Page->module_name); // strtolower
    }

    ///Additional Functions
    function duplicate($lang_id = NULL, $id = NULL) {
        if(empty($lang_id))
            $lang_id = "en";

        if($lang_id)
            $ct[] = "$this->tbConfig.lang_id = '$lang_id' ";
        if($id)
            $ct[] = "$this->tbConfig.id = '$id' ";

        if(is_array($ct))
        {
            $condition = implode("AND ", $ct);
            $condition = "WHERE ".$condition;
        }

        $field = "*";
        $sql = "SELECT $field FROM $this->tbConfig ";
        $sql.= $condition;
        return $data = DB::query($sql, "count");
    }

    function select($lang_id = NULL, $id = NULL, $status = NULL, $query_type = NULL) {
        global $oLanguages;
        //PRE($oLanguages);

        if(!isset($query_type))
            $query_type = "fetchAssoc";

        if(!isset($lang_id))
            $lang_id = $oLanguages->id;

        $sql = "SELECT * FROM `$this->tbConfig`";

        if($lang_id)
            $ct[] = "`$this->tbConfig`.`lang_id` = '$lang_id' ";
        if($id)
            $ct[] = "$this->tbConfig.`id` = '$id' ";
        if($status)
            $ct[] = "`$this->tbConfig`.`status` = '$status' ";
        if(is_array($ct))
            $sql = $sql." WHERE ".implode(" AND ", $ct);

        $data = DB::query($sql, $query_type);
        return $data;
    }

    function insert($post_vars) {
        global $oLanguages;
        if(!isset($lang_id))
            $lang_id = $oLanguages->id;

        if($post_vars)
            extract($post_vars);

        $name = DB::escape($name);
        $address = DB::escape($address);
        $telephone = DB::escape($telephone);
        $contact_name = DB::escape($contact_name);
        $contact_email = DB::escape($contact_email);

        $meta_title = DB::escape($meta_title);
        $meta_keywords = DB::escape($meta_keywords);
        $meta_description = DB::escape($meta_description);

        $link_facebook = DB::escape($link_facebook);
        $link_youtube = DB::escape($link_youtube);
        $link_skype = DB::escape($link_skype);
        $link_twitter = DB::escape($link_twitter);
        $link_instagram = DB::escape($link_instagram);
        $link_linkedin = DB::escape($link_linkedin);
        $link_pinterest = DB::escape($link_pinterest);
        $link_tripadvisor = DB::escape($link_tripadvisor);
        $link_googleplus = DB::escape($link_googleplus);
        $link_line = DB::escape($link_line);
        //$googlemap_embed = DB::escape($googlemap_embed);
        $googlemap_embed = addslashes($googlemap_embed);

        //INSERT INTO `pws_config` (`id`, `lang_id`, `name`, `email`, `address`, `telephone`, `contact_name`, `contact_email`,
        //`meta_title`, `meta_keywords`, `meta_description`, `link_facebook`, `link_youtube`, `link_skype`, `link_twitter`,
        //`link_instagram`, `link_linkedin`, `link_pinterest`, `link_tripadvisor`, `link_googleplus`, `link_line`,
        //`facebook_embed`, `googlemap_embed`, `file_map`, `file_logo`, `theme_id`, `is_active`)

        $field = "`id`, `lang_id`, `name`, `email`, `address`, `telephone`, `contact_name`, `contact_email`, `meta_title`, `meta_keywords`, `meta_description`, `link_facebook`, `link_youtube`, `link_skype`, `link_twitter`, `link_instagram`, `link_linkedin`, `link_pinterest`, `link_tripadvisor`, `link_googleplus`, `link_line`, `facebook_embed`, `googlemap_embed`, `file_map`, `file_logo`, `theme_id`, `is_active`";
        $value = "'$itemid', '$lang_id', '$name', '$email', '$address', '$telephone', '$contact_name', '$contact_email', '$meta_title', '$meta_keywords', '$meta_description', '$link_facebook', '$link_youtube', '$link_skype', '$link_twitter', '$link_instagram', '$link_linkedin', '$link_pinterest', '$link_tripadvisor', '$link_googleplus', '$link_line', '$facebook_embed', '$googlemap_embed', '$file_map', '$file_logo', '$theme_id', '$is_active' ";

        $sql = "INSERT INTO $this->tbConfig ($field) VALUES ($value)";

        $id = DB::query($sql, "lastInsertId");
        if($id) {
            //Loop for insert all languages
            global $languages;
            $data_languages = $languages->select(NULL, NULL, "Y");
            //PRE($data_languages);
            if($data_languages) {
                foreach($data_languages as $lang_item):
                    $duplicate_check = $this->duplicate($lang_item['id'], $id);
                    //PRE($duplicate_check);

                    //INSERT INTO `pws_config` (`id`, `lang_id`, `name`, `email`, `address`, `telephone`, `contact_name`, `contact_email`,
                    //`meta_title`, `meta_keywords`, `meta_description`, `link_facebook`, `link_youtube`, `link_skype`, `link_twitter`,
                    //`link_instagram`, `link_linkedin`, `link_pinterest`, `link_tripadvisor`, `link_googleplus`, `link_line`,
                    //`facebook_embed`, `googlemap_embed`, `file_map`, `file_logo`, `theme_id`, `is_active`)
                    if($duplicate_check){
                        $post_vars = array(
                            "id" => $id,
                            "lang_id" => $lang_item['id'],
                            "name" => $name,
                            "email" => $email,
                            "address" => $address,
                            "telephone" => $telephone,
                            "contact_name" => $contact_name,
                            "contact_email" => $contact_email,
                            "meta_title" => $meta_title,
                            "meta_keywords" => $meta_keywords,
                            "meta_description" => $meta_description,
                            "link_facebook" => $link_facebook,
                            "link_youtube" => $link_youtube,
                            "link_skype" => $link_skype,
                            "link_twitter" => $link_twitter,
                            "link_instagram" => $link_instagram,
                            "link_linkedin" => $link_linkedin,
                            "link_pinterest" => $link_pinterest,
                            "link_tripadvisor" => $link_tripadvisor,
                            "link_googleplus" => $link_googleplus,
                            "link_line" => $link_line,
                            "facebook_embed" => $facebook_embed,
                            "googlemap_embed" => $googlemap_embed,
                            "file_map" => $file_map,
                            "file_logo" => $file_logo,
                            "theme_id" => $theme_id,
                            "is_active" => $is_active
                        );
                        $this->update($post_vars);
                    }else{
                        $value = "'$itemid', '$lang_item[id]', '$name', '$email', '$address', '$telephone', '$contact_name', '$contact_email', '$meta_title', '$meta_keywords', '$meta_description', '$link_facebook', '$link_youtube', '$link_skype', '$link_twitter', '$link_instagram', '$link_linkedin', '$link_pinterest', '$link_tripadvisor', '$link_googleplus', '$link_line', '$facebook_embed', '$googlemap_embed', '$file_map', '$file_logo', '$theme_id', '$is_active' ";
                        $sql = "INSERT INTO $this->tbConfig ($field) VALUES ($value)";
                        $processok = DB::query($sql, "lastInsertId");
                    }
                endforeach;
                unset($data_languages);
            }

            //Map Photos Upload
            if($_FILES['file_map']['name']){
                // images upload
                $file_map = $_FILES['file_map'];
                if(count($file_map) > 0) {
                    $file_map_name = uploadFile($this->module_name, $id, $file_map, "photo", "location_map_photo.jpg");
                    if($file_map_name)
                        $this->update_file_map($id, $file_map_name);
                }
                unset($file_map, $file_count);
            }

            //Logo Upload
            if($_FILES['file_logo']['name']){
                // images upload
                $file_logo = $_FILES['file_logo'];
                if(count($file_logo) > 0) {
                    $file_logo_name = uploadFile($this->module_name, $id, $file_logo, "photo", "logo.png");
                    if($file_logo_name)
                        $this->update_file_logo($id, $file_logo_name);
                }
                unset($file_logo, $file_count);
            }

            return $id;
        } else {
            return false;
        }
    }

    function update($post_vars) {
        global $oLanguages;
        if(!isset($lang_id))
            $lang_id = $oLanguages->id;

        if($post_vars)
            extract($post_vars);

        $name = DB::escape($name);
        $email = DB::escape($email);
        $address = DB::escape($address);
        $telephone = DB::escape($telephone);

        $contact_name = DB::escape($contact_name);
        $contact_email = DB::escape($contact_email);

        $meta_title = DB::escape($meta_title);
        $meta_keywords = DB::escape($meta_keywords);
        $meta_description = DB::escape($meta_description);
        $facebook_embed = DB::escape($facebook_embed);
        //$googlemap_embed = DB::escape($googlemap_embed);
		$googlemap_embed = addslashes($googlemap_embed);
		
        $link_youtube = DB::escape($link_youtube);
        $link_facebook = DB::escape($link_facebook);
        $link_skype = DB::escape($link_skype);
        $link_twitter = DB::escape($link_twitter);
        $link_instagram = DB::escape($link_instagram);
        $link_linkedin = DB::escape($link_linkedin);
        $link_pinterest = DB::escape($link_pinterest);
        $link_tripadvisor = DB::escape($link_tripadvisor);
        $link_googleplus = DB::escape($link_googleplus);
        $link_line = DB::escape($link_line);

        //INSERT INTO `pws_config` (`id`, `lang_id`, `name`, `email`, `address`, `telephone`, `contact_name`, `contact_email`,
        // `meta_title`, `meta_description`, `meta_keywords`, `link_facebook`, `link_youtube`, `link_skype`, `link_twitter`,
        //`link_instagram`, `link_linkedin`, `link_pinterest`, `link_tripadvisor`, `link_googleplus`, `link_line`, `facebook_embed`,
        //`googlemap_embed`, `file_map`, `file_logo`, `theme_id`, `is_active`) VALUES

        $f = " name = '$name' ";

        if($email)
            $f.= ", email = '$email' ";
        if($address)
            $f.= ", address = '$address' ";
        if($telephone)
            $f.= ", telephone = '$telephone' ";
        if($contact_name)
            $f.= ", contact_name = '$contact_name' ";
        if($contact_email)
            $f.= ", contact_email = '$contact_email' ";
        if($meta_title)
            $f.= ", meta_title = '$meta_title' ";
        if($meta_keywords)
            $f.= ", meta_keywords = '$meta_keywords' ";
        if($meta_description)
            $f.= ", meta_description = '$meta_description' ";
        if($link_facebook)
            $f.= ", link_facebook = '$link_facebook' ";
        if($link_youtube)
            $f.= ", link_youtube = '$link_youtube' ";
        if($link_skype)
            $f.= ", link_skype = '$link_skype' ";
        if($link_twitter)
            $f.= ", link_twitter = '$link_twitter' ";
        if($link_instagram)
            $f.= ", link_instagram = '$link_instagram' ";
        if($link_linkedin)
            $f.= ", link_linkedin = '$link_linkedin' ";
        if($link_pinterest)
            $f.= ", link_pinterest = '$link_pinterest' ";
        if($link_tripadvisor)
            $f.= ", link_tripadvisor = '$link_tripadvisor' ";
        if($link_googleplus)
            $f.= ", link_googleplus = '$link_googleplus' ";
        if($link_line)
            $f.= ", link_line = '$link_line' ";

        if($facebook_embed)
            $f.= ", facebook_embed = '$facebook_embed' ";
        if($googlemap_embed)
            $f.= ", googlemap_embed = '$googlemap_embed' ";
        //if($file_map)
        //    $f.= ", file_map = '$file_map' ";
        //if($file_logo)
        //    $f.= ", file_logo = '$file_logo' ";
        if($theme_id)
            $f.= ", theme_id = '$theme_id' ";
        if($is_active)
            $f.= ", is_active = '$is_active' ";

        $sql = "UPDATE $this->tbConfig SET $f WHERE $this->tbConfig.id = '$itemid' AND $this->tbConfig.lang_id = '$lang_id'";
        //return $data = DB::query($sql);
        $data = DB::query($sql);
        if($data){
            //Loop for insert all languages
            global $languages;
            $data_languages = $languages->select(NULL, NULL, "Y");
            //PRE($data_languages);
            if($data_languages) {
                foreach($data_languages as $lang_item):
                    $duplicate_check = $this->duplicate($lang_item['id'], $itemid);
                    //PRE($duplicate_check);
                    if($duplicate_check){
                        //do nothing
                    }else{
                        $value = "'$itemid', '$lang_item[id]', '$name', '$email', '$address', '$telephone', '$contact_name', '$contact_email', '$meta_title', '$meta_keywords', '$meta_description', '$link_facebook', '$link_youtube', '$link_skype', '$link_twitter', '$link_instagram', '$link_linkedin', '$link_pinterest', '$link_tripadvisor', '$link_googleplus', '$link_line', '$facebook_embed', '$googlemap_embed', '$file_map', '$file_logo', '$theme_id', '$is_active' ";
                        $sql = "INSERT INTO $this->tbConfig ($field) VALUES ($value)";
                        $processok = DB::query($sql, "lastInsertId");
                    }
                endforeach;
                unset($data_languages);
            }

            //Map Photos Upload
            if($_FILES['file_map']['name']){
                // images upload
                $file_map = $_FILES['file_map'];
                //print_r($file_map);
                if(count($file_map) > 0) {
                    $file_map_name = uploadFile($this->module_name, $itemid, $file_map, "photo", "location_map_photo.jpg");
                    if($file_map_name)
                        $this->update_file_map($itemid, $file_map_name);
                }
                unset($file_map, $file_count);
            }

            //Logo Upload
            if($_FILES['file_logo']['name']){
                // images upload
                $file_logo = $_FILES['file_logo'];
                //print_r($file_logo);
                if(count($file_logo) > 0) {
                    $file_logo_name = uploadFile($this->module_name, $itemid, $file_logo, "photo", "logo.png");
                    if($file_logo_name)
                        $this->update_file_logo($itemid, $file_logo_name);
                }
                unset($file_logo, $file_count);
            }

            return $data;
        }
    }

    function update_file_map($id, $file_map) {
        $field = "file_map = '$file_map' ";
        $sql = "UPDATE $this->tbConfig SET $field WHERE id = '$id'";
        return $data = DB::query($sql);
    }

    function update_file_logo($id, $file_logo) {
        $field = "file_logo = '$file_logo' ";
        $sql = "UPDATE $this->tbConfig SET $field WHERE id = '$id'";
        return $data = DB::query($sql);
    }

    function update_status($id, $is_active) {
        $field = "is_active = '$is_active' ";
        $sql = "UPDATE $this->tbConfig SET $field WHERE id = '$id'";
        return $data = DB::query($sql);
    }

    function delete($id) {
        if($id != 1) {
            $sql = "DELETE FROM $this->tbConfig WHERE id = $id ";
            $data = DB::query($sql);
            if($data) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

}
?>