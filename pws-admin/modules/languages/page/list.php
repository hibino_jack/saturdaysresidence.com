<?php
$data = $$module_class->select();
$datatotal = count($data);
//PRE($data);
?>


<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default panel-border">
            <div class="panel-heading">
                <?php echo $languages->dropdown_languages($module, $action, $path, $itemid, $task); ?>
                <h4 class="header-title"><b><?php echo $module_class." ".$action;?></b></h4>
                <p class="text-muted font-13 m-b-10">Total <?php echo $datatotal; ?> record(s) on the list.</p>
            </div>
            <div class="panel-body">
                <table id="datatable-buttons" class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Default</th>
                        <th>Icon</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if($data) {
                        $j = 1;
                        for($i=0; $i < $datatotal; $i++) { ?>
                            <tr>
                                <td><?php echo $j; ?></td>
                                <td><strong><?php echo $data[$i]["name"]." [".$data[$i]["id"]."]"; ?></strong></td>
                                <td><?php echo $data[$i]['default']; ?></td>
                                <td><?php echo $data[$i]['icon']; ?></td>
                                <td><span class="label label-table label-<?php echo ($data[$i]['is_active'] == "Y") ? "success" : "inverse"; ?>"><?php echo ($data[$i]['is_active'] == "Y") ? "Active" : "Disabled"; ?></span></td>
                                <td class="actions">
                                    <button class="btn btn-default btn-xs btn-icon fa fa-pencil btn-attr-item" data-lang_id="<?php echo $lang_id;?>" data-module="<?php echo $module;?>" data-action="update" data-id="<?php echo $data[$i]['id']; ?>" data-task="<?php echo $task;?>" data-path="<?php echo $path;?>"></button>
                                    <button class="btn btn-danger btn-xs btn-icon fa fa-trash-o delete-item" data-lang_id="<?php echo $lang_id;?>" data-module="<?php echo $module;?>" data-action="delete" data-id="<?php echo $data[$i]['id']; ?>" data-task="<?php echo $task;?>" data-path="<?php echo $path;?>"></button>
                                </td>
                            </tr>
                            <?php  $j++;
                        } //for
                    }else{ ?>
                        <tr>
                            <td colspan="5">Sorry, Record not found</td>
                        </tr>
                    <?php }?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>