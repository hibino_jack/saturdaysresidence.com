<?php
$data = $$module_class->select();
$datatotal = count($data);
//PRE($data);
?>

<!--- Show the photo gallery list --->
<div class="row">
    <div class="col-lg-12">
        <div class="portlet"><!-- /primary heading -->
            <div class="portlet-heading bg-pink">
                <h3 class="portlet-title text-uppercase">
                    Photos List
                </h3>
                <div class="portlet-widgets">
                    <a href="javascript:void(0);" data-toggle="reload"><i class="ion-refresh"></i></a>
                    <span class="divider"></span>
                    <a data-toggle="collapse" data-parent="#accordion2" href="#portlet2"><i class="ion-minus-round"></i></a>
                    <span class="divider"></span>
                    <a href="javascript:void(0);" data-toggle="remove"><i class="ion-close-round"></i></a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div id="portlet2" class="panel-collapse collapse in">
                <div class="portlet-body">
                    <!-- SECTION FILTER
                    ================================================== -->
                    <!--
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 ">
                            <div class="portfolioFilter">
                                <a href="#" data-filter="*" class="current">All</a>
                                <a href="#" data-filter=".webdesign">Web Design</a>
                                <a href="#" data-filter=".graphicdesign">Graphic Design</a>
                                <a href="#" data-filter=".illustrator">Illustrator</a>
                                <a href="#" data-filter=".photography">Photography</a>
                            </div>
                        </div>
                    </div>
                    -->
                    <?php if($data){ //PRE($data); ?>
                        <div class="pull-right m-r-10">
                            <button type="button" class="btn btn-danger delete-all-items-img"
                                    data-module="<?php echo $module;?>"
                                    data-action="<?php echo $action;?>"
                                    data-task="delete_all_gallery_item"
                                    data-lang_id="<?php echo $lang_id;?>"
                                    data-path="<?php echo $path;?>"
                                    data-itemid="<?php echo $itemid;?>">
                                <i class="glyphicon glyphicon-trash"></i>
                                <span>Delete all photos</span>
                            </button>
                        </div>
                        <div class="clearfix"></div>
                        <div class="row port">
                            <div class="portfolioContainer">
                                <?php
                                $no = 1;
                                for($i=0; $i < count($data); $i++) { ?>
                                    <div class="col-sm-6 col-lg-3 col-md-4 category<?php echo $data[$i]['gallery_categories_id']; ?> content_<?php echo $data[$i]['content_id']; ?>">
                                        <div class="gal-detail thumb photo<?php echo $data[$i]['id']; ?>">
                                            <?php
                                            $pic = BASE_UPLOAD.$module."/".$itemid."/".$data[$i]['filename'];
                                            $pic_realpath = PATH_UPLOAD_ROOT.$module."/".$itemid."/".$data[$i]['filename'];
                                            if(is_file($pic_realpath)) { ?>
                                                <a href="<?php echo $pic; ?>" class="image-popup" title="<?php echo $data[$i]['title']; ?>">
                                                    <img src="<?php echo $pic; ?>" class="thumb-img" alt="work-thumbnail">
                                                </a>
                                            <?php }else{ ?>
                                                <img src="<?php echo BASE_IMAGES; ?>nophoto.jpg" alt="<?php echo $data[$i]['title']; ?>" class="thumb-img">
                                            <?php } ?>
                                            <h4><?php echo $data[$i]['title']." - No.".$no; ?></h4>
                                            <div class="btn-action">
                                                <a href="javascript:void(0);" class="btn btn-success btn-sm btn-attr-item" data-lang_id="<?php echo $lang_id;?>" data-module="<?php echo $module;?>" data-action="update" data-id="<?php echo $data[$i]['id']; ?>" data-task="<?php echo $task;?>" data-path="<?php echo $path;?>"><i class="md md-mode-edit"></i></a>
                                                <a href="javascript:void(0);" class="btn btn-danger btn-sm delete-item-img"
                                                   data-module="<?php echo $module;?>"
                                                   data-action="<?php echo $action;?>"
                                                   data-task="delete_gallery_item"
                                                   data-lang_id="<?php echo $lang_id;?>"
                                                   data-path="<?php echo $path;?>"
                                                   data-filename="<?php echo $data[$i]['filename'];?>"
                                                   data-content_id="<?php echo $data[$i]['content_id'];?>"
                                                   data-itemid="<?php echo $data[$i]['id'];?>"><i class="md md-close"></i></a>
                                            </div>
                                        </div>
                                    </div>

                                    <?php $no++; } ?>
                            </div>
                        </div> <!-- End row -->
                    <?php } else {
                        echo "<p>No Photo.</p>";
                    }
                    ?>



                </div>
            </div>
        </div>
    </div> <!-- end col -->


</div>

<!-- end row -->
