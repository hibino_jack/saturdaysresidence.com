<?php
class Chats extends DB
{
    var $tbUsers = "pws_users";
    var $tbChats = "pws_chats";
    var $tbChatsRooms = "pws_chats_rooms";

    function __construct()
    {
        $this->Page = new Page();
        $this->Page->module_name = "Chats"; // ucfirst
        $this->module_name = strtolower($this->Page->module_name); // strtolower
    }

    function select($room_id = NULL, $sender_id = NULL, $receiver_id = NULL, $limit = 6) {
        if (!isset($query_type))
            $query_type = "fetchAssoc";

        $cond = "";

        if ($room_id)
            $ct[] = "`$this->tbChats`.`room_id` = '$room_id' ";
//        if ($sender_id)
//            $ct[] = "`$this->tbChats`.`sender_id` = '$sender_id' ";
//        if ($receiver_id)
//            $ct[] = "`$this->tbChats`.`receiver_id` = '$receiver_id' ";

        $ct[] = "(`$this->tbChats`.`sender_id` = '$sender_id' AND `$this->tbChats`.`receiver_id` = '$receiver_id') OR (`$this->tbChats`.`sender_id` = '$receiver_id' AND `$this->tbChats`.`receiver_id` = '$sender_id')";


        $ct[] = "`$this->tbChats`.`is_active` = 'Y' ";
        //$ct[] = " ORDER BY `$this->tbChats`.`id` DESC LIMIT ".$limit;

        if (is_array($ct))
            $cond = " WHERE " . implode(" AND ", $ct);

        $s = "`sub`.*, `$this->tbUsers`.`first_name`, `$this->tbUsers`.`last_name`, `$this->tbUsers`.`profile_photo`";
        $sql = "SELECT $s FROM ( ";
        $sql .= "SELECT * FROM `$this->tbChats` " . $cond;
        $sql.= " ORDER BY `$this->tbChats`.`id` DESC LIMIT ".$limit;
        $sql .= ") AS sub ";
        $sql .= "LEFT JOIN `$this->tbUsers` ON `sub`.`sender_id` = `$this->tbUsers`.`id` ";
        $sql .= "ORDER BY `sub`.`id` ASC ";

        //echo $sql;

        $data = DB::query($sql, $query_type);
        if ($return_type == "json") {
            return json_encode($data);
        } else {
            return $data;
        }
    }

/*
    function select($id = NULL, $room_id = NULL, $sender_id = NULL, $receiver_id = NULL, $is_active = NULL, $query_type = NULL, $return_type = NULL, $order_by = NULL, $order_type = NULL, $limit = 100)
    {
        if (!isset($query_type))
            $query_type = "fetchAssoc";

        $cond = "";

        if ($id)
            $ct[] = "$this->tbChats.`id` = '$id' ";
        if ($room_id)
            $ct[] = "`$this->tbChats`.`room_id` = '$room_id' ";
        if ($sender_id)
            $ct[] = "`$this->tbChats`.`sender_id` = '$sender_id' ";
        if ($receiver_id)
            $ct[] = "`$this->tbChats`.`receiver_id` = '$receiver_id' ";
        if ($is_active)
            $ct[] = "`$this->tbChats`.`is_active` = '$is_active' ";
        if (is_array($ct))
            $cond = " WHERE " . implode(" AND ", $ct);

        if ($order_by) {
            $order_type = ($order_type == "") ? "DESC" : $order_type;
            $cond.= " ORDER BY `$this->tbChats`.`" . $order_by . "` " . $order_type . " LIMIT " . $limit;
        } else {
            $cond.= " ORDER BY `$this->tbChats`.`id` DESC LIMIT 8 ";
        }

        $s = "`sub`.*, `$this->tbUsers`.`first_name`, `$this->tbUsers`.`last_name`, `$this->tbUsers`.`profile_photo`";
        $sql = "SELECT $s FROM ( ";
        $sql .= "SELECT * FROM `$this->tbChats` " . $cond;
        //$sql.= " ORDER BY `$this->tbChats`.`id` DESC LIMIT 100 ";
        $sql .= ") AS sub ";
        $sql .= "LEFT JOIN `$this->tbUsers` ON `sub`.`sender_id` = `$this->tbUsers`.`id` ";
        $sql .= "ORDER BY `sub`.`id` ASC ";

        //echo $sql;

        $data = DB::query($sql, $query_type);
        if ($return_type == "json") {
            return json_encode($data);
        } else {
            return $data;
        }
    }
*/

    function select_dashboard()
    {
        $s = "`sub`.*, `$this->tbUsers`.`first_name`, `$this->tbUsers`.`last_name`, `$this->tbUsers`.`profile_photo`";
        $sql = "SELECT $s FROM ( ";
        $sql .= "SELECT * FROM `$this->tbChats` WHERE `pws_chats`.`receiver_id` ='0' ";
        $sql .= " ORDER BY `$this->tbChats`.`id` DESC LIMIT 10 ";
        $sql .= ") AS sub ";
        $sql .= "LEFT JOIN `$this->tbUsers` ON `sub`.`sender_id` = `$this->tbUsers`.`id` ";
        $sql .= "ORDER BY `sub`.`id` ASC ";
        //echo $sql;

// SELECT `sub`.*, `pws_users`.`first_name`, `pws_users`.`last_name`, `pws_users`.`profile_photo` FROM
//( SELECT * FROM `pws_chats` ORDER BY `pws_chats`.`id` DESC LIMIT 10 ) AS sub
//LEFT JOIN `pws_users` ON `sub`.`sender_id` = `pws_users`.`id` ORDER BY `sub`.`id` ASC

        $data = DB::query($sql, "fetchAssoc");
        if ($data) {
            return $data;
        } else {
            return false;
        }
    }

    function insert($post_vars)
    {
        if ($post_vars)
            extract($post_vars);

        $message = DB::escape($message);

        $field = "`id`, `room_id`, `sender_id`, `receiver_id`, `message`, `sent_date`, `seen_date`, `filename`, `is_active`";
        $value = "'$id', '$room_id', '$sender_id', '$receiver_id', '$message', NOW(), '$seen_date', '$filename', 'Y'";
        $sql = "INSERT INTO $this->tbChats ($field) VALUES ($value)";

        $id = DB::query($sql, "lastInsertId");
        if ($id) {
            return $id;
        }
    }

//INSERT INTO `pws_chats` (`id`, `room_id`, `sender_id`, `receiver_id`, `message`, `sent_date`, `seen_date`, `filename`, `is_active`) VALUES
    function update($post_vars)
    {

        if ($post_vars)
            extract($post_vars);

        $message = DB::escape($message);

        if ($room_id)
            $ct[] = "$this->tbChats.room_id = '$room_id' ";
        if ($sender_id)
            $ct[] = "$this->tbChats.sender_id = '$sender_id' ";
        if ($receiver_id)
            $ct[] = "$this->tbChats.receiver_id = '$receiver_id' ";
        if ($message)
            $ct[] = "$this->tbChats.message = '$message' ";
        if ($sent_date)
            $ct[] = "$this->tbChats.sent_date = '$sent_date' ";
        if ($seen_date)
            $ct[] = "$this->tbChats.seen_date = '$seen_date' ";
        if ($filename)
            $ct[] = "$this->tbChats.filename = '$filename' ";
        if ($is_active)
            $ct[] = "$this->tbChats.is_active = '$is_active' ";

        if (is_array($ct))
            $field = implode(", ", $ct);

        $sql = "UPDATE $this->tbChats SET $field WHERE id = '$itemid'";
        $data = DB::query($sql);
        if ($data) {
            return $data;
        } else {
            return false;
        }
    }

    function update_status($id, $status)
    {
        $field = "status = '$status' ";
        $sql = "UPDATE $this->tbChats SET $field WHERE id = '$id'";
        return $data = DB::query($sql, "fetchAssoc");
    }

    function delete($post_vars)
    {
        if ($post_vars)
            extract($post_vars);

        if ($itemid != "1") {
            $sql = "DELETE FROM `$this->tbChats` WHERE `$this->tbChats`.`id` = '$itemid' ";
            $data = DB::query($sql);
            if ($data) {
                //delete
                removeFile($itemid, $this->module_name, "profile_photo.png");
            }
        } else {
            return false;
        }
    }

    function load_data($post_vars) {
        global $oUser;
        if ($post_vars)
            extract($post_vars);

        $item_per_page = 10;
        $page_number = filter_var($page, FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH);
        $position = (($page_number-1) * $item_per_page);
        $limit = $position.",".$item_per_page;

        //sender
        $data = $this->select($room_id, $sender_id, $receiver_id, $limit);

        //$data = $this->select($id, $room_id, $sender_id, $receiver_id, "Y", "fetchAssoc", NULL, "id", "DESC", $limit);
        if($data){
            foreach ($data as $key => $value) {
                //print_r($value);
                //PRE($data_chats[$i]);
                $pic = BASE_UPLOAD."users/".$value['sender_id']."/".$value['profile_photo'];
                $pic_realpath = PATH_UPLOAD_ROOT."users/".$value['sender_id']."/".$value['profile_photo'];
                if(is_file($pic_realpath)) {
                    $img_src = '<img src="'.$pic.'" alt="'.$value['first_name'].' '.$value['last_name'].'">';
                }else{
                    $img_src = '<img src="'.BASE_IMAGES.'nophoto.jpg" alt="'.$value['first_name'].' '.$value['last_name'].'">';
                }
                if($oUser->id == $value['sender_id']) { $odd = ""; } else { $odd = "odd"; }
                echo '
                        <li class="clearfix '.$odd.'">
                            <div class="chat-avatar">
                                '.$img_src.'
                            </div>
                            <div class="conversation-text">
                                <div class="ctext-wrap">
                                    <i>'.$value['first_name'].' '.$value['last_name'].'</i>
                                    <p>'.$value['message'].'</p>
                                </div>
                                <br /><i>'.date("jS F, Y H:i:s", strtotime($value['sent_date'])).'</i>
                            </div>
                        </li>
                        ';
            }
        } else {
            return false;
        }
    }
}