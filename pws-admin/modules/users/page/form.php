<?php
if($action == "update")    {
    if($$module_class->itemid) {
        $data = $$module_class->select($$module_class->itemid);
        $itemid = $$module_class->itemid;
        if($data){
            extract($data[0]);
            //PRE($data);
        }
    }
}
?>
<div class="row">
    <div class="col-md-12">
        <?php echo $languages->dropdown_languages($module, $action, $path, $itemid, $task); ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?php echo $module_class." ".$action;?></h3>
            </div>
            <div class="panel-body">
                <form name="create-form" id="create-form" class="form-horizontal" role="form" data-parsley-validate novalidate enctype="multipart/form-data">
                    <div class="alert alert-danger alert-dismissable hidden">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <h4>Oh snap!</h4>
                        <p>This form seems to be invalid :(</p>
                    </div>
                    <div class="alert alert-success alert-dismissable hidden">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <h4>Yay!</h4>
                        <p>Everything seems to be ok., Re-submit your form then. :)</p>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-md-2 control-label">Group</label>
                                <div class="col-md-10">
                                    <?php
                                    $group_id_data = array("1" => "Administrator", "2" => "Webmaster / Editor", "3" => "Staff", "4" => "Guest");
                                    echo DROPDOWNMANUAL("group_id", $group_id, $group_id_data, NULL, NULL, "id='group_id' required");
                                    ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Username</label>
                                <div class="col-md-10">
                                    <?php
                                    if($action == "update") {
                                        echo '<input type="text" class="form-control" name="username" parsley-trigger="change" required placeholder="Enter Username" value="'.$username.'" readonly>';
                                    } else {
                                        echo '
                                            <input class="form-control" name="username" type="text" minlength="4"
                                           data-parsley-remote data-parsley-remote-options=\'{ "type": "POST", "data": { "token": "value" } }\'
                                           data-parsley-remote-validator="CheckUsername"
                                           data-parsley-errors-container="#errorUsername"
                                           data-parsley-remote-message="Your username is already taken."
                                           id="username" value="" required />
                                    <span id="errorUsername"></span>';
                                    }
                                    ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Password</label>
                                <div class="col-md-10">
                                    <?php
                                    if($action == "update") {
                                        $passwd_txt = '
                                    <input type="hidden" name="old_password" value="'.$password.'">
                                    <input type="text" class="form-control" name="password" placeholder="Type your new Password" value="">
                                    <span class="help-block"><small>Leave it blank if you does not need to change the password.</small></span>
                                    ';
                                    } else {
                                        $passwd_txt = '
                                    <input type="text" class="form-control" name="password" parsley-trigger="change" required placeholder="Enter Password" value="'.$password.'">
                                    ';
                                    }
                                    echo $passwd_txt;
                                    ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Email</label>
                                <div class="col-md-10">
                                    <?php
                                    if($action == "update") {
                                        echo '<input type="text" class="form-control" name="email" parsley-trigger="change" required placeholder="Enter Email" value="'.$email.'" readonly>';
                                    } else {
                                        echo '
                                    <input class="form-control" type="email" name="email"
                                           data-parsley-remote data-parsley-remote-options=\'{ "type": "POST", "data": { "token": "value" } }\'
                                           data-parsley-remote-validator="emailvalidation"
                                           data-parsley-errors-container="#errorEmail"
                                           data-parsley-remote-message="Your email is already taken."
                                           id="email1" value="" required />
                                    <span id="errorEmail"></span>';
                                    }
                                    ?>



                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">First Name</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="first_name" parsley-trigger="change" required placeholder="Enter First Name" value="<?php echo $first_name;?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Last Name</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="last_name" parsley-trigger="change" required placeholder="Enter Last name" value="<?php echo $last_name;?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Photo</label>
                                <div class="col-md-10">
                                    <input type="file" name="profile_photo" id="profile_photo" class="filestyle" data-buttonbefore="true"><br />
                                    <?php
                                    $pic = BASE_UPLOAD.$module."/".$id."/".$profile_photo;
                                    $pic_realpath = PATH_UPLOAD_ROOT.$module."/".$id."/".$profile_photo;
                                    if(is_file($pic_realpath)) {
                                        ?>
                                        <a class="image-popup" href="<?php echo $pic; ?>" title="Profile Photo">
                                            <img src="<?php echo PATH_ROOT; ?>timthumb.php?src=<?php echo $pic_realpath; ?>&w=250&h=200" width="250" height="200" class="img-responsive img-thumbnail">
                                        </a>
                                    <?php }else{ ?>
                                        <img src="<?php echo BASE_IMAGES; ?>nophoto.jpg" width="250" height="200" class="img-responsive img-thumbnail"/>
                                    <?php } ?>

                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Status</label>
                                <div class="col-md-10">
                                    <?php
                                    $status_data = array("Active" => "Active", "Inactive" => "Inactive", "Forget" => "Forget", "Pending" => "Pending");
                                    echo DROPDOWNMANUAL("status", $status, $status_data, NULL, NULL, "id='status' required");
                                    ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Active</label>
                                <div class="col-sm-10">
                                    <?php
                                    $is_active_data = array("Y" => "Enable", "N" => "Disabled");
                                    echo RADIOITEMMANUAL("is_active", $is_active, $is_active_data);
                                    ?>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group pull-right">
                                <input type="hidden" id="module" name="module" value="<?php echo $module; ?>">
                                <input type="hidden" id="action" name="action" value="<?php echo $action; ?>">
                                <input type="hidden" id="task" name="task" value="<?php echo $task; ?>">
                                <input type="hidden" id="path" name="path" value="<?php echo $path; ?>">
                                <input type="hidden" id="lang_id" name="lang_id" value="<?php echo $lang_id; ?>">
                                <input type="hidden" id="itemid" name="itemid" value="<?php echo $itemid; ?>">

                                <button type="submit" class="btn btn-default waves-effect waves-light btn-lg">Submit</button>
                                <button type="reset" class="btn btn-white waves-effect waves-light btn-lg">Cancel</button>
                            </div>
                        </div>
                    </div>
                </form>

            </div>
        </div>

    </div>
</div>