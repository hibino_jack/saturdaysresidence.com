<?php


//PRE($data);
$data_content[0]['description'] = str_ireplace(array("\r","\n",'\r','\n'),'', $data_content[0]['description']);
//$image_header = "http://placehold.it/1450x500&text=".$data_content[0]['title'];
if($data_content[0]['header_photo'] != "")
{
	$pic = BASE_UPLOAD."contents/".$data_content[0]['id']."/".$data_content[0]['header_photo'];
	$pic_realpath = PATH_UPLOAD_ROOT."contents/".$data_content[0]['id']."/".$data_content[0]['header_photo'];
	if(is_file($pic_realpath)) {
		$image_header = PATH_ROOT."timthumb.php?src=".$pic."&w=1450&h=500";
	}
}

if($data_content[0]['id']) {
    /*
    *  Get Media items
    */
    $data_gallery = $contents->Media->select(NULL, $data_content[0]['id']);
}

?>
<!--header-->
<header> 
    <div class="page-heading">
        <div class="container">
            <div class="row">
            	<div class="col-md-offset-3 col-md-6 text-center">
            	<h1><?php echo $data_content[0]['title']; ?></h1>
                <?php if(!empty($data_content[0]['subtitle'])){ ?>           
                    <h3 style="font-style:italic"><?php echo $data_content[0]['subtitle']; ?></h3>
				<?php }
				if(!empty($data_content[0]['short_description'])){ ?>                                
                    <p><?php echo $data_content[0]['short_description']; ?></p>
				<?php } ?>       
				</div>
            </div>
		</div>
    </div>
    <?php if($image_header){ ?>
    <!-- room header Section -->
    <div class="inner-header">
        <div class="container">
                <div class="row">
                    <div class="inner-header-fill" style="background-image:url('<?php echo $image_header; ?>');"></div>
                </div>
         </div>
    </div>
    <?php } ?>    
</header>

<section class="content-area page-gallery">
    <?php if(!empty($data_content[0]['description'])){ ?>
    <div id="innerpage_intro">
        <div class="container">
            <div class="row">
                <div class="col-lg-offset-1 col-lg-10">
                	<div class="row innerpage_intro <?php echo ($image_header) ? "":"no-margin"; ?>">
                    <?php echo $data_content[0]['description']; ?>
                    </div><!--//saturdays_intro-->
                </div><!--//col-md-6-->
         	</div>
         </div>
      </div>
    <?php } ?>

	<div class="gallery">
    	<div class="container">
    		<div class="row">
                <div class="col-xs-12">
            	<h2>Gallery</h2>
                </div>
        		<ul id="gallery-list">
                <?php if(is_array($data_gallery) && (count($data_gallery) > 0)){ 
					
					for($i = 0 ; $i < 12 ; $i++){
						if($data_gallery[$i]["filename"] != "")
						{
							$pic = BASE_UPLOAD."contents/".$data_content[0]["id"]."/".$data_gallery[$i]["filename"];
							$pic_realpath = PATH_UPLOAD_ROOT."contents/".$data_content[0]["id"]."/".$data_gallery[$i]["filename"];
							if(is_file($pic_realpath)) {
								$image_header = PATH_ROOT."timthumb.php?src=".$pic."&w=350&h=350";
							}
						}
					?>
                        <li class="col-md-3 col-sm-4 col-xs-6">
                        <a data-fancybox="gallery" href="<?php echo $pic; ?>" data-caption="Saturdays Residence"><img src="<?php echo $image_header; ?>" alt="Image" style="max-width:100%;" /></a>
                    </li>
                    <?php
					}//for
					
					/*foreach($data_gallery as $item){
					//PRE($item);
					if($item["filename"] != "")
					{
						$pic = BASE_UPLOAD."contents/".$data_content[0]["id"]."/".$item["filename"];
						$pic_realpath = PATH_UPLOAD_ROOT."contents/".$data_content[0]["id"]."/".$item["filename"];
						if(is_file($pic_realpath)) {
							$image_header = PATH_ROOT."timthumb.php?src=".$pic."&w=250&h=250";
					}
						
				?>
                    <li class="col-md-3 col-sm-4 col-xs-6">
                    <a data-fancybox="gallery" href="<?php echo $pic; ?>" data-caption="Saturdays Residence"><img src="<?php echo $image_header; ?>" alt="Image" style="max-width:100%;" /></a>
                    </li>
                <?php 
					} //if filename
					} //foreach */
				}//if
				?>                                
                </ul>
                </div>
                <?php if(count($data_gallery) > 12){?>
                <div class="row text-center">
                	<!--<a href="" class="btn btn-default" >load more photos</a>-->
                    <button id="galleryload"  class="btn btn-default" >Load More</button>
                </div>
                <?php } ?>
                
                <div class="row" style="margin-top:40px;">
                <div class="col-xs-12">
                <h2>#saturdaysresidence</h2>
                </div>
                <ul id="instafeed">
                </ul>
                </div>
                <div class="row text-center">
                    <button id="load"  class="btn btn-default" >Load More</button>
                </div>
            </div>
    	</div>
        
    </section>