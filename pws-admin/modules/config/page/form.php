<?php
if($action == "update")    {
    if($$module_class->itemid) {
        $data = $$module_class->select($lang_id, $$module_class->itemid);
        $itemid = $$module_class->itemid;
        if($data){
            extract($data[0]);
            //PRE($data);
        }
    }
}
//INSERT INTO `pws_config` (`id`, `name`, `email`, `address`, `telephone`, `contact_name`, `contact_email`, `meta_title`, `meta_description`, `meta_keywords`, `show_fbbox`, `link_facebook`, `link_skype`, `link_twitter`, `link_instagram`, `link_pinterest`, `link_tripadvisor`, `link_googleplus`, `link_line`, `googlemap_embed`, `file_map`, `file_logo`, `theme_id`, `is_active`) VALUES
?>
<div class="row">
<div class="col-md-12">
    <?php echo $languages->dropdown_languages($module, $action, $path, $itemid, $task); ?>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"><?php echo $module_class." ".$action;?></h3>
        </div>
        <div class="panel-body">
            <form name="create-form" id="create-form" class="form-horizontal" role="form" data-parsley-validate novalidate enctype="multipart/form-data">
            <ul class="nav nav-pills m-b-30">
                <li class="active"><a data-toggle="tab" aria-expanded="true" href="#tab_general">General Settings</a></li>
                <li class=""><a data-toggle="tab" aria-expanded="false" href="#tab_seo">SEO</a></li>
                <li class=""><a data-toggle="tab" aria-expanded="false" href="#tab_social">Social Link</a></li>
                <li class=""><a data-toggle="tab" aria-expanded="false" href="#tab_mail">Mail Settings</a></li>
            </ul>
            <div class="tab-content br-n pn">
                    <div class="alert alert-danger alert-dismissable hidden">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <h4>Oh snap!</h4>
                        <p>This form seems to be invalid :(</p>
                    </div>
                    <div class="alert alert-success alert-dismissable hidden">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <h4>Yay!</h4>
                        <p>Everything seems to be ok., Re-submit your form then. :)</p>
                    </div>

                <div id="tab_general" class="tab-pane active">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="form-group">
                                <label class="col-md-2 control-label">Name</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="name" parsley-trigger="change" required placeholder="Enter Name" value="<?php echo $name;?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Email</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="email" parsley-trigger="change" required placeholder="Enter Email" value="<?php echo $email;?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Address</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="address" parsley-trigger="change" required placeholder="Enter Address" value="<?php echo $address;?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Telephone</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="telephone" parsley-trigger="change" required placeholder="Enter Telephone" value="<?php echo $telephone;?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Theme</label>
                                <div class="col-md-10">
                                    <select class="selectpicker show-tick" data-style="btn-white" name="theme_id" id="theme_id">
                                        <option value="1">Default</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Logo</label>
                                <div class="col-md-10">
                                    <input type="file" name="file_logo" id="file_logo" class="filestyle" data-buttonbefore="true">

                                    <?php
                                    $pic = BASE_UPLOAD.$module."/".$id."/".$file_logo;
                                    $pic_realpath = PATH_UPLOAD_ROOT.$module."/".$id."/".$file_logo;
                                    if(is_file($pic_realpath)) {
                                        ?>
                                        <a class="image-popup" href="<?php echo $pic; ?>" title="Logo">
                                            <img src="<?php echo PATH_ROOT; ?>timthumb.php?src=<?php echo $pic; ?>&w=250&h=200" width="250" height="200">
                                        </a>
                                    <?php }else{ ?>
                                    <div class="col-sm-6 col-lg-3 col-md-4">
                                        <img src="<?php echo BASE_IMAGES; ?>nophoto.jpg" width="200" class="gal-detail thumb thumb-img"/>
                                    </div>
                                    <?php } ?>

                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Map file</label>
                                <div class="col-md-10">
                                    <input type="file" name="file_map" id="file_map" class="filestyle" data-buttonbefore="true">

                                    <?php
                                    $pic = BASE_UPLOAD.$module."/".$id."/".$file_map;
                                    $pic_realpath = PATH_UPLOAD_ROOT.$module."/".$id."/".$file_map;
                                    if(is_file($pic_realpath)) {
                                        ?>
                                        <a class="image-popup" href="<?php echo $pic; ?>" title="Map">
                                            <img src="<?php echo PATH_ROOT; ?>timthumb.php?src=<?php echo $pic; ?>&w=250&h=200" width="250" height="200">
                                        </a>
                                    <?php }else{ ?>
                                        <div class="col-sm-6 col-lg-3 col-md-4">
                                            <img src="<?php echo BASE_IMAGES; ?>nophoto.jpg" width="200" class="gal-detail thumb thumb-img"/>
                                        </div>
                                    <?php } ?>

                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Active</label>
                                <div class="col-sm-10">
                                    <div class="radio radio-success radio-inline">
                                        <input type="radio" name="is_active" id="is_active_Y" value="Y" <?php echo ($is_active == "Y") ? "checked" : ""; ?> required data-parsley-errors-container="#span_error_is_active">
                                        <label for="is_active_Y">Enable</label>
                                    </div>
                                    <div class="radio radio-danger radio-inline">
                                        <input type="radio" name="is_active" id="is_active_N" value="N" <?php echo ($is_active == "N") ? "checked" : ""; ?>>
                                        <label for="is_active_N">Disabled</label>
                                    </div>
                                    <span id="span_error_is_active"></span>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

                <div id="tab_seo" class="tab-pane">
                    <div class="row">
                        <div class="col-md-12">
                        <div class="form-group">
                            <label class="col-md-2 control-label">Meta Title</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" name="meta_title" parsley-trigger="change" required placeholder="Enter short description" value="<?php echo $meta_title;?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label">Meta Keywords</label>
                            <div class="col-md-10">
                                <input type="text" value="<?php echo $meta_keywords;?>" name="meta_keywords" parsley-trigger="change" required data-role="tagsinput" placeholder="Add your keywords"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label">Meta Description</label>
                            <div class="col-md-10">
                                <textarea class="form-control" rows="3" name="meta_description" parsley-trigger="change" required><?php echo $meta_description;?></textarea>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>

                <div id="tab_social" class="tab-pane">
                    <div class="row">
                        <div class="col-md-12">
                            <!--<div class="form-group">
                                <label class="col-md-2 control-label">Facebook Page Embed Box</label>
                                <div class="col-md-10">
                                    <textarea class="form-control" rows="3" name="facebook_embed"><?php echo $facebook_embed;?></textarea>
                                </div>
                            </div>-->

                            <div class="form-group">
                                <label class="col-md-2 control-label">Google Map Embed</label>
                                <div class="col-md-10">
                                    <textarea class="form-control" rows="3" name="googlemap_embed"><?php echo $googlemap_embed;?></textarea>
                                </div>
                            </div>

                            <!--<div class="form-group">
                                <label class="col-md-2 control-label"></label>
                                <div class="input-group col-md-10">
                                    <span class="input-group-addon"><i class="fa fa-youtube"></i> Youtube</span>
                                    <input type="text" id="link_youtube" name="link_youtube" value="<?php echo $link_youtube; ?>" class="form-control" placeholder="Youtube Link">
                                </div>
                            </div>-->

                            <div class="form-group">
                                <label class="col-md-2 control-label"></label>
                                <div class="input-group col-md-10">
                                    <span class="input-group-addon"><i class="fa fa-facebook-official"></i> Facebook</span>
                                    <input type="text" id="link_facebook" name="link_facebook"
                                           value="<?php echo $link_facebook; ?>" class="form-control"
                                           placeholder="Facebook Link">
                                </div>
                            </div>

                            <!--<div class="form-group">
                                <label class="col-md-2 control-label"></label>
                                <div class="input-group col-md-10">
                                    <span class="input-group-addon"><i class="fa fa-skype"></i> Skype</span>
                                    <input type="text" id="link_skype" value="<?php echo $link_skype; ?>" name="link_skype" class="form-control" placeholder="Skype ID">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label"></label>
                                <div class="input-group col-md-10">
                                    <span class="input-group-addon"><i class="fa fa-twitter"></i> Twitter</span>
                                    <input type="text" id="link_twitter" value="<?php echo $link_twitter; ?>" name="link_twitter" class="form-control" placeholder="Twitter ID">
                                </div>
                            </div>-->

                            <div class="form-group">
                                <label class="col-md-2 control-label"></label>
                                <div class="input-group col-md-10">
                                    <span class="input-group-addon"><i class="fa fa-instagram"></i> Instagram</span>
                                    <input type="text" id="link_instagram" value="<?php echo $link_instagram; ?>" name="link_instagram" class="form-control" placeholder="instagram id">
                                </div>
                            </div>

                            <!--<div class="form-group">
                                <label class="col-md-2 control-label"></label>
                                <div class="input-group col-md-10">
                                    <span class="input-group-addon"><i class="fa fa-linkedin"></i> LinkedIn</span>
                                    <input type="text" id="link_linkedin" value="<?php echo $link_linkedin; ?>" name="link_linkedin" class="form-control" placeholder="LinkedIn Link">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label"></label>
                                <div class="input-group col-md-10">
                                    <span class="input-group-addon"><i class="fa fa-pinterest"></i> Pinterest</span>
                                    <input type="text" id="link_pinterest" value="<?php echo $link_pinterest; ?>" name="link_pinterest" class="form-control" placeholder="Pinterest Link">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label"></label>
                                <div class="input-group col-md-10">
                                    <span class="input-group-addon"><i class="fa fa-tripadvisor"></i> Tripadvisor</span>
                                    <input type="text" id="link_tripadvisor" value="<?php echo $link_tripadvisor; ?>" name="link_tripadvisor" class="form-control" placeholder="Google Plus link">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label"></label>
                                <div class="input-group col-md-10">
                                    <span class="input-group-addon"><i class="fa fa-google-plus"></i> Google+</span>
                                    <input type="text" id="link_googleplus" value="<?php echo $link_googleplus; ?>" name="link_googleplus" class="form-control" placeholder="Tripadvisor Link">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label"></label>
                                <div class="input-group col-md-10">
                                    <span class="input-group-addon"><i class="fa fa-line"></i> Line</span>
                                    <input type="text" id="link_line" value="<?php echo $link_line; ?>" name="link_line" class="form-control" placeholder="Line ID">
                                </div>
                            </div>-->


                        </div>
                    </div>
                </div>

                <div id="tab_mail" class="tab-pane">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-md-2 control-label">Sender Name</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="contact_name" parsley-trigger="change" required placeholder="Enter Contact Name" value="<?php echo $contact_name;?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Sender Email</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="contact_email" parsley-trigger="change" required placeholder="Enter Contact Email" value="<?php echo $contact_email;?>">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


                <br><br>
                <div class="row">
                <div class="col-md-12">
                    <div class="form-group pull-right">
                        <input type="hidden" id="module" name="module" value="<?php echo $module; ?>">
                        <input type="hidden" id="action" name="action" value="<?php echo $action; ?>">
                        <input type="hidden" id="lang_id" name="lang_id" value="<?php echo $lang_id; ?>">
                        <input type="hidden" id="itemid" name="itemid" value="<?php echo $itemid; ?>">
                        <input type="hidden" id="path" name="path" value="cp">

                        <button type="submit" class="btn btn-default waves-effect waves-light btn-lg">Submit</button>
                        <button type="reset" class="btn btn-white waves-effect waves-light btn-lg">Cancel</button>
                    </div>
                </div>
                </div>
            </form>

        </div>
    </div>
</div>
</div>