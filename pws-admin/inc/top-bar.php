<div class="topbar">

    <!-- LOGO -->
    <div class="topbar-left">
        <div class="text-center">
            <a href="index.php" class="logo"><i class="fa fa-creative-commons"></i> <span>Cu<i class="fa fa-product-hunt"></i>ld CMS</span></a>
        </div>
    </div>

    <!-- Button mobile view to collapse sidebar menu -->
    <div class="navbar navbar-default" role="navigation">
        <div class="container">
            <div class="">
                <div class="pull-left">
                    <button class="button-menu-mobile open-left waves-effect waves-light">
                        <i class="md md-menu"></i>
                    </button>
                    <span class="clearfix"></span>
                </div>
<!--
                <form role="search" class="navbar-left app-search pull-left hidden-xs">
                    <input type="text" placeholder="Search..." class="form-control">
                    <a href=""><i class="fa fa-search"></i></a>
                </form>
-->

                <ul class="nav navbar-nav navbar-right pull-right">
                    <!--<li class="dropdown top-menu-item-xs">
                        <a href="#" data-target="#" class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="true">
                            <i class="icon-bell"></i> <span class="badge badge-xs badge-danger">3</span>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-lg">
                            <li class="notifi-title"><span class="label label-default pull-right">New 3</span>Notification</li>
                            <li class="list-group slimscroll-noti notification-list">
                                <a href="javascript:void(0);" class="list-group-item">
                                    <div class="media">
                                        <div class="pull-left p-r-10">
                                            <em class="fa fa-diamond noti-primary"></em>
                                        </div>
                                        <div class="media-body">
                                            <h5 class="media-heading">A new order has been placed A new order has been placed</h5>
                                            <p class="m-0">
                                                <small>There are new settings available</small>
                                            </p>
                                        </div>
                                    </div>
                                </a>

                                <a href="javascript:void(0);" class="list-group-item">
                                    <div class="media">
                                        <div class="pull-left p-r-10">
                                            <em class="fa fa-cog noti-warning"></em>
                                        </div>
                                        <div class="media-body">
                                            <h5 class="media-heading">New settings</h5>
                                            <p class="m-0">
                                                <small>There are new settings available</small>
                                            </p>
                                        </div>
                                    </div>
                                </a>

                                <a href="javascript:void(0);" class="list-group-item">
                                    <div class="media">
                                        <div class="pull-left p-r-10">
                                            <em class="fa fa-bell-o noti-custom"></em>
                                        </div>
                                        <div class="media-body">
                                            <h5 class="media-heading">Updates</h5>
                                            <p class="m-0">
                                                <small>There are <span class="text-primary font-600">2</span> new updates available</small>
                                            </p>
                                        </div>
                                    </div>
                                </a>

                                <a href="javascript:void(0);" class="list-group-item">
                                    <div class="media">
                                        <div class="pull-left p-r-10">
                                            <em class="fa fa-user-plus noti-pink"></em>
                                        </div>
                                        <div class="media-body">
                                            <h5 class="media-heading">New user registered</h5>
                                            <p class="m-0">
                                                <small>You have 10 unread messages</small>
                                            </p>
                                        </div>
                                    </div>
                                </a>

                                <a href="javascript:void(0);" class="list-group-item">
                                    <div class="media">
                                        <div class="pull-left p-r-10">
                                            <em class="fa fa-diamond noti-primary"></em>
                                        </div>
                                        <div class="media-body">
                                            <h5 class="media-heading">A new order has been placed A new order has been placed</h5>
                                            <p class="m-0">
                                                <small>There are new settings available</small>
                                            </p>
                                        </div>
                                    </div>
                                </a>

                                <a href="javascript:void(0);" class="list-group-item">
                                    <div class="media">
                                        <div class="pull-left p-r-10">
                                            <em class="fa fa-cog noti-warning"></em>
                                        </div>
                                        <div class="media-body">
                                            <h5 class="media-heading">New settings</h5>
                                            <p class="m-0">
                                                <small>There are new settings available</small>
                                            </p>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0);" class="list-group-item text-right">
                                    <small class="font-600">See all notifications</small>
                                </a>
                            </li>
                        </ul>
                    </li>-->
                    <li class="hidden-xs">
                        <a href="#" id="btn-fullscreen" class="waves-effect waves-light"><i class="icon-size-fullscreen"></i></a>
                    </li>
                    <!--
                    <li class="hidden-xs">
                        <a href="#" class="right-bar-toggle waves-effect waves-light"><i class="icon-settings"></i></a>
                    </li>
                    -->
                    <li class="dropdown top-menu-item-xs">
                        <a href="" class="dropdown-toggle profile waves-effect waves-light" data-toggle="dropdown" aria-expanded="true">
                        <?php
                        //PRE($oUser);
                        $pic = BASE_UPLOAD."users/".$oUser->id."/".$oUser->profile_photo;
                        $pic_realpath = PATH_UPLOAD_ROOT."users/".$oUser->id."/".$oUser->profile_photo;
                        if(is_file($pic_realpath)) {
                            ?>
                            <img src="<?php echo $pic; ?>" alt="<?php echo $oUser->first_name." ".$oUser->last_name; ?>" class="img-circle">
                        <?php }else{ ?>
                            <img src="<?php echo BASE_IMAGES; ?>nophoto.jpg" alt="<?php echo $oUser->first_name." ".$oUser->last_name; ?>" class="img-circle">
                        <?php } ?>
                        </a>


                        <ul class="dropdown-menu">
                            <li><a href="<?php echo PATH_ADMIN."index.php?module=users&action=update&itemid=".$oUser->id; ?>"><i class="ti-user m-r-10 text-custom"></i> Profile</a></li>
                            <li><a href="<?php echo PATH_ADMIN."index.php?module=config&action=update&itemid=1"; ?>"><i class="ti-settings m-r-10 text-custom"></i> Settings</a></li>
                            <li><a href="lock-screen.php"><i class="ti-lock m-r-10 text-custom"></i> Lock screen</a></li>
                            <li class="divider"></li>
                            <li><a href="<?php echo PATH_ADMIN."index.php?module=users&action=logout"; ?>"><i class="ti-power-off m-r-10 text-danger"></i> Logout</a></li>
                        </ul>
                    </li>
                </ul>

                <?php
                $data_lang = $languages->select(NULL, NULL, "Y");
                if(count($data_lang) > 0) {
                ?>
                <ul class="nav navbar-nav hidden-xs pull-right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown"
                           role="button" aria-haspopup="true" aria-expanded="false">
                            <span class="flag-icon flag-icon-<?php echo $oLanguages->icon; ?>"></span> <?php echo strtoupper($oLanguages->id); ?>
                            <!--<i class="fa fa-language"></i> --><span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <?php
                            $url_path = "&path=cp";
                            if($module)
                                $url_path.= "&module=".$module;
                            if($action)
                                $url_path.= "&action=".$action;
                            if($task)
                                $url_path.= "&task=".$task;
                            if($itemid)
                                $url_path.= "&itemid=".$itemid;

                            foreach ($data_lang as $lang_item) {
                                ?>
                                <li>
                                    <a href="index.php?&lang_id=<?php echo $lang_item['id']; ?><?php echo $url_path; ?>">
                                        <span class="flag-icon flag-icon-<?php echo $lang_item['icon']; ?>"></span> Switch to <?php echo $lang_item['name']; ?>
                                    </a>
                                </li>
                                <?php
                            }
                            unset($data_lang);
                            ?>
                        </ul>
                    </li>
                </ul>
                <?php } ?>
            </div>
            <!--/.nav-collapse -->
        </div>
    </div>
</div>
