<?php
$data_content[0]['description'] = str_ireplace(array("\r","\n",'\r','\n'),'', $data_content[0]['description']);
?>
<section class="content-area">
<!-- saturdays_intro Section -->
<div id="saturdays_intro">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="row saturdays_intro">
                    <p class="saturdays_subheader">Welcome to</p>
                    <h2>Saturdays<br/>Residence</h2>

                    <h4><?php echo $data_content[0]['subtitle']; ?></h4>
                    <?php echo $data_content[0]['description']; ?>

                </div><!--//saturdays_intro-->
            </div><!--//col-md-6-->

            <div class="col-md-offset-1 col-md-4">
                <div class="row calendar-widget">
                    <div class="row title"><i class="global-icons icon-calendar-l"></i>Please Select Your Dates</div>
                    <div class="row">
                        <div class="check-in form-wrapper" id="edit-checkin" data-toggle="modal"
                             data-target="#booking-modal">
                            <p class="toplabel">Checkin</p><p class="day">16</p><p class="month">February</p>
                        </div>

                        <div class="check-out form-wrapper" id="edit-checkout" data-toggle="modal"
                             data-target="#booking-modal">
                            <p class="toplabel">Checkout</p><p class="day">17</p><p class="month">February</p>
                        </div>
                    </div>
                    <div class="row">
                        <center>
                            <button class="btn btn-lg btn-primary" data-toggle="modal" data-target="#booking-modal">Book
                                Now
                            </button>
                        </center>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>
<?php
$data_block_1 = $contents->get($oConfig->lang_id,"home-block-17");
//PRE($data_block_1);
if(is_array($data_block_1))
{
	extract($data_block_1[0]);
	$hilight_image = "http://placehold.it/800x550&text=".$title;	
	if($header_photo != "")
	{
		$pic = BASE_UPLOAD."contents/".$id."/".$header_photo;
		$pic_realpath = PATH_UPLOAD_ROOT."contents/".$id."/".$header_photo;
		if(is_file($pic_realpath)) {
			$hilight_image = PATH_ROOT."timthumb.php?src=".$pic."&w=800&h=550";
		}
	}
?>
<!-- paragraphs-box Section -->
<div class="paragraphs-box">
    <div class="container">
        <div class="row paragraphs-items">
            <div class="col-md-6">
                <div class="row paragraphs-image">
                    <img src="<?php echo $hilight_image; ?>" class="img-responsive" />
                </div>
            </div>
            <div class="col-md-6">
                <div class="row paragraphs-content">

                    <h3><?php echo $title; ?></h3>
                    <?php echo $description;?>
                    <?php if($meta_description && $meta_title) { ?>
                    <p><a href="<?php echo $meta_description; ?>" class="btn btn-default" target="_blank"><?php echo $meta_title; ?></a></p>
					<?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php } ?>
<?php
$data_block_2 = $contents->get($oConfig->lang_id,"home-block-18");
//PRE($data_block_2);
if(is_array($data_block_2))
{
	extract($data_block_2[0]);
	$hilight_image = "http://placehold.it/800x550&text=".$title;	
	if($header_photo != "")
	{
		$pic = BASE_UPLOAD."contents/".$id."/".$header_photo;
		$pic_realpath = PATH_UPLOAD_ROOT."contents/".$id."/".$header_photo;
		if(is_file($pic_realpath)) {
			$hilight_image = PATH_ROOT."timthumb.php?src=".$pic."&w=800&h=550";
		}
	}
?>
<!-- paragraphs-box Section -->
<div class="paragraphs-box">
    <div class="container">
        <div class="row paragraphs-items">
            <div class="col-md-6">
                <div class="row paragraphs-image">
                    <img src="<?php echo $hilight_image; ?>" class="img-responsive" />
                </div>
            </div>
            <div class="col-md-6">
                <div class="row paragraphs-content">

                    <h3><?php echo $title; ?></h3>
                    <?php echo $description;?>
                    <?php if($meta_description && $meta_title) { ?>
                    <p><a href="<?php echo $meta_description; ?>" class="btn btn-default" target="_blank"><?php echo $meta_title; ?></a></p>
					<?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php } ?>


<!-- paragraphs-3box Section -->
<div class="paragraphs-3box">
    <div class="container">
        <div class="row paragraphs-3box-items">
			<?php
            $data_block_3 = $contents->get($oConfig->lang_id,"home-block-19");
            //PRE($data_block_3);
            if(is_array($data_block_3))
            {
                extract($data_block_3[0]);
                $hilight_image = "http://placehold.it/800x550&text=".$title;	
                if($header_photo != "")
                {
                    $pic = BASE_UPLOAD."contents/".$id."/".$header_photo;
                    $pic_realpath = PATH_UPLOAD_ROOT."contents/".$id."/".$header_photo;
                    if(is_file($pic_realpath)) {
                        $hilight_image = PATH_ROOT."timthumb.php?src=".$pic."&w=800&h=550";
                    }
                }
            ?>
            <!--box-1-->
            <div class="col-md-4">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="paragraphs-image">
                            <img src="<?php echo $hilight_image; ?>" class="img-responsive">
                        </div>
                        <div class="paragraphs-content">
                            <h3><?php echo $title; ?></h3>
                            <?php echo $description;?>
                            <?php if($meta_description && $meta_title) { ?>
                            <p><a href="<?php echo $meta_description; ?>" class="btn btn-default" target="_blank"><?php echo $meta_title; ?></a></p>
                            <?php } ?>
                        </div>

                    </div>
                </div>
            </div>
			<?php } ?>

			<?php
            $data_block_4 = $contents->get($oConfig->lang_id,"home-block-20");
            //PRE($data_block_4);
            if(is_array($data_block_4))
            {
                extract($data_block_4[0]);
                $hilight_image = "http://placehold.it/800x550&text=".$title;	
                if($header_photo != "")
                {
                    $pic = BASE_UPLOAD."contents/".$id."/".$header_photo;
                    $pic_realpath = PATH_UPLOAD_ROOT."contents/".$id."/".$header_photo;
                    if(is_file($pic_realpath)) {
                        $hilight_image = PATH_ROOT."timthumb.php?src=".$pic."&w=800&h=550";
                    }
                }
            ?>
            <!--box-1-->
            <div class="col-md-4">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="paragraphs-image">
                            <img src="<?php echo $hilight_image; ?>" class="img-responsive">
                        </div>
                        <div class="paragraphs-content">

                            <h3><?php echo $title; ?></h3>
                            <?php echo $description;?>
                            <?php if($meta_description && $meta_title) { ?>
                            <p><a href="<?php echo $meta_description; ?>" class="btn btn-default" target="_blank"><?php echo $meta_title; ?></a></p>
                            <?php } ?>

                        </div>

                    </div>
                </div>
            </div>
			<?php } ?>

			<?php
            $data_block_5 = $contents->get($oConfig->lang_id,"home-block-21");
            //PRE($data_block_5);
            if(is_array($data_block_5))
            {
                extract($data_block_5[0]);
                $hilight_image = "http://placehold.it/800x550&text=".$title;	
                if($header_photo != "")
                {
                    $pic = BASE_UPLOAD."contents/".$id."/".$header_photo;
                    $pic_realpath = PATH_UPLOAD_ROOT."contents/".$id."/".$header_photo;
                    if(is_file($pic_realpath)) {
                        $hilight_image = PATH_ROOT."timthumb.php?src=".$pic."&w=800&h=550";
                    }
                }
            ?>
            <!--box-1-->
            <div class="col-md-4">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="paragraphs-image">
                            <img src="<?php echo $hilight_image; ?>" class="img-responsive">
                        </div>
                        <div class="paragraphs-content">

                            <h3><?php echo $title; ?></h3>
                            <?php echo $description;?>
                            <?php if($meta_description && $meta_title) { ?>
                            <p><a href="<?php echo $meta_description; ?>" class="btn btn-default" target="_blank"><?php echo $meta_title; ?></a></p>
                            <?php } ?>

                        </div>

                    </div>
                </div>
            </div>
			<?php } ?>


        </div>
    </div>
</div>

<?php
$data_block_6 = $contents->get($oConfig->lang_id,"home-block-22");
//PRE($data_block_6);
if(is_array($data_block_6))
{
	extract($data_block_6[0]);
	$hilight_image = "http://placehold.it/800x550&text=".$title;	
	if($header_photo != "")
	{
		$pic = BASE_UPLOAD."contents/".$id."/".$header_photo;
		$pic_realpath = PATH_UPLOAD_ROOT."contents/".$id."/".$header_photo;
		if(is_file($pic_realpath)) {
			$hilight_image = PATH_ROOT."timthumb.php?src=".$pic."&w=800&h=550";
		}
	}
?>
<!-- paragraphs-box Section -->
<div class="paragraphs-box">
    <div class="container">
        <div class="row paragraphs-items">
            <div class="col-md-6">
                <div class="row paragraphs-image">
                    <img src="<?php echo $hilight_image; ?>" class="img-responsive">
                </div>
            </div>
            <div class="col-md-6">
                <div class="row paragraphs-content">

                    <h3><?php echo $title; ?></h3>
                    <?php echo $description;?>
                    <?php if($meta_description && $meta_title) { ?>
                    <p><a href="<?php echo $meta_description; ?>" class="btn btn-default" target="_blank"><?php echo $meta_title; ?></a></p>
                    <?php } ?>

                </div>
            </div>
        </div>
    </div>
</div>
<?php } ?>

<?php
$data_block_7 = $contents->get($oConfig->lang_id,"home-block-23");
//PRE($data_block_7);
if(is_array($data_block_7))
{
	extract($data_block_7[0]);
	$hilight_image = "http://placehold.it/800x550&text=".$title;	
	if($header_photo != "")
	{
		$pic = BASE_UPLOAD."contents/".$id."/".$header_photo;
		$pic_realpath = PATH_UPLOAD_ROOT."contents/".$id."/".$header_photo;
		if(is_file($pic_realpath)) {
			$hilight_image = PATH_ROOT."timthumb.php?src=".$pic."&w=800&h=550";
		}
	}
?>
<!-- paragraphs-box Section -->
<div class="paragraphs-box">
    <div class="container">
        <div class="row paragraphs-items">
            <div class="col-md-6">
                <div class="row paragraphs-image">
                    <img src="<?php echo $hilight_image; ?>" class="img-responsive">
                </div>
            </div>
            <div class="col-md-6">
                <div class="row paragraphs-content">

                    <h3><?php echo $title; ?></h3>
                    <?php echo $description;?>
                    <?php if($meta_description && $meta_title) { ?>
                    <p><a href="<?php echo $meta_description; ?>" class="btn btn-default" target="_blank"><?php echo $meta_title; ?></a></p>
                    <?php } ?>

                </div>
            </div>
        </div>
    </div>
</div>
<?php } ?>
<?php
$data_block_8 = $contents->get($oConfig->lang_id,"home-block-24");
//PRE($data_block_8);
if(is_array($data_block_8))
{
	extract($data_block_8[0]);
	$hilight_image = "http://placehold.it/800x550&text=".$title;	
	if($header_photo != "")
	{
		$pic = BASE_UPLOAD."contents/".$id."/".$header_photo;
		$pic_realpath = PATH_UPLOAD_ROOT."contents/".$id."/".$header_photo;
		if(is_file($pic_realpath)) {
			$hilight_image = PATH_ROOT."timthumb.php?src=".$pic."&w=800&h=550";
		}
	}
?>
<!-- paragraphs-box Section -->
<div class="paragraphs-box">
    <div class="container">
        <div class="row paragraphs-items">
            <div class="col-md-6">
                <div class="row paragraphs-image">
                    <img src="<?php echo $hilight_image; ?>" class="img-responsive">
                </div>
            </div>
            <div class="col-md-6">
                <div class="row paragraphs-content">

                    <h3><?php echo $title; ?></h3>
                    <?php echo $description;?>
                    <?php if($meta_description && $meta_title) { ?>
                    <p><a href="<?php echo $meta_description; ?>" class="btn btn-default" target="_blank"><?php echo $meta_title; ?></a></p>
                    <?php } ?>

                </div>
            </div>
        </div>
    </div>
</div>
<?php } ?>

<div class="slider">
    <div class="container">
        <div class="row">
            <div class="carousel-wrapper">
                <h2><span>#saturdays residence</span></h2>
                <div class="flexslider carousel">
                    <ul id="home-instafeed" class="slides">
                    </ul>
                </div>

                <h4><span><a href="<?php echo BASE_URL.$lang_id; ?>/gallery/">view gallery</a></span></h4>
            </div>
        </div>
    </div>
</div>
</section>

<?php
$module = "Fieldguide";
$module_class = strtolower($module);

if(file_exists(PATH_ADMIN_MODULES_ROOT.$module_class.'/classes/class.'.$module_class.'.php')) {    
	require_once(PATH_ADMIN_MODULES_ROOT.$module_class . '/classes/class.'.$module_class.'.php');    
	$$module_class = new $module();
	
	$data_fieldguide = $$module_class->select();
	//PRE($data_fieldguide);
}


if(is_array($data_fieldguide) && (count($data_fieldguide) > 0)) {
?>

    <!-- fieldguide Section -->
    <section id="fieldguide" class="bg-light-gray">
    <div class="container">
        <div class="row">
                
                <div id="carousel-fieldguide" class="carousel slide" data-ride="carousel">
                    <h3 class="heading-title"><span>the</span> field guide</h3>
                  <!-- Indicators -->
                  <ol class="carousel-indicators">
                      <?php for ($i = 0; $i < 5; $i++) { ?>
                    <li data-target="#carousel-fieldguide" data-slide-to="<?php echo $i; ?>" <?php echo ($i == 0) ? 'class="active"':""; ?>></li>
                    <?php } ?>
                    
                  </ol>
                
                  <!-- Wrapper for slides -->
                  <div class="carousel-inner fieldguide-box" role="listbox">
                      <?php for ($i = 0; $i < 5; $i++) {
					  //PRE($data_fieldguide[$i]);
					$image_header = "http://placehold.it/600x800&text=".$data_fieldguide[$i]['title'];	
					if($data_fieldguide[$i]['header_photo'] != "")
					{
						$pic = BASE_UPLOAD.$module_class."/".$data_fieldguide[$i]['id']."/".$data_fieldguide[$i]['header_photo'];
						$pic_realpath = PATH_UPLOAD_ROOT.$module_class."/".$data_fieldguide[$i]['id']."/".$data_fieldguide[$i]['header_photo'];
						if(is_file($pic_realpath)) {
							$image_header = PATH_ROOT."timthumb.php?src=".$pic."&w=600&h=800";
						}
					}
					  
					  ?>
                      <div class="fieldguide-items item <?php echo ($i == 0) ? 'active':""; ?>">
                            <div class="col-lg-4 col-md-5 col-sm-6">
                                <div class="row fieldguide-image">
                                    <img src="<?php echo $image_header; ?>" class="img-responsive" />
                                </div>
                            </div>
                            <div class="col-lg-8 col-md-7 col-sm-6">
                                <div class="row fieldguide-content">
                                    
                                        <h3><?php echo $data_fieldguide[$i]["title"]; ?></h3>
                                        <p class="date"><?php echo date("F d,Y",strtotime($data_fieldguide[$i]["updated_date"])); ?></p>
                                        <p><?php echo substr(strip_tags($data_fieldguide[$i]["description"]),0,170); ?>...</p>
                                        
                                        <p><a href="<?php echo BASE_URL.$lang_id; ?>/field-guide/<?php echo $data_fieldguide[$i]["filename"]; ?>.html" class="btn btn-default">read more</a></p>
                                </div>
                            </div>
                      </div>
                      <?php } ?>
                  </div>
                
                  <!-- Controls -->
                  <a class="left carousel-control" href="#carousel-fieldguide" role="button" data-slide="prev">
                    <span class="fa fa-angle-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                  </a>
                  <a class="right carousel-control" href="#carousel-fieldguide" role="button" data-slide="next">
                    <span class="fa fa-angle-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                  </a>
                </div>

        </div>
    </div>
</section>
<?php } ?>